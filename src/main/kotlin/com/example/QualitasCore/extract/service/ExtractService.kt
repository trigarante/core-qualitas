package com.example.QualitasCore.extract.service

import com.example.QualitasCore.providers.connection.prod.ExtractMarshalController
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ExtractService : ExtractInterface {

    @Autowired
    private lateinit var extractMarshalController: ExtractMarshalController

    override fun extractConnection(
        clientName: String,
        email: String,
        phone: String,
        cp: String,
        gender: String,
        age: String,
        brand: String,
        model: String,
        description: String,
        subDescription: String,
        branch: String,
        policyPrice: String
    ): String {
        var respuesta = extractMarshalController.extract(
            clientName,
            email,
            phone,
            cp,
            gender,
            age,
            brand,
            model,
            description,
            subDescription,
            branch,
            policyPrice
        )
        return respuesta.addRegistrosQualitasResult.toString()
    }

}