package com.example.QualitasCore.extract.service

interface ExtractInterface {
    fun extractConnection(
        clientName: String,
        email: String,
        phone: String,
        cp: String,
        gender: String,
        age: String,
        brand: String,
        model: String,
        description: String,
        subDescription: String,
        branch: String,
        policyPrice: String
    ): String
}