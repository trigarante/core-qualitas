package com.example.QualitasCore.extract.Controller

import com.example.QualitasCore.extract.service.ExtractInterface
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestParam

@RestController
@RequestMapping("estratect/v1")
class ExtractController{

    @Autowired
    private lateinit var extractInterface: ExtractInterface

    @GetMapping("quotation")
    fun extract(@RequestParam clientName :String,@RequestParam email: String,@RequestParam phone: String,@RequestParam cp: String,@RequestParam gender: String,@RequestParam age: String,@RequestParam brand:String,@RequestParam model: String,@RequestParam description: String,@RequestParam subDescription: String = "",@RequestParam branch: String = "",@RequestParam policyPrice: String):ResponseEntity<Any>{
        return try{
            var base = extractInterface.extractConnection(clientName, email, phone, cp, gender, age, brand, model, description, subDescription, branch, policyPrice)
            ResponseEntity(base,HttpStatus.OK)
        }catch (e:Exception){
            ResponseEntity("Error al consumir servicio: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

}