package com.example.QualitasCore.businessRules.database

import com.example.QualitasCore.trigarante.models.LogEmisionModel
import com.example.QualitasCore.trigarante.models.LogInspection
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface LogsRepository : JpaRepository<LogEmisionModel, String> {

    @Transactional
    @Modifying
    @Query(
        "insert into log_Emision(requestJson, requestXml, responseJson, responseXml, fecha, emisionTerminada, codigoError, poliza, servicio, ambiente, idCotizacion) " +
                "values (:requestJson, :requestXml, :responseJson, :responseXml, :fecha, :emisionTerminada, :codigoError, :poliza, :servicio, :ambiente, :idCotizacion)",
        nativeQuery = true
    )
    fun logIssueInsert(
        @Param("requestJson") requestJson: String,
        @Param("requestXml") requestXml: String,
        @Param("responseJson") responseJson: String,
        @Param("responseXml") responseXml: String,
        @Param("fecha") fecha: String,
        @Param("emisionTerminada") emisionTerminada: String,
        @Param("codigoError") codigoError: String,
        @Param("poliza") poliza: String,
        @Param("servicio") servicio: String,
        @Param("ambiente") ambiente: String,
        @Param("idCotizacion") idCotizacion: String
    )

    @Transactional
    @Modifying
    @Query(
        "insert into log_Quotation(requestJson, requestXml, responseJson, responseXml, idCotizacion, fecha, codigoError, servicio, ambiente) " +
                "values (:requestJson, :requestXml, :responseJson, :responseXml, :idCotizacion,:fecha, :codigoError, :servicio, :ambiente)",
        nativeQuery = true
    )
    fun logQuotationInsert(
        @Param("requestJson") requestJson: String,
        @Param("requestXml") requestXml: String,
        @Param("responseJson") responseJson: String,
        @Param("responseXml") responseXml: String,
        @Param("idCotizacion") idCotizacion: String,
        @Param("fecha") fecha: String,
        @Param("codigoError") codigoError: String,
        @Param("servicio") servicio: String,
        @Param("ambiente") ambiente: String
    )

    //  METODO PARA GUARDAR LOS LOGS EN CASO DE QUE YA SE TENGA ESE NUMERO DE SEGIMIENTO ACTUALIZA LOS DATOS
    @Transactional
    @Modifying
    @Query("INSERT INTO log_Quotation ( requestjson, requestxml, responsexml, responsejson, idcotizacion, fecha, codigoerror, servicio, ambiente, idsegimiento)" +
            " VALUES (:requestJson,:requestXml,:responseXml,:responseJson,:idCotizacion,:fecha,:codigoError,:servicio,:ambiente,:idSeguimiento)" +
            " ON DUPLICATE KEY UPDATE " +
            "requestJson = :requestJson, requestXml = :requestXml, responseXml = :responseXml, responseJson = :responseJson, idCotizacion = :idCotizacion, fecha = :fecha, codigoError = :codigoError, servicio = :servicio", nativeQuery = true)
    fun logQuotationCustomInsert(
        @Param("requestJson") requestJson: String,
        @Param("requestXml") requestXml: String,
        @Param("responseXml") responseXml: String,
        @Param("responseJson") responseJson: String,
        @Param("idCotizacion") idCotizacion: String,
        @Param("fecha") fecha: String,
        @Param("codigoError") codigoError: String,
        @Param("servicio") servicio: String,
        @Param("ambiente") ambiente: String,
        @Param("idSeguimiento") idSeguimiento: String
    )
}

interface LogsInspection: JpaRepository<LogInspection, Int>{
    @Transactional
    @Modifying
    @Query(
        "insert into Ws_Qualitas_Prod.logs_inspection2(request,response, error, service) values (:request, :response, :error, :service)",
        nativeQuery = true
    )
    fun logInspection(
        request: String,
        response: String,
        error: String,
        service: String
    )
}

