package com.example.QualitasCore.businessRules.database

import com.example.QualitasCore.models.catalog.CatalogModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface DiscountRepository: JpaRepository<CatalogModel, String>{

    @Query("Select id, valor as text from descuento where negocio = :negocio", nativeQuery = true)
    fun findDiscount(negocio: String): List<CatalogModel>
}