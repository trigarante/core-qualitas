package com.example.QualitasCore.businessRules.database

import com.example.QualitasCore.models.catalog.ValidationModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface UsageValidationRepository: JpaRepository<ValidationModel, String> {

    @Query("SELECT id, uso, categoria, clave FROM n_clave_categoria where clave = :clave group by categoria, uso, id, clave order by uso asc", nativeQuery = true)
    fun getValidation(@Param("clave")clave: String): MutableList<ValidationModel>

}