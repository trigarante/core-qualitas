package com.example.QualitasCore.businessRules.database

import com.example.QualitasCore.models.catalog.DirectionModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface DirectionRepository: JpaRepository<DirectionModel, Int> {

    @Query("select ROW_NUMBER() over (order by d_codigo) id, d_codigo, d_asenta, d_tipo_asenta, D_mnpio, d_estado, d_ciudad,d_CP, c_estado, c_oficina, d_CP, c_estado, c_oficina, c_CP, c_tipo_asenta, c_mnpio, id_asenta_cpcons, d_zona, c_cve_ciudad, idCp from sepomexCat where d_codigo = :cp", nativeQuery = true)
    fun getDirections(cp: String): List<DirectionModel>
}