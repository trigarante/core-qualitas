package com.example.QualitasCore.businessRules.basic

import com.example.QualitasCore.Repository.ActivateQuoteRepository
import com.example.QualitasCore.businessRules.database.LogsRepository
import com.example.QualitasCore.businessRules.database.UsageValidationRepository
import com.example.QualitasCore.models.*
import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.providers.intermediary.QuotationIntermediary
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Service
class BasicQuotationService(
    @Autowired private var quotationIntermediary: QuotationIntermediary,
    @Autowired private var usageValidation: UsageValidationRepository,
    @Autowired private var logsRepository: LogsRepository,
    @Autowired private var activateQuoteRepository: ActivateQuoteRepository
) : BasicQuotationInterface {

    override fun requestQuotation(request: QuotationRequest, response: GeneralResponse, xml: String): GeneralResponse {
        var typeQuote = 0
        when (request.servicio) {
            service.PARTICULAR -> typeQuote = 1
            service.APP -> typeQuote = 2
            service.APPPLUS -> typeQuote = 3
            service.TAXI -> typeQuote = 4
            service.MOTOPARTICULAR -> typeQuote = 5
            service.MOTOREPARTIDOR -> typeQuote = 6
            service.PRIVADO -> typeQuote = 7
            service.CARGA -> typeQuote = 8
            service.TRACTO -> typeQuote = 9
        }


        val activation = activateQuoteRepository.findById(typeQuote).get().is_active

        if (activation == 0){
            response.codigoError = "Hay un error con los servicios de la aseguradora, Favor de cotizar mas tarde"
            return response
        }

        //  LLENDADO DE LOS DATOS GENERALES DE LA COTIZACION
        response.aseguradora = "QUALITAS"
        response.vehiculo.clave = request.clave.substringBefore("-")
        response.vehiculo.modelo = request.modelo
        response.vehiculo.descripcion = request.detalle
        response.vehiculo.subMarca = request.descripcion
        response.vehiculo.marca = request.marca
        response.vehiculo.servicio = request.servicio
        response.cliente.direccion.codPostal = request.cp
        response.cliente.genero = request.genero
        response.cliente.fechaNacimiento = request.fechaNacimiento
        response.cliente.edad = request.edad
        response.descuento = request.descuento.toString()
        response.paquete = request.paquete
        response.periodicidadDePago = request.periodicidadPago.toString()
        var xml = xml
        if (!request.aseguradora.isEmpty()) {
            val usage = request.aseguradora.substringAfter("-")
            try {
                //  VALIDACION PARA VERIFICAR QUE LA CLAVE VEHICULAR PUEDA LLEVAR EL USO DADO
                when(usage) {
                    "motos" -> validateCatalog(request.clave.substringBefore("-"), "15")
                    "particular" ->{
                        try {
                            validateCatalog(request.clave.substringBefore("-"), "1")
                        }
                        catch (e: IndexOutOfBoundsException){
                            //  EN CASO DE PARTICULAR SE VERIFICAN TAMBIEN LAS PICKUPS Y CAMBIA EL USOS AL PERMITIDO
                            validateCatalog(request.clave.substringBefore("-"), "5")
                            xml = xml.replace("<Uso>01</Uso>","<Uso>05</Uso>")
                        }
                    }
                    "uber" -> validateCatalog(request.clave.substringBefore("-"), "1")
                    "taxi" -> validateCatalog(request.clave.substringBefore("-"), "8/90")
                    "tracto" -> validateCatalog(request.clave.substringBefore("-"), "6")
                    else ->{
                        try {
                            validateCatalog(request.clave.substringBefore("-"), "1")
                        }
                        catch (e: IndexOutOfBoundsException){
                            validateCatalog(request.clave.substringBefore("-"), "5")
                            xml = xml.replace("<Uso>01</Uso>","<Uso>05</Uso>")
                        }
                    }
                }
            } catch (e: IndexOutOfBoundsException) {
                //  REGRESA EL ERROR EN CASO DE QUE LA CLAVE NO PUEDA LLEVAR ESE USO
                response.codigoError += "Usage ${usage} for key ${request.clave} not valid for this service"
                logsRepository.logQuotationInsert(
                    requestJson = JSONObject(request).toString(),
                    requestXml = xml,
                    responseJson = JSONObject(response).toString(),
                    responseXml = "",
                    idCotizacion = "",
                    fecha = LocalDate.now().toString(),
                    codigoError = response.codigoError,
                    servicio = request.aseguradora,
                    ambiente = ""
                )
                return response
            } catch (e: Exception) {
                response.codigoError += "Validation Error: ${e.message}"
                logsRepository.logQuotationInsert(
                    requestJson = JSONObject(request).toString(),
                    requestXml = xml,
                    responseJson = JSONObject(response).toString(),
                    responseXml = "",
                    idCotizacion = "",
                    fecha = LocalDate.now().toString(),
                    codigoError = response.codigoError,
                    servicio = request.aseguradora,
                    ambiente = ""
                )
                return response
            }
        }
        //  VERIFICA QUE SEA UN VEHICULO ULTIMO MODELO EN CASO DE QUE SEA VALOR FACTURA
        if(request.tipoValor == TipoValor.VALORFACTURA && request.modelo.toInt() < (LocalDate.now().year - 1)){
            response.codigoError = "Valor factura permitido unicamente para vehiculos ultimo modelo"
            return response
        }
        //REALIZA EL PROCESO DE COTIZACION
        val soapResponse = quotationIntermediary.cotizar(xml)
        val json = XML.toJSONObject(soapResponse)
        //  OBTINE LOS DATOS PERTINENTES DE LA COTIZACION
        val filledResponse = this.responseQuotation(json, response, request.tipoValor)
        logsRepository.logQuotationInsert(
            requestJson = JSONObject(request).toString(),
            requestXml = xml,
            responseJson = JSONObject(filledResponse).toString(),
            responseXml = soapResponse,
            idCotizacion = filledResponse.cotizacion.idCotizacion,
            fecha = LocalDate.now().toString(),
            codigoError = filledResponse.codigoError,
            servicio = request.aseguradora,
            ambiente = ""
        )
        return filledResponse
    }

    override fun requestQuotationCustom(request: RequestCustom, response: GeneralResponse, xml: String, paymentFrecuency: String): GeneralResponse {
        //  LLENA LOS DATOS GENERALES DE LA COTIZACION
        response.aseguradora = "QUALITAS"
        response.vehiculo.clave = request.clave.substringBefore("-")
        response.vehiculo.modelo = request.modelo
        response.vehiculo.descripcion = request.descripcion
        response.vehiculo.subMarca = request.descripcion
        response.vehiculo.marca = request.marca
        response.vehiculo.servicio = request.servicio
        response.cliente.direccion.codPostal = request.cp
        response.cliente.genero = request.genero
        response.cliente.fechaNacimiento = request.fechaNacimiento
        response.cliente.edad = request.edad
        response.descuento = request.descuento.toString()
        val idSolicutud = "${request.idSolicitud}$paymentFrecuency"
        var xml = xml
        if (!request.aseguradora.isEmpty()) {
            val usage = request.aseguradora.substringAfter("-")
            try {
                //  VALIDA QUE LA CLAVE VEHICULAR PUEDA LLEVAR EL USO INDICADO
                when(usage) {
                    "motos" -> validateCatalog(request.clave.substringBefore("-"), "15")
                    "particular" ->{
                        try {
                            validateCatalog(request.clave.substringBefore("-"), "1")
                        }
                        catch (e: IndexOutOfBoundsException){
                            //  EN CASO DEL PARTICULAR TAMBIEN SE VERIFICAN LAS PICKUPS Y CAMBIA EL USO AL PEREMITIDO
                            validateCatalog(request.clave.substringBefore("-"), "5")
                            xml = xml.replace("<Uso>01</Uso>","<Uso>05</Uso>")
                        }
                    }
                    "uber" -> validateCatalog(request.clave.substringBefore("-"), "1")
                    "taxi" -> validateCatalog(request.clave.substringBefore("-"), "8/90")
                    "tracto" -> validateCatalog(request.clave.substringBefore("-"), "6")
                    else ->{
                        try {
                            validateCatalog(request.clave.substringBefore("-"), "1")
                        }
                        catch (e: IndexOutOfBoundsException){
                            validateCatalog(request.clave.substringBefore("-"), "5")
                            xml = xml.replace("<Uso>01</Uso>","<Uso>05</Uso>")
                        }
                    }
                }
            } catch (e: IndexOutOfBoundsException) {
                //  EN CASO DE QUE NO PUEDA LLEVAR ESE USO REGRESA EL ERROR CORRESPONDIENTE
                response.codigoError += "Usage ${usage} for key ${request.clave} not valid for this service"
                try {
                    logsRepository.logQuotationCustomInsert(
                        requestJson = JSONObject(request).toString(),
                        requestXml = xml,
                        responseJson = JSONObject(response).toString(),
                        responseXml = "",
                        idCotizacion = "",
                        fecha = LocalDate.now().toString(),
                        codigoError = response.codigoError,
                        servicio = request.aseguradora,
                        ambiente = "",
                        idSeguimiento = idSolicutud
                    )
                }
                catch(e: DataIntegrityViolationException){
                    e.printStackTrace()
                }
                return response
            } catch (e: Exception) {
                response.codigoError += "Validation Error: ${e.message}"
                try {
                    logsRepository.logQuotationCustomInsert(
                        requestJson = JSONObject(request).toString(),
                        requestXml = xml,
                        responseJson = JSONObject(response).toString(),
                        responseXml = "",
                        idCotizacion = "",
                        fecha = LocalDate.now().toString(),
                        codigoError = response.codigoError,
                        servicio = request.aseguradora,
                        ambiente = "",
                        idSeguimiento = idSolicutud
                    )
                }
                catch (e: DataIntegrityViolationException) {
                    e.printStackTrace()
                }
                return response
            }
        }
        //  VALIDA QUE UNICAMENTE SEAN VEHICULOS ULTIMO MODELO EN CASO DE VALOR FACTURA
        if(request.tipoValor == TipoValor.VALORFACTURA && request.modelo.toInt() < (LocalDate.now().year)){
            response.codigoError = "Valor factura permitido unicamente para vehiculos ultimo modelo"
            return response
        }
        //  REALIZA EL PROCESO DE LA COTIZACION
        val soapResponse = quotationIntermediary.cotizar(xml)
        val json = XML.toJSONObject(soapResponse)
        //  OBTIENE LOS DATOS PERTINENTES DE LA COTIZACION
        val filledResponse = this.responseQuotation(json, response, request.tipoValor)
        try {
            logsRepository.logQuotationCustomInsert(
                requestJson = JSONObject(request).toString(),
                requestXml = xml,
                responseJson = JSONObject(filledResponse).toString(),
                responseXml = soapResponse,
                idCotizacion = filledResponse.cotizacion.idCotizacion,
                fecha = LocalDate.now().toString(),
                codigoError = filledResponse.codigoError,
                servicio = request.aseguradora,
                ambiente = "",
                idSeguimiento = idSolicutud
            )
        }
        catch(e: DataIntegrityViolationException){
            e.printStackTrace()
        }
        return filledResponse
    }

    fun responseQuotation(
        agencyResponse: JSONObject,
        response: GeneralResponse,
        valor: TipoValor
    ): GeneralResponse {
        val movi = agencyResponse.getJSONObject("Movimientos").getJSONObject("Movimiento")
        //  VERIFICA SI LA RESPUESTA TIENE ALGUN CODIGO DE ERROR
        if (movi.getString("CodigoError") != "") {
            response.codigoError = movi.getString("CodigoError")
            response.cotizacion.resultado = false
            return response
        }
        //Extraccion de coberturas
        val datosVehiculo = movi.getJSONObject("DatosVehiculo").toString()
        val jsonCoberturas = movi.getJSONObject("DatosVehiculo").getJSONArray("Coberturas")
        val coberturas: MutableList<Cobertura> = mutableListOf()
        var cobertura: Cobertura
        for (cob in 0 until jsonCoberturas.length()) {
            when (jsonCoberturas.getJSONObject(cob).getInt("NoCobertura")) {
                1 -> {
                    cobertura = Cobertura(
                        nombre = "Daños Materiales",
                        //  PONE COMO SUMA ASEGURADA EL VALOR CORRESPONDIENTE AL AMPARADO
                        sumaAsegurada = if(valor != TipoValor.COMERCIAL && valor != TipoValor.`COMERCIAL+10`) jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString() else "VALOR " + valor.toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                2 ->{
                    cobertura = Cobertura(
                        nombre = "DM Solamente Perdida Total",
                        sumaAsegurada = if(valor != TipoValor.COMERCIAL && valor != TipoValor.`COMERCIAL+10`) jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString() else "VALOR " + valor.toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                3 -> {
                    cobertura = Cobertura(
                        nombre = "Robo Total",
                        sumaAsegurada = if(valor != TipoValor.COMERCIAL && valor != TipoValor.`COMERCIAL+10`) jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString() else "VALOR " + valor.toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                4 -> {
                    val deducible = jsonCoberturas.getJSONObject(cob).get("Deducible").toString().split(" ")[0].toInt()
                    cobertura = Cobertura(
                        nombre = "Responsabilidad Civil",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible =  if(deducible == 0) "0" else "$deducible UMAS"
                    )
                    coberturas.add(cobertura)
                }
                5 -> {
                    cobertura = Cobertura(
                        nombre = "Gastos Medicos Ocupantes",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                6 -> {
                    cobertura = Cobertura(
                        nombre = "Muerte al Conductor por Acc. Automovilistico",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                7 -> {
                    cobertura = Cobertura(
                        nombre = "Gastos Legales",
                        sumaAsegurada = "AMPARADA",
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                8 ->{
                    cobertura = Cobertura(
                        nombre = "Equipo Especial",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                9 ->{
                    cobertura = Cobertura(
                        nombre = "Adaptaciones Daños Materiales",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                10 ->{
                    cobertura = Cobertura(
                        nombre = "Adaptaciones Robo Total",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                11 ->{
                    cobertura = Cobertura(
                        nombre = "Extencion de Cobertura",
                        sumaAsegurada = "AMPARADA",
                        deducible = ""
                    )
                    coberturas.add(cobertura)
                }
                12 ->{
                    cobertura = Cobertura(
                        nombre = "Exencion de Deducible por PT de Daños Materiales",
                        sumaAsegurada = "AMPARADA",
                        deducible = ""
                    )
                    coberturas.add(cobertura)
                }
                13 ->{
                    cobertura = Cobertura(
                        nombre = "Responsabilidad Civil Pasajero",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).get("Deducible").toString().replace("|", " UMA por pasajero"),
                        deducible = "0"
                    )
                    coberturas.add(cobertura)
                }
                14 -> {
                    cobertura = Cobertura(
                        nombre = if(datosVehiculo.contains("\"NoConsideracion\":39")) "Asistencia Vial Qualitas Plus"  else "Asistencia Vial Qualitas",
                        sumaAsegurada = "AMPARADA",
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                15 ->{
                    cobertura = Cobertura(
                        nombre = "Robo Parcial",
                        sumaAsegurada =  jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                17 ->{
                    cobertura = Cobertura(
                        nombre = "Gastos de Transporte por PT del Vehiculo Aseg.",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                21 ->{
                    cobertura = Cobertura(
                        nombre = "RCE. Ecologica",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                22 ->{
                    cobertura = Cobertura(
                        nombre = "RC. Daños a Ocupantes",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                26 ->{
                    cobertura = Cobertura(
                        nombre = "CADE de Daños Materiales por Colisión o Vuelco ",
                        sumaAsegurada = "AMPARADA",
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                28 ->{
                    cobertura = Cobertura(
                        nombre = "Gastos X por Perdida de Uso X Perdidas Parciales",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                31 ->{
                    cobertura = Cobertura(
                        nombre = "Daños por la Carga",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                40 ->{
                    cobertura = Cobertura(
                        nombre = "Exención de Deducible por Robo Total  ",
                        sumaAsegurada = "AMPARADA",
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                41 ->{
                    cobertura = Cobertura(
                        nombre = "Averia Mecanica y/o Electrica",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }
                47 ->{
                    cobertura = Cobertura(
                        nombre = "RC Complementaria",
                        sumaAsegurada = jsonCoberturas.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                        deducible = jsonCoberturas.getJSONObject(cob).getInt("Deducible").toString()
                    )
                    coberturas.add(cobertura)
                }

            }
        }
        response.coberturas = coberturas
        //  OBITENE LOS DATOS DE LOS PAGOS
        if (movi.toString().contains("\"Recibos\":[")) {
            val jsonRecibos = movi.getJSONArray("Recibos")
            var primerP: String
            for (url in 0 until jsonRecibos.length()) {
                primerP = jsonRecibos[url].toString()
                val jObjRecibo = JSONObject(primerP)
                val noRecibo = jObjRecibo.getInt("NoRecibo").toString()
                if (noRecibo == "1") {
                    response.cotizacion.primerPago = jObjRecibo.get("PrimaTotal").toString()
                } else {
                    response.cotizacion.pagosSubsecuentes = jObjRecibo.get("PrimaTotal").toString()
                }
            }
        }
        //  OBTIENE LOS DATOS DE LA COTIZACION
        response.cotizacion.resultado = true
        response.cotizacion.primaNeta = movi.getJSONObject("Primas").getDouble("PrimaNeta").toString()
        response.cotizacion.derechos = movi.getJSONObject("Primas").getDouble("Derecho").toString()
        response.cotizacion.primaTotal = movi.getJSONObject("Primas").getDouble("PrimaTotal").toString()
        response.cotizacion.impuesto = movi.getJSONObject("Primas").getDouble("Impuesto").toString()
        response.cotizacion.idCotizacion = movi.getString("NoCotizacion")
        response.emision.inicioVigencia = LocalDate.now().plusDays(1).format(DateTimeFormatter.ISO_DATE).toString()

        return response

    }
    //  METODO PARA VALIDAR LA CLAVE VEHICULAR CONTRA EL USO
    fun validateCatalog(clave: String, usage: String): String {
        var claveR = clave
        for (i in clave.length until 5) {
            claveR = "0" + claveR
        }
        val validated = usageValidation.getValidation(claveR)
        val filteredUSES = validated.filter { validationModel ->
            validationModel.uso
                .equals(usage)
        }
        return filteredUSES[0].uso
    }
}