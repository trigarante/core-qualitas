package com.example.QualitasCore.businessRules.basic

import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll
import org.json.JSONObject

@FunctionalInterface
abstract interface BasicQuotationInterface {
    fun requestQuotation(request: QuotationRequest, response: GeneralResponse, xml: String): GeneralResponse

    fun requestQuotationCustom(request: RequestCustom, response: GeneralResponse, xml: String, paymentFrecuency: String): GeneralResponse
}
