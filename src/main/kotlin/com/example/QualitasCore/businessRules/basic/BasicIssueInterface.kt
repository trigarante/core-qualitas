package com.example.QualitasCore.businessRules.basic

import com.example.QualitasCore.models.GeneralResponse

@FunctionalInterface
abstract interface BasicIssueInterface {

    abstract fun responseIssue(xml: String, generalResponse: GeneralResponse, credentials: String = ""): GeneralResponse
}