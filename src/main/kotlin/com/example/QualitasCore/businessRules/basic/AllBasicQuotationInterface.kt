package com.example.QualitasCore.businessRules.basic

import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll

interface AllBasicQuotationInterface {

    fun quotation(lambd:(QuotationRequest, String, String) -> GeneralResponse,request: QuotationRequest): ResponseAll
}