
package com.example.QualitasCore.businessRules.basic

import com.example.QualitasCore.models.*
import com.example.QualitasCore.providers.PrintConnection
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import java.io.IOException
import java.io.InputStream
import java.net.URL

@Service
class BasicPrintingService {
    @Autowired
    private lateinit var printConnection: PrintConnection

    @Value("\${aws.url}")
    val url = ""
    @Value("\${jwt.ambiente}")
    var ambiente: String = ""

    fun print(noPoliza: String, credentials: String = "", rfc: String): MutableList<Any> {
        try {
            val aws: String
            //  URL DEL OCR
            if(ambiente == "1")
                aws = "http://core-aws-dev.us-east-1.elasticbeanstalk.com/"
            else
                aws = "http://core-aws-prod.us-east-1.elasticbeanstalk.com/"
            //  OBTIENE LOS DOCUMENTOS DE LA POLIZA
            val polizas = printConnection.impresionQualitas(noPoliza, credentials)
            val documentos = polizas.split("|")

            var poliza = ""
            var reciboCobro = ""
            var rcExtranjero = ""
            var tyc =""

            val headers = HttpHeaders()
            var map: LinkedMultiValueMap<String, Any> = LinkedMultiValueMap()
            var requestEntity: HttpEntity<LinkedMultiValueMap<String, Any>>
            //  SEPARA Y ASIGNA CORRECTAMENTE LOS DOCUMENTOS
            documentos.forEach{
                if(it.contains("p$noPoliza"))
                    poliza = it
                else if(it.contains("r$noPoliza"))
                    reciboCobro = it
                else if(it.contains("c$noPoliza"))
                    rcExtranjero = it
                else if(it.contains("t$noPoliza"))
                    tyc = it
            }
            //  VALIDA SI SE TIENE LA POLIZA Y LA ENVIA AL S3
            if(poliza != "") {
                headers.contentType = MediaType.MULTIPART_FORM_DATA

                map.add("policyFile", MultipartInputStreamFileResource(URL(poliza).openStream(), "doc.pdf"))
                map.add("rfc", rfc)
                map.add("policy", noPoliza)

                requestEntity = HttpEntity(map, headers)
                try {
                    val resp = JSONObject(RestTemplate().postForObject(
                        aws + "services/s3/upload-policy",
                        requestEntity,
                        String::class.java
                    ))
                    if(resp.toString().contains("path"))
                        poliza = "${url}services/s3/download?path=" + resp.get("path").toString()
                } catch (e: Exception) {
                    e.printStackTrace()
                    println("Error al enviar al OCR: " + e.message)
                }
            }
            //  VALIDA SI SE TIENE EL RC EXTRANJERO Y LO ENVIA AL S3
            if(rcExtranjero != "") {
                map.clear()

                map.add("file", MultipartInputStreamFileResource(URL(rcExtranjero).openStream(), "doc.pdf"))
                map.add("rfc", rfc)
                map.add("policy", noPoliza)
                map.add("extraDocuments", "RCEXTRANJERO ")

                requestEntity = HttpEntity(map, headers)
                try {
                    val resp = JSONObject( RestTemplate().postForObject(
                        aws + "services/s3/upload-document-extras",
                        requestEntity,
                        String::class.java
                    ))
                    if(resp.toString().contains("path"))
                        rcExtranjero = "${url}services/s3/download?path=" + resp.get("path").toString()
                } catch (e: Exception) {
                    e.printStackTrace()
                    println("Error al enviar al OCR: " + e.message)
                }
            }
            //  VALIDA SI SE TIENE RECIBO DE COBRO Y LO ENVIA AL S3
            if(reciboCobro != "") {
                map.clear()

                map.add("file", MultipartInputStreamFileResource(URL(reciboCobro).openStream(), "doc.pdf"))
                map.add("rfc", rfc)
                map.add("policy", noPoliza)
                map.add("extraDocuments", "AVISODECOBRO  ")

                requestEntity = HttpEntity(map, headers)
                try {
                    val resp = JSONObject(RestTemplate().postForObject(
                        aws + "services/s3/upload-document-extras",
                        requestEntity,
                        String::class.java
                    ))
                    if(resp.toString().contains("path"))
                        reciboCobro = "${url}services/s3/download?path=" + resp.get("path").toString()
                } catch (e: Exception) {
                    e.printStackTrace()
                    println("Error al enviar al OCR: " + e.message)
                }
            }
            //  VALIDA SI SE TIENEN TERMINOS Y CONDICIONES Y LAS ENVIA AL S3
            if(!tyc.isEmpty()){

                map.clear()

                map.add("file", MultipartInputStreamFileResource(URL(tyc).openStream(), "doc.pdf"))
                map.add("rfc", rfc)
                map.add("policy", noPoliza)
                map.add("extraDocuments", "TERMINOSYCONDICIONES")

                requestEntity = HttpEntity(map, headers)
                try {
                    val resp = JSONObject(RestTemplate().postForObject(aws + "services/s3/upload-document-extras", requestEntity, String::class.java))
                    if(resp.toString().contains("path"))
                        tyc = "${url}services/s3/download?path=" + resp.get("path").toString()
                } catch (e: Exception) {
                    e.printStackTrace()
                    println("Error al enviar al OCR: " + e.message)
                }
            }
            //  VALIDA SI SE ENVIARON CORRECTAMENTE LOS DOCUMENTOS Y EN DADO CASO MANDA EL LINK ORIGINAL
            val documents = mutableListOf<Any>()
            if(poliza != "")documents.add(poliza)
            if(rcExtranjero != "")documents.add(RCExtranjero(rcExtranjero))
            if(reciboCobro != "")documents.add(ReciboCobro(reciboCobro))
            if(tyc != "") documents.add(TerminosCondiciones(tyc))
            return documents
        }
        catch (ex: Exception){
            println("Error al generar los documentos")
            ex.printStackTrace()
            return mutableListOf()
        }
    }
    //  CLASE PARA PONER EL DOCUMENTO EN UN FORMATO EN EL QUE SE PUEDE ENVIAR
    internal class MultipartInputStreamFileResource(inputStream: InputStream?, private val filename: String) :
        InputStreamResource(inputStream!!) {
        override fun getFilename(): String {
            return filename
        }

        @Throws(IOException::class)
        override fun contentLength(): Long {
            return -1 // we do not want to generally read the whole stream into memory ...
        }
    }
}