package com.example.QualitasCore.businessRules.basic

import com.example.QualitasCore.Repository.catalogos.NumeroSerieRepository
import com.example.QualitasCore.businessRules.database.LogsRepository
import com.example.QualitasCore.models.Emision
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.providers.PrintConnection
import com.example.QualitasCore.providers.intermediary.QuotationIntermediary
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import java.lang.reflect.InvocationTargetException
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class BasicIssueService(
    @Autowired var quotationIntermediary: QuotationIntermediary,
    @Autowired private var printConnection: BasicPrintingService,
    @Autowired var logsRepository: LogsRepository,
//    @Autowired var numeroSerieRepository: NumeroSerieRepository
) : BasicIssueInterface {

    @Value("\${aws.url}")
    val url = ""

    override fun responseIssue(xml: String, generalResponse: GeneralResponse, credentials: String): GeneralResponse {
        val request = generalResponse
//        if(numeroSerieRepository.findByVin(generalResponse.vehiculo.noSerie).isNotEmpty()){
//            generalResponse.codigoError = "El numero de serie ${generalResponse.vehiculo.noSerie} no se puede emitir favor de contactar a Qualitas"
//            return generalResponse
//        }
        //  REALIZA EL PROCESO DE EMISION
        val soapResponse = quotationIntermediary.cotizar(xml)
        val jsonObj = XML.toJSONObject(soapResponse).getJSONObject("Movimientos").getJSONObject("Movimiento")
        val codigoError = jsonObj.getString("CodigoError")

        //  VALIDA SI HAY ALGUN CODIGO DE ERROR
        if (!codigoError.isEmpty()) {
            generalResponse.codigoError = "Error al emitir: ${codigoError}"
            generalResponse.emision.resultado = false
            try {
                logsRepository.logIssueInsert(
                    requestJson = JSONObject(request).toString(),
                    requestXml = xml,
                    responseJson = JSONObject(generalResponse).toString(),
                    responseXml = soapResponse,
                    fecha = LocalDateTime.now().toString(),
                    emisionTerminada = "false",
                    codigoError = generalResponse.codigoError,
                    poliza = "",
                    servicio = generalResponse.aseguradora,
                    ambiente = "",
                    idCotizacion = generalResponse.cotizacion.idCotizacion
                )
            }
            //  ERROR QUE APARECE CUANDO EL NUMERO DE COTIZACION NO SE ENCUENTRA EN LOS LOGS
            catch (e: DataIntegrityViolationException){
                e.printStackTrace()
            }
            generalResponse.aseguradora = "QUALITAS"
            return generalResponse
        }
        //  OBTIENE EL NUMERO DE LA POLIZA
        val noPoliza = jsonObj.getString("NoPoliza").substring(2, 12)
        //  OBTIENE LOS DATOS DE LOS PAGOS
        var primerPago = ""
        var pagosSubsecuentes = ""
        if (jsonObj.toString().contains("\"Recibos\":[")) {
            val jsonRecibos = jsonObj.getJSONArray("Recibos")
            primerPago = jsonRecibos.getJSONObject(0).get("PrimaTotal").toString()
            pagosSubsecuentes = jsonRecibos.getJSONObject(1).get("PrimaTotal").toString()
        }
        //  OBTIENE LOS DATOS DE LA POLIZA
        generalResponse.emision = Emision(
            primaTotal = jsonObj.getJSONObject("Primas").getDouble("PrimaTotal").toString(),
            primaNeta = jsonObj.getJSONObject("Primas").getDouble("PrimaNeta").toString(),
            derechos = jsonObj.getJSONObject("Primas").getDouble("Derecho").toString(),
            impuesto = jsonObj.getJSONObject("Primas").getDouble("Impuesto").toString(),
            poliza = noPoliza,
            primerPago = primerPago,
            pagosSubsecuentes = pagosSubsecuentes,
            documento =  printConnection.print(noPoliza, credentials, generalResponse.cliente.rfc)[0].toString(),
            //  VALIDA SI LA EMISION ES CON FECHA ACTUAL O POSTFECHADA
            inicioVigencia = if(request.emision.inicioVigencia != "") generalResponse.emision.inicioVigencia else LocalDateTime.now().plusDays(1).format(
                DateTimeFormatter.ISO_DATE).toString(),
            resultado = true
        )
        generalResponse.codigoError ="Poliza $noPoliza ha sido emitida no cobrada."
        try {
            logsRepository.logIssueInsert(
                requestJson = JSONObject(request).toString(),
                requestXml = xml,
                responseJson = JSONObject(generalResponse).toString(),
                responseXml = soapResponse,
                fecha = LocalDateTime.now().toString(),
                emisionTerminada = "false",
                codigoError = generalResponse.codigoError,
                poliza = noPoliza,
                servicio = generalResponse.aseguradora,
                ambiente = "",
                idCotizacion = generalResponse.cotizacion.idCotizacion
            )
        }
        //  ERROR QUE APARECE CUANDO LA COTIZACION NO SE ENCUENTRA EN LOS LOGS
        catch (e: DataIntegrityViolationException){
            e.printStackTrace()
        }
        generalResponse.aseguradora = "QUALITAS"
        return generalResponse
    }
}