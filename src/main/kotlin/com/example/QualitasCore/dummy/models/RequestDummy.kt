package com.example.QualitasCore.dummy.models

data class RequestDummy (
    var aseguradora: String = "",
    val clave: String = "",
    val cp: String = "",
    val descripcion: String = "",
    val detalle: String = "",
    var descuento: Int = 0,
    val edad: String = "",
    val fechaNacimiento: String = "",
    val genero: String = "",
    val marca: String = "",
    val modelo: String = "",
    val movimiento: String = "",
    var servicio: service = service.PARTICULAR,
    var paquete: String = "",
    var peridiocidadPago: PaymentFrecuency = PaymentFrecuency.ANUAL,
    var tipoValor: TipoValor = TipoValor.CONVENIDO,
    var conductorUniversal: Boolean

)
enum class TipoValor(var tipoValor: String){
    COMERCIAL("03"),
    VALORFACTURA("01"),
    CONVENIDO("0"),
    `COMERCIAL+10`("07")
}

enum class PaymentFrecuency{
    ANUAL,
    SEMESTRAL,
    TRIMESTRAL,
    MENSUAL
}
enum class service(var service: String){
    PARTICULAR("PARTICULAR"),
    APP("APP"),
    APPPLUS("APPPLUS"),
    TAXI("TAXI"),
    MOTOPARTICULAR("MOTOPARTICULAR"),
    MOTOREPARTIDOR("MOTOREPARTIDOR"),
    PRIVADO("PARTICULAR"),
    CARGA("CARGA"),
    TRACTO("CARGA")
}