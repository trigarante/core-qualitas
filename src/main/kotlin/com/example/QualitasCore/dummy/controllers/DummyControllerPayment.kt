package com.example.QualitasCore.dummy.controllers

import com.example.QualitasCore.dummy.models.ResponseDummy
import com.example.QualitasCore.dummy.services.DummyServiceIssue
import com.example.QualitasCore.dummy.services.DummyServicePayment

import com.example.QualitasCore.models.ExceptionModel

import io.swagger.annotations.Api

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@CrossOrigin("*")
@RequestMapping("v2/qualitas-car")
@Api("Dummy", tags = arrayOf("Dummy"))
class DummyControllerPayment {
    @Autowired
    lateinit var dummyService: DummyServicePayment

    @PostMapping(value = ["payment-test"])
    fun getEmision(@RequestBody responseWs: ResponseDummy): ResponseEntity<Any> {
        val base = dummyService.dummyIssue(responseWs)
        if (base.emision.resultado)
            return ResponseEntity(base, HttpStatus.OK)
        else {
            val response = ExceptionModel(error = "Issue error", message = base.codigoError)
            throw response
        }
    }

}