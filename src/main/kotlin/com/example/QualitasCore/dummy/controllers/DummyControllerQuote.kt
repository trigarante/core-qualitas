package com.example.QualitasCore.dummy.controllers

import com.example.QualitasCore.dummy.models.RequestDummy
import com.example.QualitasCore.dummy.services.DummyServiceQuote
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@CrossOrigin("*")
@RequestMapping("v2/qualitas-car")
@Api("Dummy", tags = arrayOf("Dummy"))
class DummyControllerQuote {
    @Autowired
    lateinit var dummyService: DummyServiceQuote


    @PostMapping("quote-test")
    fun getCotizacion(@RequestBody request: RequestDummy): ResponseEntity<Any> {
        return try {
            val base = dummyService.quoteDummy(request)
            ResponseEntity(base, HttpStatus.OK)
        } catch (e: Exception){
            ResponseEntity("Error al Cotizar: $e", HttpStatus.INTERNAL_SERVER_ERROR)
        }



    }
}