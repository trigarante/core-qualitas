package com.example.QualitasCore.dummy.services

import com.example.QualitasCore.dummy.models.Cobertura
import com.example.QualitasCore.dummy.models.ResponseDummy
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class DummyServicePayment {

    fun dummyIssue(requestDummy: ResponseDummy): ResponseDummy {
        val responseDummy = ResponseDummy()

        responseDummy.aseguradora = "QUALITAS"
        responseDummy.diasGracia = 7

        responseDummy.cliente.apply {
            nombre = requestDummy.cliente.nombre
            apellidoMat = requestDummy.cliente.apellidoMat
            apellidoPat = requestDummy.cliente.apellidoPat
            telefono = requestDummy.cliente.telefono

            fechaNacimiento = requestDummy.cliente.fechaNacimiento
            edad = requestDummy.cliente.edad
            genero = requestDummy.cliente.genero
            rfc = requestDummy.cliente.rfc
            curp = requestDummy.cliente.curp
            tipoPersona = requestDummy.cliente.tipoPersona

            direccion.codPostal = requestDummy.cliente.direccion.codPostal
            direccion.noExt = requestDummy.cliente.direccion.noExt
            direccion.noInt = requestDummy.cliente.direccion.noInt
            direccion.colonia = requestDummy.cliente.direccion.colonia
            direccion.calle = requestDummy.cliente.direccion.calle
            direccion.ciudad = requestDummy.cliente.direccion.ciudad
            direccion.poblacion = requestDummy.cliente.direccion.poblacion
            direccion.pais = requestDummy.cliente.direccion.pais
            email = requestDummy.cliente.email

        }

        responseDummy.vehiculo.apply {
            marca = requestDummy.vehiculo.marca
            modelo = requestDummy.vehiculo.modelo
            descripcion = requestDummy.vehiculo.descripcion
            clave = requestDummy.vehiculo.clave
            servicio = requestDummy.vehiculo.servicio.toString()
            uso = requestDummy.vehiculo.uso
            noPlacas = requestDummy.vehiculo.noPlacas
            detalle = requestDummy.vehiculo.detalle
            noMotor = requestDummy.vehiculo.noMotor
            noSerie = requestDummy.vehiculo.noSerie
            noPlacas = requestDummy.vehiculo.noPlacas
        }

        responseDummy.cotizacion.apply {
            primaTotal = "15465.22"
            primaNeta = "13177.22"
            derechos = "550.0"
            impuesto = "2133.13"
            idCotizacion = "0716156519"
            resultado = true
        }

        responseDummy.emision.apply {
            primaTotal = "15465.22"
            primaNeta = "13177.22"
            derechos = "550.0"
            impuesto = "2133.13"
            idCotizacion = "0716156519"
            resultado = true
            poliza = "7160429286"
            documento =
                "https://aws-dev.core-ahorraseguros.com/services/s3/download?path=CALM980310/policies/7160429286/7160429286.pdf"
            inicioVigencia = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE).toString()
            resultado = true
        }

        responseDummy.pago.apply {
            medioPago = "DEBITO"
            nombreTarjeta = "PEDRO ARTURO CHAVEZ"
            banco = "BANORTE - PR"
            idBanco = "110072"
            noTarjeta = "4653746506970695"
            mesExp = "08"
            anioExp = "2029"
            codigoSeguridad = "123"
            noClabe = ""
            carrier = 1
            msi = "CL"
            resultado = true
        }

        responseDummy.periodicidadDePago = requestDummy.periodicidadDePago
        responseDummy.paquete = requestDummy.paquete

        val coverages = mutableListOf<Cobertura>()

        coverages.add(
            Cobertura(
                nombre = "Daños Materiales",
                sumaAsegurada = "230000",
                deducible = "5"
            ),
        )


        coverages.add(
            Cobertura(
                nombre = "Robo Total",
                sumaAsegurada = "230000",
                deducible = "10"
            ),
        )

        coverages.add(
            Cobertura(
                nombre = "Responsabilidad Civil",
                sumaAsegurada = "3000000",
                deducible = "0"
            ),
        )
        coverages.add(
            Cobertura(
                nombre = "Gastos Medicos Ocupantes",
                sumaAsegurada = "200000",
                deducible = "0"
            ),
        )

        coverages.add(
            Cobertura(
                nombre = "Muerte al Conductor por Acc. Automovilistico",
                sumaAsegurada = "100000",
                deducible = "0"
            ),
        )

        coverages.add(
            Cobertura(
                nombre = "Gastos legales",
                sumaAsegurada = "AMPARADA",
                deducible = "0"
            ),
        )

        coverages.add(
            Cobertura(
                nombre = "Accidente Vial Qualitas",
                sumaAsegurada = "AMPARADA",
                deducible = "0"
            ),
        )

        responseDummy.coberturas = coverages
        return responseDummy
    }
}