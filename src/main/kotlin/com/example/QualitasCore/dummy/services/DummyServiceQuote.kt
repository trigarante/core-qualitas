package com.example.QualitasCore.dummy.services

import com.example.QualitasCore.dummy.models.Cobertura
import com.example.QualitasCore.dummy.models.RequestDummy
import com.example.QualitasCore.dummy.models.ResponseDummy
import org.springframework.stereotype.Service

@Service
class DummyServiceQuote {

    fun quoteDummy(requestDummy: RequestDummy): ResponseDummy {
        val responseDummy = ResponseDummy()

        responseDummy.aseguradora = "QUALITAS"
        responseDummy.cliente.apply {
            fechaNacimiento = requestDummy.fechaNacimiento
            edad = requestDummy.edad
            genero = requestDummy.genero
            direccion.codPostal = requestDummy.cp
        }
        responseDummy.vehiculo.apply {
            marca = requestDummy.marca
            modelo = requestDummy.modelo
            descripcion = requestDummy.descripcion
            clave = requestDummy.clave
            servicio = requestDummy.servicio.toString()
        }

        responseDummy.cotizacion.apply {
            primaTotal = "15465.22"
            primaNeta = "13177.22"
            derechos = "550.0"
            impuesto = "2133.13"
            idCotizacion = "0716156519"
            resultado = true
        }

        responseDummy.emision.resultado = false
        responseDummy.pago.resultado = false

        val coverages = mutableListOf<Cobertura>()

        coverages.add(
            Cobertura(
                nombre = "Daños Materiales",
                sumaAsegurada = "230000",
                deducible = "5"
            ),
        )


        coverages.add(
            Cobertura(
                nombre = "Robo Total",
                sumaAsegurada = "230000",
                deducible = "10"
            ),
        )

        coverages.add(
            Cobertura(
                nombre = "Responsabilidad Civil",
                sumaAsegurada = "3000000",
                deducible = "0"
            ),
        )
        coverages.add(
            Cobertura(
                nombre = "Gastos Medicos Ocupantes",
                sumaAsegurada = "200000",
                deducible = "0"
            ),
        )

        coverages.add(
            Cobertura(
                nombre = "Muerte al Conductor por Acc. Automovilistico",
                sumaAsegurada = "100000",
                deducible = "0"
            ),
        )

        coverages.add(
            Cobertura(
                nombre = "Gastos legales",
                sumaAsegurada = "AMPARADA",
                deducible = "0"
            ),
        )

        coverages.add(
            Cobertura(
                nombre = "Accidente Vial Qualitas",
                sumaAsegurada = "AMPARADA",
                deducible = "0"
            ),
        )

        responseDummy.coberturas = coverages
        return responseDummy
    }
}