package com.example.QualitasCore.rc4.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.annotations.ApiIgnore

@RestController
@RequestMapping("/v1/encryption")
@ApiIgnore
class RC4Controller {


    @PostMapping("/tests")
    fun encrypt(@RequestParam text: String): ResponseEntity<Any>{
        return try{
            ResponseEntity(text,HttpStatus.OK)
        }catch (e:Exception){
            return ResponseEntity("Error al encryptar: ${e.message}", HttpStatus.I_AM_A_TEAPOT)
        }
    }
}