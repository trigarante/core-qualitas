package com.example.QualitasCore.handling.service.issue

import com.example.QualitasCore.businessRules.basic.BasicIssueInterface
import com.example.QualitasCore.businessRules.database.DirectionRepository
import com.example.QualitasCore.handling.service.CoverageHandlingService
import com.example.QualitasCore.extensions.*
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.trigarante.repository.LogEmisionRepository
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class IssueHandlingCoveragesService: IssueHandlingCoveragesInterface {

    @Value("\${jwt.ambiente}")
    private val ambiente : String = ""

    @Autowired
    private lateinit var issue: BasicIssueInterface

    @Autowired
    lateinit var xmlGeneralService: XMLGeneralService

    @Autowired
    lateinit var log: LogEmisionRepository

    override fun issue(responseWs: GeneralResponse): GeneralResponse {
        val xml: String
        try{
            xml = xmlGeneralService.issueAutos(responseWs)

        }catch (e: IndexOutOfBoundsException){
            responseWs.codigoError = "XML Creation Error: No se encontro la colonia ${responseWs.cliente.direccion.colonia}"
            return responseWs
        }
        catch (e:Exception){
            responseWs.codigoError = "XML Creation Error: ${e.message}"
            return responseWs
        }
        responseWs.aseguradora = "handling-${responseWs.vehiculo.servicio.service}"
        return issue.responseIssue(xml, responseWs, if(responseWs.vehiculo.servicio.service.contains("MOTO")) "motos" else "")

    }

    override fun emisiones(responseWs: GeneralResponse): GeneralResponse{
        var request = responseWs
        var xmlEmision = ""
        try{
            xmlEmision = xmlGeneralService.issue(responseWs)
        }catch (e: IndexOutOfBoundsException){
            val error = ExceptionModel()
            error.error = "XML Creation"
            error.message = "No se encontro la colonia ${responseWs.cliente.direccion.colonia}"
            log.insertarLog(
                requestJson = JSONObject(request).toString(),
                requestXml = xmlEmision,
                responseXml = "",
                responseJson = JSONObject(responseWs).toString(),
                fecha = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE).toString(),
                emisionTerminada = "false",
                codigoError = responseWs.codigoError,
                poliza = "",
                servicio = "trigarante",
                ambiente = ambiente,
                idCotizacion = responseWs.cotizacion.idCotizacion
            )
            throw  error
        }
        catch (e:Exception){
            val error = ExceptionModel()
            error.error = "XML Creation"
            error.message = e.message!!
            throw error
        }
        responseWs.aseguradora = "trigarante-${responseWs.vehiculo.servicio.service}"
        val response = issue.responseIssue(xmlEmision, responseWs, "")
        return response
    }
}