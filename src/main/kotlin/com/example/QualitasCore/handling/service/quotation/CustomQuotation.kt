package com.example.QualitasCore.handling.service.quotation

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationInterface
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.models.*
import com.example.QualitasCore.models.Custom.RequestCoverage
import com.example.QualitasCore.models.Custom.RequestCustom
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*
import kotlin.collections.HashMap

@Service
class CustomQuotation(
    @Autowired private var basicQuotationInterface: BasicQuotationInterface, @Value("\${jwt.negocio}") private val negocio: String = "",
    @Value("\${jwt.agente}") private val agente: String = "",
    @Autowired private var all: AllBasicQuotationInterface
) {
    @Autowired
    lateinit var xmlservice: XMLGeneralService

    fun quotation(request: GeneralResponse, paymentFrequency: String, paquete: String): GeneralResponse{
        val response = GeneralResponse()
        response.descuento = request.descuento
        request.aseguradora = "handling-particular"
        val requestQuote = QuotationRequest(
            cp = request.cliente.direccion.codPostal,
            clave = request.vehiculo.clave,
            descripcion = request.vehiculo.descripcion,
            paquete = request.paquete,
            aseguradora = "handling-particular",
            descuento = request.descuento.toLong(),
            edad = request.cliente.edad,
            fechaNacimiento = request.cliente.fechaNacimiento,
            genero = request.cliente.genero,
            marca = request.vehiculo.marca,
            servicio = request.vehiculo.servicio,
            movimiento = "cotizacion",
            modelo = request.vehiculo.modelo
        )
        var coverages: MutableList<Cobertura> = mutableListOf()
        if(request.paquete == "RC"){
            request.coberturas.forEach {
                if(it.nombre == "Daños Materiales"){
                    it.sumaAsegurada = "0"
                }
                else if( it.nombre == "Robo Total"){
                    it.sumaAsegurada = "0"
                }
                coverages.add(it)
            }
        }
        else
            coverages = request.coberturas as MutableList<Cobertura>
        requestQuote.aseguradora = "handling-particular"
        val xml = xmlservice.quotationCustomParticular(request,paymentFrequency, paquete, coverages)

        val base = basicQuotationInterface.requestQuotation(requestQuote, response, xml)
        return base
    }

    fun quotation(generalResponse: GeneralResponse): ResponseAll{
        val response =  ResponseAll()

        val packages = arrayOf("AMPLIA", "LIMITADA", "RC")
        val collection = arrayOf("ANUAL", "SEMESTRAL", "TRIMESTRAL", "MENSUAL")

        val quote: HashMap<String, GeneralResponse> = HashMap()
        val errors: MutableList<String> = mutableListOf()

        for(collect in collection){
            for(pack in packages){
                val request = generalResponse
                request.paquete = pack
                quote[pack+collect] = quotation(request, collect, pack)
                if(quote[pack+collect]!!.codigoError != "")
                    errors.add("[${quote[pack+collect]!!.codigoError}]")
            }
        }

        var error = ""
        errors.forEach {
            if (!error.contains(it))
                error += it
        }

        response.codigoError = error


        val amplia = CotizacionAmplia(
            mutableListOf(
                PeriocidadPago(
                    anual = quote["AMPLIAANUAL"]!!.cotizacion,
                    semestral = quote["AMPLIASEMESTRAL"]!!.cotizacion,
                    trimestral = quote["AMPLIATRIMESTRAL"]!!.cotizacion,
                    mensual = quote["AMPLIAMENSUAL"]!!.cotizacion
                )
            )
        )
        val limitada = CotizacionLimitada(
            mutableListOf(
                PeriocidadPago(
                    anual = quote["LIMITADAANUAL"]!!.cotizacion,
                    semestral = quote["LIMITADASEMESTRAL"]!!.cotizacion,
                    trimestral = quote["LIMITADATRIMESTRAL"]!!.cotizacion,
                    mensual = quote["LIMITADAMENSUAL"]!!.cotizacion
                )
            )
        )
        val rc = CotizacionRc(
            mutableListOf(
                PeriocidadPago(
                    anual = quote["RCANUAL"]!!.cotizacion,
                    semestral = quote["RCSEMESTRAL"]!!.cotizacion,
                    trimestral = quote["RCTRIMESTRAL"]!!.cotizacion
                )
            )
        )

        val coberturaAmplia = Amplia()
        coberturaAmplia.amplia = quote["AMPLIAANUAL"]!!.coberturas.toMutableList()
        val coberturaLimitada = Limitada()
        coberturaLimitada.limitada = quote["LIMITADAANUAL"]!!.coberturas.toMutableList()
        val coberturaRc = Rc()
        coberturaRc.rc = quote["RCANUAL"]!!.coberturas.toMutableList()

        response.cotizacion.add(amplia)
        response.cotizacion.add(limitada)
        response.cotizacion.add(rc)
        response.coberturas.add(coberturaAmplia)
        response.coberturas.add(coberturaLimitada)
        response.coberturas.add(coberturaRc)

        quote.clear()

        return response
    }

    fun quotation(request: RequestCustom, paymentFrequency: String): ResponseAll{
        var paquete = ""

        when(request.servicio){
            service.MOTOPARTICULAR, service.MOTOREPARTIDOR -> request.aseguradora = "handling-motos"
            service.PARTICULAR -> request.aseguradora = "handling-particular"
            service.APP, service.APPPLUS -> request.aseguradora = "handling-uber"
            service.TAXI -> request.aseguradora = "handling-taxi"
            service.TRACTO, service.CARGA -> request.aseguradora = "handling-tracto"
            service.PRIVADO -> TODO()
        }

        request.coberturas.sortBy { it.clave }

        loop@ for(i in 0 until request.coberturas.size){
            if(request.coberturas[i].clave == "1"){
                if(request.coberturas[i].selected) {
                    paquete = "AMPLIA"
                    break@loop
                }
            }
            else if(request.coberturas[i].clave == "2"){
                if(request.coberturas[i].selected){
                    paquete = "PLUS"
                    break@loop
                }
            }
            else if(request.coberturas[i].clave == "3"){
                if(request.coberturas[i].selected) {
                    paquete = "LIMITADA"
                    break@loop
                }
            }
            else
                paquete = "RC"
        }

        if(paquete == "") paquete = "RC"

        val collections = arrayOf("ANUAL", "SEMESTRAL", "TRIMESTRAL", "MENSUAL")
        val quotes: HashMap<String, GeneralResponse> = hashMapOf()

        val comercialErrors: MutableSet<String> = mutableSetOf()
        val errors = mutableSetOf<String>()

        when(request.servicio){
            service.PARTICULAR ->{
                val mappedCoverrage: HashMap<String, RequestCoverage> = hashMapOf()
                request.coberturas.forEach{
                    if(paquete == "AMPLIA"){
                        if(it.clave == "2" && it.selected) {
                            comercialErrors.add("[ La cobertura DM Solamente Perdida Total NO se puede agregar con la cobertura Daños Materiales ]")
                            it.selected = false
                        }
                        else if(it.clave == "13" && it.selected){
                            comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Responsabilidad Civil Pasajero]")
                            it.selected = false
                        }
                        else{
                            mappedCoverrage[it.clave] = it
                        }

                    }
                    else if(paquete == "LIMITADA"){
                        if(it.clave == "12" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Excencion de Deducible por PT de Daños Materiales se tiene que incluir la cobertura Daños Materiales ]")
                            it.selected = false
                        }
                        else if(it.clave == "13" && it.selected){
                            comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Responsabilidad Civil Pasajero]")
                            it.selected = false
                        }
                        else{
                            mappedCoverrage[it.clave] = it
                        }
                    }
                    else if(paquete == "RC"){
                        if(it.clave == "12" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Excencion de Deducible por PT de Daños Materiales tiene que incluir la cobertura Daños Materiales")
                            it.selected = false
                        }
                        else if(it.clave == "13" && it.selected){
                            comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Responsabilidad Civil Pasajero]")
                            it.selected = false
                        }
                        else if(it.clave == "15" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Robo Parcial tiene que incluir la cobertura Daños Materiales o Robo Total")
                            it.selected = false
                        }
                        else if(it.clave == "17" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Gastos de transporte por PT del Vehiculo Aseg. tiene que incluir la cobertura Daños Materiales o Robo Total")
                            it.selected = false
                        }
                        else if(it.clave == "26" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura CADE de Daños Materiales porColision o Vuelco tiene que incluir la cobertura Daños Materiales o Robo Total")
                            it.selected = false
                        }
                        else if(it.clave == "40" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Exencion de deducible por Robo Total tiene que incluir la cobertura Robo Total")
                            it.selected = false
                        }
                        else{
                            mappedCoverrage[it.clave] = it
                        }
                    }
                }

                if(paquete == "AMPLIA"){
                    if(!mappedCoverrage.containsKey("1")){
                        comercialErrors.add("[ La cobertura Daños Materiales el OBLIGATORIA para el paquete AMPLIA ]")
                        val coverage = RequestCoverage()
                        coverage.clave = "1"
                        coverage.selected = true
                        coverage.deducible = "5"
                        request.coberturas.add(coverage)
                    }
                    if(!mappedCoverrage.containsKey("3")){
                        comercialErrors.add("[ La cobertura Robo Total es OBLIGATORIA si se agrega la cobertura Daños Materiales ]")
                        val coverage = RequestCoverage()
                        coverage.clave = "3"
                        coverage.selected = true
                        coverage.deducible = "10"
                        request.coberturas.add(coverage)
                    }
                }
                else if(paquete == "LIMITADA"){
                    if(!mappedCoverrage.containsKey("3")){
                        comercialErrors.add("[ La cobertura Robo Total el OBLIGATORIA para el paquete LIMITADA ]")
                        val coverage = RequestCoverage()
                        coverage.clave = "3"
                        coverage.selected = true
                        coverage.deducible = "5"
                        request.coberturas.add(coverage)
                    }
                }
                if(!mappedCoverrage.containsKey("4")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Responssabilidad Civil es OBLIGATORIA ]")
                    coverage.clave = "4"
                    coverage.sumaAsegurada = "3000000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverrage.containsKey("5")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Gastos Medicos Ocupantes es OBLIGATORIA ]")
                    coverage.clave = "5"
                    coverage.sumaAsegurada = "200000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverrage.containsKey("6")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Muerte al Conductor por Acc Automovilistico es OBLIGATORIA ]")
                    coverage.clave = "6"
                    coverage.sumaAsegurada = "100000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverrage.containsKey("7")){
                    val coverage = RequestCoverage()
                    comercialErrors.add(("[ La cobertura Gastos Legales es OBLIGATORIA ]"))
                    coverage.clave = "7"
                    coverage.sumaAsegurada = "AMPARADA"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverrage.containsKey("14") && !mappedCoverrage.containsKey("39")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Asistencia Vial Qualitas es OBLIGATORIA ]")
                    coverage.clave = "14"
                    coverage.sumaAsegurada = "AMPARADA"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
            }
            service.APP ->{
                val mappedCoverages: HashMap<String, RequestCoverage> = hashMapOf()
                request.coberturas.forEach{
                    if(paquete == "RC"){
                        if(it.clave == "8" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Equipo Especial se tiene que agregar la cobertura Daños Materiales o Robo Total ]")
                            it.selected = false
                        }
                        else if(it.clave == "17" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Gastos de Transporte por PT del Vehiculo Aseg se tiene que agregar la cobertura Daños Materiales o Robo Total ]")
                            it.selected = false
                        }
                        else if(it.clave == "39" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Asistencia Vial Plus se tiene que agregar la cobertura Daños Materiales o Robo Total ]")
                        }
                        else if(it.clave == "40" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Exencion de Deducible por Robo Total se tiene que agregar la cobertura Robo Total ]")
                        }
                    }

                    if(it.clave == "9" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Adaptaciones y/o Conversiones ]")
                        it.selected = false
                    }
                    else if(it.clave == "11" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Extension de Cobertura ]")
                        it.selected = false
                    }
                    else if(it.clave == "12" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Exencion de Deduccible por PT de Daños Materiales ]")
                        it.selected = false
                    }
                    else if(it.clave == "13" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la coberuta Responsabilidad Civil Pasajero ]")
                        it.selected = false
                    }
                    else if(it.clave == "15" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Robo Parcial ]")
                        it.selected = false
                    }
                    else if(it.clave == "26" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no sse puede incluir la cobertura CADE de Daños Materiales por Collision o Vuelco ]")
                    }

                    mappedCoverages[it.clave] = it
                }
                if(paquete == "AMPLIA") {
                    if (!mappedCoverages.containsKey("3")){
                        val coverage = RequestCoverage()
                        comercialErrors.add("[ La cobertura Robo Total es OBLIGATORIA si se agrega la cobertura Daños Materiales ]")
                        coverage.clave = "3"
                        coverage.deducible = "10"
                        coverage.selected = true
                        request.coberturas.add(coverage)
                    }
                }
                if(!mappedCoverages.containsKey("4")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Responsabilidad Civil es OBLIGATORIA ]")
                    coverage.clave = "4"
                    coverage.sumaAsegurada = "3000000"
                    coverage.deducible = "50"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("5")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Gastos Medicos Ocupantes es OBLIGASTORIA ]")
                    coverage.clave = "5"
                    coverage.sumaAsegurada = "200000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("6")){
                    val coverage = RequestCoverage()
                    comercialErrors.add(("[ La cobertura Muerte al Conductor por Acc Automovilistico es OBLIGATORIA ]"))
                    coverage.clave = "6"
                    coverage.sumaAsegurada = "100000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("7")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Gastos Legales es OBLIGATORIA ]")
                    coverage.clave = "7"
                    coverage.sumaAsegurada = "AMPARADA"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("14") || !mappedCoverages.containsKey("39")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Asistencia Vial es OBLIGATORIA ]")
                    coverage.clave = "14"
                    coverage.sumaAsegurada = "AMPARADA"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("22")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura RC Daños a Ocupantes es OBLIGATORIA ]")
                    coverage.clave = "22"
                    coverage.sumaAsegurada = "1000000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
            }
            service.APPPLUS ->{
                val mappedCoverages: HashMap<String, RequestCoverage> = hashMapOf()
                request.coberturas.forEach{
                    if(paquete == "RC"){
                        if(it.clave == "8" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Equipo Especial se tiene que agregar la cobertura Daños Materiales o Robo Total ]")
                            it.selected = false
                        }
                        else if(it.clave == "17" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Gastos de Transporte por PT del Vehiculo Aseg se tiene que agregar la cobertura Daños Materiales o Robo Total ]")
                            it.selected = false
                        }
                        else if(it.clave == "39" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Asistencia Vial Plus se tiene que agregar la cobertura Daños Materiales o Robo Total ]")
                        }
                        else if(it.clave == "40" && it.selected){
                            comercialErrors.add("[ Para poder incluir la cobertura Exencion de Deducible por Robo Total se tiene que agregar la cobertura Robo Total ]")
                        }
                    }

                    if(it.clave == "9" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Adaptaciones y/o Conversiones ]")
                        it.selected = false
                    }
                    else if(it.clave == "11" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Extension de Cobertura ]")
                        it.selected = false
                    }
                    else if(it.clave == "12" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Exencion de Deduccible por PT de Daños Materiales ]")
                        it.selected = false
                    }
                    else if(it.clave == "13" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la coberuta Responsabilidad Civil Pasajero ]")
                        it.selected = false
                    }
                    else if(it.clave == "15" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Robo Parcial ]")
                        it.selected = false
                    }
                    else if(it.clave == "26" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no sse puede incluir la cobertura CADE de Daños Materiales por Collision o Vuelco ]")
                        it.selected = false
                    }
                    else if(it.clave == "40" && it.selected){
                        comercialErrors.add("[ En este tipo de servicio no se puede incluir la cobertura Exencion de Deducible por Robo Total ]")
                    }

                    mappedCoverages[it.clave] = it
                }
                if(paquete == "AMPLIA") {
                    if (!mappedCoverages.containsKey("3")){
                        val coverage = RequestCoverage()
                        comercialErrors.add("[ La cobertura Robo Total es OBLIGATORIA si se agrega la cobertura Daños Materiales ]")
                        coverage.clave = "3"
                        coverage.deducible = "20"
                        coverage.selected = true
                        request.coberturas.add(coverage)
                    }
                }
                if(!mappedCoverages.containsKey("4")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Responsabilidad Civil es OBLIGATORIA ]")
                    coverage.clave = "4"
                    coverage.sumaAsegurada = "3000000"
                    coverage.deducible = "100"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("5")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Gastos Medicos Ocupantes es OBLIGASTORIA ]")
                    coverage.clave = "5"
                    coverage.sumaAsegurada = "200000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("6")){
                    val coverage = RequestCoverage()
                    comercialErrors.add(("[ La cobertura Muerte al Conductor por Acc Automovilistico es OBLIGATORIA ]"))
                    coverage.clave = "6"
                    coverage.sumaAsegurada = "100000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("7")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Gastos Legales es OBLIGATORIA ]")
                    coverage.clave = "7"
                    coverage.sumaAsegurada = "AMPARADA"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("14") || !mappedCoverages.containsKey("39")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Asistencia Vial es OBLIGATORIA ]")
                    coverage.clave = "14"
                    coverage.sumaAsegurada = "AMPARADA"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("22")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura RC Daños a Ocupantes es OBLIGATORIA ]")
                    coverage.clave = "22"
                    coverage.sumaAsegurada = "1000000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
            }
            service.MOTOPARTICULAR ->{
                val mappedCoverages: HashMap<String, RequestCoverage> = hashMapOf()
                request.coberturas.forEach{
                    if(it.clave == "8" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura Equipo Especial ]")
                        it.selected = false
                    }
                    else if(it.clave == "9" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura Adaptaciones y/o Conversiones ]")
                        it.selected = false
                    }
                    else if(it.clave == "11" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura Extension de cobertura ]")
                        it.selected = false
                    }
                    else if(it.clave == "12" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura Exencion de Deducible por PT de Daños Materiales ]")
                        it.selected = false
                    }
                    else if(it.clave == "13" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura Responsabilidad Civil Pasajero ]")
                        it.selected = false
                    }
                    else if(it.clave == "15" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura Robo Parcial ]")
                        it.selected = false
                    }
                    else if(it.clave == "17" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura Gastos de Transporte por PT del Vehiculo Aseg ]")
                        it.selected = false
                    }
                    else if(it.clave == "22" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura RC Ocupantes ]")
                        it.selected = false
                    }
                    else if(it.clave == "26" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura CADE de Daños Materiales por Colision o Vuelco ]")
                        it.selected = false
                    }
                    else if(it.clave == "40" && it.selected){
                        comercialErrors.add("[ En este tipo de uso no se puede incluir la cobertura Exencion de Deducible por Robo Total ]")
                        it.selected = false
                    }

                    mappedCoverages[it.clave] = it
                }

                if(paquete == "AMPLIA"){
                    if(!mappedCoverages.containsKey("3")){
                        val coverage = RequestCoverage()
                        comercialErrors.add("[ La cobertura Robo Total es OBLIGATORIA si se agrega la cobertura Daños Materiales ]")
                        coverage.clave = "3"
                        coverage.deducible = "20"
                        coverage.selected = true
                        request.coberturas.add(coverage)
                    }
                }
                if(!mappedCoverages.containsKey("4")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Responsabilidad Civil es OBLIGATORIA ]")
                    coverage.clave = "4"
                    coverage.sumaAsegurada = "3000000"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
                if(!mappedCoverages.containsKey("7")){
                    val coverage = RequestCoverage()
                    comercialErrors.add("[ La cobertura Gastos Legales es OBLIGATORIA ]")
                    coverage.clave = "7"
                    coverage.sumaAsegurada = "AMPARADA"
                    coverage.selected = true
                    request.coberturas.add(coverage)
                }
            }

            service.TAXI -> TODO()
            service.MOTOREPARTIDOR -> TODO()
            service.PRIVADO -> TODO()
            service.CARGA -> TODO()
            service.TRACTO -> TODO()
        }

        collections.forEach {
            try {

                val xml = xmlservice.custom(request, paquete, it)
                val base = basicQuotationInterface.requestQuotationCustom(request, GeneralResponse(), xml, it)
                if(base.codigoError != "") {
                    base.codigoError = "[${base.codigoError}]"
                    errors.add(base.codigoError)
                }
                quotes[it] = base
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        var errorFilter = ""

        errors.forEach {
            if(!errorFilter.contains(it) && it != " []")
                errorFilter += "$it"
        }
        val response = com.example.QualitasCore.models.ResponseAll()

        response.aseguradora = "QUALITAS"
        response.descuento = request.descuento.toString()
        response.diasGracia = request.diasGracia
        response.paquete = paquete

        response.cliente.direccion.codPostal = request.cp
        response.cliente.fechaNacimiento = request.fechaNacimiento
        response.cliente.genero = request.genero
        response.cliente.edad = request.edad

        response.vehiculo.clave = request.clave
        response.vehiculo.modelo = request.modelo
        response.vehiculo.marca = request.marca
        response.vehiculo.descripcion = request.descripcion
        response.vehiculo.servicio = request.servicio
        response.vehiculo.subMarca = request.subMarca

        errorFilter = errorFilter.replace(" []", "").replace("[0184-- Excede el monto de la Suma x Categoria DM]", "[Monto de la Suma x Categoria DM no valido]").trim()
        if(errorFilter != ""){
            val exception = ExceptionModel(message = errorFilter, error = "Quotation Error")
            throw exception
        }

        if(comercialErrors.isNotEmpty()){
            response.codigoError = comercialErrors
        }

        if(quotes.containsKey("ANUAL"))
            response.coberturas = quotes["ANUAL"]!!.coberturas as ArrayList<Any>
        else if(quotes.containsKey("SEMESTRAL"))
            response.coberturas = quotes["SEMESTRAL"]!!.coberturas as ArrayList<Any>
        else if(quotes.containsKey("TRIMESTRAL"))
            response.coberturas = quotes["TRIMESTRAL"]!!.coberturas as ArrayList<Any>
        else if(quotes.containsKey(("MENSUAL")))
            response.coberturas = quotes["MENSUAL"]!!.coberturas as ArrayList<Any>

        response.cotizacion.add(
            PeriocidadPago(
                anual = quotes["ANUAL"]!!.cotizacion,
                semestral = quotes["SEMESTRAL"]!!.cotizacion,
                trimestral = quotes["TRIMESTRAL"]!!.cotizacion,
                mensual = quotes["MENSUAL"]!!.cotizacion
            )
        )
        quotes.clear()
        return response
    }
}