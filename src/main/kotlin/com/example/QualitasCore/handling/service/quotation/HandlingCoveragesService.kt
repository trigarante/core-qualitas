package com.example.QualitasCore.handling.service.quotation

import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.businessRules.database.DirectionRepository
import com.example.QualitasCore.models.HandlingCoveragesModel
import com.example.QualitasCore.handling.service.CoverageHandlingService
import com.example.QualitasCore.trigarante.repository.LogEmisionRepository
import com.example.QualitasCore.providers.intermediary.QuotationIntermediary
import com.example.QualitasCore.extensions.collectionType
import com.example.QualitasCore.extensions.digitValidation
import com.example.QualitasCore.extensions.packageSelection
import com.example.QualitasCore.models.Coverage
import com.example.QualitasCore.models.*
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class HandlingCoveragesService(
    @Autowired private val coverageHandlingService: CoverageHandlingService,
    @Autowired private val quotationIntermediary: QuotationIntermediary,
    @Autowired private val logQuotation: LogEmisionRepository,
    @Autowired private val direccionesRepository: DirectionRepository,
    @Value("\${jwt.ambiente}") private val ambiente: String = "",
    @Value("\${jwt.negocio}") private val negocio: String = "",
    @Value("\${jwt.agente}") private val agente: String = "",
) : HandlingCoveragesInterface {

    @Autowired
    lateinit var basicQuotation: BasicQuotationInterface

    /**Funcion para la creacion de request en xml para el servicio
     * se toman los datos necesarios directamente del modelo de peticion a nuestro servicio
     * @param handlingCoveragesModel
     * @return string del xml request */
    override fun createXML(handlingCoveragesModel: HandlingCoveragesModel): String {
        /**Se busca atraves del cp las claves de estado y municipio*/
        val direcciones = direccionesRepository.getDirections(handlingCoveragesModel.cp)
        var RP: Boolean = false
        var cover = coverageHandlingService.coveragesTransformation(
            handlingCoveragesModel.coverages,
            handlingCoveragesModel.description
        ).replace(",", "")
        if (cover.contains("MDFG")) {
            cover = cover.replace("MDFG", "")
            RP = true
        }
        val xmlCotizacion: String = "<Movimientos>" +
                "<Movimiento TipoMovimiento=\"${2}\" NoPoliza=\"\" NoCotizacion=\"\" NoEndoso=\"\" TipoEndoso=\"\" NoOTra=\"\" NoNegocio=\"${negocio}\">" +
                "<DatosAsegurado NoAsegurado=\"\">" +
                "<Nombre/>" +
                "<Direccion/>" +
                "<Colonia/>" +
                "<Poblacion/>" +
                "<Estado>${direcciones[0].c_estado}</Estado>" +
                "<CodigoPostal>${handlingCoveragesModel.cp}</CodigoPostal>" +
                "<NoEmpleado/>" +
                "<Agrupador/>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>3</TipoRegla>" +
                "<ValorRegla></ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>4</TipoRegla>" +
                "<ValorRegla>1</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>3</TipoRegla>" +
                "<ValorRegla>MEXICO</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>4</TipoRegla>" +
                "<ValorRegla>NOMBRE2</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>5</TipoRegla>" +
                "<ValorRegla>APELLIDOP2</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>6</TipoRegla>" +
                "<ValorRegla>APELLIDOM2</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "</DatosAsegurado>" +
                "<DatosVehiculo NoInciso=\"1\">" +
                "<ClaveAmis>${handlingCoveragesModel.key}</ClaveAmis>" +
                "<Modelo>${handlingCoveragesModel.model}</Modelo>" +
                "<DescripcionVehiculo>${handlingCoveragesModel.description}</DescripcionVehiculo>" +
                "<Uso>01</Uso>" + // 01 regresar
                "<Servicio>01</Servicio>" +
                "<Paquete>${packageSelection(handlingCoveragesModel.packet)}</Paquete>" + // 01 Amplio , 03 Limitado , 04 RC
                "<Motor/>" +
                "<Serie/>" +
                cover +
                /**Consideracion especial para robo parcial, se manda S si es para robo parcial la consideracion
                , N si es vehiculo blindado; el calculo de robo parcial se hace desde la aseguradora*/
                {
                    if (RP) "<ConsideracionesAdicionalesDV NoConsideracion=\"39\">" +
                            "<TipoRegla/>" +
                            "<ValorRegla>S</ValorRegla>" +
                            "</ConsideracionesAdicionalesDV>"
                    else ""
                } +
                "</DatosVehiculo>" +
                "<DatosGenerales>" +
                "<FechaEmision>" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE) + "</FechaEmision>" +
                "<FechaInicio>" + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE) + "</FechaInicio>" +
                "<FechaTermino>" + LocalDateTime.now().plusYears(1)
            .format(DateTimeFormatter.ISO_DATE) + "</FechaTermino>" +
                "<Moneda>0</Moneda>" +
                "<Agente>" + agente + "</Agente>" +
                "<FormaPago>" + collectionType(handlingCoveragesModel.periodicity) + "</FormaPago>" + //forma de pago C anual, M mensual, S semestral
                "<TarifaValores>LINEA</TarifaValores>" +
                "<TarifaCuotas>LINEA</TarifaCuotas>" +
                "<TarifaDerechos>LINEA</TarifaDerechos>" +
                "<Plazo/>" +
                "<Agencia/>" +
                "<Contrato/>" +
                "<PorcentajeDescuento>${handlingCoveragesModel.discount}</PorcentajeDescuento>" + // Descuento variable
                "<ConsideracionesAdicionalesDG NoConsideracion=\"01\">" +
                "<TipoRegla>1</TipoRegla>" +
                "<ValorRegla>${digitValidation(handlingCoveragesModel.key)}</ValorRegla>" +
                "</ConsideracionesAdicionalesDG>" +
                "<ConsideracionesAdicionalesDG NoConsideracion=\"04\">" +
                "<TipoRegla>1</TipoRegla>" +
                "<ValorRegla>" + ambiente + "</ValorRegla>" +
                /** 1 - Pruebas 0 - Produccion*/
                "</ConsideracionesAdicionalesDG>" +
                "</DatosGenerales>" +
                "<Primas>" +
                "<PrimaNeta/>" +
                "<Derecho>550</Derecho>" +
                "<Recargo/>" +
                "<Impuesto/>" +
                "<PrimaTotal/>" +
                "<Comision/>" +
                "</Primas>" +
                "<CodigoError/>" +
                "</Movimiento>" +
                "</Movimientos>"
        return xmlCotizacion
    }

    /**Funcion principal de cotizacion
     *Hace la peticion al servicio, limpia la respuesta y mete los datos necesarios a la respuesta del servicio
     * @param handlingCoveragesModel
     * @return responseQuotation */
    override fun quotation(
        handlingCoveragesModel: HandlingCoveragesModel,
        responseWs: GeneralResponse
    ): GeneralResponse {
        responseWs.aseguradora = "QUALITAS"
        responseWs.vehiculo.clave = handlingCoveragesModel.key
        responseWs.vehiculo.descripcion = handlingCoveragesModel.description
        responseWs.vehiculo.modelo = handlingCoveragesModel.model
        responseWs.vehiculo.marca = handlingCoveragesModel.brand
        responseWs.cliente.direccion.codPostal = handlingCoveragesModel.cp
        responseWs.paquete = handlingCoveragesModel.packet
        responseWs.descuento = handlingCoveragesModel.discount.toString()
        responseWs.periodicidadDePago = handlingCoveragesModel.periodicity

        /**Obtencion del xml de request y consumo de servicio*/
        val xmlCotizacion = this.createXML(handlingCoveragesModel)
        val soapresponseQuotation = quotationIntermediary.cotizar(xmlCotizacion)// Respuesta de el servicio SOAP
        val jsonObj = XML.toJSONObject(soapresponseQuotation)
        val movi = jsonObj.getJSONObject("Movimientos").getJSONObject("Movimiento")

        /**Verificacion de errores*/
        if (movi.getString("CodigoError") != "") {
            responseWs.codigoError = movi.getString("CodigoError")
            responseWs.cotizacion.resultado = false
            logQuotation.insertarLogQuotation(
                JSONObject(handlingCoveragesModel).toString(),
                xmlCotizacion,
                JSONObject(responseWs).toString(),
                soapresponseQuotation,
                LocalDateTime.now().format(
                    DateTimeFormatter.ISO_DATE
                ),
                responseWs.codigoError,
                "Handling Coverages",
                ""
            )
            return responseWs
        } else {
            /**Limpieza de coberturas apartir del idCobertura*/
            val jsonCoverages = movi.getJSONObject("DatosVehiculo").getJSONArray("Coberturas")
            val coverages: MutableList<Cobertura> = mutableListOf()
            for (cob in 0 until jsonCoverages.length()) {
                when (jsonCoverages.getJSONObject(cob).getInt("NoCobertura")) {
                    1 -> {
                        coverages.add(
                            Cobertura(
                                nombre = "Daños Materiales",
                                sumaAsegurada = jsonCoverages.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                                deducible = jsonCoverages.getJSONObject(cob).getInt("Deducible").toString()
                            )
                        )
                    }

                    3 -> {
                        coverages.add(
                            Cobertura(
                                nombre = "Robo Total",
                                sumaAsegurada = jsonCoverages.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                                deducible = jsonCoverages.getJSONObject(cob).getInt("Deducible").toString()
                            )
                        )
                    }

                    4 -> {
                        coverages.add(
                            Cobertura(
                                nombre = "Responsabilidad Civil",
                                sumaAsegurada = jsonCoverages.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                                deducible = jsonCoverages.getJSONObject(cob).getInt("Deducible").toString()
                            )
                        )
                    }

                    5 -> {
                        coverages.add(
                            Cobertura(
                                nombre = "Gastos Medicos",
                                sumaAsegurada = jsonCoverages.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                                deducible = jsonCoverages.getJSONObject(cob).getInt("Deducible").toString()
                            )
                        )
                    }

                    6 -> {
                        coverages.add(
                            Cobertura(
                                nombre = "Muerte al Conductor X AA",
                                sumaAsegurada = jsonCoverages.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                                deducible = jsonCoverages.getJSONObject(cob).getInt("Deducible").toString()
                            )
                        )
                    }

                    7 -> {
                        coverages.add(
                            Cobertura(
                                nombre = "Gastos Legales",
                                sumaAsegurada = jsonCoverages.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                                deducible = jsonCoverages.getJSONObject(cob).getInt("Deducible").toString()
                            )
                        )
                    }

                    14 -> {
                        coverages.add(
                            Cobertura(
                                nombre = "Asistencia Vial",
                                sumaAsegurada = jsonCoverages.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                                deducible = jsonCoverages.getJSONObject(cob).getInt("Deducible").toString()
                            )
                        )
                    }

                    15 -> {
                        coverages.add(
                            Cobertura(
                                nombre = "Robo Parcial",
                                sumaAsegurada = jsonCoverages.getJSONObject(cob).getInt("SumaAsegurada").toString(),
                                deducible = jsonCoverages.getJSONObject(cob).getInt("Deducible").toString()
                            )
                        )
                    }

                    else -> {
                        println("Coverage not maped")
                    }
                }
            }

            responseWs.coberturas = coverages

            /**Si la respuesta contiene mas de un recibo se añaden*/
            if (movi.toString().contains("\"Recibos\":[")) {
                val jsonRecibos = movi.getJSONArray("Recibos")
                for (url in 0 until jsonRecibos.length()) {
                    val jObjRecibo = JSONObject(jsonRecibos[url].toString())
                    val noRecibo = jObjRecibo.getInt("NoRecibo").toString()
                    if (noRecibo == "1") {
                        responseWs.cotizacion.primerPago = jObjRecibo.get("PrimaTotal").toString()
                    } else {
                        responseWs.cotizacion.pagosSubsecuentes = jObjRecibo.get("PrimaTotal").toString()
                    }
                }
            }

            responseWs.cotizacion.resultado = true
            responseWs.cotizacion.primaNeta = movi.getJSONObject("Primas").getDouble("PrimaNeta").toString()
            responseWs.cotizacion.derechos = movi.getJSONObject("Primas").getDouble("Derecho").toString()
            responseWs.cotizacion.primaTotal = movi.getJSONObject("Primas").getDouble("PrimaTotal").toString()
            responseWs.cotizacion.impuesto = movi.getJSONObject("Primas").getDouble("Impuesto").toString()
            responseWs.cotizacion.idCotizacion = movi.getString("NoCotizacion")
            logQuotation.insertarLogQuotation(
                requestJson = JSONObject(handlingCoveragesModel).toString(),
                requestXml = xmlCotizacion,
                responseJson = JSONObject(responseWs).toString(),
                responseXml = soapresponseQuotation,
                fecha = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE),
                codigoError = responseWs.codigoError,
                servicio = "Handling Coverages",
                ambiente = ""
            )
            return responseWs
        }
    }

    override fun quotationAll(
        handlingCoveragesModel: HandlingCoveragesModel,
        responseWs: GeneralResponse
        ): ResponseAll {
            val quote: MutableList<GeneralResponse> = mutableListOf()
            val response = ResponseAll()

            val packages = arrayOf("AMPLIA", "LIMITADA", "RC")
            val collects = arrayOf("ANUAL", "SEMESTRAL", "TRIMESTRAL")
            val threads: MutableList<Thread> = mutableListOf()
            val errors: MutableList<String> = mutableListOf()
            var counter = 0
            for (collect in collects) {
                for (pack in packages) {
                    val requestQuote = handlingCoveragesModel
                    requestQuote.packet = pack
                    requestQuote.periodicity = collect
                    if (pack == "RC") {
                        val coverages: MutableList<Coverage> = mutableListOf()
                        requestQuote.coverages.forEach {
                            if (it.name == "Daños Materiales") {
                                it.sumAssured = "0"
                                it.deductible = "5"
                            }
                            else if( it.name == "Robo Total"){
                                it.sumAssured = "0"
                                it.deductible = "10"
                            }
                            coverages.add(it)
                        }
                        requestQuote.coverages = coverages
                    }
                    var xml = ""
                    xml = createXML(requestQuote)
                    val quotaation = QuotationRequest()
                    quote.add(counter, basicQuotation.requestQuotation(quotaation,responseWs, xml))
                    counter++

                    if (quote.last().codigoError != "")
                        errors.add("[${quote.last().codigoError}]")
                }
            }
            var error = ""
            errors.forEach {
                if (!error.contains(it))
                    error += it
            }

            response.codigoError = error

            response.vehiculo.clave = handlingCoveragesModel.key
            response.vehiculo.descripcion = handlingCoveragesModel.description
            response.vehiculo.marca = handlingCoveragesModel.brand
            response.vehiculo.modelo = handlingCoveragesModel.model

            response.cliente.edad = handlingCoveragesModel.age
            response.cliente.fechaNacimiento = handlingCoveragesModel.dateOfBirth
            response.cliente.direccion.codPostal = handlingCoveragesModel.cp
            response.aseguradora = "QUALITAS"
            response.descuento = handlingCoveragesModel.discount.toString()

            val amplia = CotizacionAmplia(
                mutableListOf(
                    PeriocidadPago(
                        anual = quote[0]!!.cotizacion,
                        semestral = quote[3]!!.cotizacion,
                        trimestral = quote[6]!!.cotizacion
                    )
                )
            )
            val limitada = CotizacionLimitada(
                mutableListOf(
                    PeriocidadPago(
                        anual = quote[1]!!.cotizacion,
                        semestral = quote[4]!!.cotizacion,
                        trimestral = quote[7]!!.cotizacion
                    )
                )
            )
            val rc = CotizacionRc(
                mutableListOf(
                    PeriocidadPago(
                        anual = quote[2]!!.cotizacion,
                        semestral = quote[5]!!.cotizacion,
                        trimestral = quote[8]!!.cotizacion
                    )
                )
            )

            val coberturaAmplia = Amplia()
            coberturaAmplia.amplia = quote[0]!!.coberturas.toMutableList()
            val coberturaLimitada = Limitada()
            coberturaLimitada.limitada = quote[1]!!.coberturas.toMutableList()
            val coberturaRc = Rc()
            coberturaRc.rc = quote[2]!!.coberturas.toMutableList()

            response.cotizacion.add(amplia)
            response.cotizacion.add(limitada)
            response.cotizacion.add(rc)
            response.coberturas.add(coberturaAmplia)
            response.coberturas.add(coberturaLimitada)
            response.coberturas.add(coberturaRc)

            quote.clear()

            return response
    }
}