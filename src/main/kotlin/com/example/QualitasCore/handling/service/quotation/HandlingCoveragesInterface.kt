package com.example.QualitasCore.handling.service.quotation

import com.example.QualitasCore.models.HandlingCoveragesModel
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.ResponseAll

abstract interface HandlingCoveragesInterface {

    abstract fun createXML(handlingCoveragesModel: HandlingCoveragesModel): String

    abstract fun quotation(handlingCoveragesModel: HandlingCoveragesModel, responseWs: GeneralResponse): GeneralResponse

    fun quotationAll(handlingCoveragesModel: HandlingCoveragesModel, responseWs: GeneralResponse): ResponseAll
}