package com.example.QualitasCore.handling.service

import com.example.QualitasCore.models.Cobertura
import com.example.QualitasCore.models.Coverage
import org.springframework.stereotype.Service

@Service
class CoverageHandlingService {

    /**
     * Funcion de limpieza de coberturas a modificar
     * No siempre se traeran las coberturas obligatorias, se mapea las coberturas que se mandan, se combierten en el xml
     * Si no traen las coberturas obligatorias se ponen y se organizan conforme a idCobertura
     * @param coverages
     * @return string de xml de las coberturas con los datos modificados y organizadas*/
    fun coveragesTransformation(coverages: MutableList<Coverage>, description: String): String {
        var cob: HashMap<Int,String> = hashMapOf()
        var coverage = ""
        var id = 0
        //var amplia: MutableList<String> = mutableListOf()
        var xmlCoverage = StringBuilder()
        /**Se mapean las coberturas que mandan en el servicio*/
        for(i in 0 until coverages.size) {
            when (coverages[i].name.uppercase()) {
                "DAÑOS MATERIALES" -> {
                    coverage = "1"
                    id = 1
                }

                "ROBO TOTAL" -> {
                    coverage = "3"
                    id = 2
                }

                "RESPONSABILIDAD CIVIL" -> {
                    coverage = "4"
                    id = 3
                }

                "GASTOS MEDICOS" -> {
                    coverage = "5"
                    id = 4
                }

                "MUERTE AL CONDUCTOR X AA", "ACCIDENTES OCUPANTES" -> {
                    coverage = "6"
                    id = 5
                }

                "GASTOS LEGALES" -> {
                    coverage = "7"
                    id = 6
                }

                "ASISTENCIA VIAL" -> {
                    coverage = "14"
                    id = 7
                }

                "ROBO PARCIAL" -> {
                    coverage = "15"
                    id = 8
                }

                "ASISTENCIA VIAL AV PLUS" -> {
                    coverage = "F"
                    id = 9
                }

                else -> {
                    println("Coverage no mapeada")
                }
            }
            /**Se Convierten las coberturas enviadas en su forma de xml*/
            if (coverage == "F") {
                cob[id] = "MDFG"
            } else {
                if (coverage == "15") {
                    var xml = "<Coberturas NoCobertura=\"${coverage}\">" +
                            "<SumaAsegurada>${description}||${
                                coverages[i].sumAssured.replace(
                                    "Amparada",
                                    "0"
                                )
                            }</SumaAsegurada>" +
                            "<TipoSuma>0</TipoSuma>" +
                            "<Deducible>${coverages[i].deductible.replace("Amparada", "0")}</Deducible>" +
                            "<Prima>0</Prima>" +
                            "</Coberturas>"
                    cob[id] = xml
                } else {
                    var xml = "<Coberturas NoCobertura=\"${coverage}\">" +
                            "<SumaAsegurada>${coverages[i].sumAssured.replace("Amparada", "0")}</SumaAsegurada>" +
                            "<TipoSuma>0</TipoSuma>" +
                            "<Deducible>${coverages[i].deductible.replace("Amparada", "0")}</Deducible>" +
                            "<Prima>0</Prima>" +
                            "</Coberturas>"
                    cob[id] = xml
                }
            }
        }
        /**Ya que el servicio pide un orden especifico, en este tramo se organizan las coberturas, si la cobertura no existe y es obligatoria
         * se aplica por default en su lugar*/
        if(cob.keys.contains(1)){
            xmlCoverage.append(cob[1].toString())
        }else{
            xmlCoverage.append("<Coberturas NoCobertura=\"1\">" +
                    "<SumaAsegurada>0</SumaAsegurada>" +
                    "<TipoSuma>0</TipoSuma>" +
                    "<Deducible>5</Deducible>" +
                    "<Prima>0</Prima>" +
                    "</Coberturas>")
        }
        if (cob.keys.contains(2)){
            xmlCoverage.append(cob[2].toString())
        }else{
            xmlCoverage.append("<Coberturas NoCobertura=\"3\">" +
                    "<SumaAsegurada>0</SumaAsegurada>" +
                    "<TipoSuma>0</TipoSuma>" +
                    "<Deducible>10</Deducible>" +
                    "<Prima>0</Prima>" +
                    "</Coberturas>")
        }
        if (cob.keys.contains(3)){
            xmlCoverage.append(cob[3].toString())
        }else{
            xmlCoverage.append("<Coberturas NoCobertura=\"4\">" +
                    "<SumaAsegurada>3000000</SumaAsegurada>" +
                    "<TipoSuma>0</TipoSuma>" +
                    "<Deducible>0</Deducible>" +
                    "<Prima>0</Prima>" +
                    "</Coberturas>")
        }
        if (cob.keys.contains(4)){
            xmlCoverage.append(cob[4].toString())
        }else{
            xmlCoverage.append("<Coberturas NoCobertura=\"5\">" +
                    "<SumaAsegurada>200000</SumaAsegurada>" +
                    "<TipoSuma>0</TipoSuma>" +
                    "<Deducible>0</Deducible>" +
                    "<Prima>0</Prima>" +
                    "</Coberturas>")
        }
        if (cob.keys.contains(5)){
            xmlCoverage.append(cob[5].toString())
        }else{
            xmlCoverage.append("<Coberturas NoCobertura=\"6\">" +
                    "<SumaAsegurada>100000</SumaAsegurada>" +
                    "<TipoSuma>0</TipoSuma>" +
                    "<Deducible>0</Deducible>" +
                    "<Prima>0</Prima>" +
                    "</Coberturas>")
        }
        if (cob.keys.contains(6)){
            xmlCoverage.append(cob[6].toString())
        }else{
            xmlCoverage.append("<Coberturas NoCobertura=\"7\">" +
                    "<SumaAsegurada>0</SumaAsegurada>" +
                    "<TipoSuma>0</TipoSuma>" +
                    "<Deducible>0</Deducible>" +
                    "<Prima>0</Prima>" +
                    "</Coberturas>")
        }
        if (cob.keys.contains(7)){
            xmlCoverage.append(cob[7].toString())
        }
        else{
            xmlCoverage.append("<Coberturas NoCobertura=\"14\">" +
                    "<SumaAsegurada>0</SumaAsegurada>" +
                    "<TipoSuma>0</TipoSuma>" +
                    "<Deducible>0</Deducible>" +
                    "<Prima>0</Prima>" +
                    "</Coberturas>")
        }
        if (cob.keys.contains(8))xmlCoverage.append(cob[8].toString())
        if (cob.keys.contains(9))xmlCoverage.append(cob[9].toString())

        cob.clear()
        return xmlCoverage.toString().replace("]","").replace("[","")
    }

    fun transformCoberturastoCoverages(coberturas: List<Cobertura>, description: String): String{
        var coverages: MutableList<Coverage> = mutableListOf()
        for (i in 0 until coberturas.size){
            var cob: Coverage = Coverage(
                name = coberturas[i].nombre,
                sumAssured = coberturas[i].sumaAsegurada,
                deductible = coberturas[i].deducible
            )
            coverages.add(cob)
        }
        return this.coveragesTransformation(coverages,description)
    }
}