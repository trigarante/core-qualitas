package com.example.QualitasCore.handling.service.Payment

import com.example.QualitasCore.businessRules.basic.BasicIssueInterface
import com.example.QualitasCore.businessRules.basic.BasicPrintingService
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.Repository.cobros.logCollection_Repository
import com.example.QualitasCore.businessRules.basic.BasicPaymentService
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.service
import com.example.QualitasCore.providers.intermediary.CollectionIntermediary
import com.example.QualitasCore.trigarante.repository.LogEmisionRepository
import com.example.QualitasCore.trigarante.xmls.xml_cotizacion
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class PaymentService {
    @Autowired
    private lateinit var cobroXml: XMLGeneralService

    @Autowired
    private lateinit var pay: BasicPaymentService

    fun cobrar(responseWs: GeneralResponse): GeneralResponse {

        responseWs.codigoError = ""
        var credencials = if(responseWs.vehiculo.servicio == service.MOTOPARTICULAR || responseWs.vehiculo.servicio == service.MOTOREPARTIDOR)
            "motos"
        else
            ""

        responseWs.aseguradora = "HANDLING"
        try {
            val xml = cobroXml.payment(responseWs)
            return pay.pagar(responseWs, xml, credencials)
        }
        catch (e: IndexOutOfBoundsException){
            val error = ExceptionModel(error = "Catalog Error", message = "Banco invalido")
            throw error
        }
    }
}