package com.example.QualitasCore.handling.service.issue

import com.example.QualitasCore.models.GeneralResponse

@FunctionalInterface

interface IssueHandlingCoveragesInterface {

    fun issue(responseWs: GeneralResponse): GeneralResponse

    fun emisiones(responseWs: GeneralResponse): GeneralResponse
}