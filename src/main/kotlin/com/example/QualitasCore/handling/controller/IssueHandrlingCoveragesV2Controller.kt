package com.example.QualitasCore.handling.controller

import com.example.QualitasCore.handling.service.issue.IssueHandlingCoveragesInterface
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin("*")
@RestController
@Api("Issue Service", tags = arrayOf("issue"))
@RequestMapping("v2/qualitas")
class IssueHandrlingCoveragesV2Controller {
    @Autowired
    private lateinit var emisiones: IssueHandlingCoveragesInterface

    @PostMapping("issue-policy", produces = ["application/json"])
    fun issue(@RequestBody request: GeneralResponse): ResponseEntity<Any>{
        val base = emisiones.issue(request)
        if(base.emision.resultado)
            return ResponseEntity(base, HttpStatus.OK)
        else{
            val response = ExceptionModel(error = "Issue error", message = base.codigoError)
            throw response
        }
    }
}