package com.example.QualitasCore.handling.controller

import com.example.QualitasCore.models.HandlingCoveragesModel
import com.example.QualitasCore.handling.service.quotation.HandlingCoveragesInterface
import com.example.QualitasCore.extensions.discountBasicValidation
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@RequestMapping("v1/quotation")
@Api("Quotation Service", tags = arrayOf("quotation"))
class HandlingCoveragesController {
    @Autowired
    lateinit var handlingCoveragesInterface: HandlingCoveragesInterface

    @ApiOperation("Coverage Handling Quotation Service")
    @PostMapping("coverage")
    fun coverages(@RequestBody handlingCoveragesModel: HandlingCoveragesModel): ResponseEntity<Any>{
        return try{
            handlingCoveragesModel.discount = discountBasicValidation(handlingCoveragesModel.discount.toString()).toLong()
            val base = handlingCoveragesInterface.quotation(handlingCoveragesModel, GeneralResponse())
            ResponseEntity(base,HttpStatus.OK)
        }catch (e:Exception){
            ResponseEntity("Error: ${e.message}",HttpStatus.BAD_REQUEST)
        }
    }
}