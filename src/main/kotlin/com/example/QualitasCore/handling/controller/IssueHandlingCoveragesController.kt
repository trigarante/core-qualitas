package com.example.QualitasCore.handling.controller

import com.example.QualitasCore.handling.service.issue.IssueHandlingCoveragesInterface
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@RequestMapping(value = ["v1/issue","v1/qualitas-car"])
@Api("Issue Service", tags = arrayOf("issue"))
class IssueHandlingCoveragesController {
    @Autowired
    private lateinit var issueHandlingCoveragesInterface: IssueHandlingCoveragesInterface

    @ApiOperation("Coverage Handling Issue Service")
    @PostMapping(value = ["coverage","issues-custom"])
    fun issueCoverages(@RequestBody responseWs: GeneralResponse):ResponseEntity<Any>{
        return try{
            var base = issueHandlingCoveragesInterface.issue(responseWs)
            ResponseEntity(base, HttpStatus.OK)
        }
        catch (e:Exception){
            ResponseEntity("Error: ${e.message}",HttpStatus.BAD_REQUEST)
        }
    }
}