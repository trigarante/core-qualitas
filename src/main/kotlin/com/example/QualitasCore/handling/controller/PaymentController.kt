package com.example.QualitasCore.handling.controller

import com.example.QualitasCore.handling.service.Payment.PaymentService
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@Api("Payment Service", tags = arrayOf("Collection"))
@RequestMapping("v2/qualitas")
class PaymentController {
    @Autowired
    lateinit var pago: PaymentService

    @PostMapping("payment", produces = ["application/json"])
    fun payment(@RequestBody request: GeneralResponse): ResponseEntity<Any> {
        val base = pago.cobrar(request)
        return ResponseEntity(base, HttpStatus.OK)
    }
}