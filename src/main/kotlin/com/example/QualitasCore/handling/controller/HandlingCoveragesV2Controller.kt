package com.example.QualitasCore.handling.controller

import com.example.QualitasCore.handling.service.quotation.CustomQuotation
import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@Api("Quotation Service", tags = arrayOf("quotation"))
class HandlingCoveragesV2Controller {

    @Autowired
    lateinit var quote: CustomQuotation

    //  SERVICIO DE LA COTIZACION CUSTOM DE COTIZADOR INTERNO
    @ApiOperation("Handling Coverage quotation service, second version, consumes response model")
    @PostMapping( "v2/qualitas/custom")
    fun quotation(@RequestBody seguro: RequestCustom): ResponseEntity<Any> {
        val base = quote.quotation(seguro, "ANUAL")
        return ResponseEntity(base, HttpStatus.OK)
    }
    //  SERVICIO QUE CONSUME FRONT PARA MODIFICAF EL DESCUENTO
    @PostMapping("v2/qualitas-car/quotations-custom")
    fun customQuotation(@RequestBody request: GeneralResponse): ResponseEntity<Any>{
        if(request.paquete == "ALL"){
            val base = quote.quotation(request)
            return ResponseEntity(base, HttpStatus.OK)
        }
        else{
            val base = quote.quotation(request, "", request.paquete)
            return ResponseEntity(base, HttpStatus.OK)
        }
    }
}

