package com.example.QualitasCore.trigarante.controller.quotation

import com.example.QualitasCore.intern.service.quotationV2.quotationInterface
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin
@RequestMapping(value = ["v1/quotation","v1/qualitas-car"])
@Api("Quotation Service", tags = arrayOf("quotation"))
class QuotationTrigaranteController {

    @Autowired
    private lateinit var quotationInterface: quotationInterface


    @ApiOperation("Trigarante's Quotation")
    @PostMapping("trigarante")
    fun quotate(@RequestBody request: QuotationRequest): ResponseEntity<Any>{
        return try{
            when(request.paquete.uppercase()){
                "ALL"->{
                    val base = quotationInterface.quotation(request)
                    if(base.codigoError == "")
                        ResponseEntity(base, HttpStatus.OK)
                    else
                        ResponseEntity(base, HttpStatus.OK)
                }
                else->{
                    val base = quotationInterface.quotation(request, request.periodicidadPago.toString(), request.paquete)
                    if(base.codigoError == "")
                        ResponseEntity(base, HttpStatus.OK)
                    else
                        ResponseEntity(base, HttpStatus.OK)
                }
            }
        }catch (e:Exception){
            ResponseEntity("Quotation Service Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

}