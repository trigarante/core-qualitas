package com.example.QualitasCore.trigarante.controller.link

import com.example.QualitasCore.trigarante.models.response_ahorra
import com.example.QualitasCore.trigarante.service.link.LinkService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.annotations.ApiIgnore


@RestController
@RequestMapping("/v1/collection")
@Api("Collection Link Service", tags = arrayOf("quotation"))
@ApiIgnore
class LinkController {

    @Autowired
    private lateinit var linkService: LinkService

    @ApiOperation("Collection link generator service")
    @PostMapping("generate/link")
    fun generate(@RequestBody responseAhorra: response_ahorra):ResponseEntity<Any>{
        var base = linkService.link(responseAhorra)
        return ResponseEntity(base,HttpStatus.OK)
    }
}