package com.example.QualitasCore.trigarante.controller.impresion

import com.example.QualitasCore.trigarante.service.impresion.Impresion
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@RequestMapping("qualitas_documentos")
@Api("Print Service", tags = arrayOf("print"))
class ImpresionTrigaranteController {

    @Autowired
    lateinit var imp: Impresion

    @ApiOperation("Trigarante's printing documentation service")
    @GetMapping("imprimir")
    fun imp(@RequestParam noPoliza: String): ResponseEntity<Any> {
        return try {
            var base = imp.imprimir(noPoliza).replace("http", "https")
            ResponseEntity(base, HttpStatus.OK)
        }   catch (e:Exception){
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }
    }
}