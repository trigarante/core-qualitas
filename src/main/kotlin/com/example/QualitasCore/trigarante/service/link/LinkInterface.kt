package com.example.QualitasCore.trigarante.service.link

import com.example.QualitasCore.trigarante.models.response_ahorra

abstract interface LinkInterface {

    abstract fun generate(email: String, poliza: String): String

    abstract fun install(rfc: String, email: String,poliza: String): String

    abstract fun link(seguro: response_ahorra): response_ahorra
}