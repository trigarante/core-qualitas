package com.example.QualitasCore.trigarante.service

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationInterface
import com.example.QualitasCore.trigarante.xmls.xml_cotizacion
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.models.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository

@Repository
class cotizaciones_trigarante_service {
    @Autowired
    lateinit var basicQuotationInterface: BasicQuotationInterface
    @Autowired
    lateinit var xml : XMLGeneralService
    @Value("\${jwt.negocio}")
    val negocio : String = ""

    @Value("\${jwt.agente}")
    val agente : String = ""
    @Autowired
    lateinit var all: AllBasicQuotationInterface

    //forma pago anual C, semestral S, mensual M

    //*********** bloque de cotizaciones Amplia 3 paquetes ***********************
    fun quotation(request : QuotationRequest, paymentFrecuency: String, paquete: String) : GeneralResponse{
        val response = GeneralResponse()
        var xmlCotizacion: String
        when(request.servicio){
            service.PARTICULAR -> {
                request.aseguradora = "ahorra-particular"
                xmlCotizacion = xml.quoteParticular(request, paymentFrecuency, paquete)
            }
            service.APP -> {
                request.aseguradora = "ahorra-uber"
                xmlCotizacion = xml.quotationUber(request, paymentFrecuency, paquete)
            }
            service.APPPLUS -> {
                request.aseguradora = "ahorra-uber"
                xmlCotizacion = xml.quotationUberPlus(request, paymentFrecuency, paquete)
            }
            else -> {
                request.aseguradora = "ahorra-particular"
                xmlCotizacion = xml.quoteParticular(request, paymentFrecuency, paquete)
            }
        }


        var base = basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
        if(base.codigoError.contains("Alto Riesgo")){
            when(request.servicio){
                service.PARTICULAR -> {
                    request.aseguradora = "ahorra1"
                    xmlCotizacion = xml.quoteParticular(request, paymentFrecuency+"RIESGO", paquete)
                }
                service.APP -> {
                    request.aseguradora = "ahorra-uber"
                    xmlCotizacion = xml.quotationUber(request, paymentFrecuency+"RIESGO", paquete)
                }
                service.APPPLUS -> {
                    request.aseguradora = "ahorra-uber"
                    xmlCotizacion = xml.quotationUberPlus(request, paymentFrecuency+"RIESGO", paquete)
                }

                service.TAXI -> TODO()
                service.MOTOPARTICULAR -> TODO()
                service.MOTOREPARTIDOR -> TODO()
                service.PRIVADO -> TODO()
                service.CARGA -> TODO()
                service.TRACTO -> TODO()
            }
            base = basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
            base.codigoError = "Cotizacion con parametros de Alto Riesgo"
        }
        return base
    }

    fun quotation(quotationRequest: QuotationRequest): ResponseAll{
        return all.quotation(::quotation,quotationRequest)
    }
}