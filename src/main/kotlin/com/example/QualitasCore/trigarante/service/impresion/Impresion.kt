package com.example.QualitasCore.trigarante.service.impresion

import com.example.QualitasCore.providers.PrintConnection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class Impresion {
    @Autowired
    lateinit var  imprimeQualitas : PrintConnection

    fun imprimir(noPoliza : String, credentials: String = "") : String{
        try{
            var urlDocumentos = imprimeQualitas.impresionQualitas(noPoliza, credentials)
            return urlDocumentos

        }catch (ex : Exception){
            return "Error al llamar el servicio de impresion: " + ex.message
        }
    }
}