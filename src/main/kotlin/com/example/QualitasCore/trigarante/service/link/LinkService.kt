package com.example.QualitasCore.trigarante.service.link

import com.example.QualitasCore.providers.intermediary.CollectionIntermediary
import com.example.QualitasCore.trigarante.models.link.LinkRequestModel
import com.example.QualitasCore.trigarante.models.response_ahorra
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.*
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class LinkService: LinkInterface {

    @Value("\${jwt.link}")
    val url: String = ""

    @Value("\${jwt.wtoken}")
    val token: String = ""

    @Value("\${jwt.wpuid}")
    val wpuid : String = ""

    @Value("\${jwt.negocio}")
    val negocio : String = ""

    @Value("\${jwt.agente}")
    val agente : String = ""

    @Autowired
    lateinit var cobro : CollectionIntermediary

    override fun generate(email: String, poliza: String): String {
        var requestLink = LinkRequestModel()
        requestLink.email = email
        requestLink.m = "genlink"
        requestLink.poliza = poliza
        requestLink.wptoken = token

        var jsonRequest = JSONObject(requestLink).toString()
        var headers = HttpHeaders()
        headers.contentType = MediaType.APPLICATION_JSON
        val request = HttpEntity<String>(jsonRequest, headers)
        return try{
            var response = RestTemplate().postForEntity(url,request,String::class.java)
            response.body.toString()
        }catch (e:Exception){
            e.message.toString()
        }
    }

    override fun install(rfc: String, email: String, poliza: String): String {
        var xmlCobro = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<oplCollection>" +
                "<Collection NoNegocio=\""+ negocio +"\" NoPoliza=\"${poliza}\" wpuid=\"" + wpuid + "\" " +
                "wptoken=\""+ token + "\">" +
                "<collectionData>" +
                "<type>T</type>" +
                "<userkey>${rfc}</userkey>" +
                "<name>jorge garcia</name>" +
                "<number/>" +
                "<bankcode/>" +
                "<expmonth/>" +
                "<expyear/>" +
                "<cvv-csc/>" +
                "</collectionData>" +
                "<insuranceData>" +
                "<akey>" + agente + "</akey>" +
                "<email>${email}</email>" +
                "<PlazoPago>A</PlazoPago>" +
                "</insuranceData>" +
                "<CodigoError/>" +
                "</Collection>" +
                "</oplCollection>"
        var soapCobroResponse = cobro.collection(xmlCobro)
        var jsonObj = XML.toJSONObject(soapCobroResponse)
        return jsonObj.toString()
    }

    override fun link(seguro: response_ahorra): response_ahorra {
        var error = JSONObject(this.generate(seguro.cliente.email,seguro.emision.poliza)).get("error").toString()
        when(error){
            "No existen recibos disponibles a cobro para la poliza [${seguro.emision.poliza}] o la poliza no corresponde al Merchant"->{
                this.install(seguro.cliente.rfc,seguro.cliente.email,seguro.emision.poliza)
                this.generate(seguro.cliente.email,seguro.emision.poliza)
            }
        }
        return seguro
    }


}