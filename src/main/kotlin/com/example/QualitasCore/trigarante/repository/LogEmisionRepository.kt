package com.example.QualitasCore.trigarante.repository

import com.example.QualitasCore.trigarante.models.LogEmisionModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface LogEmisionRepository: JpaRepository<LogEmisionModel, String> {

    @Transactional
    @Modifying
    @Query(
        "insert into log_Emision(requestJson, requestXml, responseJson, responseXml, fecha, emisionTerminada, codigoError, poliza, servicio, ambiente, idCotizacion) " +
                "values (:requestJson, :requestXml, :responseJson, :responseXml, :fecha, :emisionTerminada, :codigoError, :poliza, :servicio, :ambiente, :idCotizacion)",
        nativeQuery = true
    )
    fun insertarLog(
        @Param("requestJson") requestJson: String,
        @Param("requestXml") requestXml: String,
        @Param("responseJson") responseJson: String,
        @Param("responseXml") responseXml: String,
        @Param("fecha") fecha: String,
        @Param("emisionTerminada") emisionTerminada: String,
        @Param("codigoError") codigoError: String,
        @Param("poliza") poliza: String,
        @Param("servicio") servicio: String,
        @Param("ambiente") ambiente: String,
        @Param("idCotizacion") idCotizacion: String
    )

    @Transactional
    @Modifying
    @Query(
        "insert into log_Quotation(requestJson, requestXml, responseJson, responseXml, fecha, codigoError, servicio, ambiente) " +
                "values (:requestJson, :requestXml, :responseJson, :responseXml, :fecha, :codigoError, :servicio, :ambiente)",
        nativeQuery = true
    )
    fun insertarLogQuotation(
        @Param("requestJson") requestJson: String,
        @Param("requestXml") requestXml: String,
        @Param("responseJson") responseJson: String,
        @Param("responseXml") responseXml: String,
        @Param("fecha") fecha: String,
        @Param("codigoError") codigoError: String,
        @Param("servicio") servicio: String,
        @Param("ambiente") ambiente: String
    )

}