package com.example.QualitasCore.trigarante.models

import org.hibernate.annotations.CreationTimestamp
import java.time.LocalDateTime
import javax.persistence.Basic
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name =  "log_Emision", schema = "Ws_Qualitas")
data class LogEmisionModel(
    @Id
    var requestJson: String = "",
    var requestXml: String = "",
    var responseJson: String = "",
    var responseXml: String = "",
    var fecha: String = "",
    var emisionTerminada: String = "",
    var codigoError: String = "",
    var poliza: String = "",
    var servicio: String = "",
    var ambiente: String = ""
)

@Entity
@Table(name = "logs_inspection2")
data class LogInspection(
    @get:Column(name = "id")
    @get:Id
    var id: Int? = 0,
    @get:Column( name="request")
    @get:Basic
    var request: String? = "",

    @get:Column(name="response")
    @get:Basic
    var response: String? = "",

    @get:Column(name="error")
    @get:Basic
    var error: String? = "",

    @get:Column(name="service")
    @get:Basic
    var service: String = "",

    @CreationTimestamp
    @Column(name = "creation_date")
    var creation_date: LocalDateTime? = LocalDateTime.now()
)