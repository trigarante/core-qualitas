package com.example.QualitasCore.trigarante.models.responseMayusculas

import com.fasterxml.jackson.annotation.JsonProperty

class response_model (
    @get:JsonProperty("Aseguradora")
    var Aseguradora: String = "",

    @get:JsonProperty("Cliente")
    val cliente: Cliente = Cliente("","","","","","","","",Direccion("","","","","","","",""),"","","",""),

    @get:JsonProperty("Vehiculo")
    val vehiculo: Vehiculo = Vehiculo("","","","","","","","","","","","",""),

    @get:JsonProperty("Coberturas")
    var coberturas: List<Cobertura> = emptyList(),

    @get:JsonProperty("Paquete")
    var paquete: String = "",

    @get:JsonProperty("Descuento")
    var descuento: String = "",

    @get:JsonProperty("PeriodicidadDePago")
    val periodicidadDePago: String = "",

    @get:JsonProperty("Cotizacion")
    val cotizacion: Cotizacion = Cotizacion("","","","","","","","","","","","",false),

    @get:JsonProperty("Emision")
    val emision: Emision  = Emision("","","","","","","","","","","",false),

    @get:JsonProperty("Pago")
    val pago: Pago = Pago("","","","","","","","",0,""),

    @get:JsonProperty("CodigoError")
    var codigoError: String = "",

    @get:JsonProperty("UrlRedireccion")
    val urlRedireccion: String = ""

) {}

data class Cliente (
    @get:JsonProperty("TipoPersona")
    val tipoPersona: String = "",

    @get:JsonProperty("Nombre")
    val nombre: String = "",

    @get:JsonProperty("ApellidoPat")
    val apellidoPat: String = "",

    @get:JsonProperty("ApellidoMat")
    val apellidoMat: String = "",

    @get:JsonProperty("Rfc")
    val rfc: String = "",

    @get:JsonProperty("FechaNacimiento")
    var fechaNacimiento: String = "",

    @get:JsonProperty("Ocupacion")
    val ocupacion: String = "",

    @get:JsonProperty("Curp")
    val curp: String = "",

    @get:JsonProperty("Direccion")
    var direccion: Direccion,

    @get:JsonProperty("Edad")
    var edad: String = "",

    @get:JsonProperty("Genero")
    var genero: String = "",

    @get:JsonProperty("Telefono")
    val telefono: String = "",

    @get:JsonProperty("Email")
    val email: String = ""
)

data class Direccion (
    @get:JsonProperty("Calle")
    val calle: String = "",

    @get:JsonProperty("NoExt")
    val noEXT: String = "",

    @get:JsonProperty("NoInt")
    val noInt: String = "",

    @get:JsonProperty("Colonia")
    val colonia: String = "",

    @get:JsonProperty("CodPostal")
    var codPostal: String = "",

    @get:JsonProperty("Poblacion")
    val poblacion: String = "",

    @get:JsonProperty("Ciudad")
    val ciudad: String = "",

    @get:JsonProperty("Pais")
    val pais: String = ""
)


data class Cobertura(
    @get:JsonProperty(value = "Nombre")
    var nombre: String ,

    @get:JsonProperty(value = "SumaAsegurada")
    var sumaAsegurada : String,

    @get: JsonProperty(value = "Deducible")
    var  deducible: String

)

data class Cotizacion (
    @get:JsonProperty("PrimaTotal")
    var primaTotal: String,

    @get:JsonProperty("PrimaNeta")
    var primaNeta: String,

    @get:JsonProperty("Derechos")
    var derechos: String,

    @get:JsonProperty("Impuesto")
    var impuesto: String,

    @get:JsonProperty("Recargos")
    var recargos: String,

    @get:JsonProperty("PrimerPago")
    var primerPago: String = "",

    @get:JsonProperty("PagosSubsecuentes")
    var pagosSubsecuentes: String = "",

    @get:JsonProperty("IdCotizacion")
    var idCotizacion: String,

    @get:JsonProperty("CotID")
    var cotID: Any? = null,

    @get:JsonProperty("VerID")@field:JsonProperty("VerID")
    var verID: Any? = null,

    @get:JsonProperty("CotIncID")@field:JsonProperty("CotIncID")
    var cotIncID: Any? = null,

    @get:JsonProperty("VerIncID")@field:JsonProperty("VerIncID")
    var verIncID: Any? = null,

    @get:JsonProperty("Resultado")
    var resultado: Boolean
)

data class Emision (
    @get:JsonProperty("PrimaTotal")
    var primaTotal: String,

    @get:JsonProperty("PrimaNeta")
    var primaNeta: String,

    @get:JsonProperty("Derechos")
    var derechos: String,

    @get:JsonProperty("Impuesto")
    var impuesto: String,

    @get:JsonProperty("Recargos")
    var recargos: String,

    @get:JsonProperty("PrimerPago")
    var primerPago: String,

    @get:JsonProperty("PagosSubsecuentes")
    var pagosSubsecuentes: String,

    @get:JsonProperty("IdCotizacion")
    var idCotizacion: String,

    @get:JsonProperty("Terminal")
    var terminal: String,

    @get:JsonProperty("Documento")
    var documento: String,

    @get:JsonProperty("Poliza")
    var poliza: String,

    @get:JsonProperty("Resultado")
    var resultado: Boolean
)

data class Pago (
    @get:JsonProperty("MedioPago")
    val medioPago: String,

    @get:JsonProperty("NombreTarjeta")
    val nombreTarjeta: String,

    @get:JsonProperty("Banco")
    val banco: String,

    @get:JsonProperty("NoTarjeta")
    val noTarjeta: String,

    @get:JsonProperty("MesExp")
    val mesExp: String,

    @get:JsonProperty("AnioExp")
    val anioExp: String,

    @get:JsonProperty("CodigoSeguridad")
    val codigoSeguridad: String,

    @get:JsonProperty("NoClabe")
    val noClabe: String,

    @get:JsonProperty("Carrier")
    val carrier: Long,

    @get:JsonProperty("MSI")
    val msi: String
)

data class Vehiculo (
    @get:JsonProperty("Uso")
    val uso: String,

    @get:JsonProperty("Marca")
    var marca: String,

    @get:JsonProperty("Modelo")
    var modelo: String,

    @get:JsonProperty("NoMotor")
    val noMotor: String,

    @get:JsonProperty("NoSerie")
    val noSerie: String,

    @get:JsonProperty("NoPlacas")
    val noPlacas: String,

    @get:JsonProperty("Descripcion")
    var descripcion: String,

    @get:JsonProperty("CodMarca")
    val codMarca: String,

    @get:JsonProperty("CodDescripcion")
    val codDescripcion: String,

    @get:JsonProperty("CodUso")
    val codUso: String,

    @get:JsonProperty("Clave")
    var clave: String,

    @get:JsonProperty("Servicio")
    val servicio: String,

    @get:JsonProperty("SubMarca")
    val subMarca: String
        )