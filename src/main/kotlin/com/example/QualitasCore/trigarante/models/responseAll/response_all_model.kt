package com.example.QualitasCore.trigarante.models.responseAll

import com.fasterxml.jackson.annotation.JsonProperty

class response_all_model (

    var aseguradora: String = "",

    var cliente: Cliente = Cliente("","","","","","","","","",Direccion("","","","","","","",""),"","","",""),

    var vehiculo: Vehiculo =  Vehiculo("","","","","","","","","","","","",""),


    var coberturas: ArrayList<Any> = arrayListOf(),

    var paquete: String = "",

    var descuento: String = "",

    var periodicidadDePago: String = "",

    var cotizacion: ArrayList<Any> = arrayListOf(),

    var emision: Emision = Emision("","","","","","","","","","","",false,""),

    var pago: Pago = Pago("","","","","","","","",0,""),

    var codigoError: String = "",

    var urlRedireccion: String = ""
)
data class Cliente (
    @get:JsonProperty("tipoPersona", required=true)@field:JsonProperty("tipoPersona", required=true)
    var tipoPersona: String,

    @get:JsonProperty("nombre", required=true)@field:JsonProperty("nombre", required=true)
    val nombre: String,

    @get:JsonProperty("apellidoPat", required=true)@field:JsonProperty("apellidoPat", required=true)
    val apellidoPat: String,

    @get:JsonProperty("apellidoMat", required=true)@field:JsonProperty("apellidoMat", required=true)
    val apellidoMat: String,

    @get:JsonProperty("rfc", required=true)@field:JsonProperty("rfc", required=true)
    val rfc: String,

    @get:JsonProperty("beneficiarioPreferente", required=true)@field:JsonProperty("beneficiarioPreferente", required=true)
    val beneficiarioPreferente: String,

    @get:JsonProperty("fechaNacimiento", required=true)@field:JsonProperty("fechaNacimiento", required=true)
    var fechaNacimiento: String,

    @get:JsonProperty("ocupacion", required=true)@field:JsonProperty("ocupacion", required=true)
    val ocupacion: String,

    @get:JsonProperty("curp", required=true)@field:JsonProperty("curp", required=true)
    val curp: String,

    @get:JsonProperty("direccion", required=true)@field:JsonProperty("direccion", required=true)
    val direccion: Direccion,

    @get:JsonProperty("edad", required=true)@field:JsonProperty("edad", required=true)
    var edad: String,

    @get:JsonProperty("genero", required=true)@field:JsonProperty("genero", required=true)
    var genero: String,

    @get:JsonProperty("telefono", required=true)@field:JsonProperty("telefono", required=true)
    val telefono: String,

    @get:JsonProperty("email", required=true)@field:JsonProperty("email", required=true)
    val email: String
)

data class Direccion (
    @get:JsonProperty("calle", required=true)@field:JsonProperty("calle", required=true)
    val calle: String,

    @get:JsonProperty("noExt", required=true)@field:JsonProperty("noExt", required=true)
    val noEXT: String,

    @get:JsonProperty("noInt", required=true)@field:JsonProperty("noInt", required=true)
    val noInt: String,

    @get:JsonProperty("colonia", required=true)@field:JsonProperty("colonia", required=true)
    val colonia: String,

    @get:JsonProperty("codPostal", required=true)@field:JsonProperty("codPostal", required=true)
    var codPostal: String,

    @get:JsonProperty("poblacion", required=true)@field:JsonProperty("poblacion", required=true)
    val poblacion: String,

    @get:JsonProperty("ciudad", required=true)@field:JsonProperty("ciudad", required=true)
    val ciudad: String,

    @get:JsonProperty("pais", required=true)@field:JsonProperty("pais", required=true)
    val pais: String
)

data class Amplia (
    @get:JsonProperty("amplia")@field:JsonProperty("amplia")
    var amplia: MutableList<CoberturaAll>? = null,
)

data class Limitada (
    @get:JsonProperty("limitada")@field:JsonProperty("limitada")
    var limitada: MutableList<CoberturaAll>? = null,
)

data class Rc(
    @get:JsonProperty("rc")@field:JsonProperty("rc")
    var rc: MutableList<CoberturaAll>? = null
)

data class CoberturaAll (
    @get:JsonProperty(value = "nombre")
    var nombre: String ,

    @get:JsonProperty(value = "sumaAsegurada")
    var sumaAsegurada : String,

    @get: JsonProperty(value = "deducible")
    var  deducible: String
)

data class CotizacionAmplia (
    @get:JsonProperty("amplia")@field:JsonProperty("amplia")
    var amplia: MutableList<PeriocidadPago>? = null,
)

data class CotizacionLimitada (
    @get:JsonProperty("limitada")@field:JsonProperty("limitada")
    var limitada: MutableList<PeriocidadPago>? = null,
)

data class CotizacionRc(
    @get:JsonProperty("rc")@field:JsonProperty("rc")
    var rc: MutableList<PeriocidadPago>? = null
)


data class PeriocidadPago (
    @get:JsonProperty("anual", required=true)@field:JsonProperty("anual", required=true)
    var anual: CotizacionAll = CotizacionAll(),

    @get:JsonProperty("semestral", required=true)@field:JsonProperty("semestral", required=true)
    var semestral: CotizacionAll = CotizacionAll(),

    @get:JsonProperty("trimestral", required=true)@field:JsonProperty("trimestral", required=true)
    var trimestral: CotizacionAll = CotizacionAll(),

    @get:JsonProperty("mensual", required=true)@field:JsonProperty("mensual", required=true)
    var mensual: CotizacionAll = CotizacionAll()
)

data class CotizacionAll (
    @get:JsonProperty("primaTotal", required=true)@field:JsonProperty("primaTotal", required=true)
    var primaTotal: String = "",

    @get:JsonProperty("primaNeta", required=true)@field:JsonProperty("primaNeta", required=true)
    var primaNeta: String = "",

    @get:JsonProperty("derechos", required=true)@field:JsonProperty("derechos", required=true)
    var derechos: String = "",

    @get:JsonProperty("impuesto", required=true)@field:JsonProperty("impuesto", required=true)
    var impuesto: String = "",

    @get:JsonProperty("recargos", required=true)@field:JsonProperty("recargos", required=true)
    var recargos: String = "",

    @get:JsonProperty("primerPago", required=true)@field:JsonProperty("primerPago", required=true)
    var primerPago: String = "",

    @get:JsonProperty("pagosSubsecuentes", required=true)@field:JsonProperty("pagosSubsecuentes", required=true)
    var pagosSubsecuentes: String = "",

    @get:JsonProperty("idCotizacion", required=true)@field:JsonProperty("idCotizacion", required=true)
    var idCotizacion: String = "",

    @get:JsonProperty("cotID", required=true)@field:JsonProperty("cotID", required=true)
    val cotID: String = "",

    @get:JsonProperty("verID", required=true)@field:JsonProperty("verID", required=true)
    val verID: String = "",

    @get:JsonProperty("cotIncID", required=true)@field:JsonProperty("cotIncID", required=true)
    val cotIncID: String = "",

    @get:JsonProperty("verIncID", required=true)@field:JsonProperty("verIncID", required=true)
    val verIncID: String = "",

    var resultado: Boolean = false
)

data class Emision(
    @get:JsonProperty("primaTotal", required=true)@field:JsonProperty("primaTotal", required=true)
    val primaTotal: String,

    @get:JsonProperty("primaNeta", required=true)@field:JsonProperty("primaNeta", required=true)
    val primaNeta: String,

    @get:JsonProperty("derechos", required=true)@field:JsonProperty("derechos", required=true)
    val derechos: String,

    @get:JsonProperty("impuesto", required=true)@field:JsonProperty("impuesto", required=true)
    val impuesto: String,

    @get:JsonProperty("recargos", required=true)@field:JsonProperty("recargos", required=true)
    val recargos: String,

    @get:JsonProperty("primerPago", required=true)@field:JsonProperty("primerPago", required=true)
    val primerPago: String,

    @get:JsonProperty("pagosSubsecuentes", required=true)@field:JsonProperty("pagosSubsecuentes", required=true)
    val pagosSubsecuentes: String,

    @get:JsonProperty("idCotizacion", required=true)@field:JsonProperty("idCotizacion", required=true)
    val idCotizacion: String,

    @get:JsonProperty("terminal", required=true)@field:JsonProperty("terminal", required=true)
    val terminal: String,

    @get:JsonProperty("documento", required=true)@field:JsonProperty("documento", required=true)
    val documento: String,

    @get:JsonProperty("poliza", required=true)@field:JsonProperty("poliza", required=true)
    val poliza: String,

    val resultado: Boolean,

    @get:JsonProperty("fechaEmision")
    var fechaEmision: String = ""

)

data class Pago (
    @get:JsonProperty("medioPago", required=true)@field:JsonProperty("medioPago", required=true)
    val medioPago: String,

    @get:JsonProperty("nombreTarjeta", required=true)@field:JsonProperty("nombreTarjeta", required=true)
    val nombreTarjeta: String,

    @get:JsonProperty("banco", required=true)@field:JsonProperty("banco", required=true)
    val banco: String,

    @get:JsonProperty("noTarjeta", required=true)@field:JsonProperty("noTarjeta", required=true)
    val noTarjeta: String,

    @get:JsonProperty("mesExp", required=true)@field:JsonProperty("mesExp", required=true)
    val mesExp: String,

    @get:JsonProperty("anioExp", required=true)@field:JsonProperty("anioExp", required=true)
    val anioExp: String,

    @get:JsonProperty("codigoSeguridad", required=true)@field:JsonProperty("codigoSeguridad", required=true)
    val codigoSeguridad: String,

    @get:JsonProperty("noClabe", required=true)@field:JsonProperty("noClabe", required=true)
    val noClabe: String,

    @get:JsonProperty("carrier", required=true)@field:JsonProperty("carrier", required=true)
    val carrier: Long,

    @get:JsonProperty("msi", required=true)@field:JsonProperty("msi", required=true)
    val msi: String
)

data class Vehiculo (
    @get:JsonProperty("uso", required=true)@field:JsonProperty("uso", required=true)
    val uso: String,

    @get:JsonProperty("marca", required=true)@field:JsonProperty("marca", required=true)
    var marca: String,

    @get:JsonProperty("modelo", required=true)@field:JsonProperty("modelo", required=true)
    var modelo: String,

    @get:JsonProperty("noMotor", required=true)@field:JsonProperty("noMotor", required=true)
    val noMotor: String,

    @get:JsonProperty("noSerie", required=true)@field:JsonProperty("noSerie", required=true)
    val noSerie: String,

    @get:JsonProperty("noPlacas", required=true)@field:JsonProperty("noPlacas", required=true)
    val noPlacas: String,

    @get:JsonProperty("descripcion", required=true)@field:JsonProperty("descripcion", required=true)
    var descripcion: String,

    @get:JsonProperty("codMarca", required=true)@field:JsonProperty("codMarca", required=true)
    val codMarca: String,

    @get:JsonProperty("codDescripcion", required=true)@field:JsonProperty("codDescripcion", required=true)
    val codDescripcion: String,

    @get:JsonProperty("codUso", required=true)@field:JsonProperty("codUso", required=true)
    val codUso: String,

    @get:JsonProperty("clave", required=true)@field:JsonProperty("clave", required=true)
    var clave: String,

    @get:JsonProperty("servicio", required=true)@field:JsonProperty("servicio", required=true)
    val servicio: String,

    @get:JsonProperty("subMarca", required=true)@field:JsonProperty("subMarca", required=true)
    val subMarca: String
        )