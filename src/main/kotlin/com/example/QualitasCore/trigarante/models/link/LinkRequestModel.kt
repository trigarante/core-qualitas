package com.example.QualitasCore.trigarante.models.link

data class LinkRequestModel(
    var wptoken: String = "",
    var m: String = "",
    var poliza: String = "",
    var email: String = ""
)