package com.example.QualitasCore.trigarante.models

import com.fasterxml.jackson.annotation.JsonProperty

class response_ahorra (
    @get:JsonProperty("aseguradora")
    var Aseguradora: String = "",

    @get:JsonProperty("cliente")
    val cliente: Cliente = Cliente("","","","","","","","",Direccion("","","","","","","",""),"","","",""),

    @get:JsonProperty("vehiculo")
    val vehiculo: Vehiculo = Vehiculo("","","","","","","","","","","","",""),

    @get:JsonProperty("coberturas")
    var coberturas: List<Cobertura> = emptyList(),

    @get:JsonProperty("paquete")
    var paquete: String = "",

    @get:JsonProperty("descuento")
    var descuento: String = "",

    @get:JsonProperty("periodicidadDePago")
    val periodicidadDePago: String = "",

    @get:JsonProperty("cotizacion")
    val cotizacion: Cotizacion = Cotizacion("","","","","","","","","","","","",false),

    @get:JsonProperty("emision")
    val emision: Emision  = Emision("","","","","","","","","","","",false),

    @get:JsonProperty("pago")
    val pago: Pago = Pago("","","","","","","","",0,""),

    @get:JsonProperty("codigoError")
    var codigoError: String = "",

    @get:JsonProperty("urlRedireccion")
    val urlRedireccion: String = ""

) {}

data class Cliente (
    @get:JsonProperty("tipoPersona")
    val tipoPersona: String = "",

    @get:JsonProperty("nombre")
    val nombre: String = "",

    @get:JsonProperty("apellidoPat")
    val apellidoPat: String = "",

    @get:JsonProperty("apellidoMat")
    val apellidoMat: String = "",

    @get:JsonProperty("rfc")
    val rfc: String = "",

    @get:JsonProperty("fechaNacimiento")
    var fechaNacimiento: String = "",

    @get:JsonProperty("ocupacion")
    val ocupacion: String = "",

    @get:JsonProperty("curp")
    val curp: String = "",

    @get:JsonProperty("direccion")
    var direccion: Direccion,

    @get:JsonProperty("edad")
    var edad: String = "",

    @get:JsonProperty("genero")
    var genero: String = "",

    @get:JsonProperty("telefono")
    val telefono: String = "",

    @get:JsonProperty("email")
    val email: String = ""
)

data class Direccion (
    @get:JsonProperty("calle")
    val calle: String = "",

    @get:JsonProperty("noExt")
    val noEXT: String = "",

    @get:JsonProperty("noInt")
    val noInt: String = "",

    @get:JsonProperty("colonia")
    val colonia: String = "",

    @get:JsonProperty("codPostal")
    var codPostal: String = "",

    @get:JsonProperty("poblacion")
    val poblacion: String = "",

    @get:JsonProperty("ciudad")
    val ciudad: String = "",

    @get:JsonProperty("pais")
    val pais: String = ""
)


data class Cobertura(
    @get:JsonProperty(value = "nombre")
    var nombre: String ,

    @get:JsonProperty(value = "sumaAsegurada")
    var sumaAsegurada : String,

    @get: JsonProperty(value = "deducible")
    var  deducible: String

)

data class Cotizacion (
    @get:JsonProperty("primaTotal")
    var primaTotal: String,

    @get:JsonProperty("primaNeta")
    var primaNeta: String,

    @get:JsonProperty("derechos")
    var derechos: String,

    @get:JsonProperty("impuesto")
    var impuesto: String,

    @get:JsonProperty("recargos")
    var recargos: String,

    @get:JsonProperty("primerPago")
    var primerPago: String = "",

    @get:JsonProperty("pagosSubsecuentes")
    var pagosSubsecuentes: String = "",

    @get:JsonProperty("idCotizacion")
    var idCotizacion: String,

    @get:JsonProperty("cotID")
    var cotID: Any? = null,

    @get:JsonProperty("verID")@field:JsonProperty("verID")
    var verID: Any? = null,

    @get:JsonProperty("cotIncID")@field:JsonProperty("cotIncID")
    var cotIncID: Any? = null,

    @get:JsonProperty("verIncID")@field:JsonProperty("verIncID")
    var verIncID: Any? = null,

    @get:JsonProperty("resultado")
    var resultado: Boolean
)

data class Emision (
    @get:JsonProperty("primaTotal")
    var primaTotal: String,

    @get:JsonProperty("primaNeta")
    var primaNeta: String,

    @get:JsonProperty("derechos")
    var derechos: String,

    @get:JsonProperty("impuesto")
    var impuesto: String,

    @get:JsonProperty("recargos")
    var recargos: String,

    @get:JsonProperty("primerPago")
    var primerPago: String,

    @get:JsonProperty("pagosSubsecuentes")
    var pagosSubsecuentes: String,

    @get:JsonProperty("idCotizacion")
    var idCotizacion: String,

    @get:JsonProperty("terminal")
    var terminal: String,

    @get:JsonProperty("documento")
    var documento: String,

    @get:JsonProperty("poliza")
    var poliza: String,

    @get:JsonProperty("resultado")
    var resultado: Boolean
)

data class Pago (
    @get:JsonProperty("medioPago")
    val medioPago: String,

    @get:JsonProperty("nombreTarjeta")
    val nombreTarjeta: String,

    @get:JsonProperty("banco")
    val banco: String,

    @get:JsonProperty("noTarjeta")
    val noTarjeta: String,

    @get:JsonProperty("mesExp")
    val mesExp: String,

    @get:JsonProperty("anioExp")
    val anioExp: String,

    @get:JsonProperty("codigoSeguridad")
    val codigoSeguridad: String,

    @get:JsonProperty("noClabe")
    val noClabe: String,

    @get:JsonProperty("carrier")
    val carrier: Long,

    @get:JsonProperty("msi")
    val msi: String
)

data class Vehiculo (
    @get:JsonProperty("uso")
    val uso: String,

    @get:JsonProperty("marca")
    var marca: String,

    @get:JsonProperty("modelo")
    var modelo: String,

    @get:JsonProperty("noMotor")
    val noMotor: String,

    @get:JsonProperty("noSerie")
    val noSerie: String,

    @get:JsonProperty("noPlacas")
    val noPlacas: String,

    @get:JsonProperty("descripcion")
    var descripcion: String,

    @get:JsonProperty("codMarca")
    val codMarca: String,

    @get:JsonProperty("codDescripcion")
    val codDescripcion: String,

    @get:JsonProperty("codUso")
    val codUso: String,

    @get:JsonProperty("clave")
    var clave: String,

    @get:JsonProperty("servicio")
    val servicio: String,

    @get:JsonProperty("subMarca")
    val subMarca: String
        )