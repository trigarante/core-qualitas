package com.example.QualitasCore.trigarante.models

import com.fasterxml.jackson.annotation.JsonAlias
import com.fasterxml.jackson.annotation.JsonProperty

class request_model (
   // @get:JsonProperty("aseguradora", required=true)@field:JsonProperty("aseguradora", required=true)
    @JsonAlias("Aseguradora","aseguradora")
    var aseguradora: String,

    //@get:JsonProperty("clave", required=true)@field:JsonProperty("clave", required=true)
    @JsonAlias("Clave","clave")
    val clave: String,

    //@get:JsonProperty("cp", required=true)@field:JsonProperty("cp", required=true)
    @JsonAlias("Cp","cp")
    val cp: String,

    //@get:JsonProperty("descripcion", required=true)@field:JsonProperty("descripcion", required=true)
    @JsonAlias("Descripcion","descripcion")
    val descripcion: String,

    //@get:JsonProperty("descuento", required=true)@field:JsonProperty("descuento", required=true)
    @JsonAlias("Descuento","descuento")
    val descuento: Long,

    //@get:JsonProperty("edad", required=true)@field:JsonProperty("edad", required=true)
    @JsonAlias("Edad","edad")
    val edad: String,

    //@get:JsonProperty("fechaNacimiento", required=true)@field:JsonProperty("fechaNacimiento", required=true)
    @JsonAlias("FechaNacimiento","fechaNacimiento")
    val fechaNacimiento: String,

    //@get:JsonProperty("genero", required=true)@field:JsonProperty("genero", required=true)
    @JsonAlias("Genero","genero")
    val genero: String,

    //@get:JsonProperty("marca", required=true)@field:JsonProperty("marca", required=true)
    @JsonAlias("Marca","marca")
    val marca: String,

    //@get:JsonProperty("modelo", required=true)@field:JsonProperty("modelo", required=true)
    @JsonAlias("Modelo","modelo")
    val modelo: String,

    //@get:JsonProperty("movimiento", required=true)@field:JsonProperty("movimiento", required=true)
    @JsonAlias("Movimiento","movimiento")
    val movimiento: String,

    //@get:JsonProperty("paquete", required=true)@field:JsonProperty("paquete", required=true)
    @JsonAlias("Paquete","paquete")
    val paquete: String,

    //@get:JsonProperty("servicio", required=true)@field:JsonProperty("servicio", required=true)
    @JsonAlias("Servicio","servicio")
    val servicio: String
        )