package com.example.QualitasCore.motos.migo.controller

import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.motos.migo.service.Cotizacion_Emision_Motos_Migo_Service
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("v1/migo-motos")
@Api("Motos Operations", tags = arrayOf("Migo"))
class Cotizacion_Emision_Motos_Migo_Controller {
    @Autowired
    lateinit var qualitasMotosService : Cotizacion_Emision_Motos_Migo_Service

    @ApiOperation("Migo's motorcycle quotation")
    @PostMapping("/quote")
    fun getCotizacion(@RequestBody request: QuotationRequest) : ResponseEntity<Any>{
        return try{
            if(request.paquete == "ALL"){
                val cotizacion = qualitasMotosService.cotizacionQualitas(request)
                ResponseEntity(cotizacion, HttpStatus.OK)
            }
            else {
                val cotizacion = qualitasMotosService.cotizacionQualitas(request, request.periodicidadPago.toString(), request.paquete)
                ResponseEntity(cotizacion, HttpStatus.OK)
            }
        }catch (e:Exception){
            ResponseEntity("Moto Quotation Service Error : ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }


    @ApiOperation("Migo's motorcycle issue")
    @PostMapping("issue-policy")
    fun getEmision(@RequestBody request: GeneralResponse): ResponseEntity<Any> {
        return try{
            val emision = qualitasMotosService.emisionQualitas(request)
            ResponseEntity(emision, HttpStatus.OK)
        }
        catch(e: Exception){
            ResponseEntity("Moto issue service Error: ${e.message}", HttpStatus.OK)
        }
    }
}