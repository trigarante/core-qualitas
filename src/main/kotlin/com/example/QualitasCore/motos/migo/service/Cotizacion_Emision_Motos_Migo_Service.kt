package com.example.QualitasCore.motos.migo.service

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationService
import com.example.QualitasCore.businessRules.basic.BasicIssueInterface
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.extensions.XMLMigoService
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class Cotizacion_Emision_Motos_Migo_Service{

    @Autowired
    lateinit var basicQuoteService: BasicQuotationInterface

    @Autowired
    lateinit var basicIssueService: BasicIssueInterface

    @Autowired
    lateinit var xmlService: XMLMigoService


    @Autowired lateinit var u: AllBasicQuotationService

    fun cotizacionQualitas(request : QuotationRequest, paymentFrecuency: String, paquete: String) : GeneralResponse {
        var xmlCotizacion: String
        val seguro = GeneralResponse()
        try {
            if(request.servicio.service == "PARTICULAR")
                xmlCotizacion = xmlService.quotationMotos(request, paymentFrecuency, paquete)
            else
                xmlCotizacion = xmlService.quotationMotosRepartidor(request, paymentFrecuency, paquete)
        }
        catch (e: IndexOutOfBoundsException){
            seguro.codigoError = "XML Creation error: No se encontro el codigo postal ${request.cp}"
            return seguro
        }
        request.aseguradora = "migo_${request.servicio}-motos"
        return basicQuoteService.requestQuotation(request, seguro, xmlCotizacion)
    }

    fun cotizacionQualitas(request: QuotationRequest): ResponseAll{
        return u.quotation(::cotizacionQualitas, request)
    }

    fun emisionQualitas(requestWs: GeneralResponse): GeneralResponse{
        var generalResponse = requestWs
        var xmlEmision = ""
        requestWs.aseguradora = "motos_migo_${requestWs.vehiculo.servicio.service}"
        try{
            xmlEmision = xmlService.issueMotos(requestWs)
        }
        catch (e: IndexOutOfBoundsException){
            generalResponse.codigoError = "XML Creation error: No se encontro la colonia ${requestWs.cliente.direccion.colonia}"
            return generalResponse
        }
        catch(e: Exception){
            generalResponse.codigoError = "XML Creation error: ${e.message}"
        }
        generalResponse = basicIssueService.responseIssue(xmlEmision, requestWs, "motosMigo")
        generalResponse.aseguradora = "Qualitas"
        return generalResponse
    }
}
