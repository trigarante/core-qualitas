package com.example.QualitasCore.motos.trigarante.service

import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll

interface QuotationMotosInterface {
    fun quotation(request: QuotationRequest, paymentFrequency:String, paquete: String): GeneralResponse

    fun quotation(request: QuotationRequest): ResponseAll
}