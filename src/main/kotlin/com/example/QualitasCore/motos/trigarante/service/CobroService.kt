package com.example.QualitasCore.motos.trigarante.service

import com.example.QualitasCore.businessRules.basic.BasicPrintingService
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.Repository.cobros.logCollection_Repository
import com.example.QualitasCore.businessRules.basic.BasicPaymentService
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.providers.intermediary.CollectionIntermediary
import com.example.QualitasCore.trigarante.service.impresion.Impresion
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CobroService {
    @Autowired
    lateinit var cobro: CollectionIntermediary

    @Autowired
    lateinit var cobroXml: XMLGeneralService

    @Autowired
    lateinit var imp: BasicPrintingService

    @Autowired
    lateinit var log: logCollection_Repository

    @Autowired
    private lateinit var paymentService: BasicPaymentService

    fun cobrar(responseWs: GeneralResponse): GeneralResponse {
        responseWs.codigoError = ""
        try{
            val xml = cobroXml.cobroMotos(responseWs)
            responseWs.aseguradora = "MOTOS"
            return paymentService.pagar(responseWs, xml, "motos")

        }
        catch (e: IndexOutOfBoundsException){
            val error = ExceptionModel(error = "Catalog Error", message = "Banco Invalido")
            throw error
        }
    }

    fun domiciliar(responseWs: GeneralResponse): GeneralResponse{
        responseWs.codigoError = ""
        var soapCobroResponse = ""
        try {
            val xml = cobroXml.domiciliacionMotos(responseWs)
            soapCobroResponse = cobro.domiciliar(xml)
            if (soapCobroResponse == "Se encolo la poliza ${responseWs.emision.poliza} al proceso de Derivacion") {
                responseWs.codigoError = soapCobroResponse
                responseWs.emision.resultado = true
                log.insertCobro(
                    request = JSONObject(responseWs).toString(),
                    request_aseguradora = xml,
                    response_aseguradora = soapCobroResponse,
                    response = responseWs.codigoError,
                    poliza = responseWs.emision.poliza,
                    estatus_cobro = "validar",
                    negocio = "AHORRA"
                )
            }
            else {
                responseWs.codigoError = soapCobroResponse
                responseWs.emision.resultado = false
                log.insertCobro(
                    request = JSONObject(responseWs).toString(),
                    request_aseguradora = xml,
                    response_aseguradora = soapCobroResponse,
                    response = responseWs.codigoError,
                    poliza = responseWs.emision.poliza,
                    estatus_cobro = "false",
                    negocio = "AHORRA"
                )
            }
            return responseWs

        } catch (ex: Exception) {
            responseWs.codigoError += ",[Error en el servicio de cobro: " + ex.message + "]"
            responseWs.emision.resultado = false
            log.insertCobro(
                JSONObject(responseWs).toString(),
                "error en el ws de cobro",
                soapCobroResponse,
                responseWs.codigoError,
                responseWs.emision.poliza,
                "false",
                "AHORRA"
            )
            return responseWs
        }
    }

    fun cancelar(poliza: String, rfc: String, motivo: String): String {
        val soapCancelationResponse: String
        var codigoError = ""
        try {
            val xml = cobroXml.cancelacion(poliza, rfc, motivo)
            soapCancelationResponse = cobro.cancel(xml)
            val jsonObj = XML.toJSONObject(soapCancelationResponse)
            val oplCollection = jsonObj.getJSONObject("oplCollection")
            val collection = oplCollection.getJSONObject("Cancelation")
            if (collection.toString().contains("\"CodigoError\":{")) {
                codigoError = collection.getJSONObject("CodigoError").get("error").toString()
                log.insertCancelacion(
                    "{ \"poliza\": $poliza, \"rfc\": \"$rfc\", \"motivo\": \"$motivo\"}",
                    xml,
                    soapCancelationResponse,
                    codigoError,
                    poliza,
                    "false",
                    "AHORRA"
                )
            }
            else{
                codigoError = "Poliza cancelada NoFolio: " + collection.get("NoFolio").toString()
                log.insertCancelacion(
                    "{ \"poliza\": $poliza, \"rfc\": \"$rfc\", \"motivo\": \"$motivo\"}",
                    xml,
                    soapCancelationResponse,
                    codigoError,
                    poliza,
                    "true",
                    "AHORRA"
                )
            }

        } catch (ex: Exception) {
            codigoError += ",[Error en el servicio de cancelacion: " + ex.message + "]"

            return codigoError
        }
        return codigoError
    }
}