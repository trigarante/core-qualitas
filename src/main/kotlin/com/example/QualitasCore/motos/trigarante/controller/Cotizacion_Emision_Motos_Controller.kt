package com.example.QualitasCore.motos.trigarante.controller

import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.motos.trigarante.service.CobroService
import com.example.QualitasCore.motos.trigarante.service.QuotationMotosInterface
import com.example.QualitasCore.motos.trigarante.service.issue.IssueMotosInterface
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore

@RestController
@RequestMapping("v1/qualitas-motos")
@Api("Motos Operations", tags = arrayOf("motosOperations"))
class Cotizacion_Emision_Motos_Controller(
    @Autowired private val quote: QuotationMotosInterface,
    @Autowired private val issue: IssueMotosInterface,
    @Autowired private val cobro: CobroService
){

    @ApiOperation("Trigarante's motorcycle quotation")
    @PostMapping("/quote")
    fun getCotizacion(@RequestBody request: QuotationRequest): ResponseEntity<Any> {
        return try{
            when(request.paquete.uppercase()){
                "ALL"->{
                    val cotizacion = quote.quotation(request)
                    ResponseEntity(cotizacion, HttpStatus.OK)
                }
                else->{
                    val cotizacion = quote.quotation(request, request.periodicidadPago.toString(), request.paquete)
                    ResponseEntity(cotizacion, HttpStatus.OK)
                }
            }

        }catch (e:Exception){
            ResponseEntity("Motos quotation Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Trigarante's motorcycle issue")
    @PostMapping("/issue-policy")
    fun getEmision(@RequestBody request: GeneralResponse): ResponseEntity<Any>{
        return try {
            val emision = issue.issueCustom(request)
            ResponseEntity(emision, HttpStatus.OK)
        }
        catch (e: Exception){
            request.codigoError = "Motos issue error: ${e.message}"
            ResponseEntity(request, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PostMapping("/payment")
    @ApiOperation("Payment of motos policies")
    fun getCobro(@RequestBody request: GeneralResponse): ResponseEntity<Any>{
        var respuestacobro = cobro.cobrar(request)
        return ResponseEntity(respuestacobro, HttpStatus.OK)
    }

    @ApiOperation("Moto's domiciliation service")
    @PostMapping("/domiciliation")
    fun getDomiciliacion(@RequestBody generalResponse: GeneralResponse) : ResponseEntity<Any> {
        val respuestacobro = cobro.domiciliar(generalResponse)
        return ResponseEntity(respuestacobro, HttpStatus.OK)
    }

    @ApiOperation("Intern's cancelation service")
    @PostMapping("/cancelation")
    @ApiIgnore
    fun getCancelacion(@RequestParam poliza: String, @RequestParam rfc: String, @RequestParam motivo: String) : ResponseEntity<Any> {
        val respuestacobro = cobro.cancelar(poliza, rfc, motivo)
        return ResponseEntity(respuestacobro, HttpStatus.OK)
    }
}