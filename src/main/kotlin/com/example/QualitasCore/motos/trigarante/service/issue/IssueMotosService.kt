package com.example.QualitasCore.motos.trigarante.service.issue

import com.example.QualitasCore.businessRules.basic.BasicIssueInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.models.GeneralResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class IssueMotosService(
    @Autowired private val issue: BasicIssueInterface,
    @Autowired private val xmlService: XMLGeneralService
): IssueMotosInterface{

    @Value("\${motos.agente}")
    var agente: String = ""
    @Value("\${jwt.negocio}")
    var negocio: String = ""

    override fun issueCustom(request: GeneralResponse): GeneralResponse{
        try{
            request.aseguradora = "motos-${request.vehiculo.servicio}"
            val xml = xmlService.issueMotos(request)
            return issue.responseIssue(xml, request, "motos")
        }
        catch (e: IndexOutOfBoundsException){
            request.codigoError = "XML Creation error: No se encontro la colonia relacionada al codigo postal ${request.cliente.direccion.codPostal}"
            return request
        }
    }
}