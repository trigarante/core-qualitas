package com.example.QualitasCore.motos.trigarante.service.issue

import com.example.QualitasCore.models.GeneralResponse

interface IssueMotosInterface{

    fun issueCustom(request: GeneralResponse): GeneralResponse
}