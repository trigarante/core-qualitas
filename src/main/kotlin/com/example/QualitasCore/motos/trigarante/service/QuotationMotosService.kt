package com.example.QualitasCore.motos.trigarante.service

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationService
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.models.*
import com.example.QualitasCore.models.Custom.RequestCustom
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*

@Service
class QuotationMotosService(

) : QuotationMotosInterface {
    @Autowired lateinit var basicQuotationInterface: BasicQuotationInterface
    @Autowired lateinit var u: AllBasicQuotationService
    @Autowired lateinit var xmlMotosGenerator: XMLGeneralService

    @Value("\${motos.agente}")
    var agente: String = ""
    @Value("\${jwt.negocio}")
    var negocio: String = ""

    override fun quotation(request: QuotationRequest, paymentFrequency: String, paquete: String): GeneralResponse {
        val response = GeneralResponse()
        response.paquete = request.paquete
        response.cliente.direccion.codPostal = request.cp
        response.cliente.edad = request.edad
        response.cliente.fechaNacimiento = request.fechaNacimiento
        response.vehiculo.clave = request.clave
        response.vehiculo.marca = request.marca
        response.vehiculo.modelo = request.modelo
        response.vehiculo.descripcion = request.descripcion
        return try {
            var xml = xmlMotosGenerator.quotationMotos(request, paymentFrequency, paquete)
            request.aseguradora = "${request.servicio}-motos"
            var base = basicQuotationInterface.requestQuotation(request, response, xml)
            if(base.codigoError.contains("Alto Riesgo")){
                    xml = xmlMotosGenerator.quotationMotos(request, paymentFrequency + "RIESGO", paquete)
                request.aseguradora = "${request.servicio}-motos"
                base = basicQuotationInterface.requestQuotation(request, response, xml)
            }
            base
        } catch (e: IndexOutOfBoundsException){
            response.codigoError = "XML Creation error: No se encontro el cp: ${request.cp}"
            response
        }
    }

    override fun quotation(request: QuotationRequest): ResponseAll {
        return u.quotation(::quotation, request)
    }
}