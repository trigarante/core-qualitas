package com.example.QualitasCore.intern.v4.quote.service

import com.example.QualitasCore.intern.v4.quote.models.RequestWs
import com.example.QualitasCore.intern.v4.quote.models.ResponseWs
import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import org.springframework.stereotype.Repository
import javax.xml.ws.Response

@FunctionalInterface
abstract interface BasicInterfaceIntern {
    fun requestQuotation(request: RequestWs, response: ResponseWs, xml: String): ResponseWs

    fun requestQuotationCustom(request: RequestCustom, response: GeneralResponse, xml: String, paymentFrecuency: String): GeneralResponse
}
