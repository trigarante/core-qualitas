package com.example.QualitasCore.intern.v4.payment.controller

import com.example.QualitasCore.intern.v4.payment.services.PaymentInternService
import com.example.QualitasCore.intern.v4.quote.models.ResponseWs
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("v4/intern/qualitas-car/")
@Api("Servicio para realizar el pago de Polizas", tags = arrayOf("Cotizador Interno"))
@CrossOrigin("*")
class PaymentInternController {

    @Autowired
    lateinit var cobro: PaymentInternService


    @ApiOperation("Servicio para realizar el pago de la poliza")
    @PostMapping(value = ["payment"])
    fun getCobro(@RequestBody generalResponse: ResponseWs): ResponseEntity<Any> {
        try {
            val resultado = cobro.cobroV2(generalResponse)
            return ResponseEntity(resultado, HttpStatus.OK)
        } catch (e: Exception) {
            generalResponse.codigoError = e.message.toString()
            return ResponseEntity(generalResponse, HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }

}