package com.example.QualitasCore.intern.v4.quote.service

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationInterface
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.intern.v4.quote.models.RequestWs
import com.example.QualitasCore.intern.v4.quote.models.ResponseWs
import com.example.QualitasCore.models.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class QuoteInternService(
    @Autowired private var xml: XMLInternalService,
    @Autowired private var basicQuotationInterface: BasicInternService,

    @Autowired private val all: AllBasicQuotationInterface
) {

    fun quotationIntern(request: RequestWs, paymentFrecuency: String, paquete: String): ResponseWs {
        val response = ResponseWs()
        var xmlCotizacion: String
        try {
            when (request.servicio.service) {
                "PARTICULAR" -> {
                    request.aseguradora = "ahorra-particular"
                    xmlCotizacion = xml.quoteParticular(request, paymentFrecuency, paquete)
                }

                /*"APP" -> {
                    request.aseguradora = "ahorra-uber"
                    xmlCotizacion = xml.quotationUber(request, paymentFrecuency, paquete)
                }

                "APPPLUS" -> {
                    request.aseguradora = "ahorra-uber"
                    xmlCotizacion = xml.quotationUberPlus(request, paymentFrecuency, paquete)
                }*/

                else -> {
                    request.aseguradora = "ahorra-particular"
                    xmlCotizacion = xml.quoteParticular(request, paymentFrecuency, paquete)
                }
            }


            var base = basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
            if (base.codigoError.contains("Alto Riesgo")) {
                when (request.servicio.service) {
                    "PARTICULAR" -> {
                        request.aseguradora = "ahorra-particular"
                        xmlCotizacion = xml.quoteParticular(request, paymentFrecuency + "RIESGO", paquete)
                    }

                    /*"APP" -> {
                        request.aseguradora = "ahorra-uber"
                        xmlCotizacion = xml.quotationUber(request, paymentFrecuency + "RIESGO", paquete)
                    }

                    "APPPLUS" -> {
                        request.aseguradora = "ahorra-uber"
                        xmlCotizacion = xml.quotationUberPlus(request, paymentFrecuency + "RIESGO", paquete)
                    }*/

                    else -> {
                        request.aseguradora = "ahorra-particular"
                        xmlCotizacion = xml.quoteParticular(request, paymentFrecuency + "RIESGO", paquete)
                    }
                }
                base = basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
                base.codigoError = "Cotizacion con parametros de Alto Riesgo"
            }
            return base
        } catch (e: IndexOutOfBoundsException) {
            response.codigoError = "XML Creation error: No se encontro el codigo postal: ${request.cp}"
            return response
        }
    }

/*    fun quotation(request: QuotationRequest): ResponseAll {
        return all.quotation(::quotation, request)
    }*/


}

