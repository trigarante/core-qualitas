package com.example.QualitasCore.intern.v4.payment.services

import com.example.QualitasCore.Repository.catalogos.Catalogos_Bancos_Repository
import com.example.QualitasCore.Repository.catalogos.Direcciones_Repository
import com.example.QualitasCore.extensions.collectionType
import com.example.QualitasCore.intern.v4.quote.models.ResponseWs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class XMLPaymentIntern {
    @Autowired
    lateinit var  bancos : Catalogos_Bancos_Repository

    @Value("\${jwt.negocio}") private val negocio: String =""

    @Value("\${jwt.agente}") private val agenteAutos: String = ""
    @Value("\${motos.agente}") private val agenteMotos: String = ""

    @Autowired
    lateinit var direccionesQualitas: Direcciones_Repository

    @Value("\${jwt.wtoken}")
    val wtoken : String = ""

    @Value("\${jwt.wpuid}")
    val wpuid : String = ""

    @Value("\${jwt.ambiente}")
    var ambiente: String = ""

    fun cobroAutos(responseWs: ResponseWs) : String{
        try {
            //  OBTIENE EL ID DEL BANCO
            val idBanco = bancos.getBanco(responseWs.pago.banco)[0]
            var idType = ""

            if (responseWs.pago.medioPago == "DEBITO") {
                idType = "CL"
            } else if (responseWs.pago.medioPago == "CREDITO") {
                if (responseWs.pago.msi == "") {
                    idType = "CL"
                } else {
                    idType = responseWs.pago.msi
                }
            }
            // MODIFICA EL ORDEN DE LOS BANCOS PARA QUE SE MUESTREN PRIMERO LOS MAS UTILIZADOS
            runBlocking {
                launch(Dispatchers.IO) {
                    try {
                        bancos.updateOrden(idBanco.id_banco)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            val xmlCobro = "<oplCollection>" +
                    "<Collection NoNegocio=\"${this.negocio}\" NoPoliza=\"" + responseWs.emision.poliza + "\" wpuid=\"" + wpuid + "\" " +
                    "wptoken=\"" + wtoken + "\">" +
                    "<collectionData>" +
                    "<type>" + idType + "</type>" +
                    "<userkey>" + responseWs.cliente.rfc + "</userkey>" +
                    "<name>" + responseWs.pago.nombreTarjeta + "</name>" +
                    "<number>" + responseWs.pago.noTarjeta + "</number>" +
                    "<bankcode>" + idBanco.id_banco + "</bankcode>" +
                    "<expmonth>" + responseWs.pago.mesExp + "</expmonth>" +
                    "<expyear>" + responseWs.pago.anioExp + "</expyear>" +
                    "<cvv-csc>" + responseWs.pago.codigoSeguridad + "</cvv-csc>" +
                    "</collectionData>" +
                    "<insuranceData>" +
                    "<akey>$agenteAutos</akey>" +
                    "<email>" + responseWs.cliente.email + "</email>" +
                    "<PlazoPago>${collectionType(responseWs.periodicidadPago)}</PlazoPago>" +
                    "</insuranceData>" +
                    "<CodigoError/>" +
                    "</Collection>" +
                    "</oplCollection>"
            return xmlCobro
        }
        catch(e: IndexOutOfBoundsException){
            val error = Exception("Banco no encontrado")
            throw error
        }
    }
}