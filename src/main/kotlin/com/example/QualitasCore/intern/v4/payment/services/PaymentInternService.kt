package com.example.QualitasCore.intern.v4.payment.services

import com.example.QualitasCore.businessRules.basic.BasicPaymentService
import com.example.QualitasCore.businessRules.basic.BasicPrintingService
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.Repository.cobros.logCollection_Repository
import com.example.QualitasCore.businessRules.basic.InternBasicPayment
import com.example.QualitasCore.intern.v4.quote.models.ResponseWs
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.providers.intermediary.CollectionIntermediary
import com.example.QualitasCore.models.GeneralResponse
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class PaymentInternService {
    @Autowired
    private lateinit var cobro: CollectionIntermediary

    @Autowired
    private lateinit var cobroXml: XMLPaymentIntern

    @Autowired
    private lateinit var imp: BasicPrintingService

    @Autowired
    private lateinit var log: logCollection_Repository

    @Autowired
    private         lateinit var pay: InternBasicPayment



//    fun domiciliar(responseWs: GeneralResponse): GeneralResponse{
//        responseWs.codigoError = ""
//        var soapCobroResponse = ""
//        var xml: String
//        try {
//            xml = cobroXml.domiciliacionAutos(responseWs)
//            soapCobroResponse = cobro.domiciliar(xml)
//            if (soapCobroResponse == "Se encolo la poliza ${responseWs.emision.poliza} al proceso de Derivacion") {
//                responseWs.codigoError = soapCobroResponse
//                responseWs.pago.resultado = true
//            }
//            else {
//                responseWs.codigoError = soapCobroResponse
//                responseWs.pago.resultado = false
//            }
//            return responseWs
//
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//            responseWs.codigoError += ",[Error en el servicio de cobro: " + ex.message + "]"
//            responseWs.pago.resultado = false
//
//            return responseWs
//        }
//        finally {
//            try {
//                log.insertCobro(
//                    JSONObject(responseWs).toString(),
//                    "error en el ws de cobro",
//                    soapCobroResponse,
//                    responseWs.codigoError,
//                    responseWs.emision.poliza,
//                    responseWs.pago.resultado.toString(),
//                    "AHORRA_DOMICILIAR"
//                )
//            }
//            catch (e: Exception){
//                e.printStackTrace()
//            }
//        }
//    }
//
//    fun cancelar(poliza: String, rfc: String, motivo: String): String {
//        var soapCancelationResponse = ""
//        var codigoError = ""
//        var xml = ""
//        try {
//            xml = cobroXml.cancelacion(poliza, rfc, motivo)
//            soapCancelationResponse = cobro.cancel(xml)
//            val jsonObj = XML.toJSONObject(soapCancelationResponse)
//            val oplCollection = jsonObj.getJSONObject("oplCollection")
//            val collection = oplCollection.getJSONObject("Cancelation")
//            if (collection.toString().contains("\"CodigoError\":{")) {
//                codigoError = collection.getJSONObject("CodigoError").get("error").toString()
//            }
//            else{
//                codigoError = "Poliza cancelada NoFolio: " + collection.get("NoFolio").toString()
//
//            }
//
//        } catch (ex: Exception) {
//            codigoError += ",[Error en el servicio de cancelacion: " + ex.message + "]"
//            return codigoError
//        }
//        finally {
//            try{
//                log.insertCancelacion(
//                    "{ \"poliza\": $poliza, \"rfc\": \"$rfc\", \"motivo\": \"$motivo\"}",
//                    xml,
//                    soapCancelationResponse,
//                    codigoError,
//                    poliza,
//                    "validar",
//                    "AHORRA"
//                )
//            }
//            catch (e: Exception){
//                e.printStackTrace()
//            }
//        }
//        return codigoError
//    }

    fun cobroV2(requestWs: ResponseWs): ResponseWs{
        val xml: String
        try{
            xml = cobroXml.cobroAutos(requestWs)
        }
        catch(e: IndexOutOfBoundsException){
            val error = ExceptionModel()
            error.error = "XML error"
            error.message= "Banco invalido"
            try{
                log.insertCobro(
                    request = JSONObject(requestWs).toString(),
                    request_aseguradora = "",
                    response_aseguradora = "",
                    response = JSONObject(error).toString(),
                    poliza = requestWs.emision.poliza,
                    estatus_cobro = "false",
                    negocio = "AHORRA_COBRO"
                )
            }
            catch (e: Exception){
                e.printStackTrace()
            }
            throw error
        }
        return pay.pagar(requestWs, xml, "")
    }
}