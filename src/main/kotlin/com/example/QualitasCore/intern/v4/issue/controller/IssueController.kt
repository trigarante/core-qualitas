package com.example.QualitasCore.intern.v4.issue.controller

import com.example.QualitasCore.intern.v4.issue.service.IssueServiceV4
import com.example.QualitasCore.intern.v4.quote.models.ResponseWs
import com.example.QualitasCore.models.ExceptionModel
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin("*")
@RequestMapping(value = ["v4/intern/qualitas-car"])
@Api("Servicio para Emitir Polizas", tags = arrayOf("Cotizador Interno"))

class IssueController {

    @Autowired
    lateinit var emisiones: IssueServiceV4

    @ApiOperation("Servicio para emitir polizas en cotizador interno")
    @PostMapping(value = ["issue-policy"])
    fun getEmision(@RequestBody responseWs: ResponseWs): ResponseEntity<Any> {
        val base = emisiones.emisiones(responseWs)
        if (base.emision.resultado)
            return ResponseEntity(base, HttpStatus.OK)
        else {
            val response = ExceptionModel(error = "Issue error", message = base.codigoError)
            throw response
        }
    }
}