package com.example.QualitasCore.intern.v4.quote.controller


import com.example.QualitasCore.intern.v4.quote.models.RequestWs
import com.example.QualitasCore.intern.v4.quote.service.QuoteInternService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@RequestMapping(value = ["v4/intern/qualitas-car"])
@Api("Servicio para realizar cotizaciones", tags = arrayOf("Cotizador Interno"))
class QuoteInternController {
    @Autowired
    lateinit var quotation: QuoteInternService

    @ApiOperation("Servicio para realizar cotizaciones")
    @PostMapping(value = ["quote"])
    fun getCotizacion(@RequestBody request: RequestWs): ResponseEntity<Any> {
        return try {
            /*                "ALL" -> {
                                val base = quotation.quoteAll(request)
                                ResponseEntity(base, HttpStatus.OK)
                            }*/

            val base = quotation.quotationIntern(request, request.periodiocidadPago.toString(), request.paquete)
            ResponseEntity(base, HttpStatus.OK)


        } catch (e: Exception) {
            ResponseEntity("Quotation Service Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }
}