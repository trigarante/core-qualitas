package com.example.QualitasCore.intern.v4.quote.models


import com.fasterxml.jackson.annotation.JsonProperty

data class ResponseWs (
    @get:JsonProperty("aseguradora")
        var aseguradora: String = "",

    @get:JsonProperty("cliente")
        val cliente: Cliente = Cliente("","","","","","","","", Direccion("","","","","","","",""),"","","",""),

    @get:JsonProperty("conductorHabitual")
        val conductorHabitual: Cliente = Cliente("","","","","","","","",
        Direccion("","","","","","","",""),"","","",""),

    @get:JsonProperty("contratante")
        val contratante: Cliente = Cliente("","","","","","","","", Direccion("","","","","","","",""),"","","",""),

    @get:JsonProperty("beneficiario")
        val beneficiario: Cliente = Cliente("","","","","","","","", Direccion("","","","","","","",""),"","","",""),

    @get:JsonProperty("vehiculo")
        val vehiculo: Vehiculo = Vehiculo("","","","","","","","","","","","","", ""),

    @get:JsonProperty("coberturas")
        var coberturas: List<Cobertura> = emptyList(),

    @get:JsonProperty("cotizacion")
        val cotizacion: Quotation = Quotation("","","","","","","","","","","","",false),

    @get:JsonProperty("emision")
        var emision: Emision = Emision("","","","","","","","","", "","","",false),

    @get:JsonProperty("pago")
        val pago: Pago = Pago("","","","","","","","","","","", ""),

    @get:JsonProperty("facturacion")
        val facturacion : Facturacion = Facturacion("", "", "","" , "", "", "", "", "", "", "", "", " ","", "", "", "", "", "",  Direccion("", "", "", "" ,"", "", "", "", "", "", "")),

    @get:JsonProperty("paquete")
        var paquete: String = "",

    @get:JsonProperty("descuento")
        var descuento: Int = 0,

    @get:JsonProperty("codigoError")
        var codigoError: String = "",

    @get:JsonProperty("urlRedireccion")
        val urlRedireccion: String = "",

    @get:JsonProperty("diasGracia")
        var diasGracia: Int = 0,

    @get:JsonProperty("periodicidadPago")
        var periodicidadPago: String = "",

    @get:JsonProperty("conductorUniversal")
        var conductorUniversal: Boolean = false

)

data class Cliente (
    @get:JsonProperty("tipoPersona")
        var tipoPersona: String = "",

    @get:JsonProperty("nombre")
        var nombre: String = "",

    @get:JsonProperty("apellidoPat")
        var apellidoPat: String = "",

    @get:JsonProperty("apellidoMat")
        var apellidoMat: String = "",

    @get:JsonProperty("rfc")
        var rfc: String = "",

    @get:JsonProperty("fechaNacimiento")
        var fechaNacimiento: String = "",

    @get:JsonProperty("ocupacion")
        val ocupacion: String = "",

    @get:JsonProperty("curp")
        var curp: String = "",

    @get:JsonProperty("direccion")
        var direccion: Direccion,

    @get:JsonProperty("edad")
        var edad: String = "",

    @get:JsonProperty("genero")
        var genero: String = "",

    @get:JsonProperty("telefono")
        var telefono: String = "",

    @get:JsonProperty("email")
        var email: String = ""
)

data class Vehiculo (
        @get:JsonProperty("uso")
        var uso: String,

        @get:JsonProperty("marca")
        var marca: String,

        @get:JsonProperty("modelo")
        var modelo: String,

        @get:JsonProperty("noMotor")
        var noMotor: String,

        @get:JsonProperty("noSerie")
        var noSerie: String,

        @get:JsonProperty("noPlacas")
        var noPlacas: String,

        @get:JsonProperty("descripcion")
        var descripcion: String,

        @get:JsonProperty("codMarca")
        val codMarca: String,

        @get:JsonProperty("codDescripcion")
        val codDescripcion: String,

        @get:JsonProperty("codUso")
        val codUso: String,

        @get:JsonProperty("clave")
        var clave: String,

        @get:JsonProperty("servicio")
        var servicio: String,

        @get:JsonProperty("subMarca")
        var subMarca: String,

        @get:JsonProperty("detalle")
        var detalle: String
)

data class Direccion (
        @get:JsonProperty("calle")
        var calle: String = "",

        @JsonProperty("noExt")
        var noExt: String = "",

        @get:JsonProperty("noInt")
        var noInt: String = "",

        @get:JsonProperty("colonia")
        var colonia: String = "",

        @get:JsonProperty("idColonia")
        val idColonia: String = "",

        @get:JsonProperty("codPostal")
        var codPostal: String = "",

        @get:JsonProperty("poblacion")
        var poblacion: String = "",

        @get:JsonProperty("idPoblacion")
        val idPoblacion: String = "",

        @get:JsonProperty("ciudad")
        var ciudad: String = "",

        @get:JsonProperty("idCiudad")
        val idCiudad: String = "",

        @get:JsonProperty("pais")
        var pais: String = ""
)

data class Quotation (
        @get:JsonProperty("primaTotal")
        var primaTotal: String,

        @get:JsonProperty("primaNeta")
        var primaNeta: String,

        @get:JsonProperty("derechos")
        var derechos: String,

        @get:JsonProperty("impuesto")
        var impuesto: String,

        @get:JsonProperty("recargos")
        var recargos: String,

        @get:JsonProperty("primerPago")
        var primerPago: String = "",

        @get:JsonProperty("pagosSubsecuentes")
        var pagosSubsecuentes: String = "",

        @get:JsonProperty("idCotizacion")
        var idCotizacion: String,

        @get:JsonProperty("cotID")
        var cotID: String = "",

        @get:JsonProperty("verID")@field:JsonProperty("verID")
        var verID: String = "",

        @get:JsonProperty("cotIncID")@field:JsonProperty("cotIncID")
        var cotIncID: String = "" ,

        @get:JsonProperty("verIncID")@field:JsonProperty("verIncID")
        var verIncID: String = "",

        @get:JsonProperty("resultado")
        var resultado: Boolean
)

data class Cobertura(
        @get:JsonProperty(value = "nombre")
        var nombre: String ,

        @get:JsonProperty(value = "sumaAsegurada")
        var sumaAsegurada : String,

        @get: JsonProperty(value = "deducible")
        var  deducible: String

)

data class Emision (
        @get:JsonProperty("primaTotal")
        var primaTotal: String,

        @get:JsonProperty("primaNeta")
        var primaNeta: String,

        @get:JsonProperty("derechos")
        var derechos: String = "",

        @get:JsonProperty("impuesto")
        var impuesto: String = "",

        @get:JsonProperty("recargos")
        var recargos: String = "",

        @get:JsonProperty("primerPago")
        var primerPago: String = "",

        @get:JsonProperty("pagosSubsecuentes")
        var pagosSubsecuentes: String = "",

        @get:JsonProperty("idCotizacion")
        var idCotizacion: String = "",

        @get:JsonProperty("terminal")
        var terminal: String = "",

        @get:JsonProperty("documento")
        var documento: String = "",

        @get:JsonProperty("poliza")
        var poliza: String,

        @get:JsonProperty("inicioVigencia", required = false)
        var inicioVigencia: String = "",

        @get:JsonProperty("resultado")
        var resultado: Boolean
)

data class Pago (
        @get:JsonProperty("medioPago")
        var medioPago: String,

        @get:JsonProperty("nombreTarjeta")
        var nombreTarjeta: String,

        @get:JsonProperty("banco")
        var banco: String,

        @get:JsonProperty("idBanco", required = false)
        var idBanco: String = "",

        @get:JsonProperty("noTarjeta")
        var noTarjeta: String,

        @get:JsonProperty("mesExp")
        var mesExp: String,

        @get:JsonProperty("anioExp")
        var anioExp: String,

        @get:JsonProperty("codigoSeguridad")
        var codigoSeguridad: String,

        @get:JsonProperty("noClabe")
        var noClabe: String,

        @get:JsonProperty("referencia")
        val referencia: String,

        @get:JsonProperty("carrier")
        var carrier: String,

        @get:JsonProperty("msi")
        var msi: String,

        @get:JsonProperty("resultado")
        var resultado: Boolean = false
)

data class Facturacion (
    @get:JsonProperty("tipoPersona")
    val tipoPersona: String,

    @get:JsonProperty("tipoSociedad")
    val tipoSociedad: String,

    @get:JsonProperty("tipoDireccion")
    val tipoDireccion: String,

    @get:JsonProperty("telefono")
    val telefono: String,

    @get:JsonProperty("puestoGobierno")
    val puestoGobierno: String,

    @get:JsonProperty("descGobierno")
    val descGobierno: String,

    @get:JsonProperty("fechaNacimiento", required = false)
    val fechaNacimiento: String = "",

    @get:JsonProperty("profesion")
    val profesion: String,

    @get:JsonProperty("actividad")
    val actividad: String,

    @get:JsonProperty("correo")
    val correo: String,

    @get:JsonProperty("curp")
    val curp: String,

    @get:JsonProperty("giro")
    val giro: String,

    @get:JsonProperty("rfc")
    val rfc: String,

    @get:JsonProperty("nombreAdministrador")
    val nombreAdministrador: String,

    @get:JsonProperty("nombreDirector")
    val nombreDirector: String,

    @get:JsonProperty("nombreComercial")
    var nombreComercial: String,

    @get:JsonProperty("nombreApoderado")
    var nombreApoderado: String,

    @get:JsonProperty("razonSocial")
    var razonSocial: String,

    @get:JsonProperty("regimen")
    var regimen: String,

    @get:JsonProperty("direccion")
    var direccion: Direccion,
)

