package com.example.QualitasCore.businessRules.basic

import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import com.example.QualitasCore.providers.intermediary.CollectionIntermediary
import com.example.QualitasCore.Repository.cobros.logCollection_Repository
import com.example.QualitasCore.intern.v4.quote.models.ResponseWs
import org.json.JSONArray
import org.json.JSONObject
import org.json.XML

@Service
class InternBasicPayment {
    @Autowired
    private lateinit var collection: CollectionIntermediary
    @Autowired
    lateinit var imp: BasicPrintingService
    @Autowired
    lateinit var log: logCollection_Repository

    fun pagar(request: ResponseWs, xml: String, credencials: String): ResponseWs {
        val responseWs = request
        responseWs.codigoError = ""
        var soapCobroResponse: String
        var codigoError = ""
        //  REALIZA EL PROCESO DEL PAGO
        soapCobroResponse = collection.collection(xml)
        val jsonObj = XML.toJSONObject(soapCobroResponse)
        if (jsonObj.toString().contains("oplCollection")) {
            val oplCollection = jsonObj.getJSONObject("oplCollection")
            val collection = oplCollection.getJSONObject("Collection")
            //  VALIDA SI LA RESPUESTA CONTIENE ALGUN CODIGO DE ERROR
            if (collection.toString().contains("\"CodigoError\":{")) {
                val error = try {
                    collection.getJSONObject("CodigoError").get("Error")
                } catch (e: Exception) {
                    collection.getJSONObject("CodigoError").get("error")
                }
                //  VALIDA SI LA RESPUESTA TIENE UNO O MAS CODIGOS
                if (error is JSONArray) {
                    for (i in 0 until error.length()) {
                        codigoError += "[${error.get(i)}]"
                    }
                } else {
                    codigoError = "[${error}]"
                }
            }
            //validacion tarjeta declinada
            if (soapCobroResponse.contains("Declinada")) {
                val errors = ExceptionModel()
                errors.error = "Payment error"
                errors.message = codigoError
                log.insertCobro(
                    request = JSONObject(request).toString(),
                    request_aseguradora = xml,
                    response_aseguradora = soapCobroResponse,
                    response = JSONObject(errors).toString(),
                    poliza = request.emision.poliza,
                    estatus_cobro = "false",
                    negocio = request.aseguradora
                )
                throw errors
            }
            //fin validacion tarjeta declinada

            //validacion no hay error
            if (codigoError != "") {
                //  AUN QUE APARESCA ESTE ERROR SI SE REALIZO EL COBRO
                if (codigoError.contains("No hay calendario configurado para este negocio en OPL")) {
                    responseWs.pago.resultado = true
                    responseWs.emision.documento =
                        mutableListOf(imp.print(responseWs.emision.poliza, credencials, responseWs.cliente.rfc))[0].toString()
                    responseWs.codigoError = "[Se ha realizado el cobro]"
                    log.insertCobro(
                        request = JSONObject(request).toString(),
                        request_aseguradora = xml,
                        response_aseguradora = soapCobroResponse,
                        response = JSONObject(responseWs).toString(),
                        poliza = request.emision.poliza,
                        estatus_cobro = "validar",
                        negocio = request.aseguradora
                    )
                    return responseWs
                }
                val errors = ExceptionModel()
                errors.error = "Payment error"
                errors.message = codigoError
                log.insertCobro(
                    request = JSONObject(request).toString(),
                    request_aseguradora = xml,
                    response_aseguradora = soapCobroResponse,
                    response = JSONObject(errors).toString(),
                    poliza = request.emision.poliza,
                    estatus_cobro = "false",
                    negocio = request.aseguradora
                )
                throw errors

            }
            //  SE ENVIA UN LINK DE PAGO DIRECTAMENTE AL CLIENTE
            if (soapCobroResponse.contains("Se genero link de pago")) {
                val errors = ExceptionModel()
                errors.error = "Payment error"
                errors.message = "[Se genero link de pago]$codigoError"
                log.insertCobro(
                    request = JSONObject(request).toString(),
                    request_aseguradora = xml,
                    response_aseguradora = soapCobroResponse,
                    response = JSONObject(errors).toString(),
                    poliza = request.emision.poliza,
                    estatus_cobro = "validar",
                    negocio = request.aseguradora
                )
                throw errors
            }
            if (soapCobroResponse.contains("Bin invalido")) {
                val errors = ExceptionModel()
                errors.error = "Payment error"
                errors.message = "[Bin invalido]$codigoError"
                log.insertCobro(
                    request = JSONObject(request).toString(),
                    request_aseguradora = xml,
                    response_aseguradora = soapCobroResponse,
                    response = JSONObject(errors).toString(),
                    poliza = request.emision.poliza,
                    estatus_cobro = "false",
                    negocio = request.aseguradora
                )
                throw errors
            }

            if (responseWs.emision.poliza == "") {
                val errors = ExceptionModel()
                errors.error = "Payment error"
                errors.message = "[Numero de poliza vacio]$codigoError"
                log.insertCobro(
                    request = JSONObject(request).toString(),
                    request_aseguradora = xml,
                    response_aseguradora = soapCobroResponse,
                    response = JSONObject(errors).toString(),
                    poliza = request.emision.poliza,
                    estatus_cobro = "false",
                    negocio = request.aseguradora
                )
                throw errors
            }
            if (responseWs.codigoError.isEmpty()){
                responseWs.pago.resultado = true
                responseWs.emision.documento =
                    mutableListOf(imp.print(responseWs.emision.poliza, credencials, responseWs.cliente.rfc))[0].toString()
                responseWs.codigoError = "[Pago con respuesta correcta]"
                log.insertCobro(
                    request = JSONObject(request).toString(),
                    request_aseguradora = xml,
                    response_aseguradora = soapCobroResponse,
                    response = JSONObject(responseWs).toString(),
                    poliza = request.emision.poliza,
                    estatus_cobro = "validar",
                    negocio = request.aseguradora
                )
            }
            return responseWs
        } else {
            val error = ExceptionModel()
            error.error = "Payment error"
            error.message = "Undefined error: $soapCobroResponse"
            log.insertCobro(
                request = JSONObject(request).toString(),
                request_aseguradora = xml,
                response_aseguradora = soapCobroResponse,
                response = JSONObject(error).toString(),
                poliza = request.emision.poliza,
                estatus_cobro = "false",
                negocio = request.aseguradora
            )
            throw error
        }
    }
}