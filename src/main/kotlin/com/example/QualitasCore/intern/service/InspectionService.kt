package com.example.QualitasCore.intern.service

import com.example.QualitasCore.businessRules.database.LogsInspection
import com.example.QualitasCore.models.Inspeccion.LinkRequestModel
import com.example.QualitasCore.trigarante.models.LogInspection
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate

@Service
class InspectionService(
    @Autowired private val logs: LogsInspection,
    @Value("\${intern.link}") private val urlLink: String,
    @Value("\${intern.consult}") val urlConsult: String
) : InspectionInterface {

    override fun link(request: LinkRequestModel): String {
        return try {
            var headers = HttpHeaders()
            headers.contentType = MediaType.APPLICATION_JSON
            headers.setBasicAuth("genesys-api", "dl.#LG3n4p1")
            val jsnRequest = JSONObject(request).toString()
//        val resquest = HttpEntity<String>(jsnRequest,headers)
//        var response = RestTemplate().postForEntity(urlLink,request, String::class.java
            var response = RestTemplate().exchange(
                urlLink,
                HttpMethod.POST,
                HttpEntity<String>(jsnRequest, headers),
                String::class.java
            )
            logs.save(
                LogInspection(
                    request = JSONObject(request).toString(),
                    response = JSONObject(response.body).toString(),
                    error = "",
                    service = "link"
                )
            )
            response.body.toString()
        } catch (e: Exception) {
            logs.save(
                LogInspection(
                    request = JSONObject(request).toString(),
                    response = "{}",
                    service = "link",
                    error = "Error [${e.message}]"
                )
            )
            "Inspection Service Error: ${e.message}"
        }
    }

    override fun consult(ticket: String, agent: String): String {
        return try {
            var headers = HttpHeaders()
            headers.contentType = MediaType.APPLICATION_JSON
            headers.setBasicAuth("genesys-api", "dl.#LG3n4p1")
//        var response = RestTemplate().getForEntity(urlConsult+"${ticket}/agente/${agent}",String::class.java)
            val response = RestTemplate().exchange(
                urlConsult + "${ticket}/agente/${agent}",
                HttpMethod.GET,
                HttpEntity<String>(headers),
                String::class.java
            )
            val a = LogInspection(
                request = JSONObject(object{val ticket:String = ticket
                    val agent:String = agent}).toString(),//"{\"ticket\":\"${ticket}\", \"agent\":\"${agent}\"}",
                response = response.body,
                error = "",
                service = "consult"
            )
            logs.save(a
            )
            response.body.toString()
        } catch (e: Exception) {
            logs.save(
                LogInspection(
                    request = "{\"ticket\":\"${ticket}\", \"agent\":\"${agent}\"}",
                    response = "{\"error\":\"${e.message}\"}",
                    error = "Error: ${e.message}",
                    service = "consult"
                )
            )
            "Inspection Service Error: ${e.message}"
        }
    }
}