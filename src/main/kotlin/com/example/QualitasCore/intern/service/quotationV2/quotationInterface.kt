package com.example.QualitasCore.intern.service.quotationV2

import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll

abstract interface quotationInterface {

    fun quotation(request:QuotationRequest, paymentFrecuency: String, paquete: String): GeneralResponse
    fun quotation(request: QuotationRequest): ResponseAll
    fun quotationV2(request:QuotationRequest, paymentFrecuency: String, paquete: String): GeneralResponse
}