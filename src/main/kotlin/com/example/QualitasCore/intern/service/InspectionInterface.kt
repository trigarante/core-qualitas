package com.example.QualitasCore.intern.service

import com.example.QualitasCore.models.Inspeccion.LinkRequestModel

abstract interface InspectionInterface {

    abstract fun link(request: LinkRequestModel): String

    abstract fun consult(ticket: String, agent: String): String
}