package com.example.QualitasCore.intern.controller

import com.example.QualitasCore.Repository.FindIssuerRepository
import com.example.QualitasCore.intern.service.cobro.CobroAhorraService
import com.example.QualitasCore.intern.service.emisiones.emisiones_ahorra_service
import com.example.QualitasCore.intern.service.quotationV2.quotationInterface
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin("*")
@RequestMapping("v2/qualitas-car")
@Api("V2 Car", tags = arrayOf("v2carOperations"))
class V2Controller {
    @Autowired
    lateinit var quotation: quotationInterface

    @Autowired
    lateinit var emisiones: emisiones_ahorra_service

    @Autowired
    lateinit var findIssue: FindIssuerRepository

    @ApiOperation("Intern's Quotation Service")
    @PostMapping("quote")
    fun getCotizacion(@RequestBody request: QuotationRequest): ResponseEntity<Any> {

        return when (request.paquete) {
            "ALL" -> {
                val base = quotation.quotation(request)

                if (base.codigoError == "" && base.codigoError != "[Cotizacion con parametros de Alto Riesgo]"){
                    ResponseEntity(base, HttpStatus.OK)}
                else {
                    val response = ExceptionModel(error = "Quotation error", message = base.codigoError.toString())
                    throw response
                }
            }

            else -> {
                val base = quotation.quotationV2(request, "", request.paquete)
                ResponseEntity(base, HttpStatus.OK)
            }
        }
    }

    @ApiOperation("Intern's issue service")
    @PostMapping(value = ["issue-policy"])
    fun getEmision(@RequestBody responseWs: GeneralResponse): ResponseEntity<Any> {
        val base = emisiones.emisiones(responseWs)
        if (base.emision.resultado)
            return ResponseEntity(base, HttpStatus.OK)
        else {
            val response = ExceptionModel(error = "Issue error", message = base.codigoError)
            throw response
        }
    }

    @Autowired
    lateinit var cobro: CobroAhorraService

    @ApiOperation("Intern's collection service")
    @PostMapping(value = ["payment"])
    fun getCobro(@RequestBody generalResponse: GeneralResponse): ResponseEntity<Any> {
        try {
            val base = cobro.cobroV2(generalResponse)
            return ResponseEntity(base, HttpStatus.OK)
        } catch (e: Exception) {
            generalResponse.codigoError = e.message.toString()
            return ResponseEntity(generalResponse, HttpStatus.INTERNAL_SERVER_ERROR)
        }

    }

    @ApiOperation("Intern's domiciliation service")
    @PostMapping("/domiciliation")
    fun getDomiciliacion(@RequestBody generalResponse: GeneralResponse): ResponseEntity<Any> {
        val respuestacobro = cobro.domiciliar(generalResponse)
        return ResponseEntity(respuestacobro, HttpStatus.OK)
    }

    @ApiOperation("Intern's cancelation service")
    @PostMapping("/cancelation")
    fun getCancelacion(
        @RequestParam poliza: String,
        @RequestParam rfc: String,
        @RequestParam motivo: String
    ): ResponseEntity<Any> {
        val respuestacobro = cobro.cancelar(poliza, rfc, motivo)
        return ResponseEntity(respuestacobro, HttpStatus.OK)
    }

    @ApiOperation("Info isse service")
    @GetMapping("/info-issue", produces = ["application/json"] )
    fun getInfoIssue(@RequestParam numSerie: String): ResponseEntity<Any>{
        return try{
            val json = findIssue.getInfo(numSerie)
            val respuesta = JSONObject(json).toMap().get("responseJson")
            ResponseEntity(respuesta, HttpStatus.OK)
        } catch (e: Exception){
            ResponseEntity("Respuesta de emision no encontrada", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

}