package com.example.QualitasCore.intern.controller

import com.example.QualitasCore.models.Inspeccion.LinkRequestModel
import com.example.QualitasCore.intern.service.InspectionInterface
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@CrossOrigin("*")
@RestController
@RequestMapping("v1/pre/inspection")
@Api("Inspection Service", tags = arrayOf("inspection"))
class InspectionController{

    @Autowired
    private lateinit var inspectionInterface: InspectionInterface

    @ApiOperation("Intern's Inspection Service")
    @PostMapping("enlace")
    fun link(@RequestBody request: LinkRequestModel): ResponseEntity<Any>{
        return try{
            val base = inspectionInterface.link(request)
            ResponseEntity(base,HttpStatus.OK)
        }catch (e:Exception){
            ResponseEntity("Pre-Inspection Link Service Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Intern's consult inspection service")
    @GetMapping("consulta")
    fun consult(@RequestParam ticket: String, @RequestParam agente: String): ResponseEntity<Any>{
        return try{
            val base = inspectionInterface.consult(ticket, agente)
            ResponseEntity(base,HttpStatus.OK)
        }catch (e:Exception){
            ResponseEntity("Pre-Inspection Consultation Service Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }
}