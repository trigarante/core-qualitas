package com.example.QualitasCore.intern.controller.cobro

import com.example.QualitasCore.intern.service.cobro.CobroAhorraService
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore

@CrossOrigin("*")
@RestController
@RequestMapping(value = ["v1/qualitas-car"])
@Api("Collection Service", tags = arrayOf("collection"))
class CobroAhorraController {

    @Autowired
    lateinit var  cobro: CobroAhorraService

    @ApiOperation("Intern's collection service")
    @PostMapping(value = [ "payment"])
    fun getCobro(@RequestBody generalResponse: GeneralResponse) : ResponseEntity<Any> {
        var respuestacobro = cobro.cobrar(generalResponse)
        return ResponseEntity(respuestacobro, HttpStatus.OK)
    }

    @ApiOperation("Intern's domiciliation service")
    @PostMapping("/domiciliation")
    fun getDomiciliacion(@RequestBody generalResponse: GeneralResponse) : ResponseEntity<Any> {
        val respuestacobro = cobro.domiciliar(generalResponse)
        return ResponseEntity(respuestacobro, HttpStatus.OK)
    }

    @ApiOperation("Intern's cancelation service")
    @PostMapping("/cancelation")
    fun getCancelacion(@RequestParam poliza: String, @RequestParam rfc: String, @RequestParam motivo: String) : ResponseEntity<Any> {
        val respuestacobro = cobro.cancelar(poliza, rfc, motivo)
        return ResponseEntity(respuestacobro, HttpStatus.OK)
    }
}