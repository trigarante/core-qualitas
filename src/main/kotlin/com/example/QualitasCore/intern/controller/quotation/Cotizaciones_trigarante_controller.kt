package com.example.QualitasCore.autos.trigarante.controller.quotation


import com.example.QualitasCore.intern.service.quotationV2.quotationInterface
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@RequestMapping(value = [ "v1/qualitas-car"])
@Api("Quotation Service", tags = arrayOf("quotation"))
class cotizaciones_trigarante_controller {
    @Autowired
    lateinit var quotation: quotationInterface

    @ApiOperation("Intern's Quotation Service")
    @PostMapping(value = ["quote"])
    fun getCotizacion(@RequestBody request: QuotationRequest): ResponseEntity<Any> {
        return try {
            when (request.paquete) {
                "ALL" -> {
                    val base = quotation.quotation(request)
                    ResponseEntity(base, HttpStatus.OK)
                }
                else -> {
                    val base = quotation.quotation(request, request.periodicidadPago.toString(), request.paquete)
                    ResponseEntity(base, HttpStatus.OK)
                }
            }
        } catch (e: Exception) {
            ResponseEntity("Quotation Service Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }
}