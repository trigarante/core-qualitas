package com.example.QualitasCore.autos.trigarante.controller.emision

import com.example.QualitasCore.intern.service.emisiones.emisiones_ahorra_service
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@CrossOrigin("*")
@RestController
@RequestMapping(value = ["v1/qualitas-car"])
@Api("Issue Service", tags = arrayOf("issue"))
class EmisionController (
    @Autowired var emisiones: emisiones_ahorra_service
        ){
    @ApiOperation("Intern's issue service")
    @PostMapping(value = [ "issue-policy"])
    fun getEmision(@RequestBody responseWs: GeneralResponse) : ResponseEntity<Any> {
        return try {
            val respuesta = emisiones.emisiones(responseWs)
            ResponseEntity(respuesta, HttpStatus.OK)
        }catch (e:Exception){
            ResponseEntity("Issue Service Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }
}