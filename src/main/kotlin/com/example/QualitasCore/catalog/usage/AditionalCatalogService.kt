package com.example.QualitasCore.catalog.usage

import com.example.QualitasCore.Repository.ConcentradoPaquetesRepository
import com.example.QualitasCore.Repository.UsosRepository
import com.example.QualitasCore.Repository.catalogos.CatalogActivities
import com.example.QualitasCore.Repository.catalogos.CovereagesRepository
import com.example.QualitasCore.Repository.catalogos.DescuentosRepository
import com.example.QualitasCore.Repository.catalogos.PaquetesRepository
import com.example.QualitasCore.catalog.CatalogService
import com.example.QualitasCore.models.Custom.*
import com.example.QualitasCore.models.SpringFK.PaquetesModel
import com.example.QualitasCore.models.SpringFK.UsosModel
import com.example.QualitasCore.models.catalog.CatalogModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cache.annotation.CacheConfig
import org.springframework.stereotype.Service
import java.util.*

@Service
@CacheConfig(cacheNames = ["Aditional"])
class AditionalCatalogService {
    @Autowired private lateinit var repositori: CatalogActivities
    @Autowired private lateinit var concentradoPaquetesRepository: ConcentradoPaquetesRepository
    @Autowired private lateinit var concentradoCoverages: CovereagesRepository
    @Autowired private lateinit var concentradoPaquetes: PaquetesRepository
    @Autowired private lateinit var usosRepository: UsosRepository
    @Autowired private lateinit var descuentosRepository: DescuentosRepository
    //  VARIABLES DONDE SE VAN A GUARDAR LOS DATOS PARA REDUCIR LAS CONSULTAS A LAS BASES DE DATOS
    private var paquetes: List<PaquetesModel> = listOf()
    private var coberturas: HashMap<String, ResponseCoverages> = HashMap()
    private var tipos: List<UsosModel> = listOf()

    fun getColonies(codigo: String): List<CatalogModel>{
        return try{
            if(codigo == ""){
                repositori.getColinas()
            }
            else{
                repositori.getColinas(codigo)
            }
        }
        catch (e: Exception){
            e.printStackTrace()
            val ex = Exception("Colonies catalog error: ${e.message}")
            throw ex
        }
    }

    fun getTypes(): List<UsosModel>{
        return try{
            if(tipos.isEmpty())
               tipos = usosRepository.findByEnabled()
            tipos
        }
        catch (e: Exception){
            val ex = Exception("Types catalog error: ${e.message}")
            throw ex
        }
    }


    fun getPackets(usage: CatalogService.usos): List<PaquetesModel>?{
        if(paquetes.isEmpty()){
            val response = concentradoPaquetes.findPaquetes(usage.toString())
            paquetes = response
        }
        return paquetes
    }

    fun getValues(): MutableList<CatalogModel>{
        val values = mutableListOf<CatalogModel>()

        values.add(
            CatalogModel(
                id= "1",
                text= "COMERCIAL"
            )
        )
        values.add(
            CatalogModel(
                id= "2",
                text= "CONVENIDO"
            )
        )
        return values
    }

    fun getCoverages(paquete: String, tipo: CatalogService.usos, negocio: String): ResponseCoverages?{
        try {
            if(!coberturas.containsKey("$paquete-$tipo")) {
                val response = ResponseCoverages()
                val periodoDeGracia = PeriodoDeGracia()
                val descuento = Descuento()

                val coberturasList =
                    concentradoCoverages.findCoverages(
                        paquete,
                        tipo.toString()
                    )

                periodoDeGracia.default = "7"
                periodoDeGracia.disponible = true
                for (i in 3 until 15) {
                    periodoDeGracia.rango.add(
                        value(value = "$i")
                    )
                }

                response.periodoDeGracia = periodoDeGracia
                //  VALIDA EL TIPO DE USO PARA PODER ASIGNAR EL DESCUENTO
                val descuentosList = descuentosRepository.findByNegocio(negocio)

                val maxDescuento = when(tipo){
                    CatalogService.usos.PARTICULAR -> descuentosList[0].particular
                    CatalogService.usos.APP, CatalogService.usos.APPPLUS -> descuentosList[0].uber
                    CatalogService.usos.MOTOPARTICULAR, CatalogService.usos.MOTOREPARTIDOR -> descuentosList[0].motos
                    else -> descuentosList[0].particular
                }

                descuento.default = "0"
                var i = 0
                while (i <= maxDescuento) {
                    if ((i % 5) == 0) {
                        descuento.rango.add(value(value = "$i"))
                    } else if (i == maxDescuento)
                        descuento.rango.add(value(value = "$i"))
                    i++
                }
                response.descuento = descuento

                var cobertura: Coverages
                i = 0
                coberturasList.forEach {
                        cobertura = Coverages()
                        cobertura.nombre = it.cobertura.nombre
                        cobertura.obligatoria = it.requerida
                        cobertura.clave = it.cobertura.clave.toString()

                        var indice = 0
                        //  OBTIENE EL INDICE DEL QUE SE EXTRAERAN LOS DATOS DEPENDIENDO DEL USO
                        if (it.cobertura.maximo.contains("|")) {
                            when(tipo.toString()){
                                "PARTICULAR" ->{
                                    indice = 0
                                }
                                "APP" ->{
                                    indice = 1
                                }
                                "APPPLUS" ->{
                                    indice = 2
                                }
                                "MOTOPARTICULAR" ->{
                                    indice = 3
                                }
                            }
                        }
                        //  VALIDA QUE SE PUEDE MODIFICAR EN LA COVERTURA SI EL DEDUCIBLE O LA SUMA ASEGURADA
                        if (it.cobertura.tipoJuegoId.tipo_juego_id == 3) {
                            //  PONE EL FORMATO ESPECCIFICO DE LAS COBERTURAS CON FORMATO ESPECIAL
                            cobertura.deducible = if (it.cobertura.clave == 4) "${it.default} UMAS"
                            else if (it.cobertura.clave == 13)
                                "${it.default} UMA por pasajero"
                            else
                                it.default
                            i = it.cobertura.minimo.split("|")[indice].toInt()
                            //  OBTIENE EL INCREMENTO CON EL QUE SE VA A TRABAJAR
                            var incrmento = it.cobertura.incremento.split("|")[indice].toInt()
                            while (i <= it.cobertura.maximo.split("|")[indice].toInt()) {

                                if (it.cobertura.clave == 4)
                                    cobertura.rangoDeducible.add(value(value = "$i UMAS"))
                                else if (it.cobertura.clave == 13)
                                    cobertura.rangoDeducible.add(value(value = "$i UMA por pasajero"))
                                else
                                    cobertura.rangoDeducible.add(value(value = "$i"))

                                //  MODIFICA EL INCREMENTO DEPENDIENDO DE LA COBERTURA Y EL USO
                                if (it.cobertura.clave == 1 && tipo.toString() == "PARTICULAR") {
                                    if (i == 10)
                                        incrmento = incrmento * 2
                                } else if (it.cobertura.clave == 4) {
                                    if (i == 50)
                                        incrmento = incrmento * 2
                                }
                                else if (it.cobertura.clave == 3 && tipo.toString() == "PARTICULAR") {
                                    if (i == 10)
                                        incrmento = incrmento * 2
                                }
                                if (it.cobertura.clave == 1 && i < 5)
                                    i = 5
                                else
                                    i += incrmento
                            }
                        }
                        else {
                            cobertura.sumaAsegurada = it.default
                            //  VALIDA SI LA COBERTURA TIENE SUMA ASEGURADA O SOLO ESTA AMPARADA
                            if (it.default == "Amparada") {
                                cobertura.rangoSA.add(value(value = "Amparada"))
                            } else {
                                //  EN CASO DE LA COBERTURA RESPONSABILIDAD CIVIL SE VALIDA SI SE PUEDEN MODIFICAR LOS DEDUCIBLES Y LAS SUMAS ASEGURADAS
                                if(it.cobertura.clave == 4){
                                    //  PROCESO EN CASO DE QUE SE PUEDAN MODIFICAR LOS DOS PARAMETROS
                                    if(it.cobertura.minimo.split("|")[indice].contains(",")) {
                                        if (it.default.contains(",")){
                                            cobertura.sumaAsegurada = it.default.split(",")[1]
                                            cobertura.deducible = it.default.split(",")[0] + " UMAS"
                                        } else {
                                            cobertura.sumaAsegurada = it.default
                                            cobertura.deducible = it.default.split(",")[0] + " UMAS"
                                        }


                                        i = it.cobertura.minimo.split("|")[indice].split(",")[0].toInt()
                                        while (i <= it.cobertura.maximo.split("|")[indice].split(",")[0].toInt()) {
                                            cobertura.rangoDeducible.add(value(value = "$i UMAS"))
                                            i += it.cobertura.incremento.split("|")[indice].split(",")[0].toInt()
                                        }

                                        i = it.cobertura.minimo.split("|")[indice].split(",")[1].toInt()
                                        while (i <= it.cobertura.maximo.split("|")[indice].split(",")[1].toInt()) {
                                            cobertura.rangoSA.add(value(value = "$i"))
                                            i += it.cobertura.incremento.split("|")[indice].split(",")[1].toInt()
                                        }
                                    }
                                    //  CASO EN EL QUE SOLO SE PUEDE MODIFICAR LA SUMA ASEGURADA
                                    else{
                                        i = it.cobertura.minimo.split("|")[indice].toInt()
                                        while (i <= it.cobertura.maximo.split("|")[indice].toInt()) {
                                            cobertura.rangoSA.add(value(value = "$i"))
                                            i += it.cobertura.incremento.split("|")[indice].toInt()
                                        }
                                    }
                                }
                                else {
                                    i = it.cobertura.minimo.split("|")[indice].toInt()
                                    while (i <= it.cobertura.maximo.split("|")[indice].toInt()) {
                                        cobertura.rangoSA.add(value(value = "$i"))
                                        if(i < 50000 && it.cobertura.clave == 6)
                                            i = 50000
                                        else
                                            i += it.cobertura.incremento.split("|")[indice].toInt()
                                    }
                                }
                            }
                        }
                        response.coberturas.add(cobertura)
                    }
                coberturas["$paquete-$tipo"] = response
            }
            return coberturas["$paquete-$tipo"]
        }
        catch (e: Exception){
            e.printStackTrace()
            throw e
        }
    }
    //  LIMPIA TODOS LOS DATOS PARA VOLVER A CONSULTAR LA BASE DE DATOS
    fun clean(){
        paquetes = listOf()
        coberturas.clear()
        tipos = listOf()
    }
}