package com.example.QualitasCore.catalog

import com.example.QualitasCore.Repository.CatalogUsageRepository
import com.example.QualitasCore.Repository.catalogos.Busqueda_Catalogos
import com.example.QualitasCore.Repository.catalogos.InfoCarRepository
import com.example.QualitasCore.models.catalog.CatalogModel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

@Suppress("UNCHECKED_CAST")
@Service
@Scope("singleton")
class CatalogService(
    @Autowired private val usage: CatalogUsageRepository,
    @Autowired private val infoCar: InfoCarRepository,
    @Autowired private val search: Busqueda_Catalogos,
    private val e: Set<CatalogModel> = setOf(CatalogModel("Error","Catalog not found"))
    ){

    enum class  usos(var uso: Array<String>){
        //  USOS UTILIZADOS POR LA COTIZACION CUSTOM
        PARTICULAR(arrayOf("1","5")),
        APP(arrayOf("1")),
        APPPLUS(arrayOf("1")),
        MOTOPARTICULAR(arrayOf("15")),
        MOTOREPARTIDOR(arrayOf("15")),
        TRACTO(arrayOf("6")),
        TODOTERRENO(arrayOf("11")),
        //  USOS UTILIZADOS POR E-COMMERCE
        car(arrayOf("1","5")),
        motos(arrayOf("15")),
        `private`(arrayOf("1")),
        tracto(arrayOf("6")),
        todoterreno(arrayOf("11"))
    }

    enum class paquets(var paquete: String){
        AMPLIA("AMPLIA"),
        LIMITADA("LIMITADA"),
        RC("RC"),
        CUSTOM("CUSTOM")
    }

    private var modelMap: HashMap<String, MutableList<CatalogModel>> = HashMap()
    private var yearMap: HashMap<String, MutableList<CatalogModel>> = HashMap()
    private var brandMap: HashMap<String, MutableList<CatalogModel>> = HashMap()

    /**
     * It returns a list of brands of a given type.
     *
     * @param type The type of vehicle you want to get the catalog of.
     * @return A list of brands
     */
    fun catalog(type: usos): MutableList<Any> {
        if (!brandMap.keys.contains(type.toString())) {
            brandMap[type.toString()] = usage.getBrands(type.uso)
        }
        return brandMap[type.toString()] as MutableList<Any>
    }

    /**
     * It takes a brand and a type, and returns a list of models for that brand and type
     *
     * @param brand The brand of the car
     * @param type The type of vehicle, either "private" or "car"
     * @return A list of models of a brand
     */
    fun catalog(brand: String, type: usos): MutableList<Any> {
        if(!yearMap.keys.contains((brand+type.toString()).uppercase())){
            if(type == usos.private)
                yearMap[(brand+type.toString()).uppercase()] = usage.getYearsUber(type.uso, brand)
            else
                yearMap[(brand+type.toString()).uppercase()] = usage.getYears(type.uso, brand)
        }
        return yearMap[(brand+type.toString()).uppercase()] as MutableList<Any>
    }

    /**
     * It takes a brand, model and type as parameters and returns a list of submodels for that brand and model
     *
     * @param brand The brand of the car
     * @param model The model of the car
     * @param type "PRIVATE" or "CAR"
     * @return A list of sub-brands
     */
    fun catalog(brand: String, year: String, type: usos): MutableList<Any>{
        if(!modelMap.keys.contains((brand+year+type.toString()).uppercase())){
            modelMap[(brand+year+type.toString()).uppercase()] = usage.getModels(type.uso, brand, year)
        }
        return modelMap[(brand+year+type.toString()).uppercase()] as MutableList<Any>
    }

    /**
     * It takes in a brand, model, subBrand, and type, and returns a list of descriptions of the car
     *
     * @param brand The brand of the car
     * @param model The model of the car
     * @param subBrand The sub-brand of the car, for example, the sub-brand of a BMW is BMW M, BMW i, BMW X, etc.
     * @param type The type of vehicle you want to search for.
     * @return A list of objects that are of type Any.
     */
    fun catalog(brand: String, year: String, model: String, type: usos): Set<Any>{
        return usage.getVariants(type.uso, brand, year, model)
    }

    fun find(clave: String, modelo: String, type: usos): MutableList<Any>{
        try {
            return when (type) {
                 usos.private,  -> {
                    search.getCarU(clave, modelo) as MutableList<Any>
                }

                usos.car, -> {
                    search.getCar(clave, modelo) as MutableList<Any>
                }

                else -> search.getCar(clave, modelo, type.uso) as MutableList<Any>
            }
        }
        catch(e: Exception){
            e.printStackTrace()
            return mutableListOf()
        }

    }

    fun infoCar(marca: String, modelo: String, subMarca: String, descripcion: String, type: usos): MutableList<Any>{
        try {
            return when (type) {
                usos.private,  -> {
                    infoCar.getInfoCar(marca, modelo, subMarca, descripcion) as MutableList<Any>
                }

                usos.car, -> {
                    infoCar.getInfoCar(marca, modelo, subMarca, descripcion) as MutableList<Any>
                }

                else -> infoCar.getInfoCar(marca, modelo, subMarca, descripcion) as MutableList<Any>
            }
        }
        catch(e: Exception){
            e.printStackTrace()
            return mutableListOf()
        }

    }

    /* A function that cleans the cache of the catalogs. */
    fun clean(): String{
        modelMap.clear()
        yearMap.clear()
        brandMap.clear()
        return "Limpio, patron"
    }
}