package com.example.QualitasCore.catalog

import com.example.QualitasCore.catalog.usage.AditionalCatalogService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore

@RestController
@CrossOrigin("*")
@RequestMapping(value = ["/v2/qualitas-{type}"])
@Api(tags = arrayOf("catalog", "GeneralCatalogV2"))
class CatalogController(
    @Autowired private var catalogService: CatalogService,
    @Autowired private var aditionalCatalogService: AditionalCatalogService
) {
    @GetMapping(value=["brands"], produces = ["application/json"])
    @ApiOperation("general brands")
    fun getBrands(@PathVariable type: CatalogService.usos): ResponseEntity<Any>{
        return try{
            ResponseEntity(catalogService.catalog(type), HttpStatus.OK)
        }catch (e:Exception){
            ResponseEntity("Brands error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Shows all Models based on the brand")
    @GetMapping(value=[ "years"])
    fun getModels(@PathVariable type: CatalogService.usos, @RequestParam brand: String): ResponseEntity<Any>{
        return try{
            val base = catalogService.catalog(brand,type)
            ResponseEntity(base, HttpStatus.OK)
        }catch (e: Exception){
            ResponseEntity("Years Catalog Error: ${e.message}",HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Shows all models based on the brand and year")
    @GetMapping(value=[ "models"])
    fun getmodel(@PathVariable type: CatalogService.usos, @RequestParam brand: String, @RequestParam year: String): ResponseEntity<Any>{
        return try{
            val base = catalogService.catalog(brand,year,type)
            ResponseEntity(base, HttpStatus.OK)
        }catch (e: Exception){
            ResponseEntity("Models Catalog Error: ${e.message}",HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Shows all descriptions based on the brand, year and model")
    @GetMapping(value=[ "variants"])
    fun getDescription(@PathVariable type: CatalogService.usos, @RequestParam brand: String, @RequestParam year: String, @RequestParam model: String): ResponseEntity<Any>{
        return try{
            val base = catalogService.catalog(brand,year, model,type)
            ResponseEntity(base, HttpStatus.OK)
        }catch (e: Exception){
            ResponseEntity("Variants Catalog Error: ${e.message}",HttpStatus.BAD_REQUEST)
        }
    }
    //  SERVICIO PARA OBTENER LOS DATOS DEL VEHICULO DEPENDIENDO DE LA CLAVE Y EL MODELO
    @GetMapping("findCar")
    fun getCar(@PathVariable type: CatalogService.usos, @RequestParam("clave") clave: String, @RequestParam(value = "modelo", defaultValue = "", required = false) modelo: String = ""): ResponseEntity<Any>{
        return try{
            val data = clave.split("-")
            if(data.size > 1)
                ResponseEntity(catalogService.find(data[0], data[1], type), HttpStatus.OK)
            else
                ResponseEntity(catalogService.find(clave, modelo, type), HttpStatus.OK)
        }
        catch(e: Exception){
            ResponseEntity("Profession catalog error: ${e.message}", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
    //  SERVICIO PARA LIMPIAR LOS DATOS
    @ApiIgnore
    @GetMapping("clean")
    fun clean(): ResponseEntity<Any>{
        return try{
            ResponseEntity(catalogService.clean(),HttpStatus.OK)
        }catch (e: Exception){
            ResponseEntity("Error al limpiar patron D: ${e.message}", HttpStatus.CONFLICT)
        }
    }
    //  SERVICIOS PARA EL PROCESO DE LA COTIZACION CUSTOM
    @GetMapping("packages")
    fun getPackets(@PathVariable type: CatalogService.usos): ResponseEntity<Any>{
        return ResponseEntity(aditionalCatalogService.getPackets(type), HttpStatus.OK)
    }

    @GetMapping("values")
    fun getValues(@PathVariable type: CatalogService): ResponseEntity<Any>{
        return ResponseEntity(aditionalCatalogService.getValues(), HttpStatus.OK)
    }

    @GetMapping("coverages")
    fun getCoverages(@PathVariable type: CatalogService.usos, @RequestParam("package") paquete: CatalogService.paquets): ResponseEntity<Any>{
        return try {
            ResponseEntity(aditionalCatalogService.getCoverages(paquete.paquete, type, "ahorra"), HttpStatus.OK)
        }
        catch (e: Exception){
            ResponseEntity(e.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}