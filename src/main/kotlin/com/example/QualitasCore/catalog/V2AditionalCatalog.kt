package com.example.QualitasCore.catalog

import com.example.QualitasCore.catalog.usage.AditionalCatalogService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin("*")
@RequestMapping("v2/qualitas")
@Api(tags = arrayOf("catalog", "AditionalCatalogV2"))
class V2AditionalCatalog {
    @Autowired
    private lateinit var catalogService: AditionalCatalogService

    @GetMapping("colonies")
    fun getColonies(@RequestParam("codigo", required = false, defaultValue = "") codigo: String = ""): ResponseEntity<Any>{
        return try{
            ResponseEntity(catalogService.getColonies(codigo), HttpStatus.OK)
        }
        catch (e: Exception){
            ResponseEntity(e.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("types")
    fun getTypes(): ResponseEntity<Any>{
        return try{
            ResponseEntity(catalogService.getTypes(), HttpStatus.OK)
        }
        catch(e: Exception){
            ResponseEntity(e.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}