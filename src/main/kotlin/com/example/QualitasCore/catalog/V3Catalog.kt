package com.example.QualitasCore.catalog

import com.example.QualitasCore.Repository.CatalogUsageRepository
import com.example.QualitasCore.Repository.ConcentradoPaquetesRepository
import com.example.QualitasCore.Repository.catalogos.CatalogActivities
import com.example.QualitasCore.catalog.usage.AditionalCatalogService
import com.example.QualitasCore.models.catalog.CatalogModel
import com.example.QualitasCore.models.catalog.RequestInfoCarModel
import io.swagger.annotations.Api
import io.swagger.annotations.ApiModelProperty
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import springfox.documentation.annotations.ApiIgnore

@RestController
@CrossOrigin("*")
@RequestMapping(value = ["/v3/qualitas-{type}"])
@Api(tags = arrayOf("catalog", "GeneralCatalogV3"))
class V3Catalog(


    @Autowired
    private var catalogService: CatalogService,

    @Autowired
    private var catalogRepository: CatalogUsageRepository,
    @Autowired private var aditionalCatalogService: AditionalCatalogService,

    @Autowired
    var getMsi: CatalogActivities,


    ) {

    @GetMapping(value = ["brands"])
    @ApiOperation("general brands")
    fun getBrands(@PathVariable type: CatalogService.usos): ResponseEntity<Any> {
        return try {

            when (type) {
                CatalogService.usos.car, CatalogService.usos.private -> {
                    val listaMasVendidos = listOf(
                        CatalogModel("m1", "NISSAN"),
                        CatalogModel("m2", "CHEVROLET"),
                        CatalogModel("m5", "VOLKSWAGEN"),
                        CatalogModel("m4", "FORD"),
                        CatalogModel("m3", "HONDA"),
                        CatalogModel("m6", "TOYOTA"),
                        CatalogModel("m8", "DODGE"),
                        CatalogModel("m54", "KIA"),
                        CatalogModel("m43", "HYUNDAI"),
                        CatalogModel("m80", "SEAT")
                    )

                    val base = mutableListOf<CatalogModel>()
                    base.addAll(listaMasVendidos)
                    base.addAll(catalogRepository.getBrands(type.uso))

                    ResponseEntity(base.distinct(), HttpStatus.OK)
                }

                CatalogService.usos.motos -> {
                    val base = catalogRepository.getBrands(type.uso)
                    return ResponseEntity(base, HttpStatus.OK)
                }

                CatalogService.usos.TRACTO -> TODO()
                CatalogService.usos.TODOTERRENO -> TODO()
                CatalogService.usos.tracto -> TODO()
                CatalogService.usos.todoterreno -> TODO()
                CatalogService.usos.PARTICULAR -> TODO()
                CatalogService.usos.APP -> TODO()
                CatalogService.usos.APPPLUS -> TODO()
                CatalogService.usos.MOTOPARTICULAR -> TODO()
                CatalogService.usos.MOTOREPARTIDOR -> TODO()
            }


        } catch (e: Exception) {
            ResponseEntity("Brands error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Shows all Models based on the brand")
    @GetMapping(value = ["years"])
    fun getModels(@PathVariable type: CatalogService.usos, @RequestParam brand: String): ResponseEntity<Any> {
        return try {
            val base = catalogService.catalog(brand, type)
            ResponseEntity(base, HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity("Years Catalog Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Shows all models based on the brand and year")
    @GetMapping(value = ["models"])
    fun getmodel(
        @PathVariable type: CatalogService.usos,
        @RequestParam brand: String,
        @RequestParam year: String
    ): ResponseEntity<Any> {
        return try {
            val base = catalogService.catalog(brand, year, type)
            ResponseEntity(base, HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity("Models Catalog Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Shows all descriptions based on the brand, year and model")
    @GetMapping(value = ["variants"])
    fun getDescription(
        @PathVariable type: CatalogService.usos,
        @RequestParam brand: String,
        @RequestParam year: String,
        @RequestParam model: String
    ): ResponseEntity<Any> {
        return try {
            val base = catalogService.catalog(brand, year, model, type)
            ResponseEntity(base, HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity("Variants Catalog Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @GetMapping("findCar")
    fun getCar(
        @PathVariable type: CatalogService.usos,
        @RequestParam("clave") clave: String,
        @RequestParam(value = "modelo", defaultValue = "", required = false) modelo: String = ""
    ): ResponseEntity<Any> {
        return try {
            val data = clave.split("-")
            if (data.size > 1)
                ResponseEntity(catalogService.find(data[0], data[1], type), HttpStatus.OK)
            else
                ResponseEntity(catalogService.find(clave, modelo, type), HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity("Profession catalog error: ${e.message}", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PostMapping("/infoCar", produces = ["application/json"])
    fun getInfoCar(@PathVariable type: CatalogService.usos,@RequestBody requestInfoCar: RequestInfoCarModel): ResponseEntity<Any> {
        return try {
            ResponseEntity(catalogService.infoCar(requestInfoCar.marca, requestInfoCar.modelo, requestInfoCar.subMarca, requestInfoCar.descripcion, type), HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity("Respuesta no encontrada", HttpStatus.NOT_FOUND)
        }
    }


    @ApiIgnore
    @GetMapping("clean")
    fun clean(): ResponseEntity<Any> {
        return try {
            aditionalCatalogService.clean()
            ResponseEntity(catalogService.clean(), HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity("Error al limpiar patron D: ${e.message}", HttpStatus.CONFLICT)
        }
    }

    @GetMapping("packages")
    fun getPackets(@PathVariable type: CatalogService.usos): ResponseEntity<Any> {
        return ResponseEntity(aditionalCatalogService.getPackets(type), HttpStatus.OK)
    }

    @GetMapping("values")
    fun getValues(@PathVariable type: CatalogService.usos): ResponseEntity<Any> {
        return ResponseEntity(aditionalCatalogService.getValues(), HttpStatus.OK)
    }

    @GetMapping("coverages")
    fun getCoverages(
        @PathVariable type: CatalogService.usos,
        @RequestParam("package") paquete: CatalogService.paquets
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity(aditionalCatalogService.getCoverages(paquete.paquete, type, "ahorra"), HttpStatus.OK)
        } catch (e: Exception) {
            e.printStackTrace()
            ResponseEntity(e.message, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("getMsi")
    fun getMsi(@RequestParam idBanco: Int): ResponseEntity<Any> {
        return try {
            ResponseEntity(getMsi.getMsi(idBanco), HttpStatus.OK)
        } catch (e: Exception) {
            ResponseEntity(
                "Ocurrio un error al momento de consultar el catalogo de MSI",
                HttpStatus.INTERNAL_SERVER_ERROR
            )
        }

    }
}