package com.example.QualitasCore.catalog

import com.example.QualitasCore.Repository.catalogos.CatalogActivities
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin("*")
@RequestMapping("v1/qualitas-car/")
@Api(tags = arrayOf("AditionalCatalogs"))
class AditionalCatalog(
    @Autowired private var activitiesService: CatalogActivities
) {
    @GetMapping("colonies")
    fun getColonies(@RequestParam("codigo", required = false, defaultValue = "") codigo: String =  ""): ResponseEntity<Any>{
        return try{
            if(codigo == ""){
                ResponseEntity(activitiesService.getColinas(), HttpStatus.OK)
            }
            else{
                ResponseEntity(activitiesService.getColinas(codigo), HttpStatus.OK)
            }
        }
        catch (e: Exception){
            e.printStackTrace()
            ResponseEntity("Colonies catalog error: ${e.message}", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}