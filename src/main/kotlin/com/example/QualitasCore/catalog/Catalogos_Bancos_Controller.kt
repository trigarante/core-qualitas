package com.example.QualitasCore.catalog

import com.example.QualitasCore.Repository.catalogos.Catalogos_Bancos_Repository
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@CrossOrigin("*")
@RestController
@RequestMapping("catalogos")
@Api("v1 Catalogs", tags = arrayOf("catalog"))
class Catalogos_Bancos_Controller {
    @Autowired
    lateinit var qualitasService : Catalogos_Bancos_Repository

    @ApiOperation("Qualitas banks catalog service")
    @GetMapping("/bancos")
    fun getBancos() : ResponseEntity<Any>{
        val bancos = qualitasService.getBancos()
        return  ResponseEntity(bancos,HttpStatus.OK)
    }
}