package com.example.QualitasCore

import com.example.QualitasCore.businessRules.basic.BasicPrintingService
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RequestMapping("v1/")
@CrossOrigin("*")
@RestController
@Api("Issue Service", tags = arrayOf("print"))
class GeneralPrintController {

    enum class Service(var type:String){
        moto("motos"),
        intern(""),
        go("go"),
        motoMigo("motosMigo"),
        migo("particularMigo"),
        appMigo("uberMigo")

    }

    @Autowired
    lateinit var printservice: BasicPrintingService

    @GetMapping("qualitas-{servicio}/print")
    fun generalPrint(@PathVariable("servicio") servicio: Service, @RequestParam("poliza")poliza: String, @RequestParam("rfc")rfc: String): ResponseEntity<Any>{
        try{
            return ResponseEntity(printservice.print( noPoliza = poliza, credentials = servicio.type, rfc =  rfc), HttpStatus.OK)
        }
        catch (e:Exception){
            e.printStackTrace()
            return ResponseEntity("No se pudo obtener los documentos", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}