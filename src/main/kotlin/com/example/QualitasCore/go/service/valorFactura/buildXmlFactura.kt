package com.example.QualitasCore.go.service.valorFactura

import com.example.QualitasCore.Repository.catalogos.Direcciones_Repository
import com.example.QualitasCore.businessRules.database.DiscountRepository
import com.example.QualitasCore.extensions.collectionType
import com.example.QualitasCore.extensions.digitValidation
import com.example.QualitasCore.extensions.packageSelection
import com.example.QualitasCore.models.QuotationRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Repository
class buildXmlFactura(
    @Autowired private val direccionesQualitas: Direcciones_Repository,
    @Autowired private val discountRepository: DiscountRepository,
    @Value("\${jwt.ambiente}") val ambiente: String = "",
    @Value("\${jwt.negocio_go}") val negocio: String = "",
    @Value("\${jwt.agente_go}") val agente: String = "",
) {

    fun buildXML(request: QuotationRequest, paymentFrecuency: String, paquete: String): String {
        val directions = direccionesQualitas.getDireccionesByCPostal(request.cp)
        var claveDividida = request.clave.split("-").toTypedArray()
        val xmlCotizacion: String = "<Movimientos>" +
                "<Movimiento TipoMovimiento=\"2\" NoPoliza=\"\" NoCotizacion=\"\" NoEndoso=\"\" TipoEndoso=\"\" NoOTra=\"\" NoNegocio=\"${negocio}\">" +
                "<DatosAsegurado NoAsegurado=\"\">" +
                "<Nombre/>" +
                "<Direccion/>" +
                "<Colonia/>" +
                "<Poblacion/>" +
                "<Estado>${directions[0].IDEdo_Qua}</Estado>" +
                "<CodigoPostal>${request.cp}</CodigoPostal>" +
                "<NoEmpleado/>" +
                "<Agrupador/>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>3</TipoRegla>" +
                "<ValorRegla></ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>4</TipoRegla>" +
                "<ValorRegla>1</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>3</TipoRegla>" +
                "<ValorRegla>MEXICO</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>4</TipoRegla>" +
                "<ValorRegla>NOMBRE2</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>5</TipoRegla>" +
                "<ValorRegla>APELLIDOP2</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "<ConsideracionesAdicionalesDA NoConsideracion=\"40\">" +
                "<TipoRegla>6</TipoRegla>" +
                "<ValorRegla>APELLIDOM2</ValorRegla>" +
                "</ConsideracionesAdicionalesDA>" +
                "</DatosAsegurado>" +
                "<DatosVehiculo NoInciso=\"1\">" +
                "<ClaveAmis>${claveDividida[0]}</ClaveAmis>" +
                "<Modelo>${request.modelo}</Modelo>" +
                "<DescripcionVehiculo>${request.descripcion}</DescripcionVehiculo>" +
                "<Uso>01</Uso>" + // 01 regresar
                "<Servicio>01</Servicio>" +
                "<Paquete>${packageSelection(paquete)}</Paquete>" + // 01 Amplio , 03 Limitado , 04 RC
                "<Motor/>" +
                "<Serie/>" +
                "<Coberturas NoCobertura=\"1\">" +
                "<SumaAsegurada>${claveDividida[1]}</SumaAsegurada>" +
                "<TipoSuma>1</TipoSuma>" +
                "<Deducible>5</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>" +
                "<Coberturas NoCobertura=\"3\">" +
                "<SumaAsegurada>${claveDividida[1]}</SumaAsegurada>" +
                "<TipoSuma>1</TipoSuma>" +
                "<Deducible>10</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>" +
                "<Coberturas NoCobertura=\"4\">" +
                "<SumaAsegurada>3000000</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>" +
                "<Coberturas NoCobertura=\"5\">" +
                "<SumaAsegurada>200000</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>" +
                "<Coberturas NoCobertura=\"6\">" +
                "<SumaAsegurada>100000</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>" +
                "<Coberturas NoCobertura=\"7\">" +
                "<SumaAsegurada>0</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>" +
                "<Coberturas NoCobertura=\"14\">" +
                "<SumaAsegurada>0</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>" +
                "</DatosVehiculo>" +
                "<DatosGenerales>" +
                "<FechaEmision>${LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)}</FechaEmision>" +
                "<FechaInicio>${LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)}</FechaInicio>" +
                "<FechaTermino>${LocalDateTime.now().plusYears(1)
                    .format(DateTimeFormatter.ISO_DATE)}</FechaTermino>" +
                "<Moneda>0</Moneda>" +
                "<Agente>${agente}</Agente>" +
                "<FormaPago>${collectionType(paymentFrecuency)}</FormaPago>" + //forma de pago C anual, M mensual, S semestral
                "<TarifaValores>LINEA</TarifaValores>" +
                "<TarifaCuotas>LINEA</TarifaCuotas>" +
                "<TarifaDerechos>LINEA</TarifaDerechos>" +
                "<Plazo/>" +
                "<Agencia/>" +
                "<Contrato/>" +
                "<PorcentajeDescuento>${request.descuento}</PorcentajeDescuento>" + // Descuento variable
                "<ConsideracionesAdicionalesDG NoConsideracion=\"01\">" +
                "<TipoRegla>1</TipoRegla>" +
                "<ValorRegla>${digitValidation(claveDividida[0])}</ValorRegla>" +
                "</ConsideracionesAdicionalesDG>" +
                "<ConsideracionesAdicionalesDG NoConsideracion=\"04\">" +
                "<TipoRegla>1</TipoRegla>" +
                "<ValorRegla>${ambiente}</ValorRegla>" + // 1 - Pruebas 0 - Produccion
                "</ConsideracionesAdicionalesDG>" +
                "</DatosGenerales>" +
                "<Primas>" +
                "<PrimaNeta/>" +
                "<Derecho>520</Derecho>" +
                "<Recargo/>" +
                "<Impuesto/>" +
                "<PrimaTotal/>" +
                "<Comision/>" +
                "</Primas>" +
                "<CodigoError/>" +
                "</Movimiento>" +
                "</Movimientos>"
        return xmlCotizacion
    }
}