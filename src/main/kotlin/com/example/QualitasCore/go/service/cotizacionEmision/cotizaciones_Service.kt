package com.example.QualitasCore.go.service.cotizacionEmision

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationInterface
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.extensions.XMLGoService
import com.example.QualitasCore.models.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Repository

@Repository
class cotizaciones_Service(
    @Autowired private var basicQuotationInterface: BasicQuotationInterface,
    @Autowired private var all: AllBasicQuotationInterface,
    @Autowired private var xmlServcie: XMLGoService,
) {

    fun quotation(request: QuotationRequest, paymentFrequency: String, paquete: String): GeneralResponse {
        val response = GeneralResponse()
        response.descuento = request.descuento.toString()
        var xml: String

        when(request.servicio.service){
            "PARTICULAR" -> {
                request.aseguradora = "go-particular"
                xml = xmlServcie.quoteParticular(request,paymentFrequency, paquete)
                println("XML QUOTATION!!    $xml")
            }
            "APP" -> {
                request.aseguradora = "go-uber"
                xml = xmlServcie.quotationUber(request,paymentFrequency, paquete)
            }
            "APPPLUS" -> {
                request.aseguradora = "go-uber"
                xml = xmlServcie.quotationUberPlus(request,paymentFrequency, paquete)
            }
            else -> {
                request.aseguradora = "go-particular"
                xml = xmlServcie.quoteParticular(request,paymentFrequency, paquete)
            }
        }


        var base = basicQuotationInterface.requestQuotation(request, response, xml)
        if(base.codigoError.contains("Alto Riesgo")){
            when(request.servicio.service){
                "PARTICULAR" -> {
                    request.aseguradora = "go-particular"
                    xml = xmlServcie.quoteParticular(request,paymentFrequency + "RIESGO", paquete)
                }
                "APP" -> {
                    request.aseguradora = "go-uber"
                    xml = xmlServcie.quotationUber(request,paymentFrequency + "RIESGO", paquete)
                }
                "APPPLUS" -> {
                    request.aseguradora = "go-uber"
                    xml = xmlServcie.quotationUberPlus(request,paymentFrequency + "RIESGO", paquete)
                }
                else -> {
                    request.aseguradora = "go-particular"
                    xml = xmlServcie.quoteParticular(request,paymentFrequency + "RIESGO", paquete)
                }
            }
            base = basicQuotationInterface.requestQuotation(request, response, xml)
            base.codigoError = "Cotizacion con parametros de Alto Riesgo"
        }
        return base
    }

    fun quotation(request: QuotationRequest): ResponseAll{
        return all.quotation(::quotation,request)
    }
}