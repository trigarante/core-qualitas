package com.example.QualitasCore.go.service.valorFactura

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationInterface
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.models.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class valorFacturaService(
    @Autowired private val basicQuotationInterface: BasicQuotationInterface,
    @Autowired private var xml: buildXmlFactura,
    @Autowired private val all: AllBasicQuotationInterface
): InvoiceValueInterface{

    override fun quotation(request: QuotationRequest, paymentFrecuency: String, paquete: String): GeneralResponse {
        val response = GeneralResponse()
        val xmlCotizacion = xml.buildXML(request, paymentFrecuency, paquete)
        request.aseguradora = "VALORFACTURA-particular"
        return basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
    }

    override fun quotation(request: QuotationRequest): ResponseAll {
        return all.quotation(::quotation,request)
    }
}