package com.example.QualitasCore.go.service.cobro

import com.example.QualitasCore.businessRules.basic.BasicPaymentService
import com.example.QualitasCore.extensions.XMLGoService
import com.example.QualitasCore.models.GeneralResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class cobros_Service {

    @Autowired
    private lateinit var basicPaymentService: BasicPaymentService
    @Autowired
    private lateinit var requestGenerator: XMLGoService

    fun cobrar(responseWs: GeneralResponse) : GeneralResponse{
        return try {
            responseWs.aseguradora = "GO"
            val xml = requestGenerator.cobroAutos(responseWs)
            basicPaymentService.pagar(responseWs,xml, "go")
        } catch (e: IndexOutOfBoundsException){
            responseWs.codigoError = "Banco no valido"
            responseWs
        }
    }
}