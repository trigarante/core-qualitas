package com.example.QualitasCore.go.service.valorFactura

import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll

abstract interface InvoiceValueInterface {

    abstract fun quotation(request: QuotationRequest, paymentFrecuency: String, paquete: String): GeneralResponse

    abstract fun quotation(request: QuotationRequest): ResponseAll
}