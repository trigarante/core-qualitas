package com.example.QualitasCore.go.service.impresion

import com.example.QualitasCore.businessRules.basic.BasicPrintingService
import com.example.QualitasCore.providers.PrintGoConnection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class impresiones {
    @Autowired
    lateinit var  imprimeQualitas : BasicPrintingService
    fun imprimir(noPoliza : String): MutableList<Any> {
        try{
            var urlDocumentos = imprimeQualitas.print(noPoliza, "go","")
            return urlDocumentos

        }catch (ex : Exception){
            ex.printStackTrace()
            return mutableListOf()
        }
    }
}