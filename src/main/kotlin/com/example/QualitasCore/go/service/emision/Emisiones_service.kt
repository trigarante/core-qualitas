package com.example.QualitasCore.go.service.emision

import com.example.QualitasCore.businessRules.basic.BasicIssueInterface
import com.example.QualitasCore.extensions.XMLGoService
import com.example.QualitasCore.models.GeneralResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class emisiones_service(
    @Autowired private var xml : XMLGoService,
    @Autowired private val issue: BasicIssueInterface
){

    fun emisiones(request: GeneralResponse) : GeneralResponse{
        val xmlEmision = xml.issueAutos(request)
        request.aseguradora = "go-${request.vehiculo.servicio.service}"
        return issue.responseIssue(xmlEmision, request, "go")
    }

}