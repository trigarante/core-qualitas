package com.example.QualitasCore.go.controller.cotizacion

import com.example.QualitasCore.go.service.cotizacionEmision.cotizaciones_Service
import com.example.QualitasCore.extensions.discountBasicValidation
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping( "v2/qualitas-car/go")
@Api("Quotation Service", tags = arrayOf("Go"))
class cotizaciones_Controller {
    @Autowired
    lateinit var cotizacion : cotizaciones_Service

    @ApiOperation("Go's quotation service")
    @PostMapping( "quote")
    fun getCotizacion(@RequestBody request : QuotationRequest) : ResponseEntity<Any> {
        return try{
            request.descuento = discountBasicValidation(request.descuento.toString()).toLong()
            when(request.paquete.uppercase()){
                "ALL"->{
                    val base = cotizacion.quotation(request)
                    ResponseEntity(base, HttpStatus.OK)
                }
                else->{
                    val base = cotizacion.quotation(request,"", request.paquete)
                    ResponseEntity(base, HttpStatus.OK)
                }
            }
        }catch (e:Exception){
            ResponseEntity("Quotation Service Error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

}