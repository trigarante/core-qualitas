package com.example.QualitasCore.go.controller.emision

import com.example.QualitasCore.go.service.emision.emisiones_service
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("v2/qualitas-car/go")
@Api("Issue Service", tags = arrayOf("Go"))
class emisiones_Controller {

    @Autowired
    lateinit var emisiones : emisiones_service

    @ApiOperation("Go's issue service")
    @PostMapping( "issue-policy")
    fun getEmision(@RequestBody responseWs: GeneralResponse) : ResponseEntity<Any> {
        val emision =  emisiones.emisiones(responseWs)
        return ResponseEntity(emision, HttpStatus.OK)
    }
}