package com.example.QualitasCore.go.controller.cobro

import com.example.QualitasCore.go.service.cobro.cobros_Service
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("v1/qualitas-car/go")
@Api("Collection Service", tags = arrayOf("Go"))
class Cobro_Autos_Controller {
    @Autowired
    lateinit var  cobro : cobros_Service

    @ApiOperation("Go's Collection service")
    @PostMapping( "/payments")
   fun getCobro(@RequestBody responseWs: GeneralResponse) : ResponseEntity<Any> {
       val respuestacobro = cobro.cobrar(responseWs)
        return ResponseEntity(respuestacobro,HttpStatus.OK)
    }
}