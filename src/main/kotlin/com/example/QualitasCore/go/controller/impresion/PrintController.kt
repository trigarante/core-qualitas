package com.example.QualitasCore.go.controller.impresion

import com.example.QualitasCore.go.service.impresion.impresiones
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping( "v1/qualitas-car/go")
@Api("Print Service", tags = arrayOf("Go"))
class PrintController{

    @Autowired
    private lateinit var imp: impresiones

    @ApiOperation("Go's printing service")
    @GetMapping("/print")
    fun print(@RequestParam noPoliza: String): ResponseEntity<Any>{
        return try{
            var base = imp.imprimir(noPoliza)
            ResponseEntity(base, HttpStatus.OK)
        }catch (e: Exception){
            ResponseEntity("Error en el fufurufo: ${e.message}", HttpStatus.I_AM_A_TEAPOT)
        }
    }
}