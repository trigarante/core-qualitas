package com.example.QualitasCore.go.controller.valorFactura

import com.example.QualitasCore.go.service.valorFactura.InvoiceValueInterface
import io.swagger.annotations.Api
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("v1/qualitas/quotation")
@Api("Quotation Service", tags = arrayOf("quotation"))
class valorFacturaController(invoiceValueInterface: InvoiceValueInterface): valorFacturaEndpoints<Any>(invoiceValueInterface)