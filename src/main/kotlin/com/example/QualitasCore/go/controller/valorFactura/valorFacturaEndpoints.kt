package com.example.QualitasCore.go.controller.valorFactura

import com.example.QualitasCore.go.service.valorFactura.InvoiceValueInterface
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.ApiOperation
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody

abstract class valorFacturaEndpoints<T>(private val invoiceValueInterface: InvoiceValueInterface) {

    @ApiOperation("Invoice value Go's quotation service")
    @PostMapping("invoice/value")
    fun invoice(@RequestBody request: QuotationRequest): ResponseEntity<Any> {
        return try {
            when (request.paquete.uppercase()) {
                "ALL" -> {
                    val base = invoiceValueInterface.quotation(request)
                    ResponseEntity(base,HttpStatus.OK)
                }
                else -> {
                    val base = invoiceValueInterface.quotation(request, "", request.paquete)
                    ResponseEntity(base,HttpStatus.OK)
                }
            }
        } catch (e: Exception) {
            ResponseEntity("Error al cotizar Valor factura: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }
}