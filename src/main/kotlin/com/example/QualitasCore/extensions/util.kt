package com.example.QualitasCore.extensions

import com.example.QualitasCore.models.Cobertura
import com.example.QualitasCore.models.Custom.RequestCoverage
import com.example.QualitasCore.models.catalog.DirectionModel
import com.example.QualitasCore.models.service
import java.text.Normalizer
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Extension witch uses the @param Gender, and returns the required code for the service
 * M - "Mujer"
 * H - "Hombre"
 * Default Value: H
 */
fun genderAssigment(gender: String): String {
    when (gender.uppercase()) {
        "MASCULINO" -> {
            return "H"
        }
        "FEMENINO" -> {
            return "M"
        }
        else -> {
            println("Gender Not Found")
            return "H"
        }
    }
}

/**
 * Extension witch uses the @param collection / payment Frequency, and returns the required code for the service
 * C - Contado-Anual
 * S - Semestral
 * M - Mensual
 * T - Trimestral
 * Default Value: C
 */
fun collectionType(collection: String): String {
    when (collection.uppercase()) {
        "ANUAL" -> {
            return "C"
        }
        "SEMESTRAL" -> {
            return "S"
        }
        "MENSUAL" -> {
            return "M"
        }
        "TRIMESTRAL" -> {
            return "T"
        }
        else -> {
            return "C"
        }
    }
}

/**
 * Extension witch uses the @param package, and returns the required code for the service
 * 1 - AMPLIA
 * 3 - LIMITADA
 * 4 - RC
 * 11 - AMPLIAPUBLICO
 * 13 - LIMITADAPUBLICO
 * 10 - RCPUBLICO
 * Default Value: 1
 */
fun packageSelection(packages: String): String {
    return when (packages.uppercase()) {
        "AMPLIA" -> {
            "01"
        }
        "PLUS" ->{
            "02"
        }
        "LIMITADA" -> {
            "03"
        }
        "RC" -> {
            "04"
        }
        "AMPLIAPUBLICO" -> {
            "11"
        }
        "LIMITADAPUBLICO" -> {
            "13"
        }
        "RCPUBLICO" -> {
            "10"
        }
        "PREMIUM"->{
            "14"
        }
        else -> {
            "01"
        }
    }
}

/**
 * Special algorithm, provided by Qualitas
 * It takes the vehicle key and with the equation, verified it, internally
 * The key length must be 5 numbers
 */
fun digitValidation(claveV: String): String {
    var clave = claveV
    if (clave.length == 4) {
        clave = "0" + clave
    } else if (clave.length == 3) {
        clave = "00" + clave
    }
    val impar1: String = clave.substring(0, 1)
    val impar2: String = clave.substring(2, 3)
    val impar3: String = clave.substring(4, 5)
    val par1: String = clave.substring(1, 2)
    val par2: String = clave.substring(3, 4)
    val sumaDigitos: Int = ((impar1.toInt() + impar2.toInt() + impar3.toInt()) * 3) + (par1.toInt() + par2.toInt())
    if (sumaDigitos % 10 == 0) {
        val divisor: Int = sumaDigitos % 10
        return divisor.toString()
    } else {
        val divisor: Int = 10 - (sumaDigitos % 10)
        return divisor.toString()
    }
}

fun discountBasicValidation(discount: String): String {
    var descuento = discount
    if (discount == "0") descuento = "23"
    if (discount == "") descuento = "0"
    return descuento
}

private val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()

/**
 * "Borrowed code"
 * replaces the characters with accent for an unaccented one
 */
fun CharSequence.unaccent(): String {
    val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
    return REGEX_UNACCENT.replace(temp, "")
}

/**
 * Filters the list where the consulted suBurbs equals colonia
 * @param colonia
 * @return List<DirectionModel> with subBurbs equals to colonia
 */

fun List<DirectionModel>.colony(colonia: String): List<DirectionModel>{
    return this.filter {
            direccionesModel ->
        direccionesModel.d_asenta.unaccent().uppercase() == colonia.unaccent().uppercase()
    }
}
/**
 * Date format accepted by Qualitas: dd/MM/yyyy
 * verify the format of @param date if the format is the same accepted by Qualitas stays
 * else it changes to the accepted
 * the format of date depends on the authorized format
 */
fun dateReformat(date: String): String {
    var sdf = date
    if (date.contains("-")) {
        sdf = LocalDate.parse(
            date, DateTimeFormatter.ofPattern("yyyy-MM-dd")
        ).format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))
    }
    return sdf
}

/**
 * TRANSFORM THE COVERAGES INTO A XML FORMAT AND IF NOT
 * EXISTS A COVERAGE REQUIRED ADDIT FOR PRIVATE USE
 */
fun transformCovegares(coveragesList: List<Cobertura>, description: String): String{
    val coverges = StringBuilder()
    val mappedCoverage = HashMap<Int, String>()
    var coverage = ""
    var id = 0

    coveragesList.forEach{
        when(it.nombre.uppercase()){
            "DAÑOS MATERIALES" -> {
                coverage = "1"
                id = 1
            }
            "DM SOLO PERDIDA TOTAL" ->{
                coverage = "2"
                id = 2
            }
            "ROBO TOTAL" -> {
                coverage = "3"
                id = 3
            }

            "RESPONSABILIDAD CIVIL" -> {
                coverage = "4"
                id = 4
            }

            "GASTOS MEDICOS OCUPANTES" -> {
                coverage = "5"
                id = 5
            }

            "MUERTE AL CONDUCTOR X AA", "ACCIDENTES OCUPANTES", "MUERTE AL CONDUCTOR POR ACC. AUTOMOVILISTICO" -> {
                coverage = "6"
                id = 6
            }

            "GASTOS LEGALES" -> {
                coverage = "7"
                id = 7
            }
            "EQUIPO ESPECIAL " ->{
                coverage = "8"
                id = 8
            }
            "ADAPTACIONES DAÑOS MATERIALES" ->{
                coverage = "9"
                id = 9
            }
            "ADAPTACIONES ROBO TOTAL" ->{
                coverage = "10"
                id = 10
            }
            "EXTENCION DE RC" ->{
                coverage = "11"
                id = 11
            }
            "EXCENCION DE DEDUCIBLE" ->{
                coverage = "12"
                id = 12
            }
            "RC. DAÑOS A OCUPANTES" ->{
                coverage = "13"
                id = 13
            }
            "ASISTENCIA VIAL QUALITAS" -> {
                coverage = "14"
                id = 14
            }

            "ROBO PARCIAL" -> {
                coverage = "15"
                id = 15
            }
            "GASTOS DE TRANSPORTE"->{
                coverage = "17"
                id = 16
            }
            "RC ECOLOGICA" ->{
                coverage = "21"
                id = 17
            }
            "RC. LEGAL OCUPANTES" ->{
                coverage = "22"
                id = 18
            }
            "EXCENCION DE DEDUCIBLE POR VUECO O COLISION"->{
                coverage = "26"
                id = 19
            }
            "GASTOS X PERDIDA DE USO X PERDIDAS PARCIALES" ->{
                coverage = "28"
                id = 20
            }
            "DAÑOS POR LA CARGA" ->{
                coverage  = "31"
                id = 21
            }
            "EXCENCION DE DEDUCIBLE POR ROBO TOTAL"->{
                coverage = "40"
                id = 22
            }
            "AVERIA MECANICA Y/O ELECTRICA" ->{
                coverage = "41"
                id = 23
            }
            "RC COMPLEMENTARIA" ->{
                coverage = "47"
                id = 24
            }
            "ASISTENCIA VIAL AV PLUS" -> {
                coverage = "F"
                id = 25
            }
            else -> {

            }
        }
        //  MAPEO DE LAS COVERTURASY PONE EL FORMATO ESPECIFICO EN CASO DE QUE SE REQUIERA
        if (coverage == "F") {
            mappedCoverage[id] = "MDFG"
        } else {
            if (coverage == "15") {
                mappedCoverage[id]  = ("<Coberturas NoCobertura=\"${coverage}\">" +
                        "<SumaAsegurada>${description}||${
                            it.sumaAsegurada.replace(
                                "Amparada",
                                "0"
                            )
                        }</SumaAsegurada>" +
                        "<TipoSuma>0</TipoSuma>" +
                        "<Deducible>${it.deducible.replace("Amparada", "0")}</Deducible>" +
                        "<Prima>0</Prima>" +
                        "</Coberturas>"
                        )

            } else {
                mappedCoverage[id] = ("<Coberturas NoCobertura=\"${coverage}\">" +
                        "<SumaAsegurada>${it.sumaAsegurada.replace("Amparada", "0")}</SumaAsegurada>" +
                        "<TipoSuma>0</TipoSuma>" +
                        "<Deducible>${it.deducible.replace("Amparada", "0").replace(" UMAS", "")}</Deducible>" +
                        "<Prima>0</Prima>" +
                        "</Coberturas>"
                        )
            }
        }
    }


    if(mappedCoverage.containsKey(1)){
        coverges.append(mappedCoverage[1])
    }
    else{
        coverges.append("<Coberturas NoCobertura=\"1\">" +
                "<SumaAsegurada>0</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>5</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>")
    }

    if(mappedCoverage.containsKey(3)){
        coverges.append(mappedCoverage[3])
    }
    else{
        coverges.append("<Coberturas NoCobertura=\"3\">" +
                "<SumaAsegurada>0</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>10</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>")
    }

    if(mappedCoverage.containsKey(4)){
        coverges.append(mappedCoverage[4])
    }
    else{
        coverges.append("<Coberturas NoCobertura=\"4\">" +
                "<SumaAsegurada>200000</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>")
    }

    if(mappedCoverage.containsKey(5)){
        coverges.append(mappedCoverage[5])
    }
    else{
        coverges.append("<Coberturas NoCobertura=\"5\">" +
                "<SumaAsegurada>100000</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>")
    }

    if(mappedCoverage.containsKey(6)){
        coverges.append(mappedCoverage[6])
    }
    else{
        coverges.append("<Coberturas NoCobertura=\"6\">" +
                "<SumaAsegurada>0</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>")
    }

    if(mappedCoverage.containsKey(7)){
        coverges.append(mappedCoverage[7])
    }
    else{
        coverges.append("<Coberturas NoCobertura=\"7\">" +
                "<SumaAsegurada>0</SumaAsegurada>" +
                "<TipoSuma>0</TipoSuma>" +
                "<Deducible>0</Deducible>" +
                "<Prima>0</Prima>" +
                "</Coberturas>")
    }
    if (mappedCoverage.keys.contains(8))
        coverges.append(mappedCoverage[8].toString())
    if(mappedCoverage.containsKey(9)){
        coverges.append(
            "<ConsideracionesAdicionalesDV NoConsideracion=\"9\">" +
                    "<TipoRegla/>" +
                    "<ValorRegla>S</ValorRegla>" +
                    "</ConsideracionesAdicionalesDV>"
        )
    }
    if(mappedCoverage.containsKey(14)){
        coverges.append(mappedCoverage[14])
    }
    else{
        coverges.append(
            "<Coberturas NoCobertura=\"14\">" +
                    "<SumaAsegurada>0</SumaAsegurada>" +
                    "<TipoSuma>0</TipoSuma>" +
                    "<Deducible>0</Deducible>" +
                    "<Prima>0</Prima>" +
                    "</Coberturas>"
        )
    }
    return coverges.toString()
}

/***
 * METODO PARA MAPEAR LA COBERTURAS DE MANERA QUE SOLO SE AGREGEN LAS OBLIGATORIAS
 * Y TAMBIEN SE MODIFICAN LOS DEDUCIBLES Y SUMAS ASEGURADAS
 */
fun customCoverasges(coverageList: MutableList<RequestCoverage>, tipoSuma: String, valor: String, paquete: String, servicio: service): String{
    val xml = StringBuilder()
    val mappedCoverage: HashMap<String, String> = hashMapOf()
    coverageList.forEach {
        when(it.clave){
            //  MAPEO DE LAS COBERTURAS CON LAS REGLAS ESPECIFICAS Y FORMATOS DE CADA UNA
            "1"->{
                if(tipoSuma == "01"){
                    mappedCoverage[it.clave] =
                        "<Coberturas NoCobertura=\"${it.clave}\">" +
                                "<SumaAsegurada>${valor}</SumaAsegurada>" +
                                "<TipoSuma>${if (it.selected) "01" else "10"}</TipoSuma>" +
                                "<Deducible>${
                                    it.deducible.replace("AMPARADA", "0").replace(" UMAS", "").replace(" UMAS por pasajero", "")
                                }</Deducible>" +
                                "<Prima>0</Prima>" +
                                "</Coberturas>"
                }
                else{
                    mappedCoverage[it.clave] =
                        "<Coberturas NoCobertura=\"${it.clave}\">" +
                                "<SumaAsegurada>${it.sumaAsegurada}</SumaAsegurada>" +
                                "<TipoSuma>${if (it.selected) tipoSuma else "10"}</TipoSuma>" +
                                "<Deducible>${
                                    it.deducible.replace("AMPARADA", "0").replace(" UMAS", "").replace(" UMAS por pasajero", "")
                                }</Deducible>" +
                                "<Prima>0</Prima>" +
                                "</Coberturas>"
                }
            }
            "2" ->{
                if(it.selected) {
                    if (tipoSuma == "01") {
                        mappedCoverage[it.clave] =
                            "<Coberturas NoCobertura=\"${it.clave}\">" +
                                    "<SumaAsegurada>${valor}</SumaAsegurada>" +
                                    "<TipoSuma>${if (it.selected) "01" else "10"}</TipoSuma>" +
                                    "<Deducible>${
                                        it.deducible.replace("AMPARADA", "0").replace(" UMAS", "")
                                    }</Deducible>" +
                                    "<Prima>0</Prima>" +
                                    "</Coberturas>"
                    } else {
                        mappedCoverage[it.clave] =
                            "<Coberturas NoCobertura=\"${it.clave}\">" +
                                    "<SumaAsegurada>${it.sumaAsegurada}</SumaAsegurada>" +
                                    "<TipoSuma>${if (it.selected) tipoSuma else "10"}</TipoSuma>" +
                                    "<Deducible>${
                                        it.deducible.replace("AMPARADA", "0").replace(" UMAS", "")
                                            .replace(" UMAS por pasajero", "")
                                    }</Deducible>" +
                                    "<Prima>0</Prima>" +
                                    "</Coberturas>"
                    }
                }
            }
            "3" ->{
                if(tipoSuma == "01"){
                    mappedCoverage[it.clave] =
                        "<Coberturas NoCobertura=\"${it.clave}\">" +
                                "<SumaAsegurada>${valor}</SumaAsegurada>" +
                                "<TipoSuma>${if (it.selected) tipoSuma else "10"}</TipoSuma>" +
                                "<Deducible>${
                                    it.deducible.replace("AMPARADA", "0").replace(" UMAS", "").replace(" UMAS por pasajero", "")
                                }</Deducible>" +
                                "<Prima>0</Prima>" +
                                "</Coberturas>"
                }
                else{
                    mappedCoverage[it.clave] =
                        "<Coberturas NoCobertura=\"${it.clave}\">" +
                                "<SumaAsegurada>${it.sumaAsegurada}</SumaAsegurada>" +
                                "<TipoSuma>${if (it.selected) tipoSuma else "10"}</TipoSuma>" +
                                "<Deducible>${
                                    it.deducible.replace("AMPARADA", "0").replace(" UMAS", "").replace(" UMAS por pasajero", "")
                                }</Deducible>" +
                                "<Prima>0</Prima>" +
                                "</Coberturas>"
                }
            }
            "4" ->{
                mappedCoverage[it.clave] =
                    "<Coberturas NoCobertura=\"${it.clave}\">" +
                            "<SumaAsegurada>${it.sumaAsegurada}</SumaAsegurada>" +
                            "<TipoSuma>${if (it.selected) "0" else "10"}</TipoSuma>" +
                            "<Deducible>${
                                when(servicio){
                                    service.PARTICULAR -> "0"
                                    service.APP -> "50"
                                    service.APPPLUS -> "100"
                                    service.MOTOPARTICULAR -> it.deducible.substringBefore(" ")
                                    else -> "0"
                                }
                            }</Deducible>" +
                            "<Prima>0</Prima>" +
                            "</Coberturas>"
            }
            "5", "6", "7", "14"->{
                mappedCoverage[it.clave] =
                    "<Coberturas NoCobertura=\"${it.clave}\">" +
                            "<SumaAsegurada>${it.sumaAsegurada.replace("AMPARADA", "0")}</SumaAsegurada>" +
                            "<TipoSuma>${if (it.selected) "0" else "10"}</TipoSuma>" +
                            "<Deducible>${
                                it.deducible.replace("AMPARADA", "0").replace(" UMAS", "").replace(" UMAS por pasajero", "")
                            }</Deducible>" +
                            "<Prima>0</Prima>" +
                            "</Coberturas>"
            }
            "9" ->{
                if(it.selected){
                    if(paquete =="AMPLIA") {
                        mappedCoverage[it.clave] =
                            "<Coberturas NoCobertura=\"9\">" +
                                    "<SumaAsegurada>${it.descripcion}|${it.sumaAsegurada}</SumaAsegurada>" +
                                    "<TipoSuma>0</TipoSuma>" +
                                    "<Deducible>0</Deducible>" +
                                    "<Prima>0</Prima>" +
                                    "</Coberturas>"
                    }
                    else{
                        mappedCoverage[it.clave] =
                            "<Coberturas NoCobertura=\"10\">" +
                                    "<SumaAsegurada>${it.descripcion}|${it.sumaAsegurada}</SumaAsegurada>" +
                                    "<TipoSuma>0</TipoSuma>" +
                                    "<Deducible>0</Deducible>" +
                                    "<Prima>0</Prima>" +
                                    "</Coberturas>"
                    }
                }
            }
            "8" ->{
                if(it.selected) {
                    mappedCoverage[it.clave] =
                        "<Coberturas NoCobertura=\"${it.clave}\">" +
                                "<SumaAsegurada>${it.descripcion}|${it.sumaAsegurada}</SumaAsegurada>" +
                                "<TipoSuma>0</TipoSuma>" +
                                "<Deducible>${it.deducible}</Deducible>" +
                                "<Prima>0</Prima>" +
                                "</Coberturas>"
                }
            }
            "15" -> {
                mappedCoverage[it.clave] =
                    "<Coberturas NoCobertura=\"${it.clave}\">" +
                            "<SumaAsegurada>${it.descripcion}||1</SumaAsegurada>" +
                            "<TipoSuma>0</TipoSuma>" +
                            "<Deducible>${it.deducible}</Deducible>" +
                            "<Prima>0</Prima>" +
                            "</Coberturas>"
            }
            "26" ->{
                if(it.selected) {
                    if (tipoSuma == "01") {
                        mappedCoverage[it.clave] =
                            "<Coberturas NoCobertura=\"${it.clave}\">" +
                                    "<SumaAsegurada>${it.sumaAsegurada.replace("AMPARADA", "0")}</SumaAsegurada>" +
                                    "<TipoSuma>${tipoSuma}</TipoSuma>" +
                                    "<Deducible>${
                                        it.deducible.replace(" UMAS", "")
                                            .replace(" UMAS por pasajero", "")
                                    }</Deducible>" +
                                    "<Prima>0</Prima>" +
                                    "</Coberturas>"
                    }
                    else{
                        mappedCoverage[it.clave] =
                            "<Coberturas NoCobertura=\"${it.clave}\">" +
                                    "<SumaAsegurada>${it.sumaAsegurada}</SumaAsegurada>" +
                                    "<TipoSuma>${if (it.selected) tipoSuma else "10"}</TipoSuma>" +
                                    "<Deducible>${
                                        it.deducible.replace("AMPARADA", "0")
                                    }</Deducible>" +
                                    "<Prima>0</Prima>" +
                                    "</Coberturas>"
                    }
                }
            }
            "39" ->{
                mappedCoverage[it.clave] =
                    "<ConsideracionesAdicionalesDV NoConsideracion=\"39\">"+
                        "<TipoRegla>0</TipoRegla>"+
                        "<ValorRegla>N|S</ValorRegla>"+
                        "</ConsideracionesAdicionalesDV>"
            }
            "47" ->{
                if(it.selected){
                    mappedCoverage[it.clave] =
                        "<Coberturas NoCobertura=\"${it.clave}\">" +
                            "<SumaAsegurada>${it.sumaAsegurada.replace("AMPARADA", "0")}</SumaAsegurada>" +
                            "<TipoSuma>0</TipoSuma>" +
                            "<Deducible>${
                                it.deducible.replace("AMPARADA", "0").replace(" UMAS", "").replace(" UMAS por pasajero", "")
                            }</Deducible>" +
                            "<Prima>0</Prima>" +
                            "</Coberturas>"
                }
            }
            else ->{
                if(it.selected){
                    mappedCoverage[it.clave] =
                        "<Coberturas NoCobertura=\"${it.clave}\">" +
                                "<SumaAsegurada>${it.sumaAsegurada.replace("AMPARADA", "0")}</SumaAsegurada>" +
                                "<TipoSuma>0</TipoSuma>" +
                                "<Deducible>${
                                    it.deducible.substringBefore(" ")
                                }</Deducible>" +
                                "<Prima>0</Prima>" +
                                "</Coberturas>"
                }
            }
        }
    }
    //  PONE LAS COBERTURAS EN ORDEN, TAMBIEN AGREGA LAS OBLIGATORIAS DEPENDIENDO DEL PAQUETE Y
    //  TAMBIEN MANDA EL FORMATO ESPECIFICO PARA LAS COBERTURAS QUE NO SE QUIERAN AGREGAR Y VENGAN POR DEFAULT
    if(mappedCoverage.containsKey("1"))
        xml.append(mappedCoverage["1"])
    else if(paquete == "AMPLIA"){
        if(tipoSuma == "01"){
            xml.append(
                "<Coberturas NoCobertura=\"1\">" +
                        "<SumaAsegurada>${valor}</SumaAsegurada>" +
                        "<TipoSuma>01</TipoSuma>" +
                        "<Deducible>${if(servicio == service.MOTOPARTICULAR) 10 else 5}</Deducible>" +
                        "<Prima>0</Prima>" +
                        "</Coberturas>"
            )
        }
        else{
            xml.append(
                "<Coberturas NoCobertura=\"1\">" +
                        "<SumaAsegurada>0</SumaAsegurada>" +
                        "<TipoSuma>${tipoSuma}</TipoSuma>" +
                        "<Deducible>${if(servicio == service.MOTOPARTICULAR) 10 else 5}</Deducible>" +
                        "<Prima>0</Prima>" +
                        "</Coberturas>"
            )
        }
    }

    if(mappedCoverage.containsKey("2"))
        xml.append(mappedCoverage["2"])

    if(mappedCoverage.containsKey("3"))
        xml.append(mappedCoverage["3"])
    else if(paquete == "AMPLIA" || paquete == "LIMITADA"){
        if(tipoSuma == "01"){
            xml.append(
                "<Coberturas NoCobertura=\"3\">" +
                        "<SumaAsegurada>${valor}</SumaAsegurada>" +
                        "<TipoSuma>01</TipoSuma>" +
                        "<Deducible>${if(servicio == service.MOTOPARTICULAR) 20 else 10}</Deducible>" +
                        "<Prima>0</Prima>" +
                        "</Coberturas>"
            )
        }
        else{
            xml.append(
                "<Coberturas NoCobertura=\"3\">" +
                        "<SumaAsegurada>0</SumaAsegurada>" +
                        "<TipoSuma>${tipoSuma}</TipoSuma>" +
                        "<Deducible>${if(servicio == service.MOTOPARTICULAR) 20 else 10}</Deducible>" +
                        "<Prima>0</Prima>" +
                        "</Coberturas>"
            )
        }
    }

    if(mappedCoverage.containsKey("4"))
        xml.append(mappedCoverage["4"])

    if(mappedCoverage.containsKey("5"))
        xml.append(mappedCoverage["5"])
    else{
        xml.append("<Coberturas NoCobertura=\"5\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>10</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
    }

    if(mappedCoverage.containsKey("6"))
        xml.append(mappedCoverage["6"])
    else{
        xml.append("<Coberturas NoCobertura=\"6\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>10</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
    }

    if(mappedCoverage.containsKey("7"))
        xml.append(mappedCoverage["7"])
    else{
        xml.append("<Coberturas NoCobertura=\"7\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>10</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
    }

    if(mappedCoverage.containsKey("8"))
        xml.append(mappedCoverage["8"])

    if(mappedCoverage.containsKey("9"))
        xml.append(mappedCoverage["9"])

    if(mappedCoverage.containsKey("11"))
        xml.append(mappedCoverage["11"])

    if(mappedCoverage.containsKey("12"))
        xml.append(mappedCoverage["12"])

    if(mappedCoverage.containsKey("13"))
        xml.append(mappedCoverage["13"])

    if(mappedCoverage.containsKey("14"))
        xml.append(mappedCoverage["14"])
    else{
        if(mappedCoverage.containsKey("39")){
            xml.append("<Coberturas NoCobertura=\"14\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>0</TipoSuma>")
            xml.append("<Deducible>0</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        else {
            xml.append("<Coberturas NoCobertura=\"14\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>10</TipoSuma>")
            xml.append("<Deducible>0</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
    }

    if(mappedCoverage.containsKey("15"))
        xml.append(mappedCoverage["15"])

    if(mappedCoverage.containsKey("17"))
        xml.append(mappedCoverage["17"])

    if(mappedCoverage.containsKey("21"))
        xml.append(mappedCoverage["21"])

    if(mappedCoverage.containsKey("22"))
        xml.append(mappedCoverage["22"])

    if(mappedCoverage.containsKey("26"))
        xml.append(mappedCoverage["26"])

    if(mappedCoverage.containsKey("28"))
        xml.append(mappedCoverage["28"])

    if(mappedCoverage.containsKey("31"))
        xml.append(mappedCoverage["31"])

    if(mappedCoverage.containsKey("35"))
        xml.append(mappedCoverage["35"])

    if(mappedCoverage.containsKey("40"))
        xml.append(mappedCoverage["40"])

    if(mappedCoverage.containsKey("41"))
        xml.append(mappedCoverage["41"])

    if(mappedCoverage.containsKey("47"))
        xml.append(mappedCoverage["47"])

    if(mappedCoverage.containsKey("39"))
        xml.append(mappedCoverage["39"])

    return xml.toString()
}