package com.example.QualitasCore.extensions

import com.example.QualitasCore.businessRules.database.DirectionRepository
import com.example.QualitasCore.Repository.catalogos.Catalogos_Bancos_Repository
import com.example.QualitasCore.Repository.catalogos.Direcciones_Repository
import com.example.QualitasCore.models.Cobertura
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.TipoValor
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
/***
 * GENERACION DE XMLS CON LAS CREDENCIALES DE MIGO
 * POR NORMA COMERCIAL LAS EMISIONES NO PUEDEN SER A DIA CORRIENTE SI NO A DIA POSTERIOR
 * POR NORMA COMERCIAL PARA EL PAGO EL NUMERO DE NEGOCIO SIEMPRE SERA EL DE AHORRA
 * POR NORMA COMERCIAL HAY CIERTO CASOS QUE SE CONSIDERA COTIZACION DE ALTO RIESGO Y EN ESOS CASOS EL DEDUCIBLE
 * DE ROBO TOTAL AUMENTA LA 20%
 */
class XMLMigoService(
    @Autowired private val direction: DirectionRepository,
    @Autowired
    private val bancos : Catalogos_Bancos_Repository
) {
    @Value("\${migo.negocio_motos}")
    private val negocioMotos: String = ""
    @Value("\${migo.negocio_autos}")
    private val negocioAutos: String = ""
    @Value("\${migo.negocio_uber}")
    private val negocioUber: String = ""


    @Value("\${migo.agente_autos}")
    private val agenteAutos: String = ""
    @Value("\${migo.agente_motos}")
    private val agenteMotos: String = ""
    @Value("\${migo.agente_uber}")
    private val agenteUber: String = ""

    @Autowired
    lateinit var direccionesQualitas: Direcciones_Repository

    @Value("\${jwt.wtoken}")
    val wtoken: String = ""

    @Value("\${jwt.wpuid}")
    val wpuid: String = ""

    @Value("\${jwt.ambiente}")
    var ambiente: String = ""

    @Value("\${jwt.negocio}") private val negocioCobro: String =""

    fun quoteParticular(request: QuotationRequest, paymentFrecuency: String, paquete: String): String {
        var altoRiesgo = false
        var frequency = paymentFrecuency

        if (paymentFrecuency.contains("RIESGO")) {
            altoRiesgo = true
            frequency = paymentFrecuency.replace("RIESGO", "")
        }

        val direcciones = direccionesQualitas.getDireccionesByCPostal(request.cp)
        val xml = StringBuilder()
        xml.append("<Movimientos>")
        xml.append("<Movimiento TipoMovimiento=\"${2}\" NoPoliza=\"\" NoCotizacion=\"\" NoEndoso=\"\" TipoEndoso=\"\" NoOTra=\"\" NoNegocio=\"${negocioAutos}\">")
        xml.append("<DatosAsegurado NoAsegurado=\"\">")
        xml.append("<Nombre/>")
        xml.append("<Direccion/>")
        xml.append("<Colonia/>")
        xml.append("<Poblacion/>")
        xml.append("<Estado>${direcciones[0].IDEdo_Qua}</Estado>")
        xml.append("<CodigoPostal>${request.cp}</CodigoPostal>")
        xml.append("<NoEmpleado/>")
        xml.append("<Agrupador/>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla></ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>1</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla>MEXICO</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>NOMBRE2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>5</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOP2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>6</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOM2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("</DatosAsegurado>")
        xml.append("<DatosVehiculo NoInciso=\"1\">")
        xml.append("<ClaveAmis>${request.clave.substringBefore("-")}</ClaveAmis>")
        xml.append("<Modelo>${request.modelo}</Modelo>")
        xml.append("<DescripcionVehiculo>${request.descripcion}</DescripcionVehiculo>")
        xml.append("<Uso>01</Uso>") // 01 regresar
        xml.append("<Servicio>01</Servicio>")
        xml.append("<Paquete>${packageSelection(paquete)}</Paquete>") // 01 Amplio , 03 Limitado , 04 RC
        xml.append("<Motor/>")
        xml.append("<Serie/>")
        if(request.tipoValor != TipoValor.VALORFACTURA) {
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>5</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>${if (altoRiesgo) 20 else 10}</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        else{
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>5</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>${if (altoRiesgo) 20 else 10}</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        xml.append("<Coberturas NoCobertura=\"4\">")
        xml.append("<SumaAsegurada>3000000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"5\">")
        xml.append("<SumaAsegurada>200000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"6\">")
        xml.append("<SumaAsegurada>100000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"7\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"14\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("</DatosVehiculo>")
        xml.append("<DatosGenerales>")
        xml.append("<FechaEmision>${LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)}</FechaEmision>")
        xml.append("<FechaInicio>${LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)}</FechaInicio>")
        xml.append(
            "<FechaTermino>${
                LocalDateTime.now().plusYears(1).format(DateTimeFormatter.ISO_DATE)
            }</FechaTermino>"
        )
        xml.append("<Moneda>0</Moneda>")
        xml.append("<Agente>${agenteAutos}</Agente>")
        xml.append("<FormaPago>${collectionType(frequency)}</FormaPago>") //forma de pago C anual, M mensual, S semestral
        xml.append("<TarifaValores>LINEA</TarifaValores>")
        xml.append("<TarifaCuotas>LINEA</TarifaCuotas>")
        xml.append("<TarifaDerechos>LINEA</TarifaDerechos>")
        xml.append("<Plazo/>")
        xml.append("<Agencia/>")
        xml.append("<Contrato/>")
        xml.append("<PorcentajeDescuento>${request.descuento}</PorcentajeDescuento>") // Descuento variable
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"01\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${digitValidation(request.clave)}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"04\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${ambiente}</ValorRegla>") // 1 - Pruebas 0 - Produccion
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"05\">")
        xml.append("<TipoRegla>1</TipoRegla>")//    CONSIDERACION PARA DESCUENTO POR PRONTO PAGO
        xml.append("<ValorRegla>${7}</ValorRegla>") // NUMERO DE DIAS PARA PAGAR
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("</DatosGenerales>")
        xml.append("<Primas>")
        xml.append("<PrimaNeta/>")
        xml.append("<Derecho>600</Derecho>")
        xml.append("<Recargo/>")
        xml.append("<Impuesto/>")
        xml.append("<PrimaTotal/>")
        xml.append("<Comision/>")
        xml.append("</Primas>")
        xml.append("<CodigoError/>")
        xml.append("</Movimiento>")
        xml.append("</Movimientos>")
        return xml.toString()
    }

    fun quotationUber(request: QuotationRequest, paymentFrecuency: String, paquete: String): String {
        var altoRiesgo = false
        var frequency = paymentFrecuency
        if (paymentFrecuency.contains("RIESGO")) {
            altoRiesgo = true
            frequency = paymentFrecuency.replace("RIESGO", "")
        }

        val direcciones = direccionesQualitas.getDireccionesByCPostal(request.cp)
        val xml = StringBuilder()
        xml.append("<Movimientos>")
        xml.append("<Movimiento TipoMovimiento=\"${2}\" NoPoliza=\"\" NoCotizacion=\"\" NoEndoso=\"\" TipoEndoso=\"\" NoOTra=\"\" NoNegocio=\"${negocioUber}\">")
        xml.append("<DatosAsegurado NoAsegurado=\"\">")
        xml.append("<Nombre/>")
        xml.append("<Direccion/>")
        xml.append("<Colonia/>")
        xml.append("<Poblacion/>")
        xml.append("<Estado>${direcciones[0].IDEdo_Qua}</Estado>")
        xml.append("<CodigoPostal>${request.cp}</CodigoPostal>")
        xml.append("<NoEmpleado/>")
        xml.append("<Agrupador/>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla></ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>1</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla>MEXICO</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>NOMBRE2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>5</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOP2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>6</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOM2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("</DatosAsegurado>")
        xml.append("<DatosVehiculo NoInciso=\"1\">")
        xml.append("<ClaveAmis>${request.clave.substringBefore("-")}</ClaveAmis>")
        xml.append("<Modelo>${request.modelo}</Modelo>")
        xml.append("<DescripcionVehiculo>${request.descripcion}</DescripcionVehiculo>")
        xml.append("<Uso>40</Uso>") // 01 regresar
        xml.append("<Servicio>01</Servicio>")
        xml.append("<Paquete>${packageSelection(paquete)}</Paquete>") // 01 Amplio , 03 Limitado , 04 RC
        xml.append("<Motor/>")
        xml.append("<Serie/>")
        if(request.tipoValor != TipoValor.VALORFACTURA) {
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>10</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>${if (altoRiesgo) 20 else 10}</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        else{
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>10</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>${if (altoRiesgo) 20 else 10}</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        xml.append("<Coberturas NoCobertura=\"4\">")
        xml.append("<SumaAsegurada>3000000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>50</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"5\">")
        xml.append("<SumaAsegurada>200000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"6\">")
        xml.append("<SumaAsegurada>100000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"7\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"14\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>") // 384.88
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"22\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>") // 384.88
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")

        xml.append("</DatosVehiculo>")
        xml.append("<DatosGenerales>")
        xml.append("<FechaEmision>${LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)}</FechaEmision>")
        xml.append("<FechaInicio>${LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)}</FechaInicio>")
        xml.append(
            "<FechaTermino>${
                LocalDateTime.now().plusYears(1).format(DateTimeFormatter.ISO_DATE)
            }</FechaTermino>"
        )
        xml.append("<Moneda>0</Moneda>")
        xml.append("<Agente>${agenteUber}</Agente>")
        xml.append("<FormaPago>${collectionType(frequency)}</FormaPago>") //forma de pago C anual, M mensual, S semestral
        xml.append("<TarifaValores>LINEA</TarifaValores>")
        xml.append("<TarifaCuotas>LINEA</TarifaCuotas>")
        xml.append("<TarifaDerechos>LINEA</TarifaDerechos>")
        xml.append("<Plazo/>")
        xml.append("<Agencia/>")
        xml.append("<Contrato/>")
        xml.append("<PorcentajeDescuento>${request.descuento}</PorcentajeDescuento>") // Descuento variable
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"01\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${digitValidation(request.clave)}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"04\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${ambiente}</ValorRegla>") // 1 - Pruebas 0 - Produccion
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"05\">")
        xml.append("<TipoRegla>1</TipoRegla>")//    CONSIDERACION PARA DESCUENTO POR PRONTO PAGO
        xml.append("<ValorRegla>${7}</ValorRegla>") // NUMERO DE DIAS PARA PAGAR
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("</DatosGenerales>")
        xml.append("<Primas>")
        xml.append("<PrimaNeta/>")
        xml.append("<Derecho>600</Derecho>")
        xml.append("<Recargo/>")
        xml.append("<Impuesto/>")
        xml.append("<PrimaTotal/>")
        xml.append("<Comision/>")
        xml.append("</Primas>")
        xml.append("<CodigoError/>")
        xml.append("</Movimiento>")
        xml.append("</Movimientos>")
        return xml.toString()
    }

    fun quotationUberPlus(request: QuotationRequest, paymentFrecuency: String, paquete: String): String {
        var frequency = paymentFrecuency

        val direcciones = direccionesQualitas.getDireccionesByCPostal(request.cp)
        val xml = StringBuilder()
        xml.append("<Movimientos>")
        xml.append("<Movimiento TipoMovimiento=\"${2}\" NoPoliza=\"\" NoCotizacion=\"\" NoEndoso=\"\" TipoEndoso=\"\" NoOTra=\"\" NoNegocio=\"$negocioUber\">")
        xml.append("<DatosAsegurado NoAsegurado=\"\">")
        xml.append("<Nombre/>")
        xml.append("<Direccion/>")
        xml.append("<Colonia/>")
        xml.append("<Poblacion/>")
        xml.append("<Estado>${direcciones[0].IDEdo_Qua}</Estado>")
        xml.append("<CodigoPostal>${request.cp}</CodigoPostal>")
        xml.append("<NoEmpleado/>")
        xml.append("<Agrupador/>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla></ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>1</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla>MEXICO</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>NOMBRE2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>5</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOP2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>6</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOM2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("</DatosAsegurado>")
        xml.append("<DatosVehiculo NoInciso=\"1\">")
        xml.append("<ClaveAmis>${request.clave.substringBefore("-")}</ClaveAmis>")
        xml.append("<Modelo>${request.modelo}</Modelo>")
        xml.append("<DescripcionVehiculo>${request.descripcion}</DescripcionVehiculo>")
        xml.append("<Uso>39</Uso>") // 01 regresar
        xml.append("<Servicio>01</Servicio>")
        xml.append("<Paquete>${packageSelection(paquete)}</Paquete>") // 01 Amplio , 03 Limitado , 04 RC
        xml.append("<Motor/>")
        xml.append("<Serie/>")
        if(request.tipoValor != TipoValor.VALORFACTURA) {
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>15</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>20</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        else{
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>15</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>20</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        xml.append("<Coberturas NoCobertura=\"4\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>100</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"5\">")
        xml.append("<SumaAsegurada>200000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"6\">")
        xml.append("<SumaAsegurada>100000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"7\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"14\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>") // 384.88
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"22\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>") // 384.88
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")


        xml.append("</DatosVehiculo>")
        xml.append("<DatosGenerales>")
        xml.append("<FechaEmision>${LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)}</FechaEmision>")
        xml.append("<FechaInicio>${LocalDateTime.now().format(DateTimeFormatter.ISO_DATE)}</FechaInicio>")
        xml.append(
            "<FechaTermino>${
                LocalDateTime.now().plusYears(1).format(DateTimeFormatter.ISO_DATE)
            }</FechaTermino>"
        )
        xml.append("<Moneda>0</Moneda>")
        xml.append("<Agente>$agenteUber</Agente>")
        xml.append("<FormaPago>${collectionType(frequency)}</FormaPago>") //forma de pago C anual, M mensual, S semestral
        xml.append("<TarifaValores>LINEA</TarifaValores>")
        xml.append("<TarifaCuotas>LINEA</TarifaCuotas>")
        xml.append("<TarifaDerechos>LINEA</TarifaDerechos>")
        xml.append("<Plazo/>")
        xml.append("<Agencia/>")
        xml.append("<Contrato/>")
        xml.append("<PorcentajeDescuento>${request.descuento}</PorcentajeDescuento>") // Descuento variable
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"01\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${digitValidation(request.clave)}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"04\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${ambiente}</ValorRegla>") // 1 - Pruebas 0 - Produccion
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"05\">")
        xml.append("<TipoRegla>1</TipoRegla>")//    CONSIDERACION PARA DESCUENTO POR PRONTO PAGO
        xml.append("<ValorRegla>${7}</ValorRegla>") // NUMERO DE DIAS PARA PAGAR
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("</DatosGenerales>")
        xml.append("<Primas>")
        xml.append("<PrimaNeta/>")
        xml.append("<Derecho>600</Derecho>")
        xml.append("<Recargo/>")
        xml.append("<Impuesto/>")
        xml.append("<PrimaTotal/>")
        xml.append("<Comision/>")
        xml.append("</Primas>")
        xml.append("<CodigoError/>")
        xml.append("</Movimiento>")
        xml.append("</Movimientos>")
        return xml.toString()
    }

    fun issueAutos(responseWs: GeneralResponse): String {
        responseWs.cliente.fechaNacimiento = dateReformat(responseWs.cliente.fechaNacimiento)
        val nombreCompleto =
            responseWs.cliente.nombre + " " + responseWs.cliente.apellidoPat + " " + responseWs.cliente.apellidoMat
        val consultaDireccion =
            direction.getDirections(responseWs.cliente.direccion.codPostal).colony(responseWs.cliente.direccion.colonia)
        val divFin = responseWs.emision.inicioVigencia.split("-")

        val uso: String
        val negocio: String
        val agente: String

        when (responseWs.vehiculo.servicio.service) {
            "PARTICULAR" -> {
                uso = "01"
                negocio = negocioAutos
                agente = agenteAutos
            }

            "APP" -> {
                uso = "40"
                negocio = negocioUber
                agente = agenteUber
            }

            "APPPLUS" -> {
                uso = "39"
                negocio = negocioUber
                agente = agenteUber
            }

            "TAXI" -> {
                uso = "08"
                negocio = negocioAutos
                agente = agenteAutos
            }

            "CARGA" -> {
                uso = "06"
                negocio = negocioAutos
                agente = agenteAutos
            }

            else -> {
                uso = "01"
                negocio = negocioAutos
                agente = agenteAutos
            }
        }

        val servicio = if (responseWs.vehiculo.servicio.service == "TAXI") "02" else "01"

        val xml = StringBuilder()
        xml.append("<Movimientos>")
        xml.append("<Movimiento NoNegocio=\"$negocio\" NoOTra=\"\" TipoEndoso=\"\" NoEndoso=\"\" NoCotizacion=\"${responseWs.cotizacion.idCotizacion}\" NoPoliza=\"\" TipoMovimiento=\"3\">")
        xml.append("<DatosAsegurado NoAsegurado=\"\">")
        //  VALIDA SI EL CLIENTE ES MORAL O FISICO
        if(responseWs.cliente.tipoPersona != "M") {
            xml.append("<Nombre>${nombreCompleto}</Nombre>") //cambiar a nombre dinamico
            xml.append("<Direccion>${responseWs.cliente.direccion.calle}</Direccion>")
            xml.append("<Colonia>${responseWs.cliente.direccion.colonia}</Colonia>")
            xml.append("<Poblacion>${consultaDireccion[0].D_mnpio}</Poblacion>")
            xml.append("<Estado>${consultaDireccion[0].c_estado}</Estado>")
            xml.append("<CodigoPostal>${consultaDireccion[0].d_codigo}</CodigoPostal>")
            xml.append("<NoEmpleado/>")
            xml.append("<Agrupador/>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>1</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.direccion.noExt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>2</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.direccion.noInt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>3</TipoRegla>")
            xml.append("<ValorRegla>MEXICO</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>4</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.nombre}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>5</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.apellidoPat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>6</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.apellidoMat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>7</TipoRegla>")
            xml.append("<ValorRegla>${consultaDireccion[0].c_mnpio}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>8</TipoRegla>")
            xml.append("<ValorRegla>${consultaDireccion[0].id_asenta_cpcons}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>19</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>20</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.fechaNacimiento}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>21</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>23</TipoRegla>")
            xml.append("<ValorRegla>Empleado</ValorRegla>") //se agrega ocupacion
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>24</TipoRegla>")
            xml.append("<ValorRegla>Sector</ValorRegla>") // se agrega sector
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>25</TipoRegla>")
            xml.append("<ValorRegla>Empleado</ValorRegla>") // se agrega la profesion del asegurado
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>28</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.rfc}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>56</TipoRegla>")
            xml.append("<ValorRegla>${genderAssigment(responseWs.cliente.genero)}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>26</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.email}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>70</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.telefono}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
        }
        else{
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>1</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.direccion.noExt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>2</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.direccion.noInt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>4</TipoRegla>")
            xml.append("<ValorRegla>${nombreCompleto}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>7</TipoRegla>")
            xml.append("<ValorRegla>${consultaDireccion[0].c_mnpio}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>8</TipoRegla>")
            xml.append("<ValorRegla>${consultaDireccion[0].id_asenta_cpcons}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>19</TipoRegla>")
            xml.append("<ValorRegla>2</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>24</TipoRegla>")
            xml.append("<ValorRegla>10</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>28</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.rfc}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>29</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>31</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.email}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>32</TipoRegla>")
            xml.append("<ValorRegla>${dateReformat(responseWs.cliente.fechaNacimiento)}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>34</TipoRegla>")
            xml.append("<ValorRegla>Rodolfo Juarez Vazquez</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>35</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>57</TipoRegla>")
            xml.append("<ValorRegla>N</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>69</TipoRegla>")
            xml.append("<ValorRegla>N</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>59</TipoRegla>")
            xml.append("<ValorRegla>N</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>61</TipoRegla>")
            xml.append("<ValorRegla></ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>62</TipoRegla>")
            xml.append("<ValorRegla>INE</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>63</TipoRegla>")
            xml.append("<ValorRegla>1234567890</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>65</TipoRegla>")
            xml.append("<ValorRegla>07/08/2001</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>66</TipoRegla>")
            xml.append("<ValorRegla>DF</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>67</TipoRegla>")
            xml.append("<ValorRegla>MEXICO</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>68</TipoRegla>")
            xml.append("<ValorRegla>jjuarez@ahorra.io</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>70</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.telefono}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>72</TipoRegla>")
            xml.append("<ValorRegla>N-20170621148</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
        }
        //  VALIDA SI SE TIENE CONTRATANTE
        if(responseWs.contratante.nombre != "") {
            //  AGINACION DE LOS DATOS DEL CONTRATANTE
            val direccion = direction.getDirections(responseWs.contratante.direccion.codPostal).colony(responseWs.contratante.direccion.colonia)
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>11</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.direccion.codPostal}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>12</TipoRegla>")
            xml.append("<ValorRegla>${direccion[0].c_estado}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>13</TipoRegla>")
            xml.append("<ValorRegla>${direccion[0].c_mnpio}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>14</TipoRegla>")
            xml.append("<ValorRegla>${direccion[0].id_asenta_cpcons}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>15</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.telefono}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>16</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.direccion.calle}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>17</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.direccion.noExt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>18</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.direccion.noInt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>36</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.nombre}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>37</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.apellidoPat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>38</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.apellidoMat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>39</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>40</TipoRegla>")
            xml.append("<ValorRegla>${ dateReformat(responseWs.contratante.fechaNacimiento) }</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>41</TipoRegla>")
            xml.append("<ValorRegla>${1}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>43</TipoRegla>")
            xml.append("<ValorRegla>${1}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>44</TipoRegla>")
            xml.append("<ValorRegla>${10}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>45</TipoRegla>")
            xml.append("<ValorRegla>${1}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>46</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.email}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>47</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.curp}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>48</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.rfc}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>73</TipoRegla>")
            xml.append("<ValorRegla>${genderAssigment(responseWs.contratante.genero)}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>74</TipoRegla>")
            xml.append("<ValorRegla>${"001"}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
        }

        xml.append("</DatosAsegurado>")
        xml.append("<DatosVehiculo NoInciso=\"1\">")
        xml.append("<ClaveAmis>${responseWs.vehiculo.clave}</ClaveAmis>")
        xml.append("<Modelo>${responseWs.vehiculo.modelo}</Modelo>")
        xml.append("<DescripcionVehiculo>${responseWs.vehiculo.descripcion}</DescripcionVehiculo>")
        xml.append("<Uso>$uso</Uso>") // 01 regresr
        xml.append("<Servicio>$servicio</Servicio>")
        xml.append("<Paquete>${packageSelection(responseWs.paquete)}</Paquete>")
        xml.append("<Motor>${responseWs.vehiculo.noMotor}</Motor>")
        xml.append("<Serie>${responseWs.vehiculo.noSerie}</Serie>")

        //  EN EL CASO DE LA EMISION NO IMPORTA QUE COBERTURAS SE ENVIEN SE TOMAN LAS DE LA COTIZACION
        xml.append("<Coberturas NoCobertura=\"1\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>5</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"3\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>10</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"4\">")
        xml.append("<SumaAsegurada>3000000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"5\">")
        xml.append("<SumaAsegurada>200000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"6\">")
        xml.append("<SumaAsegurada>100000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"7\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"14\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("</DatosVehiculo>")
        xml.append("<DatosGenerales>")
        //  VALIDA SI LA EMISION ES POSTFECHADA O NO
        xml.append(
            "<FechaEmision>${
                (if (responseWs.emision.inicioVigencia == "" || responseWs.emision.inicioVigencia == LocalDateTime.now()
                        .format(
                            DateTimeFormatter.ISO_DATE
                        )
                ) LocalDateTime.now().plusDays(1)
                    .format(DateTimeFormatter.ISO_DATE) else responseWs.emision.inicioVigencia)
            }</FechaEmision>"
        )
        xml.append(
            "<FechaInicio>${
                (if (responseWs.emision.inicioVigencia == "" || responseWs.emision.inicioVigencia == LocalDateTime.now()
                        .format(
                            DateTimeFormatter.ISO_DATE
                        )
                ) LocalDateTime.now().plusDays(1)
                    .format(DateTimeFormatter.ISO_DATE) else responseWs.emision.inicioVigencia)
            }</FechaInicio>"
        )
        xml.append(
            "<FechaTermino>${
                (if (responseWs.emision.inicioVigencia == "" || responseWs.emision.inicioVigencia == LocalDateTime.now()
                        .format(
                            DateTimeFormatter.ISO_DATE
                        )
                ) LocalDateTime.now().plusYears(1).plusDays(1)
                    .format(DateTimeFormatter.ISO_DATE) else ((divFin[0].toInt() + 1).toString() + "-" + divFin[1] + "-" + divFin[2]))
            }</FechaTermino>"
        )
        xml.append("<Moneda>0</Moneda>")
        xml.append("<Agente>$agente</Agente>")
        xml.append("<FormaPago>${collectionType(responseWs.periodicidadDePago)}</FormaPago>")
        xml.append("<TarifaValores>LINEA</TarifaValores>")
        xml.append("<TarifaCuotas>LINEA</TarifaCuotas>")
        xml.append("<TarifaDerechos>LINEA</TarifaDerechos>")
        xml.append("<Plazo/>")
        xml.append("<Agencia/>")
        xml.append("<Contrato/>")
        xml.append("<PorcentajeDescuento>${responseWs.descuento}</PorcentajeDescuento>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"01\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${digitValidation(responseWs.vehiculo.clave)}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"04\">")
        xml.append("<TipoRegla>0</TipoRegla>")
        xml.append("<ValorRegla>${ambiente}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"05\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>7</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        if (responseWs.conductorHabitual.nombre != "") { //  VERIFICA SI LA POLIZA TIENE CONDUCTOR HABITUAL
            xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"10\">") //  CONSIDERACION PARA PONER EL CONDUCTOR HABITUAL EN LA POLIZA
            xml.append("<TipoRegla>82</TipoRegla>")
            //  NOMBRE DEL CONDUCTOR HABITUAL
            //  NOTA: EL NOMBRE NO DEBE IR EN UN FORMATO ESPECIFICO
            xml.append("<ValorRegla>${responseWs.conductorHabitual.nombre} ${responseWs.conductorHabitual.apellidoPat} ${responseWs.conductorHabitual.apellidoMat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDG>")
        }
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"16\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${responseWs.vehiculo.noPlacas}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"10\">")
        xml.append("<TipoRegla>16</TipoRegla>")
        xml.append("<ValorRegla>${responseWs.vehiculo.noPlacas}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("</DatosGenerales>")
        xml.append("<Primas>")
        xml.append("<PrimaNeta/>")
        xml.append("<Derecho>600</Derecho>")
        xml.append("<Recargo/>")
        xml.append("<Impuesto/>")
        xml.append("<PrimaTotal/>")
        xml.append("<Comision/>")
        xml.append("</Primas>")
        xml.append("<CodigoError/>")
        xml.append("</Movimiento>")
        xml.append("</Movimientos>")
        println("XML_EMISION_MIGO: $xml")
        return xml.toString()
    }

    fun quotationMotosRepartidor(request: QuotationRequest, paymentFrequency: String, paquete: String): String {
        val directions = direction.getDirections(request.cp)

        val xml = StringBuilder()
        xml.append("<Movimientos>")
        xml.append("<Movimiento TipoMovimiento=\"2\" NoPoliza=\"\" NoCotizacion=\"\" NoEndoso=\"\" TipoEndoso=\"\" NoOTra=\"\" NoNegocio=\"$negocioMotos\">")
        xml.append("<DatosAsegurado NoAsegurado=\"\">")
        xml.append("<Nombre/>")
        xml.append("<Direccion/>")
        xml.append("<Colonia/>")
        xml.append("<Poblacion/>")
        xml.append("<Estado>${directions[0].c_estado}</Estado>")
        xml.append("<CodigoPostal>${request.cp}</CodigoPostal>")
        xml.append("<NoEmpleado/>")
        xml.append("<Agrupador/>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla></ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>1</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla>MEXICO</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>NOMBRE2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>5</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOP2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>6</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOM2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("</DatosAsegurado>")
        xml.append("<DatosVehiculo NoInciso=\"1\">")
        xml.append("<ClaveAmis>${request.clave.substringBefore("-")}</ClaveAmis>")
        xml.append("<Modelo>${request.modelo}</Modelo>")
        xml.append("<DescripcionVehiculo>${request.descripcion}</DescripcionVehiculo>")
        xml.append("<Uso>81</Uso>")
        xml.append("<Servicio>01</Servicio>")
        xml.append("<Paquete>${packageSelection(paquete)}</Paquete>") // 01 Amplio , 03 Limitado
        xml.append("<Motor/>")
        xml.append("<Serie/>")
        if(request.tipoValor != TipoValor.VALORFACTURA) {
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>10</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>20</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        else{
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>10</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>20</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        xml.append("<Coberturas NoCobertura=\"4\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"5 \">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>10</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"7\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"14\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("</DatosVehiculo>")
        xml.append("<DatosGenerales>")
        xml.append("<FechaEmision>${LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ISO_DATE)}</FechaEmision>")
        xml.append("<FechaInicio>${LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ISO_DATE)}</FechaInicio>")
        xml.append(
            "<FechaTermino>${
                LocalDateTime.now().plusYears(1).plusDays(1).format(DateTimeFormatter.ISO_DATE)
            }</FechaTermino>"
        )
        xml.append("<Moneda>0</Moneda>")
        xml.append("<Agente>$agenteMotos</Agente>")
        xml.append("<FormaPago>${collectionType(paymentFrequency)}</FormaPago>")
        xml.append("<TarifaValores>LINEA</TarifaValores>")
        xml.append("<TarifaCuotas>LINEA</TarifaCuotas>")
        xml.append("<TarifaDerechos>LINEA</TarifaDerechos>")
        xml.append("<Plazo/>")
        xml.append("<Agencia/>")
        xml.append("<Contrato/>")
        xml.append("<PorcentajeDescuento>${request.descuento}</PorcentajeDescuento>") // Descuento variable
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"01\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${digitValidation(request.clave)}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"04\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>$ambiente</ValorRegla>") // 1 - Pruebas 0 - Produccion
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"05\">")
        xml.append("<TipoRegla>1</TipoRegla>")//    CONSIDERACION PARA DESCUENTO POR PRONTO PAGO
        xml.append("<ValorRegla>${7}</ValorRegla>") // NUMERO DE DIAS PARA PAGAR
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("</DatosGenerales>")
        xml.append("<Primas>")
        xml.append("<PrimaNeta/>")
        xml.append("<Derecho>600</Derecho>")
        xml.append("<Recargo/>")
        xml.append("<Impuesto/>")
        xml.append("<PrimaTotal/>")
        xml.append("<Comision/>")
        xml.append("</Primas>")
        xml.append("<CodigoError/>")
        xml.append("</Movimiento>")
        xml.append("</Movimientos>")
        return xml.toString()
    }

    fun quotationMotos(request: QuotationRequest, paymentFrequency: String, paquete: String): String {
        val directions = direction.getDirections(request.cp)

        val xml = StringBuilder()
        xml.append("<Movimientos>")
        xml.append("<Movimiento TipoMovimiento=\"2\" NoPoliza=\"\" NoCotizacion=\"\" NoEndoso=\"\" TipoEndoso=\"\" NoOTra=\"\" NoNegocio=\"$negocioMotos\">")
        xml.append("<DatosAsegurado NoAsegurado=\"\">")
        xml.append("<Nombre/>")
        xml.append("<Direccion/>")
        xml.append("<Colonia/>")
        xml.append("<Poblacion/>")
        xml.append("<Estado>${directions[0].c_estado}</Estado>")
        xml.append("<CodigoPostal>${request.cp}</CodigoPostal>")
        xml.append("<NoEmpleado/>")
        xml.append("<Agrupador/>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla></ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>1</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>3</TipoRegla>")
        xml.append("<ValorRegla>MEXICO</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>4</TipoRegla>")
        xml.append("<ValorRegla>NOMBRE2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>5</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOP2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
        xml.append("<TipoRegla>6</TipoRegla>")
        xml.append("<ValorRegla>APELLIDOM2</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDA>")
        xml.append("</DatosAsegurado>")
        xml.append("<DatosVehiculo NoInciso=\"1\">")
        xml.append("<ClaveAmis>${request.clave.substringBefore("-")}</ClaveAmis>")
        xml.append("<Modelo>${request.modelo}</Modelo>")
        xml.append("<DescripcionVehiculo>${request.descripcion}</DescripcionVehiculo>")
        xml.append("<Uso>15</Uso>")
        xml.append("<Servicio>01</Servicio>")
        xml.append("<Paquete>${packageSelection(paquete)}</Paquete>") // 01 Amplio , 03 Limitado
        xml.append("<Motor/>")
        xml.append("<Serie/>")
        if(request.tipoValor != TipoValor.VALORFACTURA) {
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>10</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>0</SumaAsegurada>")
            xml.append("<TipoSuma>${request.tipoValor.tipoValor}</TipoSuma>")
            xml.append("<Deducible>20</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        else{
            xml.append("<Coberturas NoCobertura=\"1\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>10</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
            xml.append("<Coberturas NoCobertura=\"3\">")
            xml.append("<SumaAsegurada>${request.clave.substringAfter("-")}</SumaAsegurada>")
            xml.append("<TipoSuma>01</TipoSuma>")
            xml.append("<Deducible>20</Deducible>")
            xml.append("<Prima>0</Prima>")
            xml.append("</Coberturas>")
        }
        xml.append("<Coberturas NoCobertura=\"4\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>25</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"5 \">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"7\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"14\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>10</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("</DatosVehiculo>")
        xml.append("<DatosGenerales>")
        xml.append("<FechaEmision>${LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ISO_DATE)}</FechaEmision>")
        xml.append("<FechaInicio>${LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ISO_DATE)}</FechaInicio>")
        xml.append(
            "<FechaTermino>${
                LocalDateTime.now().plusYears(1).plusDays(1).format(DateTimeFormatter.ISO_DATE)
            }</FechaTermino>"
        )
        xml.append("<Moneda>0</Moneda>")
        xml.append("<Agente>$agenteMotos</Agente>")
        xml.append("<FormaPago>${collectionType(paymentFrequency)}</FormaPago>")
        xml.append("<TarifaValores>LINEA</TarifaValores>")
        xml.append("<TarifaCuotas>LINEA</TarifaCuotas>")
        xml.append("<TarifaDerechos>LINEA</TarifaDerechos>")
        xml.append("<Plazo/>")
        xml.append("<Agencia/>")
        xml.append("<Contrato/>")
        xml.append("<PorcentajeDescuento>${request.descuento}</PorcentajeDescuento>") // Descuento variable
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"01\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${digitValidation(request.clave)}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"04\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>$ambiente</ValorRegla>") // 1 - Pruebas 0 - Produccion
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"05\">")
        xml.append("<TipoRegla>1</TipoRegla>")//    CONSIDERACION PARA DESCUENTO POR PRONTO PAGO
        xml.append("<ValorRegla>${7}</ValorRegla>") // NUMERO DE DIAS PARA PAGAR
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("</DatosGenerales>")
        xml.append("<Primas>")
        xml.append("<PrimaNeta/>")
        xml.append("<Derecho>520</Derecho>")
        xml.append("<Recargo/>")
        xml.append("<Impuesto/>")
        xml.append("<PrimaTotal/>")
        xml.append("<Comision/>")
        xml.append("</Primas>")
        xml.append("<CodigoError/>")
        xml.append("</Movimiento>")
        xml.append("</Movimientos>")
        return xml.toString()
    }

    fun issueMotos(responseWs: GeneralResponse): String {
        val nombreCompleto =
            responseWs.cliente.nombre + " " + responseWs.cliente.apellidoPat + " " + responseWs.cliente.apellidoMat
        val consultaDireccion =
            direction.getDirections(responseWs.cliente.direccion.codPostal).colony(responseWs.cliente.direccion.colonia)
        val divFin = responseWs.emision.inicioVigencia.split("-")

        val xml = StringBuilder()
        xml.append("<Movimientos>")
        xml.append("<Movimiento NoNegocio=\"$negocioMotos\" NoOTra=\"\" TipoEndoso=\"\" NoEndoso=\"\" NoCotizacion=\"${responseWs.cotizacion.idCotizacion}\" NoPoliza=\"\" TipoMovimiento=\"3\">")
        xml.append("<DatosAsegurado NoAsegurado=\"\">")
        //  VALIDA SI EL CLIENTE ES MORAL O FISICO
        if(responseWs.cliente.tipoPersona != "M") {
            xml.append("<Nombre>${nombreCompleto}</Nombre>") //cambiar a nombre dinamico
            xml.append("<Direccion>${responseWs.cliente.direccion.calle}</Direccion>")
            xml.append("<Colonia>${responseWs.cliente.direccion.colonia}</Colonia>")
            xml.append("<Poblacion>${consultaDireccion[0].D_mnpio}</Poblacion>")
            xml.append("<Estado>${consultaDireccion[0].c_estado}</Estado>")
            xml.append("<CodigoPostal>${consultaDireccion[0].d_codigo}</CodigoPostal>")
            xml.append("<NoEmpleado/>")
            xml.append("<Agrupador/>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>1</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.direccion.noExt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>2</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.direccion.noInt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>3</TipoRegla>")
            xml.append("<ValorRegla>MEXICO</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>4</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.nombre}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>5</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.apellidoPat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>6</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.apellidoMat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>7</TipoRegla>")
            xml.append("<ValorRegla>${consultaDireccion[0].c_mnpio}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>8</TipoRegla>")
            xml.append("<ValorRegla>${consultaDireccion[0].id_asenta_cpcons}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>19</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>20</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.fechaNacimiento}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>21</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>23</TipoRegla>")
            xml.append("<ValorRegla>Empleado</ValorRegla>") //se agrega ocupacion
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>24</TipoRegla>")
            xml.append("<ValorRegla>Sector</ValorRegla>") // se agrega sector
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>25</TipoRegla>")
            xml.append("<ValorRegla>Empleado</ValorRegla>") // se agrega la profesion del asegurado
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>28</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.rfc}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>56</TipoRegla>")
            xml.append("<ValorRegla>${genderAssigment(responseWs.cliente.genero)}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>26</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.email}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>70</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.telefono}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
        }
        else{
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>1</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.direccion.noExt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>2</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.direccion.noInt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>4</TipoRegla>")
            xml.append("<ValorRegla>${nombreCompleto}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>7</TipoRegla>")
            xml.append("<ValorRegla>${consultaDireccion[0].c_mnpio}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>8</TipoRegla>")
            xml.append("<ValorRegla>${consultaDireccion[0].id_asenta_cpcons}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>19</TipoRegla>")
            xml.append("<ValorRegla>2</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>24</TipoRegla>")
            xml.append("<ValorRegla>10</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>28</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.rfc}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>29</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>31</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.email}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>32</TipoRegla>")
            xml.append("<ValorRegla>${dateReformat(responseWs.cliente.fechaNacimiento)}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>34</TipoRegla>")
            xml.append("<ValorRegla>Rodolfo Juarez Vazquez</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>35</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>57</TipoRegla>")
            xml.append("<ValorRegla>N</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>69</TipoRegla>")
            xml.append("<ValorRegla>N</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>59</TipoRegla>")
            xml.append("<ValorRegla>N</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>61</TipoRegla>")
            xml.append("<ValorRegla></ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>62</TipoRegla>")
            xml.append("<ValorRegla>INE</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>63</TipoRegla>")
            xml.append("<ValorRegla>1234567890</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>65</TipoRegla>")
            xml.append("<ValorRegla>07/08/2001</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>66</TipoRegla>")
            xml.append("<ValorRegla>DF</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>67</TipoRegla>")
            xml.append("<ValorRegla>MEXICO</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>68</TipoRegla>")
            xml.append("<ValorRegla>jjuarez@ahorra.io</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>70</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.cliente.telefono}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>72</TipoRegla>")
            xml.append("<ValorRegla>N-20170621148</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
        }
        //  VALIDA SI SE TIENE CONTRATANTE
        if(responseWs.contratante.nombre != "") {
            //  AGINACION DE LOS DATOS DEL CONTRATANTE
            val direccion = direction.getDirections(responseWs.contratante.direccion.codPostal).colony(responseWs.contratante.direccion.colonia)
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>11</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.direccion.codPostal}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>12</TipoRegla>")
            xml.append("<ValorRegla>${direccion[0].c_estado}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>13</TipoRegla>")
            xml.append("<ValorRegla>${direccion[0].c_mnpio}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>14</TipoRegla>")
            xml.append("<ValorRegla>${direccion[0].id_asenta_cpcons}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>15</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.telefono}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>16</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.direccion.calle}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>17</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.direccion.noExt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>18</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.direccion.noInt}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>36</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.nombre}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>37</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.apellidoPat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>38</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.apellidoMat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>39</TipoRegla>")
            xml.append("<ValorRegla>1</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>40</TipoRegla>")
            xml.append("<ValorRegla>${ dateReformat(responseWs.contratante.fechaNacimiento) }</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>41</TipoRegla>")
            xml.append("<ValorRegla>${1}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>43</TipoRegla>")
            xml.append("<ValorRegla>${1}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>44</TipoRegla>")
            xml.append("<ValorRegla>${10}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>45</TipoRegla>")
            xml.append("<ValorRegla>${1}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>46</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.email}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>47</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.curp}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>48</TipoRegla>")
            xml.append("<ValorRegla>${responseWs.contratante.rfc}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>73</TipoRegla>")
            xml.append("<ValorRegla>${genderAssigment(responseWs.contratante.genero)}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
            xml.append("<ConsideracionesAdicionalesDA NoConsideracion=\"40\">")
            xml.append("<TipoRegla>74</TipoRegla>")
            xml.append("<ValorRegla>${"001"}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDA>")
        }

        xml.append("</DatosAsegurado>")
        xml.append("<DatosVehiculo NoInciso=\"1\">")
        xml.append("<ClaveAmis>${responseWs.vehiculo.clave}</ClaveAmis>")
        xml.append("<Modelo>${responseWs.vehiculo.modelo}</Modelo>")
        xml.append("<DescripcionVehiculo>${responseWs.vehiculo.descripcion}</DescripcionVehiculo>")
        xml.append("<Uso>01</Uso>") // 01 regresr
        xml.append("<Servicio>01</Servicio>")
        xml.append("<Paquete>${packageSelection(responseWs.paquete)}</Paquete>")
        xml.append("<Motor>${responseWs.vehiculo.noMotor}</Motor>")
        xml.append("<Serie>${responseWs.vehiculo.noSerie}</Serie>")

        //  EN EL CASO DE LA EMISION NO IMPORTA QUE COBERTURAS SE ENVIEN SE TOMAN LAS DE LA COTIZACION
        xml.append("<Coberturas NoCobertura=\"1\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>5</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"3\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>10</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"4\">")
        xml.append("<SumaAsegurada>3000000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"5\">")
        xml.append("<SumaAsegurada>200000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"6\">")
        xml.append("<SumaAsegurada>100000</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"7\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")
        xml.append("<Coberturas NoCobertura=\"14\">")
        xml.append("<SumaAsegurada>0</SumaAsegurada>")
        xml.append("<TipoSuma>0</TipoSuma>")
        xml.append("<Deducible>0</Deducible>")
        xml.append("<Prima>0</Prima>")
        xml.append("</Coberturas>")

        xml.append("</DatosVehiculo>")
        xml.append("<DatosGenerales>")
        //  VALIDA SI LA EMISION ES POSTFECHADA O NO
        xml.append(
            "<FechaEmision>${
                (if (responseWs.emision.inicioVigencia == "" || responseWs.emision.inicioVigencia == LocalDateTime.now()
                        .format(
                            DateTimeFormatter.ISO_DATE
                        )
                ) LocalDateTime.now().plusDays(1)
                    .format(DateTimeFormatter.ISO_DATE) else responseWs.emision.inicioVigencia)
            }</FechaEmision>"
        )
        xml.append(
            "<FechaInicio>${
                (if (responseWs.emision.inicioVigencia == "" || responseWs.emision.inicioVigencia == LocalDateTime.now()
                        .format(
                            DateTimeFormatter.ISO_DATE
                        )
                ) LocalDateTime.now().plusDays(1)
                    .format(DateTimeFormatter.ISO_DATE) else responseWs.emision.inicioVigencia)
            }</FechaInicio>"
        )
        xml.append(
            "<FechaTermino>${
                (if (responseWs.emision.inicioVigencia == "" || responseWs.emision.inicioVigencia == LocalDateTime.now()
                        .format(
                            DateTimeFormatter.ISO_DATE
                        )
                ) LocalDateTime.now().plusYears(1).plusDays(1)
                    .format(DateTimeFormatter.ISO_DATE) else ((divFin[0].toInt() + 1).toString() + "-" + divFin[1] + "-" + divFin[2]))
            }</FechaTermino>"
        )
        xml.append("<Moneda>0</Moneda>")
        xml.append("<Agente>$agenteMotos</Agente>")
        xml.append("<FormaPago>${collectionType(responseWs.periodicidadDePago)}</FormaPago>")
        xml.append("<TarifaValores>LINEA</TarifaValores>")
        xml.append("<TarifaCuotas>LINEA</TarifaCuotas>")
        xml.append("<TarifaDerechos>LINEA</TarifaDerechos>")
        xml.append("<Plazo/>")
        xml.append("<Agencia/>")
        xml.append("<Contrato/>")
        xml.append("<PorcentajeDescuento>${responseWs.descuento}</PorcentajeDescuento>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"01\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${digitValidation(responseWs.vehiculo.clave)}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"04\">")
        xml.append("<TipoRegla>0</TipoRegla>")
        xml.append("<ValorRegla>${ambiente}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"05\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>7</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        if (responseWs.conductorHabitual.nombre != "") { //  VERIFICA SI LA POLIZA TIENE CONDUCTOR HABITUAL
            xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"10\">") //  CONSIDERACION PARA PONER EL CONDUCTOR HABITUAL EN LA POLIZA
            xml.append("<TipoRegla>82</TipoRegla>")
            //  NOMBRE DEL CONDUCTOR HABITUAL
            //  NOTA: EL NOMBRE NO DEBE IR EN UN FORMATO ESPECIFICO
            xml.append("<ValorRegla>${responseWs.conductorHabitual.nombre} ${responseWs.conductorHabitual.apellidoPat} ${responseWs.conductorHabitual.apellidoMat}</ValorRegla>")
            xml.append("</ConsideracionesAdicionalesDG>")
        }
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"16\">")
        xml.append("<TipoRegla>1</TipoRegla>")
        xml.append("<ValorRegla>${responseWs.vehiculo.noPlacas}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("<ConsideracionesAdicionalesDG NoConsideracion=\"10\">")
        xml.append("<TipoRegla>16</TipoRegla>")
        xml.append("<ValorRegla>${responseWs.vehiculo.noPlacas}</ValorRegla>")
        xml.append("</ConsideracionesAdicionalesDG>")
        xml.append("</DatosGenerales>")
        xml.append("<Primas>")
        xml.append("<PrimaNeta/>")
        xml.append("<Derecho>600</Derecho>")
        xml.append("<Recargo/>")
        xml.append("<Impuesto/>")
        xml.append("<PrimaTotal/>")
        xml.append("<Comision/>")
        xml.append("</Primas>")
        xml.append("<CodigoError/>")
        xml.append("</Movimiento>")
        xml.append("</Movimientos>")
        return xml.toString()
    }

    fun cobroAutos(responseWs: GeneralResponse): String {
        val idBanco = bancos.getBanco(responseWs.pago.banco)
        var idType = ""
        val agente: String

        if (responseWs.pago.medioPago == "DEBITO") {
            idType = "CL"
        } else if (responseWs.pago.medioPago == "CREDITO") {
            if (responseWs.pago.msi == "") {
                idType = "CL"
            } else {
                idType = responseWs.pago.msi
            }
        }
        // MODIFICA EL ORDEN DE LOS BANCOS PARA QUE SE MUESTREN PRIMERO LOS MAS UTILIZADOS
        runBlocking {
            launch(Dispatchers.IO) {
                try {
                    bancos.updateOrden(idBanco[0].id_banco)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        when (responseWs.vehiculo.servicio.service) {
            "PARTICULAR" -> {
                agente = agenteAutos
            }

            "APP" -> {
                agente = agenteUber
            }

            "APPPLUS" -> {
                agente = agenteUber
            }

            "TAXI" -> {
                agente = agenteAutos
            }

            "CARGA" -> {
                agente = agenteAutos
            }

            else -> {
                agente = agenteAutos
            }
        }

        var xmlCobro = "<oplCollection>" +
                "<Collection NoNegocio=\"$negocioCobro\" NoPoliza=\"" + responseWs.emision.poliza + "\" wpuid=\"" + wpuid + "\" " +
                "wptoken=\"" + wtoken + "\">" +
                "<collectionData>" +
                "<type>" + idType + "</type>" +
                "<userkey>" + responseWs.cliente.rfc + "</userkey>" +
                "<name>" + responseWs.pago.nombreTarjeta + "</name>" +
                "<number>" + responseWs.pago.noTarjeta + "</number>" +
                "<bankcode>" + idBanco[0].id_banco + "</bankcode>" +
                "<expmonth>" + responseWs.pago.mesExp + "</expmonth>" +
                "<expyear>" + responseWs.pago.anioExp + "</expyear>" +
                "<cvv-csc>" + responseWs.pago.codigoSeguridad + "</cvv-csc>" +
                "</collectionData>" +
                "<insuranceData>" +
                "<akey>$agente</akey>" +
                "<email>" + responseWs.cliente.email + "</email>" +
                "<PlazoPago>${collectionType(responseWs.periodicidadDePago)}</PlazoPago>" +
                "</insuranceData>" +
                "<CodigoError/>" +
                "</Collection>" +
                "</oplCollection>"
        return xmlCobro
    }

    fun cobroMotos(responseWs: GeneralResponse): String {
        var idBanco = bancos.getBanco(responseWs.pago.banco)
        var idType = ""

        if (responseWs.pago.medioPago == "DEBITO") {
            idType = "CL"
        } else if (responseWs.pago.medioPago == "CREDITO") {
            if (responseWs.pago.msi == "") {
                idType = "CL"
            } else {
                idType = responseWs.pago.msi
            }
        }

        // MODIFICA EL ORDEN DE LOS BANCOS PARA QUE SE MUESTREN PRIMERO LOS MAS UTILIZADOS
        runBlocking {
            launch(Dispatchers.IO) {
                try {
                    bancos.updateOrden(idBanco[0].id_banco)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        var xmlCobro = "<oplCollection>" +
                "<Collection NoNegocio=\"$negocioCobro\" NoPoliza=\"" + responseWs.emision.poliza + "\" wpuid=\"" + wpuid + "\" " +
                "wptoken=\"" + wtoken + "\">" +
                "<collectionData>" +
                "<type>" + idType + "</type>" +
                "<userkey>" + responseWs.cliente.rfc + "</userkey>" +
                "<name>" + responseWs.pago.nombreTarjeta + "</name>" +
                "<number>" + responseWs.pago.noTarjeta + "</number>" +
                "<bankcode>" + idBanco[0].id_banco + "</bankcode>" +
                "<expmonth>" + responseWs.pago.mesExp + "</expmonth>" +
                "<expyear>" + responseWs.pago.anioExp + "</expyear>" +
                "<cvv-csc>" + responseWs.pago.codigoSeguridad + "</cvv-csc>" +
                "</collectionData>" +
                "<insuranceData>" +
                "<akey>$agenteMotos</akey>" +
                "<email>" + responseWs.cliente.email + "</email>" +
                "<PlazoPago>${collectionType(responseWs.periodicidadDePago)}</PlazoPago>" +
                "</insuranceData>" +
                "<CodigoError/>" +
                "</Collection>" +
                "</oplCollection>"
        return xmlCobro
    }

    fun domiciliacionAutos(responseWs: GeneralResponse): String {
        val xmlDomiciliacion = StringBuilder()
        val idBanco = bancos.getBanco(responseWs.pago.banco)
        val idType: String

        var agente: String

        idType = if (responseWs.pago.banco.uppercase() == "AMERICAN EXPRESS")
            "A"
        else {
            when (responseWs.pago.medioPago.uppercase()) {
                "DEBITO" -> "D"
                "CREDITO" -> "C"
                else -> "D"
            }
        }

        // MODIFICA EL ORDEN DE LOS BANCOS PARA QUE SE MUESTREN PRIMERO LOS MAS UTILIZADOS
        runBlocking {
            launch(Dispatchers.IO) {
                try {
                    bancos.updateOrden(idBanco[0].id_banco)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        agente = when (responseWs.vehiculo.servicio.service) {
            "PARTICULAR" -> {
                agenteAutos
            }

            "APP", "APPPLUS" -> {
                agenteUber
            }

            else -> {
                agenteAutos
            }
        }

        xmlDomiciliacion.append("<oplCollection>")
        xmlDomiciliacion.append("<Collection NoNegocio=\"$negocioCobro\" NoPoliza=\"${responseWs.emision.poliza}\" wpuid=\"${wpuid}\" ")
        xmlDomiciliacion.append("wptoken=\"$wtoken\">")
        xmlDomiciliacion.append("<collectionData>")
        xmlDomiciliacion.append("<type>$idType</type>")
        xmlDomiciliacion.append("<userkey>${responseWs.cliente.rfc}</userkey>")
        xmlDomiciliacion.append("<name>${responseWs.pago.nombreTarjeta}</name>")
        xmlDomiciliacion.append("<crypto>0</crypto>")
        xmlDomiciliacion.append("<number>${responseWs.pago.noTarjeta}</number>")
        xmlDomiciliacion.append("<bankcode>${idBanco[0].id_banco}</bankcode>")
        xmlDomiciliacion.append("<expmonth>${responseWs.pago.mesExp}</expmonth>")
        xmlDomiciliacion.append("<expyear>${responseWs.pago.anioExp}</expyear>")
        xmlDomiciliacion.append("</collectionData>")
        xmlDomiciliacion.append("<insuranceData>")
        xmlDomiciliacion.append("<akey>$agente</akey>")
        xmlDomiciliacion.append("<email>${responseWs.cliente.email}</email>")
        xmlDomiciliacion.append("<PlazoPago>${collectionType(responseWs.periodicidadDePago)}</PlazoPago>")
        xmlDomiciliacion.append("<currency>mxn</currency>")
        xmlDomiciliacion.append("</insuranceData>")
        xmlDomiciliacion.append("<CodigoError/></Collection></oplCollection>")
        return xmlDomiciliacion.toString()
    }

    fun domiciliacionMotos(responseWs: GeneralResponse): String {
        val xmlDomiciliacion = StringBuilder()
        val idBanco = bancos.getBanco(responseWs.pago.banco)
        val idType: String
        idType = if (responseWs.pago.banco.uppercase() == "AMERICAN EXPRESS")
            "A"
        else {
            when (responseWs.pago.medioPago.uppercase()) {
                "DEBITO" -> "D"
                "CREDITO" -> "C"
                else -> "D"
            }
        }
        // MODIFICA EL ORDEN DE LOS BANCOS PARA QUE SE MUESTREN PRIMERO LOS MAS UTILIZADOS
        runBlocking {
            launch(Dispatchers.IO) {
                try {
                    bancos.updateOrden(idBanco[0].id_banco)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        xmlDomiciliacion.append("<oplCollection>")
        xmlDomiciliacion.append("<Collection NoNegocio=\"$negocioCobro\" NoPoliza=\"${responseWs.emision.poliza}\" wpuid=\"${wpuid}\" ")
        xmlDomiciliacion.append("wptoken=\"$wtoken\">")
        xmlDomiciliacion.append("<collectionData>")
        xmlDomiciliacion.append("<type>$idType</type>")
        xmlDomiciliacion.append("<userkey>${responseWs.cliente.rfc}</userkey>")
        xmlDomiciliacion.append("<name>${responseWs.pago.nombreTarjeta}</name>")
        xmlDomiciliacion.append("<crypto>0</crypto>")
        xmlDomiciliacion.append("<number>${responseWs.pago.noTarjeta}</number>")
        xmlDomiciliacion.append("<bankcode>${idBanco[0].id_banco}</bankcode>")
        xmlDomiciliacion.append("<expmonth>${responseWs.pago.mesExp}</expmonth>")
        xmlDomiciliacion.append("<expyear>${responseWs.pago.anioExp}</expyear>")
        xmlDomiciliacion.append("</collectionData>")
        xmlDomiciliacion.append("<insuranceData>")
        xmlDomiciliacion.append("<akey>$agenteMotos</akey>")
        xmlDomiciliacion.append("<email>${responseWs.cliente.email}</email>")
        xmlDomiciliacion.append("<PlazoPago>${collectionType(responseWs.periodicidadDePago)}</PlazoPago>")
        xmlDomiciliacion.append("<currency>mxn</currency>")
        xmlDomiciliacion.append("</insuranceData>")
        xmlDomiciliacion.append("<CodigoError/></Collection></oplCollection>")
        return xmlDomiciliacion.toString()
    }
}