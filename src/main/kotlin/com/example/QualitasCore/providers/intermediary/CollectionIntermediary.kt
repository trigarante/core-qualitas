package com.example.QualitasCore.providers.intermediary

import com.example.QualitasCore.providers.connection.dev.CollectionDevConnection
import com.example.QualitasCore.providers.connection.prod.CollectionProdConnection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class CollectionIntermediary {
    @Value("\${jwt.ambiente}")
    private var ambiente = ""

    @Autowired
    private lateinit var collectionDevConnection: CollectionDevConnection

    @Autowired
    private lateinit var cobroController: CollectionProdConnection

    fun collection(xml:String):String{
        return try {
            when(ambiente) {
                "0" -> {
                    cobroController.cobrar(xml).oplCollectionResult
                }
                "1" -> {
                    collectionDevConnection.cobrar(xml).oplCollectionResult
                }
                else ->{
                    "Servicio de cobro no encontrado"
                }
            }
        }catch (e:Exception){
            e.printStackTrace()
            return "Error en el serivcio de cobro: ${e.message}"
        }
    }

    fun domiciliar(xml:String):String{
        return try {
            when(ambiente) {
                "0" -> {
                    cobroController.domiciliar(xml).domiOplResult
                }
                "1" -> {
                    collectionDevConnection.domiciliar(xml).domiOplResult
                }
                else ->{
                    "Servicio de cobro no encontrado"
                }
            }
        }catch (e:Exception){
            return "Error en el serivcio de cobro: ${e.message}"
        }
    }

    fun cancel(xml:String):String{
        return try {
            when(ambiente) {
                "0" -> {
                    cobroController.cancelar(xml).oplCancelationResult
                }
                "1" -> {
                    collectionDevConnection.cancelar(xml).oplCancelationResult
                }
                else ->{
                    "Servicio de cobro no encontrado"
                }
            }
        }catch (e:Exception){
            return "Error en el serivcio de cobro: ${e.message}"
        }
    }
}