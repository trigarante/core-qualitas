package com.example.QualitasCore.providers.intermediary

import com.example.QualitasCore.providers.connection.dev.QuotationDevConnection
import com.example.QualitasCore.providers.connection.prod.QuotationProdConnection
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class QuotationIntermediary {

    @Autowired
    lateinit var prod: QuotationProdConnection

    @Autowired
    lateinit var dev: QuotationDevConnection

    @Value("\${jwt.ambiente}")
    var ambiente = ""

    fun cotizar(xml: String): String{
        when(ambiente){
            "0"->{
                return prod.cotizadorQualitas(xml).obtenerNuevaEmisionResult

            }
            "1"->{
                return dev.cotizar(xml).obtenerNuevaEmisionResult
            }
            else->{
                return "Como es que llegaste aqui D:"
            }
        }
    }
}