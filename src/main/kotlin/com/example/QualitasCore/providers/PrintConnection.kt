package com.example.QualitasCore.providers

import core.qualitas.wsdl.prod.RecuperaImpresionM15
import core.qualitas.wsdl.prod.RecuperaImpresionM15Response
import core.qualitas.wsdl.prod.RecuperaImpresionPrueba
import core.qualitas.wsdl.prod.RecuperaImpresionPruebaResponse
import org.springframework.beans.factory.annotation.Value
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.core.SoapActionCallback

class PrintConnection : WebServiceGatewaySupport() {

    @Value("\${jwt.ambiente}")
    val ambiente: String = ""

    @Value("\${jwt.negocio}")
    val negocio: String = ""
    @Value("\${jwt.agente}")
    val agente: String = ""

    @Value("\${renewal.negocio}")
    val renovacionesNegocio: String = ""
    @Value("\${renewal.agente}")
    val renovacionesAgente: String = ""

    @Value("\${jwt.negocio_go}")
    private val negocio_go: String = ""
    @Value("\${jwt.agente_go}")
    private val agente_go: String = ""

    @Value("\${motos.agente}")
    private val agente_motos: String = ""

    @Value("\${migo.negocio_motos}")
    private val negocioMotosMigo: String = ""
    @Value("\${migo.negocio_autos}")
    private val negocioAutosMigo: String = ""
    @Value("\${migo.negocio_uber}")
    private val negocioUberMigo: String = ""


    @Value("\${migo.agente_autos}")
    private val agenteAutosMigo: String = ""
    @Value("\${migo.agente_motos}")
    private val agenteMotosMigo: String = ""
    @Value("\${migo.agente_uber}")
    private val agenteUberMigo: String = ""

    fun impresionQualitas(noPoliza: String, credentials: String = ""): String {
        try {
            when (ambiente) {
                "0" -> {
                    val request = RecuperaImpresionM15()
                    request.urlPoliza = ""
                    request.urlRecibo = ""
                    request.urlTextos = ""
                    request.inciso = 0
                    request.impPol = 0
                    request.impRec = 0
                    request.impAnexo = 0
                    request.ramo = "04"
                    request.formaPol = "polizaf1_logoQ_pdf"
                    request.formaRec = "recibo_logoQ_pdf"
                    request.formaAnexo = "polizaf2_logoQ_pdf"
                    request.endoso = "000000"
                    when (credentials) {
                        "" -> {
                            request.noNegocio = negocio
                            request.agente = agente
                        }

                        "renovaciones" -> {
                            request.noNegocio = renovacionesNegocio
                            request.agente = renovacionesAgente
                        }

                        "go" -> {
                            request.noNegocio = negocio_go
                            request.agente = agente_go
                        }

                        "motos" -> {
                            request.noNegocio = negocio
                            request.agente = agente_motos
                        }

                        "motosMigo" -> {
                            request.noNegocio = negocioMotosMigo
                            request.agente = agenteMotosMigo
                        }

                        "particularMigo" -> {
                            request.noNegocio = negocioAutosMigo
                            request.agente = agenteAutosMigo
                        }

                        "uberMigo" -> {
                            request.noNegocio = negocioUberMigo
                            request.agente = agenteUberMigo
                        }

                        else -> {
                            request.noNegocio = negocio
                            request.agente = agente
                        }
                    }
                    request.usuario = "hola"
                    request.password = "holaaa"
                    request.nPoliza = noPoliza
                    val respuesta = getWebServiceTemplate()
                        .marshalSendAndReceive(
                            "http://qbcenter.qualitas.com.mx/QBCImpresion/Service.asmx",
                            request, SoapActionCallback("http://tempuri.org/RecuperaImpresionM15")
                        ) as RecuperaImpresionM15Response
                    return respuesta.recuperaImpresionM15Result
                }

                "1" -> {
                    val request = RecuperaImpresionPrueba()
                    request.urlPoliza = ""
                    request.urlRecibo = ""
                    request.urlTextos = ""
                    request.inciso = 0
                    request.impPol = 0
                    request.impRec = 0
                    request.impAnexo = 0
                    request.ramo = "04"
                    request.formaPol = "polizaf1_logoQ_pdf"
                    request.formaRec = "recibo_logoQ_pdf"
                    request.formaAnexo = "polizaf2_logoQ_pdf"
                    request.endoso = "000000"
                    when (credentials) {
                        "" -> {
                            request.noNegocio = negocio
                            request.agente = agente
                        }

                        "go" -> {
                            request.noNegocio = negocio_go
                            request.agente = agente_go
                        }

                        "renovaciones" -> {
                            request.noNegocio = renovacionesNegocio
                            request.agente = renovacionesAgente
                        }

                        "motos" -> {
                            request.noNegocio = negocio
                            request.agente = agente_motos
                        }

                        "motosMigo" -> {
                            request.noNegocio = negocioMotosMigo
                            request.agente = agenteMotosMigo
                        }

                        "particularMigo" -> {
                            request.noNegocio = negocioAutosMigo
                            request.agente = agenteAutosMigo
                        }

                        "uberMigo" -> {
                            request.noNegocio = negocioUberMigo
                            request.agente = agenteUberMigo
                        }

                        else -> {
                            request.noNegocio = negocio
                            request.agente = agente
                        }
                    }
                    request.usuario = "hola"
                    request.password = "holaaa"
                    request.nPoliza = noPoliza
                    val respuesta = getWebServiceTemplate()
                        .marshalSendAndReceive(
                            "http://qbcenter.qualitas.com.mx/QBCImpresion/Service.asmx",
                            request, SoapActionCallback("http://tempuri.org/RecuperaImpresionPrueba")
                        ) as RecuperaImpresionPruebaResponse
                    return respuesta.recuperaImpresionPruebaResult
                }
            }
        }
        catch(e: Exception){
            e.printStackTrace()
        }
        return "No se :B"
    }
}