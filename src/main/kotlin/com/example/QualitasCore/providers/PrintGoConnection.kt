package com.example.QualitasCore.providers
import core.qualitas.wsdl.prod.RecuperaImpresionM15
import core.qualitas.wsdl.prod.RecuperaImpresionM15Response
import core.qualitas.wsdl.prod.RecuperaImpresionPrueba
import core.qualitas.wsdl.prod.RecuperaImpresionPruebaResponse
import org.springframework.beans.factory.annotation.Value
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.core.SoapActionCallback

class PrintGoConnection : WebServiceGatewaySupport(){

    @Value("\${jwt.negocio_go}")
    val negocio : String = ""

    @Value("\${jwt.agente_go}")
    val agente : String = ""

    @Value("\${jwt.ambiente}")
    val ambiente : String = ""

    fun impresionQualitas(noPoliza : String): String {
        when (ambiente) {
            "0" -> {
                val request: RecuperaImpresionM15 = RecuperaImpresionM15()
                request.urlPoliza = ""
                request.urlRecibo = ""
                request.urlTextos = ""
                request.inciso = 0
                request.impPol = 0
                request.impRec = 0
                request.impAnexo = 0
                request.ramo = "04"
                request.formaPol = "polizaf1_logoQ_pdf"
                request.formaRec = "recibo_logoQ_pdf"
                request.formaAnexo = "polizaf2_logoQ_pdf"
                request.endoso = "000000"
                request.noNegocio = negocio
                request.usuario = "hola"
                request.password = "holaaa"
                request.nPoliza = noPoliza
                request.agente = agente

                val respuesta = getWebServiceTemplate()
                    .marshalSendAndReceive("http://qbcenter.qualitas.com.mx/QBCImpresion/Service.asmx",
                        request, SoapActionCallback("http://tempuri.org/RecuperaImpresionM15")
                    ) as RecuperaImpresionM15Response
                return respuesta.recuperaImpresionM15Result
            }
            "1" -> {
                val request: RecuperaImpresionPrueba = RecuperaImpresionPrueba()
                request.urlPoliza = ""
                request.urlRecibo = ""
                request.urlTextos = ""
                request.inciso = 0
                request.impPol = 0
                request.impRec = 0
                request.impAnexo = 0
                request.ramo = "04"
                request.formaPol = "polizaf1_logoQ_pdf"
                request.formaRec = "recibo_logoQ_pdf"
                request.formaAnexo = "polizaf2_logoQ_pdf"
                request.endoso = "000000"
                request.noNegocio = negocio
                request.usuario = "hola"
                request.password = "holaaa"
                request.nPoliza = noPoliza
                request.agente = agente

                val respuesta = getWebServiceTemplate()
                    .marshalSendAndReceive("http://qbcenter.qualitas.com.mx/QBCImpresion/Service.asmx",
                        request, SoapActionCallback("http://tempuri.org/RecuperaImpresionPrueba")
                    ) as RecuperaImpresionPruebaResponse
                return respuesta.recuperaImpresionPruebaResult
            }
        }
        return "No se :B"
    }
}