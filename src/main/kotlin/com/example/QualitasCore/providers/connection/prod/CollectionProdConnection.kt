package com.example.QualitasCore.providers.connection.prod

import core.qualitas.wsdl.prod.DomiOpl
import core.qualitas.wsdl.prod.DomiOplResponse
import core.qualitas.wsdl.prod.OplCancelation
import core.qualitas.wsdl.prod.OplCancelationResponse
import core.qualitas.wsdl.prod.OplCollection
import core.qualitas.wsdl.prod.OplCollectionResponse
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.core.SoapActionCallback

class CollectionProdConnection : WebServiceGatewaySupport() {

    fun cobrar(xml:String?) : OplCollectionResponse{
        val request = OplCollection()
        request.xmlCollection = xml
        val respuesta = getWebServiceTemplate()
                .marshalSendAndReceive("https://pagos.qualitas.com.mx/ws/wsCollection.php",request
                        ,SoapActionCallback("https://pagos.qualitas.com.mx/ws/wsCollection.php#oplCollection"
                )) as OplCollectionResponse
        return  respuesta
    }

    fun cancelar(xml:String?) : OplCancelationResponse {
        val request = OplCancelation()
        request.xmlCancelation = xml
        val respuesta = getWebServiceTemplate()
            .marshalSendAndReceive("https://pagos.qualitas.com.mx/ws/wsCollection.php"
                , request, SoapActionCallback("https://pagos.qualitas.com.mx/ws/wsCollection.php#oplCancelation")
            ) as OplCancelationResponse
        return  respuesta
    }

    fun domiciliar(xml:String?) : DomiOplResponse {
        val request = DomiOpl()
        request.xmlDomicilia = xml
        val respuesta = getWebServiceTemplate()
            .marshalSendAndReceive("https://pagos.qualitas.com.mx/ws/wsCollection.php"
                , request, SoapActionCallback("https://pagos.qualitas.com.mx/ws/wsCollection.php#domiOpl")
            ) as DomiOplResponse
        return  respuesta
    }
}