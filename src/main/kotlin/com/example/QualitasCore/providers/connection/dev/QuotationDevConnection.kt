package com.example.QualitasCore.providers.connection.dev

import core.qualitas.wsdl.dev.ObtenerNuevaEmision
import core.qualitas.wsdl.dev.ObtenerNuevaEmisionResponse
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.core.SoapActionCallback

class QuotationDevConnection : WebServiceGatewaySupport() {

    fun cotizar(xml: String): ObtenerNuevaEmisionResponse {

        val request = ObtenerNuevaEmision()
        request.xmlEmision = xml

        var response = getWebServiceTemplate().marshalSendAndReceive("https://qa.qualitas.com.mx:8443/wsEmision/WsEmision.asmx?wsdl"
                , request, SoapActionCallback("http://qualitas.com.mx/obtenerNuevaEmision")) as ObtenerNuevaEmisionResponse
        return response
    }
}