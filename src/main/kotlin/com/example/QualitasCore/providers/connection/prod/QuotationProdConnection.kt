package com.example.QualitasCore.providers.connection.prod

import core.qualitas.wsdl.prod.ObtenerNuevaEmision
import core.qualitas.wsdl.prod.ObtenerNuevaEmisionResponse
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.core.SoapActionCallback

class QuotationProdConnection : WebServiceGatewaySupport() {

    fun cotizadorQualitas( xml : String?) : ObtenerNuevaEmisionResponse{
       val request : ObtenerNuevaEmision = ObtenerNuevaEmision()
        request.xmlEmision = xml

        val respuesta =  getWebServiceTemplate()
                .marshalSendAndReceive("http://sio.qualitas.com.mx/WsEmision/WsEmision.asmx",
                request, SoapActionCallback("http://qualitas.com.mx/obtenerNuevaEmision")) as ObtenerNuevaEmisionResponse
        return respuesta
    }



}