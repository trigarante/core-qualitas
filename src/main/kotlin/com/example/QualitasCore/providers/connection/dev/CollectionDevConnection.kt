package com.example.QualitasCore.providers.connection.dev

import core.qualitas.wsdl.dev.DomiOpl
import core.qualitas.wsdl.dev.OplCancelation
import core.qualitas.wsdl.dev.OplCollection
import core.qualitas.wsdl.dev.OplCollectionResponse
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.core.SoapActionCallback

class CollectionDevConnection:WebServiceGatewaySupport() {

    fun cobrar(xml:String?) : OplCollectionResponse {
        val request = OplCollection()
        request.xmlCollection = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>$xml"
        val respuesta = getWebServiceTemplate()
            .marshalSendAndReceive("http://pagosqa.qualitas.com.mx/ws/wsCollection.php"
                , request, SoapActionCallback("http://pagosqa.qualitas.com.mx/ws/wsCollection.php#oplCollection")
            ) as OplCollectionResponse
        return  respuesta
    }

    fun cancelar(xml:String?) : core.qualitas.wsdl.dev.OplCancelationResponse {
        val request = OplCancelation()
        request.xmlCancelation = xml
        val respuesta = getWebServiceTemplate()
            .marshalSendAndReceive("http://pagosqa.qualitas.com.mx/ws/wsCollection.php"
                , request, SoapActionCallback("http://pagosqa.qualitas.com.mx/ws/wsCollection.php#oplCancelation")
            ) as core.qualitas.wsdl.dev.OplCancelationResponse
        return  respuesta
    }

    fun domiciliar(xml:String?) : core.qualitas.wsdl.dev.DomiOplResponse {
        val request = DomiOpl()
        request.xmlDomicilia = xml
        val respuesta = getWebServiceTemplate()
            .marshalSendAndReceive("http://pagosqa.qualitas.com.mx/ws/wsCollection.php"
                , request, SoapActionCallback("http://pagosqa.qualitas.com.mx/ws/wsCollection.php#domiOpl")
            ) as core.qualitas.wsdl.dev.DomiOplResponse
        return  respuesta
    }

}