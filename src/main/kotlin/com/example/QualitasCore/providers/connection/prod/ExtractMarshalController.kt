package com.example.QualitasCore.providers.connection.prod

import core.qualitas.wsdl.ext.AddRegistrosQualitas
import core.qualitas.wsdl.ext.AddRegistrosQualitasResponse
import core.qualitas.wsdl.ext.ObjectFactory
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.soap.client.core.SoapActionCallback
import javax.xml.bind.JAXBElement

class ExtractMarshalController : WebServiceGatewaySupport() {

    fun extract( clientName: String,
                 email: String,
                 phone: String,
                 cp: String,
                 gender: String,
                 age: String,
                 brand: String,
                 model: String,
                 description: String,
                 subDescription: String,
                 branch: String,
                 policyPrice: String): AddRegistrosQualitasResponse {
        val request = AddRegistrosQualitas()
        val obj = ObjectFactory()
        val correo :JAXBElement<String> = obj.createAddRegistrosQualitasCorreo(email)
        val codpost :JAXBElement<String> = obj.createAddRegistrosQualitasCP(cp)
        val descripcion :JAXBElement<String> = obj.createAddRegistrosQualitasDescripcion(description)
        val edad :JAXBElement<String> = obj.createAddRegistrosQualitasEdad(age)
        val detalle :JAXBElement<String> = obj.createAddRegistrosQualitasDetalle("")
        val genero :JAXBElement<String> = obj.createAddRegistrosQualitasGenero(gender)
        val marca :JAXBElement<String> = obj.createAddRegistrosQualitasMarca(brand)
        val modelo :JAXBElement<String> = obj.createAddRegistrosQualitasModelo(model)
        val prima :JAXBElement<String> = obj.createAddRegistrosQualitasPrima(policyPrice)
        val nombre :JAXBElement<String> = obj.createAddRegistrosQualitasStrNombreCompleto(clientName)
        val subDescripcion :JAXBElement<String> = obj.createAddRegistrosQualitasSubDescripcion(subDescription)
        val telefono :JAXBElement<String> = obj.createAddRegistrosQualitasTelefono(phone)


        request.correo = correo
        request.cp = codpost
        request.descripcion = descripcion
        request.edad = edad
        request.detalle = detalle
        request.genero = genero
        request.marca = marca
        request.modelo = modelo
        request.prima = prima
        request.strNombreCompleto = nombre
        request.subDescripcion = subDescripcion
        request.telefono = telefono

        //var rerequest: JAXBElement<AddRegistrosQualitas> = obj.createAddRegistrosQualitas

        val response = getWebServiceTemplate().marshalSendAndReceive(
            "http://wfcgrupoestrategas.azurewebsites.net/Service1.svc?wsdl",
            request,
            SoapActionCallback("http://tempuri.org/iService1/AddRegistrosQualitas")
        ) as AddRegistrosQualitasResponse

        return response
    }
}