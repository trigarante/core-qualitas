package com.example.QualitasCore.providers.marshallers

import com.example.QualitasCore.providers.connection.dev.QuotationDevConnection
import com.example.QualitasCore.providers.connection.prod.QuotationProdConnection
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.oxm.jaxb.Jaxb2Marshaller

@Configuration
class QuotationMarshaller {

    @Bean
    fun marshaller() : Jaxb2Marshaller{
        val marshal = Jaxb2Marshaller()
        marshal.contextPath = "core.qualitas.wsdl.prod"
        return  marshal
    }

    @Bean
    fun qualitasClient (marshaller: Jaxb2Marshaller?) : QuotationProdConnection {
        val client : QuotationProdConnection = QuotationProdConnection()
        client.defaultUri = "http://sio.qualitas.com.mx/WsEmision/WsEmision.asmx"
        client.marshaller = marshaller
        client.unmarshaller = marshaller
        return client
    }

    @Bean
    fun marshaller4(): Jaxb2Marshaller{
        val marshaller4 = Jaxb2Marshaller()
        marshaller4.contextPath = "core.qualitas.wsdl.dev"
        return marshaller4
    }

    @Bean
    fun clienteDev(marshaller4: Jaxb2Marshaller): QuotationDevConnection {
        val client = QuotationDevConnection()
        client.marshaller = marshaller4
        client.unmarshaller = marshaller4
        client.defaultUri = "https://qa.qualitas.com.mx:8443/wsEmision/WsEmision.asmx?wsdl"
        return client
    }


}