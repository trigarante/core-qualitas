package com.example.QualitasCore.providers.marshallers

import com.example.QualitasCore.catalogExtract.ExtractionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.oxm.jaxb.Jaxb2Marshaller

@Configuration
class CatalogMarshaller(
    //@Autowired private val extractionService: ExtractionService
){

    @Bean
    fun marshaller8(): Jaxb2Marshaller{
        val marshal = Jaxb2Marshaller()
        marshal.contextPath = "core.qualitas.wsdl.cat"
        return marshal
    }

    @Bean
    fun catalogClient(marshaller8: Jaxb2Marshaller?): ExtractionService{
        val client = ExtractionService()
        client.marshaller = marshaller8
        client.unmarshaller = marshaller8
        client.defaultUri = "https://qbcenter.qualitas.com.mx/wsTarifa/wsTarifa.asmx?wsdl"
        return client
    }
}