package com.example.QualitasCore.providers.marshallers

import com.example.QualitasCore.providers.connection.dev.CollectionDevConnection
import com.example.QualitasCore.providers.connection.prod.CollectionProdConnection
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.oxm.jaxb.Jaxb2Marshaller

@Configuration
class CollectionMarshaller {

    @Bean
    fun marshaller3() : Jaxb2Marshaller {
        val marshal3 = Jaxb2Marshaller()
        marshal3.contextPath = "core.qualitas.wsdl.prod"
        return  marshal3
    }

    @Bean
    fun cobroClient (marshaller3: Jaxb2Marshaller?) : CollectionProdConnection {
        val client : CollectionProdConnection = CollectionProdConnection()
        client.defaultUri = "https://pagos.qualitas.com.mx/ws/wsCollection.php"
        client.marshaller = marshaller3
        client.unmarshaller = marshaller3
        return client
    }

    @Bean
    fun marshaller5(): Jaxb2Marshaller{
        val marshaller5 = Jaxb2Marshaller()
        marshaller5.contextPath = "core.qualitas.wsdl.dev"
        return marshaller5
    }

    @Bean
    fun clientDev(marshaller5: Jaxb2Marshaller): CollectionDevConnection {
        val clientdev = CollectionDevConnection()
        clientdev.marshaller = marshaller5
        clientdev.unmarshaller = marshaller5
        clientdev.defaultUri = "http://pagosqa.qualitas.com.mx/ws/wsCollection.php?WSDL"
        return clientdev
    }
}