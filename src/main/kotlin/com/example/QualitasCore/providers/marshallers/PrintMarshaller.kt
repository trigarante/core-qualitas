package com.example.QualitasCore.providers.marshallers

import com.example.QualitasCore.providers.PrintConnection
import com.example.QualitasCore.providers.PrintGoConnection
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.oxm.jaxb.Jaxb2Marshaller

@Configuration
class PrintMarshaller {

    @Bean
    fun marshaller2() : Jaxb2Marshaller{
        val marshal = Jaxb2Marshaller()
        marshal.contextPath = "core.qualitas.wsdl.prod"
        return  marshal
    }

    @Bean
    fun imprimeGoClient(marshaller: Jaxb2Marshaller?) : PrintGoConnection {
        val client : PrintGoConnection = PrintGoConnection()
        client.defaultUri = "http://qbcenter.qualitas.com.mx/QBCImpresion/Service.asmx"
        client.marshaller = marshaller
        client.unmarshaller = marshaller
        return client
    }

    @Bean
   fun imprimeAhorraClient(marshaller: Jaxb2Marshaller?): PrintConnection {
        val client = PrintConnection()
        client.defaultUri = "http://qbcenter.qualitas.com.mx/QBCImpresion/Service.asmx"
        client.marshaller = marshaller
        client.unmarshaller = marshaller
        return client
   }
}