package com.example.QualitasCore.providers.marshallers

import com.example.QualitasCore.providers.connection.prod.ExtractMarshalController
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.oxm.jaxb.Jaxb2Marshaller

@Configuration
class ExtractConfiguration {

    @Bean
    fun marshaller7(): Jaxb2Marshaller{
        var marshaller7 = Jaxb2Marshaller()
        marshaller7.contextPath = "core.qualitas.wsdl.ext"
        return marshaller7
    }

    @Bean
    fun extractClient(marshaller7: Jaxb2Marshaller): ExtractMarshalController {
        val client = ExtractMarshalController()
        client.marshaller = marshaller7
        client.unmarshaller = marshaller7
        client.defaultUri = "http://wfcgrupoestrategas.azurewebsites.net/Service1.svc?wsdl"
        return client
    }
}