package com.example.QualitasCore.configuration

import com.example.QualitasCore.models.ApiError
import com.example.QualitasCore.models.ExceptionModel
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import javax.persistence.EntityNotFoundException


@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
/***
 *  CLASE QUE MANEJA LAS EXEPCIONES DE UNA MANERA PERSONALIZADA
 */
class RestExceptionHandler : ResponseEntityExceptionHandler() {
    override protected fun handleHttpMessageNotReadable(
        ex: HttpMessageNotReadableException,
        headers: HttpHeaders,
        status: HttpStatus,
        request: WebRequest
    ): ResponseEntity<Any?> {
        val apiError = ApiError(HttpStatus.BAD_REQUEST)
        apiError.error = "Bad Request"
        apiError.message = ex.message!!.substringBefore(";")
        return buildResponseEntity(apiError)
    }
    //  CONTROLA LA EXEPCION NOT_FOUND GENERALMENTE ES UN BAD REQUEST
    @ExceptionHandler(EntityNotFoundException::class)
    protected fun handleEntityNotFound(
        ex: EntityNotFoundException
    ): ResponseEntity<Any?>? {
        val apiError = ApiError(HttpStatus.NOT_FOUND)
        apiError.message = ex.message
        return buildResponseEntity(apiError)
    }
    //  MANEJA LAS EXEPCIONES EN GENERAL (TODAS)
    @ExceptionHandler(Exception::class)
    protected fun handleException(
        ex:Exception
    ): ResponseEntity<Any?>?{
        ex.printStackTrace()
        val apiError = ApiError(HttpStatus.INTERNAL_SERVER_ERROR)
        apiError.error = "Undefined error"
        apiError.message = ex.message
        return buildResponseEntity(apiError)
    }
    //  MANEJA LAS EXEPCIONES PESONALIZADAS
    @ExceptionHandler(ExceptionModel::class)
    protected fun customException(
        ex: ExceptionModel
    ): ResponseEntity<Any?>?{
        val apiError = ApiError(HttpStatus.INTERNAL_SERVER_ERROR)
        apiError.error = ex.error
        apiError.message = ex.message
        return buildResponseEntity(apiError)
    }
    //  ENVIA LA EXEPCION YA MANEJADA
    private fun buildResponseEntity(apiError: ApiError): ResponseEntity<Any?> {
        return ResponseEntity(apiError, apiError.status!!)
    } //other exception handlers below
}