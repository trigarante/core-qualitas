package com.example.QualitasCore.configuration

import com.example.QualitasCore.QualitasCoreApplication
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.service.Tag
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2
import java.util.*

@Configuration
@EnableSwagger2
class SwaggerConfiguration {

    fun swagerInvoice() =
            Docket(DocumentationType.SWAGGER_2)
                    .select()
                    .apis(RequestHandlerSelectors.basePackage("com.example.QualitasCore"))
                    .paths(PathSelectors.ant("com/example/QualitasCore/*"))
                    .build()


    fun getApiInfo(): ApiInfo {
        return ApiInfo(
                "Service API Qualitas",
                "Service API Description Qualitas",
                "1.0",
                " it doesn`t have",
                Contact("Codmind", "https://codmind.com", "emedina@trigarante.com, jgarcia@trigarante.com"),
                "LICENSE, it doesn`t have",
                "LICENSE URL , it doesn`t have",
                Collections.emptyList()
        )
    }
}