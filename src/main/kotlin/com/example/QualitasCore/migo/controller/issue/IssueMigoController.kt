package com.example.QualitasCore.migo.controller.issue

import com.example.QualitasCore.migo.service.issue.MigoIssueInterface
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("v1/qualitas-migo")
@Api("Issue Service", tags = arrayOf("Migo"))
class IssueMigoController(
    @Autowired private val migoIssueInterface: MigoIssueInterface
) {
    @ApiOperation("Migo's Issue Service")
    @PostMapping("issue-policy")
    fun issue(@RequestBody generalResponse: GeneralResponse): ResponseEntity<Any>{
        return try {
            val emision = migoIssueInterface.issue(generalResponse)
            ResponseEntity(emision, HttpStatus.OK)
        }catch (e: Exception){
            ResponseEntity("Issue service error: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }

    @ApiOperation("Migo's Payment Service")
    @PostMapping("payment")
    fun payment(@RequestBody generalResponse: GeneralResponse): ResponseEntity<Any>{
        val pago = migoIssueInterface.payment(generalResponse)
        return ResponseEntity(pago, HttpStatus.OK)
    }
}