package com.example.QualitasCore.migo.controller.quotation

import com.example.QualitasCore.migo.service.quotation.MigoQuotationInterface
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("v1/qualitas-migo")
@Api("Quotation Service", tags = arrayOf("Migo"))
class QuotationMigoController(
    @Autowired private var migoQuotationInterface: MigoQuotationInterface
){

    @ApiOperation("Migo's quotation Service")
    @PostMapping("quote")
    fun quotation(@RequestBody request: QuotationRequest): ResponseEntity<Any> {
        return try {
            when (request.paquete.uppercase()) {
                "ALL" -> {
                    val cotizacion = migoQuotationInterface.quotation(request)
                    ResponseEntity(cotizacion, HttpStatus.OK)
                }
                else -> {
                    val cotizacion = migoQuotationInterface.quotation(request, request.periodicidadPago.toString(),request.paquete)
                    ResponseEntity(cotizacion, HttpStatus.OK)
                }
            }
        } catch (e: Exception) {
            ResponseEntity("Error en cotizacion migo: ${e.message}", HttpStatus.BAD_REQUEST)
        }
    }
}