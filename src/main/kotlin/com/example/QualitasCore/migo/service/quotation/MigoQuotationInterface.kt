package com.example.QualitasCore.migo.service.quotation

import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll

interface MigoQuotationInterface {

    fun quotation(quotationRequest: QuotationRequest, payment: String, paquete: String): GeneralResponse

    fun quotation(quotationRequest: QuotationRequest): ResponseAll
}