package com.example.QualitasCore.migo.service.quotation

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationInterface
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.extensions.XMLMigoService
import com.example.QualitasCore.models.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class MigoQuotationService(
    @Autowired private var migoXmlQuotationService: XMLMigoService,
    @Autowired private var basicQuotationInterface: BasicQuotationInterface,
    @Autowired private var all: AllBasicQuotationInterface
) : MigoQuotationInterface {

    /**
     * The quotation function receives a quotationRequest object and a payment string, creates an XML file from the
     * quotationRequest object, calls the requestQuotation method of the basicQuotationInterface object, and returns the
     * result
     *
     * @param quotationRequest This is the object that contains the information of the quotation.
     * @param payment The payment method.
     * @return A GeneralResponse object.
     */
    override fun quotation(quotationRequest: QuotationRequest, paymentFrequency: String, paquete: String): GeneralResponse {
        val generalResponse = GeneralResponse()
        /* Creating an XML file from the quotationRequest object. */
        var xml = ""
        try {
            when (quotationRequest.servicio.service) {
                "PARTICULAR" -> {
                    xml = migoXmlQuotationService.quoteParticular(quotationRequest, paymentFrequency, paquete)
                    quotationRequest.aseguradora = "migo-particular"
                }

                "APP" -> {
                    xml = migoXmlQuotationService.quotationUber(quotationRequest, paymentFrequency, paquete)
                    quotationRequest.aseguradora = "migo-uber"
                }

                "APPPLUS" -> {
                    xml = migoXmlQuotationService.quotationUberPlus(quotationRequest, paymentFrequency, paquete)
                    quotationRequest.aseguradora = "migo-uber"
                }
            }
            /* Calling the requestQuotation method of the basicQuotationInterface object. */
            var base = basicQuotationInterface.requestQuotation(quotationRequest, generalResponse, xml)
            if (base.codigoError.contains("Alto Riesgo")) {
                when (quotationRequest.servicio.service) {
                    "PARTICULAR" -> {
                        xml = migoXmlQuotationService.quoteParticular(
                            quotationRequest,
                            paymentFrequency + "RIESGO",
                            paquete
                        )
                        quotationRequest.aseguradora = "migo-particular"
                    }

                    "APP" -> {
                        xml = migoXmlQuotationService.quotationUber(
                            quotationRequest,
                            paymentFrequency + "RIESGO",
                            paquete
                        )
                        quotationRequest.aseguradora = "migo-uber"
                    }

                    "APPPLUS" -> {
                        xml = migoXmlQuotationService.quotationUberPlus(
                            quotationRequest,
                            paymentFrequency + "RIESGO",
                            paquete
                        )
                        quotationRequest.aseguradora = "migo-uber"
                    }
                }
                base = basicQuotationInterface.requestQuotation(quotationRequest, generalResponse, xml)
                base.codigoError = "Cotizacion con parametros de Alto Riesgo"
            }

            return base
        }
        catch (e: IndexOutOfBoundsException){
            generalResponse.cliente.direccion.codPostal = quotationRequest.cp
            generalResponse.cliente.edad = quotationRequest.edad

            generalResponse.vehiculo.clave = quotationRequest.clave
            generalResponse.vehiculo.servicio = quotationRequest.servicio
            generalResponse.vehiculo.modelo = quotationRequest.modelo
            generalResponse.vehiculo.marca = quotationRequest.marca
            generalResponse.vehiculo.descripcion = quotationRequest.descripcion

            generalResponse.descuento = quotationRequest.descuento.toString()
            generalResponse.paquete = quotationRequest.paquete
            generalResponse.codigoError = "Codigo postal invalido"
            return generalResponse
        }
    }

    /**
     * It returns a ResponseAll object.
     *
     * @param quotationRequest QuotationRequest
     * @return A ResponseAll object
     */
    override fun quotation(quotationRequest: QuotationRequest): ResponseAll {
        return all.quotation(::quotation, quotationRequest)
    }
}