package com.example.QualitasCore.migo.service.issue

import com.example.QualitasCore.models.GeneralResponse

interface MigoIssueInterface {

    fun issue(generalResponse: GeneralResponse): GeneralResponse

    fun payment(request: GeneralResponse): GeneralResponse
}