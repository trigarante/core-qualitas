package com.example.QualitasCore.migo.service.issue

import com.example.QualitasCore.businessRules.basic.BasicIssueInterface
import com.example.QualitasCore.businessRules.basic.BasicPaymentService
import com.example.QualitasCore.extensions.XMLMigoService
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import com.example.QualitasCore.Repository.cobros.logCollection_Repository

@Service
class MigoIssueService(
    @Autowired private var migoXmlIssueService: XMLMigoService,
    @Autowired private var basicIssueInterface: BasicIssueInterface
): MigoIssueInterface {

    @Autowired
    private lateinit var log: logCollection_Repository

    @Autowired
    private lateinit var pay: BasicPaymentService

    override fun issue(generalResponse: GeneralResponse): GeneralResponse {
        generalResponse.aseguradora = "migo-${generalResponse.vehiculo.servicio.service}"
        val xml = migoXmlIssueService.issueAutos(generalResponse)
        val credencials = when(generalResponse.vehiculo.servicio.service){
            "PARTICULAR" -> "particularMigo"
            "APP", "APPPLUS" -> "uberMigo"
            else -> "particularMigo"
        }
        return basicIssueInterface.responseIssue(xml, generalResponse, credencials)
    }

    override fun payment(request:GeneralResponse): GeneralResponse{
        val xml: String
        try{
            xml = migoXmlIssueService.cobroAutos(request)
        }
        catch(e: IndexOutOfBoundsException){
            val error = ExceptionModel()
            error.error = "XML error"
            error.message= "Banco invalido"
            try{
                log.insertCobro(
                    request = JSONObject(request).toString(),
                    request_aseguradora = "",
                    response_aseguradora = "",
                    response = JSONObject(error).toString(),
                    poliza = request.emision.poliza,
                    estatus_cobro = "false",
                    negocio = "MIGO_COBRO"
                )
            }
            catch (e: Exception){
                e.printStackTrace()
            }
            throw error
        }
        val credencials = when(request.vehiculo.servicio.service){
            "PARTICULAR" -> "particularMigo"
            "APP", "APPPLUS" -> "uberMigo"
            else -> "particularMigo"
        }
        return pay.pagar(request, xml, credencials)
    }
}