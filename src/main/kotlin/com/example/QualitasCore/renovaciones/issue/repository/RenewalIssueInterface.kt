package com.example.QualitasCore.renovaciones.issue.repository

import com.example.QualitasCore.models.GeneralResponse

@FunctionalInterface
interface RenewalIssueInterface {

    abstract fun responseRenewalIssue(xml: String, generalResponse: GeneralResponse, credentials: String = ""): GeneralResponse
}