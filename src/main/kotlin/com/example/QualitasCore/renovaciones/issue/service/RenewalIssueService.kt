package com.example.QualitasCore.renovaciones.issue.service

import com.example.QualitasCore.businessRules.basic.BasicIssueInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.extensions.XMLRenewalService
import com.example.QualitasCore.trigarante.repository.LogEmisionRepository
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.renovaciones.issue.repository.RenewalIssueInterface
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Service
class RenewalIssueService(
    @Value("\${renewal.negocio}") val negocio: String = "",
    @Value("\${renewal.agente}") val agente: String = ""
) {

    @Autowired
    lateinit var controlQualitas: RenewalIssueInterface

    @Autowired
    lateinit var xml: XMLRenewalService

    @Autowired
    lateinit var log: LogEmisionRepository

    @Value("\${jwt.ambiente}")
    val ambiente: String = ""

    fun emisiones(responseWs: GeneralResponse): GeneralResponse {
        var request = responseWs
        var xmlEmision = ""
        try{
            xmlEmision = xml.issueAutos(responseWs)
        }catch (e: IndexOutOfBoundsException){
            val error = ExceptionModel()
            error.error = "XML Creation"
            error.message = "No se encontro la colonia ${responseWs.cliente.direccion.colonia}"
            log.insertarLog(
                requestJson = JSONObject(request).toString(),
                requestXml = xmlEmision,
                responseXml = "",
                responseJson = JSONObject(responseWs).toString(),
                fecha = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE).toString(),
                emisionTerminada = "false",
                codigoError = responseWs.codigoError,
                poliza = "",
                servicio = "trigarante",
                ambiente = ambiente,
                idCotizacion = responseWs.cotizacion.idCotizacion
            )
            throw  error
        }
        catch (e:Exception){
            val error = ExceptionModel()
            error.error = "XML Creation"
            error.message = e.message!!
            throw error
        }
        responseWs.aseguradora = "trigarante-${responseWs.vehiculo.servicio.service}"
        val response = controlQualitas.responseRenewalIssue(xmlEmision, responseWs, "renovaciones")
        return response
    }
}