package com.example.QualitasCore.renovaciones.issue.controller

import com.example.QualitasCore.renovaciones.issue.service.RenewalIssueService
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.GeneralResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*


@RestController
@CrossOrigin("*")
@RequestMapping("v1/qualitas-car/renewal")
@Api("V1 Renewal", tags = arrayOf("v2RenewalOperation"))
class RenewalControllerIssue {

    @Autowired
    lateinit var emisiones: RenewalIssueService

    @ApiOperation("Intern's issue service")
    @PostMapping(value = ["issue"])
    fun getEmision(@RequestBody responseWs: GeneralResponse): ResponseEntity<Any> {
        val base = emisiones.emisiones(responseWs)
        return if (base.emision.resultado)
            ResponseEntity(base, HttpStatus.OK)
        else {
            ResponseEntity(base, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}