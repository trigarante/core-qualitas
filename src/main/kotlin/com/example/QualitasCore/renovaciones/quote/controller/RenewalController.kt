package com.example.QualitasCore.renovaciones.quote.controller

import com.example.QualitasCore.intern.service.emisiones.emisiones_ahorra_service
import com.example.QualitasCore.renovaciones.quote.repository.RenewalQuoteInterface
import com.example.QualitasCore.models.ExceptionModel
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin("*")
@RequestMapping("v1/qualitas-car/renewal")
@Api("V1 Renewal", tags = arrayOf("v2RenewalOperation"))
class RenewalController {

    @Autowired
    lateinit var quotation: RenewalQuoteInterface

    @Autowired
    lateinit var emisiones: emisiones_ahorra_service

    @ApiOperation("Intern's Quotation Service")
    @PostMapping("quote")
    fun getCotizacion(@RequestBody request: QuotationRequest): ResponseEntity<Any> {

        if (request.descuento > 40) {
            val response = ExceptionModel(error = "Quotation error", message = "El descuento no puede exceder mas del 40%")
            throw response
        }

        return when (request.paquete) {
            "ALL" -> {
                val base = quotation.renewalQuoteAll(request)

                if (base.codigoError == "" && base.codigoError != "[Cotizacion con parametros de Alto Riesgo]"){
                    ResponseEntity(base, HttpStatus.OK)}
                else {
                    val response = ExceptionModel(error = "Quotation error", message = base.codigoError.toString())
                    throw response
                }
            }

            else -> {
                val base = quotation.renewalQuote(request, "", request.paquete)
                if (base.codigoError != "") {
                    return ResponseEntity(base, HttpStatus.INTERNAL_SERVER_ERROR)
                }
                ResponseEntity(base, HttpStatus.OK)
            }
        }
    }
}