package com.example.QualitasCore.renovaciones.quote.repository

import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll
import org.json.JSONObject

@FunctionalInterface
abstract interface BasicRenewalQuoteInterface {
    fun requestQuotation(request: QuotationRequest, response: GeneralResponse, xml: String): GeneralResponse

    fun requestQuotationCustom(request: RequestCustom, response: GeneralResponse, xml: String, paymentFrecuency: String): GeneralResponse
}
