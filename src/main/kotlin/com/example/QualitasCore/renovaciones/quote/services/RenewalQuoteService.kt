package com.example.QualitasCore.renovaciones.quote.services

import com.example.QualitasCore.extensions.XMLRenewalService
import com.example.QualitasCore.renovaciones.quote.repository.RenewalQuoteInterface
import com.example.QualitasCore.models.*
import com.example.QualitasCore.renovaciones.quote.repository.AllRenewalQuoteInterface
import com.example.QualitasCore.renovaciones.quote.repository.BasicRenewalQuoteInterface
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RenewalQuoteService(
    @Autowired private var xml: XMLRenewalService,
    @Autowired private var basicQuotationInterface: BasicRenewalQuoteInterface,
    @Autowired private val all: AllRenewalQuoteInterface
): RenewalQuoteInterface {

    override fun quotation(request:QuotationRequest, paymentFrecuency: String, paquete: String): GeneralResponse{
        val response = GeneralResponse()
        var xmlCotizacion: String
        try {
            when (request.servicio.service) {
                "PARTICULAR" -> {
                    request.aseguradora = "ahorra-particular"
                    xmlCotizacion = xml.quoteParticular(request, paymentFrecuency, paquete)
                }

                "APP" -> {
                    request.aseguradora = "ahorra-uber"
                    xmlCotizacion = xml.quotationUber(request, paymentFrecuency, paquete)
                }

                "APPPLUS" -> {
                    request.aseguradora = "ahorra-uber"
                    xmlCotizacion = xml.quotationUberPlus(request, paymentFrecuency, paquete)
                }

                else -> {
                    request.aseguradora = "ahorra-particular"
                    xmlCotizacion = xml.quoteParticular(request, paymentFrecuency, paquete)
                }
            }


            var base = basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
            if (base.codigoError.contains("Alto Riesgo")) {
                when (request.servicio.service) {
                    "PARTICULAR" -> {
                        request.aseguradora = "ahorra-particular"
                        xmlCotizacion = xml.quoteParticular(request, paymentFrecuency + "RIESGO", paquete)
                    }

                    "APP" -> {
                        request.aseguradora = "ahorra-uber"
                        xmlCotizacion = xml.quotationUber(request, paymentFrecuency + "RIESGO", paquete)
                    }

                    "APPPLUS" -> {
                        request.aseguradora = "ahorra-uber"
                        xmlCotizacion = xml.quotationUberPlus(request, paymentFrecuency + "RIESGO", paquete)
                    }

                    else -> {
                        request.aseguradora = "ahorra-particular"
                        xmlCotizacion = xml.quoteParticular(request, paymentFrecuency + "RIESGO", paquete)
                    }
                }
                base = basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
                base.codigoError = "Cotizacion con parametros de Alto Riesgo"
            }
            return base
        }
        catch(e: IndexOutOfBoundsException){
            response.codigoError = "XML Creation error: No se encontro el codigo postal: ${request.cp}"
            return response
        }
    }

    override fun renewalQuoteAll(request: QuotationRequest): ResponseAll {
        return all.quotation(::quotation,request)
    }

    override fun renewalQuote(request:QuotationRequest, paymentFrecuency: String, paquete: String): GeneralResponse{
        val response = GeneralResponse()
        try {
            var xmlCotizacion: String
            when (request.servicio.service) {
                "PARTICULAR" -> {
                    request.aseguradora = "ahorra-particular"
                    xmlCotizacion = xml.quoteParticular(request, paymentFrecuency, paquete)
                }

                "APP" -> {
                    request.aseguradora = "ahorra-uber"
                    xmlCotizacion = xml.quotationUber(request, paymentFrecuency, paquete)
                }

                "APPPLUS" -> {
                    request.aseguradora = "ahorra-uber"
                    xmlCotizacion = xml.quotationUberPlus(request, paymentFrecuency, paquete)
                }

                else -> {
                    request.aseguradora = "ahorra-particular"
                    xmlCotizacion = xml.quoteParticular(request, paymentFrecuency, paquete)
                }
            }
            //  VALIDACION PARA VERIFICAR SI LA COTIZACION ES DE ALTO RIESG0
            var base = basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
            if (base.codigoError.contains("Alto Riesgo")) {
                response.codigoError = ""
                when (request.servicio.service) {
                    "PARTICULAR" -> {
                        request.aseguradora = "ahorra-particular"
                        xmlCotizacion = xml.quoteParticular(request, paymentFrecuency + "RIESGO", paquete)
                    }

                    "APP" -> {
                        request.aseguradora = "ahorra-uber"
                        xmlCotizacion = xml.quotationUber(request, paymentFrecuency + "RIESGO", paquete)
                    }

                    "APPPLUS" -> {
                        request.aseguradora = "ahorra-uber"
                        xmlCotizacion = xml.quotationUberPlus(request, paymentFrecuency + "RIESGO", paquete)
                    }

                    else -> {
                        request.aseguradora = "ahorra-particular"
                        xmlCotizacion = xml.quoteParticular(request, paymentFrecuency + "RIESGO", paquete)
                    }
                }
                base = basicQuotationInterface.requestQuotation(request, response, xmlCotizacion)
                if (base.codigoError != "") {
                    val error = ExceptionModel()
                    error.error = "Quotation error"
                    error.message = base.codigoError
                    throw error
                }
                base.codigoError = "[Cotizacion con parametros de Alto Riesgo]"
                return base
            }
            if (base.codigoError != "") {
                val error = ExceptionModel()
                error.error = "Quotation error"
                error.message = base.codigoError
                throw error
            }
            return base
        }
        catch (e: IndexOutOfBoundsException){
            val error = ExceptionModel(error = "Catalog Error", message = "Codigo Postal invalido")
            throw error
        }
    }
}