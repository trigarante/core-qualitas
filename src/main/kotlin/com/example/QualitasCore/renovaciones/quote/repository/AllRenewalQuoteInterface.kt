package com.example.QualitasCore.renovaciones.quote.repository

import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll
import org.springframework.stereotype.Repository


interface AllRenewalQuoteInterface {

    fun quotation(lambd:(QuotationRequest, String, String) -> GeneralResponse,request: QuotationRequest): ResponseAll
}