package com.example.QualitasCore.renovaciones.quote.services

import com.example.QualitasCore.models.*
import com.example.QualitasCore.renovaciones.quote.repository.AllRenewalQuoteInterface
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Service
class AllRenewalQuoteService: AllRenewalQuoteInterface {

    override fun quotation(lambd:(QuotationRequest, String, String) -> GeneralResponse,request: QuotationRequest): ResponseAll {
        //  VARIABLE DONDE SE VAN A ALMACENAR TODAS LAS COTIZACIONES
        val all: HashMap<String, GeneralResponse> = HashMap()
        val responseAll = ResponseAll()
        //  LLENADO DE LOS DATOS BASICOS DE LA COTIZACION
        responseAll.aseguradora = "Qualitas"
        responseAll.vehiculo.clave = request.clave.substringBefore("-")
        responseAll.vehiculo.marca = request.marca
        responseAll.vehiculo.modelo = request.modelo
        responseAll.vehiculo.subMarca = request.descripcion
        responseAll.vehiculo.descripcion = request.descripcion
        responseAll.vehiculo.servicio = request.servicio
        responseAll.descuento = request.descuento.toString()
        responseAll.cliente.direccion.codPostal = request.cp
        responseAll.cliente.edad = request.edad
        responseAll.cliente.fechaNacimiento = request.fechaNacimiento
        responseAll.cliente.genero = request.genero
        responseAll.paquete = request.paquete

        //  DATOS NECESARIOS PARA EL USO DE LOS HILOS
        val collections = arrayOf("ANUAL", "SEMESTRAL", "TRIMESTRAL")
        val packs = arrayOf("AMPLIA", "LIMITADA", "RC")
        val errors: MutableList<String> = mutableListOf()
        //  CREA LA CANTIDAD DE HILOS NECESARIOS PARA COTIZAR TODOS LOS PAQUETES Y PERIODICIDADES
        val threads: MutableList<Thread> = mutableListOf()
        for (collect in collections) {
            for (pack in packs) {
                threads.add(
                    Thread{
                        val requestQuote = request
                        requestQuote.paquete = pack
                        //  EJECUTA EL METODO ENVIADO COMO lambd PARA REALIZAR LA COTIZACION
                        all[pack+collect] = lambd(requestQuote, collect, pack)
                        //  VERIFICA SI HAY CODIGO DE ERROR, Y SI EXISTE LO ALMACENA EN LA LISTA DE ERRORES
                        if(!all[pack+collect]!!.codigoError.isEmpty()) errors.add("[${all[pack+collect]!!.codigoError}]")
                    }
                )
            }
        }
        //  INICIA LOS PROCESOS DE LOS HILOS
        threads.forEach {
            it.start()
        }
        //  ESPERA HASTA QUE RESPONDAN TODOS LOS HILOS PARA CONTINUAR EL PROCESO
        threads.forEach {
            it.join()
        }

        var error = ""
        //  ELIMINA LOS ERRORES DUPLICADOS
        errors.forEach {
            if(!error.contains(it))
                error += it
        }
        responseAll.codigoError = error

        //  VALIDA LOS ERRORES PARA VERIFICAR QUE SE PUEDA CONTINUAR EL PROCESO
        if(error != "" && error != "[Cotizacion con parametros de Alto Riesgo]") {
            return responseAll
        }
        //  OBTIENE Y MAPEA CORRECTAMENTE LOS DATOS DE LA COTIZACION
        val cotizacionAmplia = CotizacionAmplia(
            mutableListOf(
                PeriocidadPago(
                    anual = if(all.containsKey("AMPLIA"+"ANUAL"))all["AMPLIA"+"ANUAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false),
                    semestral = if(all.containsKey("AMPLIA"+"SEMESTRAL"))all["AMPLIA"+"SEMESTRAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false),
                    trimestral = if(all.containsKey("AMPLIA"+"TRIMESTRAL"))all["AMPLIA"+"TRIMESTRAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false)
                )
            )
        )
        val cotizacionLimitada = CotizacionLimitada(
            mutableListOf(
                PeriocidadPago(
                    anual = if(all.containsKey("LIMITADA"+"ANUAL"))all["LIMITADA"+"ANUAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false),
                    semestral = if(all.containsKey("LIMITADA"+"SEMESTRAL"))all["LIMITADA"+"SEMESTRAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false),
                    trimestral = if(all.containsKey("LIMITADA"+"TRIMESTRAL"))all["LIMITADA"+"TRIMESTRAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false)
                )
            )
        )
        val cotizacionRc = CotizacionRc(
            mutableListOf(
                PeriocidadPago(
                    anual = if(all.containsKey("RC"+"ANUAL"))all["RC"+"ANUAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false),
                    semestral = if(all.containsKey("RC"+"SEMESTRAL"))all["RC"+"SEMESTRAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false),
                    trimestral = if(all.containsKey("RC"+"TRIMESTRAL"))all["RC"+"TRIMESTRAL"]!!.cotizacion else Cotizacion("","","","","","","","","","","","",false)
                )
            )
        )
        //  OBTIENE Y GUARDA EN EL RESPONSE LAS COBERTURAS
        val amplia = Amplia()
        amplia.amplia = if(all.containsKey("AMPLIA"+"ANUAL")) all["AMPLIA"+"ANUAL"]!!.coberturas.toMutableList() else mutableListOf()
        val limitada = Limitada()
        limitada.limitada = if(all.containsKey("LIMITADA"+"ANUAL")) all["LIMITADA"+"ANUAL"]!!.coberturas.toMutableList() else mutableListOf()
        val rc = Rc()
        rc.rc = if(all.containsKey("RC"+"ANUAL")) all["RC"+"ANUAL"]!!.coberturas.toMutableList() else mutableListOf()
        responseAll.coberturas.add(amplia)
        responseAll.coberturas.add(limitada)
        responseAll.coberturas.add(rc)
        //  ASIGNA LAS COTIZACIONES AL RESPONSE
        responseAll.cotizacion.add(cotizacionAmplia)
        responseAll.cotizacion.add(cotizacionLimitada)
        responseAll.cotizacion.add(cotizacionRc)
        responseAll.emision.inicioVigencia = LocalDate.now().plusDays(1).format(DateTimeFormatter.ISO_DATE).toString()
        //  VACIA LOS DATOS DE LAS COTIZACIONES
        all.clear()
        return responseAll
    }
}