package com.example.QualitasCore.renovaciones.quote.repository

import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import com.example.QualitasCore.models.ResponseAll

abstract interface RenewalQuoteInterface {
    fun quotation(request:QuotationRequest, paymentFrecuency: String, paquete: String): GeneralResponse

    fun renewalQuoteAll(request: QuotationRequest): ResponseAll

    fun renewalQuote(request:QuotationRequest, paymentFrecuency: String, paquete: String): GeneralResponse
}