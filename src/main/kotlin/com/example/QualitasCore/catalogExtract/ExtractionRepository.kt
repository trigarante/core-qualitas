package com.example.QualitasCore.catalogExtract

import com.example.QualitasCore.models.catalog.CatalogModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface ExtractionRepository: JpaRepository<CatalogModel, String>{

    @Modifying
    @Transactional
    @Query("insert into extraction(marca, submarca, modelo, descripcion, clave, valor1, valor2, categoria) values (:marca, :submarca, :modelo, :descripcion, :clave, :valor1, :valor2, :categoria)", nativeQuery = true)
    fun insert(marca: String, submarca: String, modelo: String, descripcion: String, clave: String, valor1: String, valor2: String, categoria: String)

    @Modifying
    @Transactional
    @Query("DELETE FROM extraction", nativeQuery = true)
    fun delete()

    @Modifying
    @Transactional
    @Query("CALL create_concentrado()", nativeQuery = true)
    fun deploy()
}