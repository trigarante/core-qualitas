package com.example.QualitasCore.catalogExtract

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import springfox.documentation.annotations.ApiIgnore

@RestController
@RequestMapping("ext")
@ApiIgnore
class ExtractionController(
    @Autowired private val ext: ExtractionService
){

    @GetMapping("cat")
    fun extCat():ResponseEntity<Any>{
        return try{
            ResponseEntity(ext.ext(),HttpStatus.OK)
        }catch (e:Exception){
            ResponseEntity("Catalog Extraction Error: ${e.message}",HttpStatus.BAD_REQUEST)
        }
    }
    //  EL FORMATO DEL CRON ES SEGUNDO MINUTO HORA DIA MES AÑO
    @Scheduled(cron = "0 0 0 1 * *")
    fun extCatProgam(){
        ext.ext()
        println("Se actualizaron los catalogos favor de actualizar el comparador")
    }
}