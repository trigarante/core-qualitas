package com.example.QualitasCore.catalogExtract

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import core.qualitas.wsdl.cat.ListaMarcas
import core.qualitas.wsdl.cat.ListaMarcasResponse
import core.qualitas.wsdl.cat.ListaTarifas
import core.qualitas.wsdl.cat.ListaTarifasResponse
import core.qualitas.wsdl.cat.ObjectFactory
import org.json.JSONObject
import org.json.XML
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.ws.client.core.support.WebServiceGatewaySupport
import org.springframework.ws.config.WebServicesNamespaceHandler
import org.springframework.ws.soap.client.core.SoapActionCallback
import java.time.LocalDate
import javax.xml.bind.JAXBElement

class ExtractionService : WebServiceGatewaySupport() {
    @Value("\${jwt.ambiente}")
    val ambiente : String = ""
    @Autowired
    private lateinit var extractionRepository: ExtractionRepository
    private fun marcas(): ListaMarcasResponse {
        val listaMarcas = ListaMarcas()
        listaMarcas.cTarifa = "linea"
        listaMarcas.cUsuario = "linea"

//        val obj = ObjectFactory()
//        val request = obj.createListaMarcas()
//        request.cTarifa = "linea"
//        request.cUsuario = "linea"

        val response = getWebServiceTemplate().marshalSendAndReceive(
            "https://qbcenter.qualitas.com.mx/wsTarifa/wsTarifa.asmx",
            listaMarcas,
            SoapActionCallback("http://tempuri.org/WSQBC/QBCDE/listaMarcas")
        ) as ListaMarcasResponse
        return response
    }

    private fun tarifas(marca: String, modelo: String, categoria: String): ListaTarifasResponse {
        val tarifa = ListaTarifas()
        tarifa.cTarifa = "linea"
        tarifa.cUsuario = "linea"
        tarifa.cMarca = marca
        tarifa.cCategoria = categoria
        tarifa.cModelo = modelo

        val response = getWebServiceTemplate().marshalSendAndReceive(
            "https://qbcenter.qualitas.com.mx/wsTarifa/wsTarifa.asmx?wsdl",
            tarifa,
            SoapActionCallback("http://tempuri.org/WSQBC/QBCDE/listaTarifas")
        ) as ListaTarifasResponse

        return response
    }

    //  LISTA DE CATEGORIAS MAPEADAS PARA SU CLASIFICACION DEPENDIENDO DE SU USO
    private val cat: Array<String> = arrayOf("105","100","104", "103","132","200","210","102","135","220","221","230","231","111","250","251","252","206","207","400", "405","205","222","209","213","225","208","216","223", "214")

    /** ANTES DE EJECUTAR LA EXTRACCION SE DEBE VACIAR LA TABLA DE extraccion PARA EVITAR DUPLICADOS
     *  DESPUES DE REALIZAR LA EXTRACCION SE TIENE QUE EJECUTAR fill_categoria PARA ACTUALIZAR LA
     *  TABLA DE CATEGORIAS Y SE AGREGEN LOS NUEVOS VEHICULOS
     */
    fun ext(): String {
        println("ARRANCA SERVICIO DE EXTRACCION DE CATALOGOS")
        if(ambiente == "1") {
            extractionRepository.delete()
            var xmlMarcas = ""
            var xmlTarifa = ""
            val content = marcas().listaMarcasResult.content
            val marcasMap = XmlMapper()
            for (m in 0 until content.size) {
                marcasMap.enable(SerializationFeature.INDENT_OUTPUT)
                xmlMarcas = marcasMap.writeValueAsString(content[m])
            }
            val marcasx = xmlMarcas.replace("&lt;", "<").replace("&gt;", ">").replace("?", "")
            val marcasjson = XML.toJSONObject(marcasx)
            val meh = marcasjson.getJSONObject("ElementNSImpl").getJSONObject("salida").getJSONObject("datos")
                .getJSONArray("Elemento")
            for (m in 0 until meh.length()) {
                val marca = meh.getJSONObject(m).get("cMarcaLarga").toString()
                for (model in 1979 until LocalDate.now().plusYears(2).year)  {
                    for (categoria in 0 until cat.size) {
                        try {
                            val tarifa = tarifas(marca, model.toString(), cat[categoria]).listaTarifasResult.content
                            val tarifMap = XmlMapper()
                            for (t in 0 until tarifa.size) {
                                tarifMap.enable(SerializationFeature.INDENT_OUTPUT)
                                xmlTarifa = tarifMap.writeValueAsString(tarifa[t])
                            }
                            val tarifas = xmlTarifa.replace("&lt;", "<").replace("&gt;", ">").replace("?", "")
                            val tarifaJson = XML.toJSONObject(tarifas)
                            if (tarifaJson.toString()
                                    .contains("No se encontraron elementos para el criterio de selección especificado")
                            ) {
                                println("No se encontro vehiculo con los datos marca: ${marca}, modelo ${model.toString()}, categoria: ${cat[categoria]}")
                            } else {
                                val nah =
                                    tarifaJson.getJSONObject("ElementNSImpl").getJSONObject("salida")
                                        .getJSONObject("datos")
                                if (nah.toString().contains("\"Elemento\":[") || nah.toString()
                                        .contains("\"Elemento\": [")
                                ) {
                                    val completo = nah.getJSONArray("Elemento")
                                    for (d in 0 until completo.length()) {
                                        extractionRepository.insert(
                                            marca,
                                            completo.getJSONObject(d).get("cTipo").toString(),
                                            model.toString(),
                                            completo.getJSONObject(d).get("cVersion").toString(),
                                            completo.getJSONObject(d).get("CAMIS").toString(),
                                            completo.getJSONObject(d).get("nV1").toString(),
                                            completo.getJSONObject(d).get("nV2").toString(),
                                            cat[categoria]
                                        )
                                    }
                                } else {
                                    println(nah.toString())
                                    val completo = nah.getJSONObject("Elemento")
                                    extractionRepository.insert(
                                        marca,
                                        completo.get("cTipo").toString(),
                                        model.toString(),
                                        completo.get("cVersion").toString(),
                                        completo.get("CAMIS").toString(),
                                        completo.get("nV1").toString(),
                                        completo.get("nV2").toString(),
                                        cat[categoria]
                                    )
                                }
                            }
                        } catch (e: Exception) {
                            println("Error: ${e.message}")
                        }
                    }
                }
            }
            extractionRepository.deploy()
            println("PROCESO TERMINADO")
        }
        else
            println("Solo se puede realizar la extraccion en DEV")
        return ""
    }
}