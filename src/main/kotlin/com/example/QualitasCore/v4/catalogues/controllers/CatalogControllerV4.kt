package com.example.QualitasCore.v4.catalogues.controllers

import com.example.QualitasCore.v4.catalogues.repositories.CatalogRepositoryV4
import com.example.QualitasCore.models.catalog.CatalogModel
import com.example.QualitasCore.v4.catalogues.models.ErrorResponse
import com.example.QualitasCore.v4.catalogues.models.TypeVehicle
import com.example.QualitasCore.v4.catalogues.services.CatalogServicesV4
import org.json.JSONArray
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@CrossOrigin("*")
@RequestMapping("v4/qualitas-{type}")
class CatalogControllerV4 {

    @Autowired
    lateinit var catalogRepository: CatalogRepositoryV4

    @Autowired
    lateinit var catalogServices: CatalogServicesV4

    var firsttime: Boolean = true
    var marcasHash: MutableList<CatalogModel> = mutableListOf()

    fun memorizedMarcas(type: String): MutableList<CatalogModel> {

        val listaMasVendidos: Collection<CatalogModel> = listOf(
                CatalogModel("75", "SEAT"),
                CatalogModel("38", "HYUNDAI"),
                CatalogModel("48", "KIA"),
                CatalogModel("22", "DODGE"),
                CatalogModel("85", "TOYOTA"),
                CatalogModel("37", "HONDA"),
                CatalogModel("29", "FORD"),
                CatalogModel("88", "VOLKSWAGEN"),
                CatalogModel("17", "CHEVROLET"),
                CatalogModel("64", "NISSAN"),
        ) as MutableList<CatalogModel>

        val listaMarcas = catalogServices.listBrands(type)
        listaMasVendidos.forEach {
            listaMarcas.add(0, it)
        }
        marcasHash = listaMarcas.distinct() as MutableList<CatalogModel>
        firsttime = false

        return marcasHash
    }


    @GetMapping("/brands")
    fun getBrands(@PathVariable type: TypeVehicle): ResponseEntity<Any> {
        try {
            when (type) {
                TypeVehicle.CAR -> {
                    val brands = memorizedMarcas("AUTOS")
                    return ResponseEntity(brands, HttpStatus.OK)
                }

                TypeVehicle.MOTO -> {
                    val brand = catalogRepository.getBrands("MOTOS")
                    return ResponseEntity(brand, HttpStatus.OK)
                }
            }
        } catch (e: Exception) {
            val errorResponse = ErrorResponse("Error", "Error al consultar las marcas", e.message.toString())
            return ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/years")
    fun getYears(@PathVariable type: TypeVehicle, @RequestParam brand: String): ResponseEntity<Any> {
        try {
            when (type) {
                TypeVehicle.CAR -> {
                    val years = catalogRepository.getYears("AUTOS", brand)
                    return ResponseEntity(years, HttpStatus.OK)
                }

                TypeVehicle.MOTO -> {
                    val years = catalogRepository.getYears(type.toString(), brand)
                    if (years.isEmpty()) {
                        val errorResponse = ErrorResponse("404",
                                "Modelos no encontrados para la marca $brand en el servicio de $type, valide su peticion",
                                "")
                        return ResponseEntity(errorResponse, HttpStatus.NOT_FOUND)
                    } else {
                        return ResponseEntity(years, HttpStatus.OK)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            val errorResponse = ErrorResponse("Error", "Error al consultar los modelos (years)", e.message.toString())
            return ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/models")
    fun getModels(
            @PathVariable type: TypeVehicle,
            @RequestParam brand: String,
            @RequestParam year: String
    ): ResponseEntity<Any> {
        try {
            when (type) {
                TypeVehicle.CAR -> {
                    val models = catalogRepository.getModels("AUTOS", brand, year)
                    if (models.isEmpty()) {
                        val errorResponse = ErrorResponse("404", "Submarcas no encontradas para la marca $brand, modelo: $year para el tipo $type")
                        return ResponseEntity(errorResponse, HttpStatus.NOT_FOUND)
                    } else {
                        return ResponseEntity(models, HttpStatus.OK)
                    }
                }

                TypeVehicle.MOTO -> {
                    val models = catalogRepository.getModels("MOTOS", brand, year)
                    val modelsToJson = JSONArray(models)

                    for (i in 0 until modelsToJson.length()){
                        val jsonObject = modelsToJson.getJSONObject(i)
                        val getText = jsonObject.getString("text")

                        if (getText.contains(" ")){
                            val text = getText.replace(" ", "")
                            jsonObject.put("text", text)
                        }

                        if (getText.contains("-")){
                            val text = getText.replace("-", "")
                            jsonObject.put("text", text)
                        }

                    }
                    println("MODELS: $modelsToJson")
                    return ResponseEntity(modelsToJson.toString(), HttpStatus.OK)
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
            val errorResponse = ErrorResponse("Erro", "Ocurrio un error al consultar las submarcas", e.message.toString())
            return ResponseEntity(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @GetMapping("/variants")
    fun getVariants(
            @PathVariable type: TypeVehicle,
            @RequestParam brand: String,
            @RequestParam year: String,
            @RequestParam model: String
    ): ResponseEntity<Any> {
        try {
            when (type) {
                TypeVehicle.CAR -> {
                    val variants = catalogRepository.getVariants("AUTOS", brand, year, model)
                    return ResponseEntity(variants, HttpStatus.OK)
                }

                TypeVehicle.MOTO -> {
                    val variants = catalogRepository.getVariants(type.toString(), brand, year, model)
                    return ResponseEntity(variants, HttpStatus.OK)

                }
            }
        }catch (e: Exception){
            e.printStackTrace()
            val errorRespone = ErrorResponse("Error", "Ocurrio un error al obtener las descripciones", e.message.toString())
            return ResponseEntity(errorRespone, HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

}