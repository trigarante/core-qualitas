package com.example.QualitasCore.v4.catalogues.services

import com.example.QualitasCore.models.catalog.CatalogModel
import com.example.QualitasCore.v4.catalogues.repositories.CatalogRepositoryV4
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class CatalogServicesV4 {

    @Autowired
    lateinit var usage: CatalogRepositoryV4
    private var brandMap: HashMap<String, MutableList<CatalogModel>> = HashMap()

    fun listBrands(type: String):MutableList<Any>{
        if (!brandMap.keys.contains(type)) {
            brandMap[type] = usage.getBrands(type)
        }
        return brandMap[type] as MutableList<Any>
    }
}