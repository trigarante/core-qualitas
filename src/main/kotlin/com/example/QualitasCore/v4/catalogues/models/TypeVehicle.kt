package com.example.QualitasCore.v4.catalogues.models

enum class TypeVehicle {
    CAR,
    MOTO
}

data class ErrorResponse(
        val status: String = "",
        val message: String = "",
        val real: String = ""
)
