package com.example.QualitasCore.v4.catalogues.repositories

import com.example.QualitasCore.models.catalog.CatalogModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface CatalogRepositoryV4: JpaRepository<CatalogModel, String> {


    @Query("SELECT ROW_NUMBER() OVER (ORDER BY marca) AS id, marca AS text \n" +
            "FROM (SELECT DISTINCT marca FROM Ws_Qualitas_DEV.extraction_2024 WHERE tipo = :type) AS marcas;", nativeQuery = true)
    fun getBrands(type: String): MutableList<CatalogModel>

    @Query("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY modelo DESC) as id, modelo as text FROM (SELECT DISTINCT modelo FROM Ws_Qualitas_DEV.extraction_2024 WHERE marca = :brand AND tipo = :type) AS modelos", nativeQuery = true)
    fun getYears(type: String, brand: String): MutableList<CatalogModel>

    @Query("SELECT DISTINCT ROW_NUMBER() OVER (ORDER BY submarca) as id, submarca as text FROM (SELECT DISTINCT submarca FROM Ws_Qualitas_DEV.extraction_2024 WHERE marca = :brand AND modelo = :modelo AND tipo = :type ) as submarcas", nativeQuery = true)
    fun getModels(type: String, brand: String, modelo: String): MutableList<CatalogModel>

    @Query("SELECT DISTINCT clave as id, descripcion as text FROM Ws_Qualitas_DEV.extraction_2024 WHERE marca = :brand AND modelo = :modelo AND submarca = :subMarca AND tipo = :type", nativeQuery = true)
    fun getVariants(type: String, brand: String, modelo: String, subMarca: String): MutableList<CatalogModel>

}