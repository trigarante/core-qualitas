package com.example.QualitasCore.Repository

import com.example.QualitasCore.models.SpringFK.UsosModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UsosRepository: JpaRepository<UsosModel, Int> {
    @Query("SELECT id_uso, uso, enabled FROM  usos WHERE enabled = ?1", nativeQuery = true)
    fun findByEnabled(enabled: Boolean = true): List<UsosModel>
}