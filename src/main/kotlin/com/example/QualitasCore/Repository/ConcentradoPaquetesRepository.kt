package com.example.QualitasCore.Repository

import com.example.QualitasCore.models.SpringFK.ConcentradoPaquetesModel
import com.example.QualitasCore.models.SpringFK.PaquetesModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ConcentradoPaquetesRepository: JpaRepository<ConcentradoPaquetesModel, Int> {

    @Query("SELECT c.paquete FROM ConcentradoPaquetesModel c WHERE c.paquete.enabled = TRUE AND c.uso.text = ?1 GROUP BY c.paquete.id")
    fun findPaquetes(typo: String): Set<PaquetesModel>

    fun findConcentradoPaquetesModelByPaquete_TextAndAndUso_TextAndCobertura_EnabledOrderByCoberturaAsc(paquete: String, uso: String, cobertura: Boolean = true): List<ConcentradoPaquetesModel>
}