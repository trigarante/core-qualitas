package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.SpringFK.ConcentradoPaquetesModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface CovereagesRepository : JpaRepository<ConcentradoPaquetesModel, Int> {


    @Query(
        "SELECT concentrado_paquetes.id, concentrado_paquetes.cobertura_id AS coberturaId,concentrado_paquetes.cobertura_id, paquetes.id_paquete AS paqueteId, paquetes.id_paquete AS paquete_id,usos.id_uso AS uso_id, concentrado_paquetes.uso_id as usoId, concentrado_paquetes.default, concentrado_paquetes.requerida FROM Ws_Qualitas_Prod.concentrado_paquetes\n" +
                "INNER JOIN Ws_Qualitas_Prod.paquetes ON paquetes.id_paquete = concentrado_paquetes.paquete_id\n" +
                "INNER JOIN Ws_Qualitas_Prod.coberturas ON coberturas.id_cobertura = concentrado_paquetes.cobertura_id\n" +
                "INNER JOIN Ws_Qualitas_Prod.usos ON usos.id_uso = concentrado_paquetes.uso_id WHERE paquetes.nombre = 'AMPLIA' AND usos.uso = 'PARTICULAR' AND coberturas.enabled = true;",
        nativeQuery = true
    )

    fun findCoverages(paquete: String, uso: String): List<ConcentradoPaquetesModel>
}
