package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.catalog.DescuentosModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface DescuentosRepository: JpaRepository<DescuentosModel, Int> {
    fun findByNegocio(negocio: String): List<DescuentosModel>
}