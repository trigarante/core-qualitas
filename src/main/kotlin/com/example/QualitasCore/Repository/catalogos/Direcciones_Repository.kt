package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.catalog.Direcciones_Model
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface Direcciones_Repository : JpaRepository<Direcciones_Model,String>{

    @Query("select ROW_NUMBER() OVER(order by CPostal) id, CPostal, IDEdo_Qua, IDMun_Qua, IDColonia_Qua, Colonia, Municipio, Estado, Ciudad " +
            "from CodPostales where CPostal = :codPostal", nativeQuery = true)
    fun getDireccionesByCPostal(@Param("codPostal") codPostal : String ) : List<Direcciones_Model>

}