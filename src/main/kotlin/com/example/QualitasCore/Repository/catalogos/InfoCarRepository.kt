package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.catalog.ExtraccionModel
import com.example.QualitasCore.models.catalog.InfoCarModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

interface InfoCarRepository: JpaRepository<InfoCarModel, String> {


    @Query("SELECT DISTINCT \n" +
            "    Clv.Clave AS clave, \n" +
            "    M.Marca AS marca, \n" +
            "    S.SubMarcas AS sub_marca, \n" +
            "    Md.Modelo AS modelo, \n" +
            "    D.Descripcion AS descripcion \n" +
            "FROM \n" +
            "    Ws_Qualitas_DEV.Concentrado C \n" +
            "    JOIN Ws_Qualitas_DEV.Marcas M ON C.idMarca = M.id \n" +
            "    JOIN Ws_Qualitas_DEV.SubMarcas S ON C.idSubMarca = S.id \n" +
            "    JOIN Ws_Qualitas_DEV.Modelos Md ON C.idModelo = Md.id \n" +
            "    JOIN Ws_Qualitas_DEV.Claves Clv ON C.idClave = Clv.id \n" +
            "    JOIN Ws_Qualitas_DEV.Descripciones D ON C.idDescripcion = D.id \n" +
            "    JOIN Ws_Qualitas_DEV.n_clave_categoria cc ON Clv.Clave = cc.clave \n" +
            "WHERE \n" +
            "    Md.Modelo = :modelo\n" +
            "    AND M.Marca = :marca\n" +
            "    AND S.SubMarcas = :subMarca\n" +
            "    AND D.Descripcion = :descripcion\n" +
            "    AND cc.uso NOT IN ('15','76','8/90', '6', '48')"
,
        nativeQuery = true
    )
    fun getInfoCar(@Param("marca") marca: String, @Param("modelo") modelo: String, @Param("subMarca") subMarca: String, @Param("descripcion") descripcion: String ): List<InfoCarModel>
}