package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.SpringFK.PaquetesModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface PaquetesRepository : JpaRepository<PaquetesModel, Int>{


    @Query("SELECT paquetes.id_paquete, paquetes.nombre, paquetes.enabled FROM Ws_Qualitas_Prod.concentrado_paquetes\n" +
            "INNER JOIN Ws_Qualitas.paquetes ON paquetes.id_paquete = concentrado_paquetes.paquete_id\n" +
            "INNER JOIN Ws_Qualitas.coberturas ON coberturas.id_cobertura = concentrado_paquetes.cobertura_id\n" +
            "INNER JOIN Ws_Qualitas.usos ON usos.id_uso = concentrado_paquetes.uso_id WHERE paquetes.enabled = true  AND usos.uso = ?1 group by paquetes.id_paquete", nativeQuery = true)
    fun findPaquetes(typo: String): List<PaquetesModel>
}