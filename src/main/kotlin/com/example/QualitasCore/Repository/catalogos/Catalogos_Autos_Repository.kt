package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.catalog.CatalogModel
import com.example.QualitasCore.models.catalog.ExtraccionModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface Catalogos_Autos_Repository : JpaRepository<CatalogModel, String> {

    @Query(
        "SELECT CONCAT( 'm', ROW_NUMBER() OVER ( ORDER BY ( CASE WHEN o.ordenMarcas IS NULL THEN 1 ELSE 0 END ), o.ordenMarcas, m.marca )) AS id, m.marca AS text FROM Concentrado c Join Marcas m on c.idMarca = m.id JOIN Claves clv ON c.idClave = clv.id JOIN n_clave_categoria cc on clv.Clave = cc.clave LEFT JOIN Orden o ON m.marca = o.marca where cc.uso in ('1','5') GROUP BY m.marca, o.ordenMarcas",
        nativeQuery = true
    )
    fun getMarcas(): MutableList<CatalogModel>

    @Query(
        "select distinct c.idModelo as id , mo.Modelo as text from `Concentrado` c join Marcas m on c.idMarca = m.id join Modelos mo on c.idModelo = mo.id JOIN Claves clv ON c.idClave = clv.id JOIN n_clave_categoria cc on clv.Clave = cc.clave where m.Marca = :marca and cc.uso in ('1','5') order by mo.Modelo  DESC",
        nativeQuery = true
    )
    fun getModelos(@Param("marca") marca: String): List<CatalogModel>

    @Query(
        "SELECT CONCAT( 'm', ROW_NUMBER() OVER ( ORDER BY ( CASE WHEN o.ordenSubmarcas IS NULL THEN 1 ELSE 0 END ), o.ordenSubmarcas, s.SubMarcas )) id, s.SubMarcas AS text FROM Concentrado c join Marcas m on m.id = c.idMarca join Modelos mo on mo.id = c.idModelo join SubMarcas s on s.id = c.idSubmarca JOIN Claves clv ON c.idClave = clv.id JOIN n_clave_categoria cc on clv.Clave = cc.clave LEFT JOIN Orden o ON m.marca = o.marca AND s.SubMarcas = o.submarca WHERE m.marca = :marca AND modelo = :modelo and cc.uso in ('1','5') GROUP BY s.SubMarcas, o.ordenSubmarcas",
        nativeQuery = true
    )
    fun getSubMarcas(@Param("marca") marca: String, @Param("modelo") modelo: String): List<CatalogModel>

    @Query(
        "SELECT * FROM (SELECT DISTINCT Clv.Clave  as id, D.Descripcion as text, row_number() OVER (PARTITION BY D.Descripcion order by D.Descripcion ) as count from Concentrado C join Marcas M on C.idMarca = M.id join SubMarcas S on C.idSubMarca = S.id Join Modelos Md on C.idModelo = Md.id join Claves Clv on C.idClave = Clv.id join Descripciones D on C.idDescripcion = D.id LEFT JOIN Transmision tran ON C.Transmision = tran.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave WHERE M.MARCA = :marca and  Md.Modelo = :modelo and S.SubMarcas = :subMarca and cc.uso in ('1','5') ORDER BY D.Descripcion) conditions where count = 1",
        nativeQuery = true
    )
    fun getDescripciones(
        @Param("marca") marca: String,
        @Param("modelo") modelo: String,
        @Param("subMarca") subMarca: String
    ): Set<CatalogModel>
}

@Repository
interface Busqueda_Catalogos : JpaRepository<ExtraccionModel, String> {

    @Query(
        "SELECT DISTINCT Clv.Clave  as id, M.Marca as marca, S.SubMarcas as submarca, Md.Modelo as modelo, D.Descripcion  as descripcion from Concentrado C join Marcas M on C.idMarca = M.id join SubMarcas S on C.idSubMarca = S.id Join Modelos Md on C.idModelo = Md.id join Claves Clv on C.idClave = Clv.id join Descripciones D on C.idDescripcion = D.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave WHERE Md.Modelo = :modelo AND Clv.Clave = :clave and cc.uso not in ('15','76','8/90', '6', '48')",
        nativeQuery = true
    )
    fun getCar(@Param("clave") clave: String, @Param("modelo") modelo: String): List<ExtraccionModel>

    @Query(
        "SELECT DISTINCT Clv.Clave  as id,  M.Marca as marca, S.SubMarcas as submarca, Md.Modelo as modelo, D.Descripcion  as descripcion from Concentrado_U C join Marcas_U M on C.idMarca = M.id join SubMarcas_U S on C.idSubMarca = S.id Join Modelos_U Md on C.idModelo = Md.id join Claves_U Clv on C.idClave = Clv.id join Descripciones_U D on C.idDescripcion = D.id WHERE Md.Modelo = :modelo AND Clv.Clave = :clave",
        nativeQuery = true
    )
    fun getCarU(@Param("clave") clave: String, @Param("modelo") modelo: String): List<ExtraccionModel>

    @Query(
        "SELECT cl.Clave  as id, M.Marca as marca, SM.SubMarcas as submarca, mo.Modelo as modelo, d.Descripcion  as descripcion from Concentrado C JOIN Marcas M on C.idMarca = M.id JOIN SubMarcas SM on C.idSubMarca = SM.id Join Modelos mo on C.idModelo = mo.id join Claves cl on C.idClave = cl.id join Descripciones d on C.idDescripcion = d.id JOIN n_clave_categoria cc on cl.Clave = cc.clave WHERE mo.Modelo = :modelo and cl.Clave = :clave and cc.uso IN (:usage) group by cl.Clave, d.Descripcion",
        nativeQuery = true
    )
    fun getCar(
        @Param("clave") clave: String,
        @Param("modelo") model: String,
        @Param("usage") uso: Array<String>
    ): MutableList<ExtraccionModel>
}