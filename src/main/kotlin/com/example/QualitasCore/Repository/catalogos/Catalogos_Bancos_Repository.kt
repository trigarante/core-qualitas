package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.catalog.Catalogos_Bancos_Model
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface Catalogos_Bancos_Repository : JpaRepository<Catalogos_Bancos_Model,String>{

    @Query("Select id_banco, Nombre, Abreviacion from CatBancos order by Orden DESC",nativeQuery = true)
    fun getBancos() : List<Catalogos_Bancos_Model>

    @Query("Select id_banco, Nombre, Abreviacion from CatBancos where Abreviacion like %:banco%",nativeQuery = true)
    fun getBanco (@Param("banco") banco : String) : List<Catalogos_Bancos_Model>

    @Modifying
    @Transactional
    @Query("UPDATE CatBancos SET Orden = Orden +1 WHERE id_banco = :id", nativeQuery = true)
    fun updateOrden (@Param("id") id: String)
}