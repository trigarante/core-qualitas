package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.catalog.CatalogModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface CatalogActivities: JpaRepository<CatalogModel, String> {
    @Query("SELECT ROW_NUMBER() over (ORDER BY (d_codigo)) as id, d_asenta as text FROM sepomexCat WHERE d_codigo = :codigo", nativeQuery = true)
    fun getColinas(@Param("codigo") codigo: String):MutableList<CatalogModel>

    @Query("SELECT ROW_NUMBER() over (ORDER BY (d_codigo)) as id, d_asenta as text FROM sepomexCat", nativeQuery = true)
    fun getColinas():MutableList<CatalogModel>

    @Query("SELECT valorMsi as id, nombreMsi as text FROM CatMsi WHERE idBanco =:idBanco", nativeQuery = true)
    fun getMsi(@Param("idBanco") idBanco: Int):MutableList<CatalogModel>

}