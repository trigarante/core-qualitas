package com.example.QualitasCore.Repository.catalogos

import com.example.QualitasCore.models.catalog.NumeroSerieModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface NumeroSerieRepository : JpaRepository<NumeroSerieModel, Int> {
    fun findByVin(vin: String): List<NumeroSerieModel>
}