package com.example.QualitasCore.Repository

import com.example.QualitasCore.models.SearchIssueModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface FindIssuerRepository: JpaRepository<SearchIssueModel, String> {

    @Query("SELECT responseJson FROM Ws_Qualitas_Prod.log_Emision WHERE JSON_EXTRACT(responseJson,'\$.vehiculo.noSerie') = ?1 and JSON_EXTRACT(responseJson, '\$.emision.resultado')=true order by idEmision desc limit 1", nativeQuery = true)
    fun getInfo(noSerie: String): SearchIssueModel
}
