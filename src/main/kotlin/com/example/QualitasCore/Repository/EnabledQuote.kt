package com.example.QualitasCore.Repository

import com.example.QualitasCore.models.ActivateQuoteModel
import com.example.QualitasCore.models.SearchIssueModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository



@Repository
interface ActivateQuoteRepository : JpaRepository<ActivateQuoteModel,Int>{
}