package com.example.QualitasCore.Repository

import com.example.QualitasCore.models.catalog.CatalogModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface CatalogUsageRepository : JpaRepository<CatalogModel, String> {

//    @Query(
//        "SELECT CONCAT('m',ROW_NUMBER ( ) OVER ( ORDER BY ( CASE WHEN o.ordenMarcas IS NULL THEN 1 ELSE 0 END ), o.ordenMarcas, m.marca )) AS id, m.marca AS text FROM Concentrado c JOIN Marcas m ON c.idMarca = m.id LEFT JOIN Orden o ON m.marca = o.marca join Claves Clv on c.idClave = Clv.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave where cc.uso = :usage GROUP BY m.marca, o.ordenMarcas",
//        nativeQuery = true
//    )
//    fun getBrands(@Param("usage")usage: String): MutableList<CatalogModel>

    @Query(
        "select c.idModelo as id , mo.Modelo as text from Concentrado c join Marcas m on c.idMarca = m.id join Modelos mo on c.idModelo = mo.id join Claves Clv on c.idClave = Clv.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave where m.Marca = :brand and cc.uso = :usage group by c.idModelo,mo.Modelo order by mo.Modelo DESC",
        nativeQuery = true
    )
    fun getYears(@Param("usage")usage:String, @Param("brand")brand:String): MutableList<CatalogModel>

    @Query(
        "SELECT CONCAT( 'm', ROW_NUMBER() OVER ( ORDER BY ( CASE WHEN o.ordenSubmarcas IS NULL THEN 1 ELSE 0 END ), o.ordenSubmarcas, s.SubMarcas )) id, s.SubMarcas AS text FROM Concentrado c join Marcas m on m.id = c.idMarca join Modelos mo on mo.id = c.idModelo join SubMarcas s on s.id = c.idSubmarca LEFT JOIN Orden o ON m.marca = o.marca AND s.SubMarcas = o.submarca join Claves Clv on c.idClave = Clv.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave WHERE m.marca = :brand AND modelo = :year and cc.uso = :usage GROUP BY s.SubMarcas, o.ordenSubmarcas",
        nativeQuery = true
    )
    fun getModels(@Param("usage")usage: String, @Param("brand") brand: String, @Param("year") year: String): MutableList<CatalogModel>

    @Query(
        "SELECT cl.Clave  as id, d.Descripcion  as text from Concentrado c join Marcas m on c.idMarca = m.id join SubMarcas s on c.idSubMarca = s.id Join Modelos mo on c.idModelo = mo.id join Claves cl on c.idClave = cl.id join Descripciones d on c.idDescripcion = d.id JOIN n_clave_categoria cc on cl.Clave = cc.clave WHERE m.MARCA = :brand and  mo.Modelo = :year and s.SubMarcas = :model and cc.uso = :usage group by cl.Clave, d.Descripcion",
        nativeQuery = true
    )
    fun getVariants(@Param("usage")usage: String,
        @Param("brand") brand: String,
        @Param("year") year: String,
        @Param("model") model: String
    ): Set<CatalogModel>

    //  NUEVA OBTENCION DE CATALGOS
    @Query("SELECT DISTINCT CONCAT('m',ROW_NUMBER ( ) OVER ( ORDER BY ( CASE WHEN o.ordenMarcas IS NULL THEN 1 ELSE 0 END ), o.ordenMarcas, m.marca )) AS id, m.marca AS text FROM Concentrado c JOIN Marcas m ON c.idMarca = m.id LEFT JOIN Orden o ON m.marca = o.marca join Claves Clv on c.idClave = Clv.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave where cc.uso IN (:usage) GROUP BY m.marca, o.ordenMarcas ORDER BY text ASC" ,
        nativeQuery = true
    )
    fun getBrands(@Param("usage")usage: Array<String>): MutableList<CatalogModel>

    @Query(
        "select c.idModelo as id , mo.Modelo as text from Concentrado c join Marcas m on c.idMarca = m.id join Modelos mo on c.idModelo = mo.id join Claves Clv on c.idClave = Clv.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave where m.Marca = :brand and cc.uso IN (:usage) group by c.idModelo,mo.Modelo order by mo.Modelo DESC",
        nativeQuery = true
    )
    fun getYears(@Param("usage")usage:Array<String>, @Param("brand")brand:String): MutableList<CatalogModel>

    @Query("select c.idModelo as id , mo.Modelo as text from Concentrado c join Marcas m on c.idMarca = m.id join Modelos mo on c.idModelo = mo.id join Claves Clv on c.idClave = Clv.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave where m.Marca = :brand and cc.uso IN (:usage) AND mo.Modelo > 2013 group by c.idModelo,mo.Modelo order by mo.Modelo DESC", nativeQuery = true)
    fun getYearsUber(@Param("usage")usage:Array<String>, @Param("brand")brand: String): MutableList<CatalogModel>

    @Query(
        "SELECT CONCAT( 'm', ROW_NUMBER() OVER ( ORDER BY ( CASE WHEN o.ordenSubmarcas IS NULL THEN 1 ELSE 0 END ), o.ordenSubmarcas, s.SubMarcas )) id, s.SubMarcas AS text FROM Concentrado c join Marcas m on m.id = c.idMarca join Modelos mo on mo.id = c.idModelo join SubMarcas s on s.id = c.idSubmarca LEFT JOIN Orden o ON m.marca = o.marca AND s.SubMarcas = o.submarca join Claves Clv on c.idClave = Clv.id JOIN n_clave_categoria cc on Clv.Clave = cc.clave WHERE m.marca = :brand AND modelo = :year and cc.uso IN (:usage) GROUP BY s.SubMarcas, o.ordenSubmarcas",
        nativeQuery = true
    )
    fun getModels(@Param("usage")usage: Array<String>, @Param("brand") brand: String, @Param("year") year: String): MutableList<CatalogModel>

    @Query(
        "SELECT * FROM (SELECT DISTINCT cl.Clave  as id, d.Descripcion  as text, row_number() OVER (PARTITION BY d.Descripcion order by d.Descripcion ) as count from Concentrado c join Marcas m on c.idMarca = m.id join SubMarcas s on c.idSubMarca = s.id Join Modelos mo on c.idModelo = mo.id join Claves cl on c.idClave = cl.id join Descripciones d on c.idDescripcion = d.id JOIN n_clave_categoria cc on cl.Clave = cc.clave WHERE m.MARCA = :brand and  mo.Modelo = :year and s.SubMarcas = :model and cc.uso IN (:usage) ORDER BY d.Descripcion) conditions where count = 1",
        nativeQuery = true
    )
    fun getVariants(@Param("usage")usage: Array<String>,
                    @Param("brand") brand: String,
                    @Param("year") year: String,
                    @Param("model") model: String
    ): Set<CatalogModel>
}