package com.example.QualitasCore.Repository.cobros

import com.example.QualitasCore.models.cobro.logModel
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import javax.transaction.Transactional


@Repository
interface logCollection_Repository : JpaRepository<logModel, String> {

    @Transactional
    @Modifying
    @Query("insert into logCobros(request,request_aseguradora,response_aseguradora,response,poliza,estatus_cobro,negocio) values (:request,:request_aseguradora,:response_aseguradora,:response,:poliza,:estatus_cobro,:negocio)",nativeQuery = true)
    fun insertCobro (@Param("request") request : String, @Param("request_aseguradora") request_aseguradora : String, @Param("response_aseguradora") response_aseguradora : String, @Param("response") response : String, @Param("poliza") poliza : String, @Param("estatus_cobro") estatus_cobro : String, @Param("negocio") negocio : String)

    @Transactional
    @Modifying
    @Query("insert into logCancelacion(request,request_aseguradora,response_aseguradora,response,poliza,estatus_cancelacion,negocio) values (:request,:request_aseguradora,:response_aseguradora,:response,:poliza,:estatus_cancelacion,:negocio)",nativeQuery = true)
    fun insertCancelacion (@Param("request") request : String, @Param("request_aseguradora") request_aseguradora : String, @Param("response_aseguradora") response_aseguradora : String, @Param("response") response : String, @Param("poliza") poliza : String, @Param("estatus_cancelacion") estatus_cobro : String,@Param("negocio") negocio : String)
}