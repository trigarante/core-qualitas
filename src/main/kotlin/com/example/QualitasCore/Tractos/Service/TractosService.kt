package com.example.QualitasCore.Tractos.Service

import com.example.QualitasCore.businessRules.basic.AllBasicQuotationInterface
import com.example.QualitasCore.businessRules.basic.BasicIssueInterface
import com.example.QualitasCore.businessRules.basic.BasicQuotationInterface
import com.example.QualitasCore.extensions.XMLGeneralService
import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.models.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.*

@Suppress("NAME_SHADOWING")
@Service
class TractosService {
    @Autowired
    lateinit var xmlService: XMLGeneralService
    @Autowired
    lateinit var quotationService: BasicQuotationInterface
    @Autowired
    lateinit var allquotationService: AllBasicQuotationInterface
    @Autowired
    lateinit var issueService: BasicIssueInterface

    @Value("\${jwt.negocio}") private val negocio: String =""
    @Value("\${intern.agente}") private val agente: String = ""

    fun quote(request: QuotationRequest, paymentFrequency: String, paquete: String): GeneralResponse{
        val response = GeneralResponse()
        response.paquete = request.paquete
        response.cliente.direccion.codPostal = request.cp
        response.cliente.edad = request.edad
        response.cliente.fechaNacimiento = request.fechaNacimiento
        response.vehiculo.clave = request.clave
        response.vehiculo.marca = request.marca
        response.vehiculo.modelo = request.modelo
        response.vehiculo.descripcion = request.descripcion
        return try{
            val xml = xmlService.quotationTracto(request, paymentFrequency, paquete)
            request.aseguradora = "ahorra-tracto"
            var base = quotationService.requestQuotation(request, response, xml)

            if(base.codigoError.contains("Alto Riesgo")){
                val xml = xmlService.quotationTracto(request, paymentFrequency + "RIESGO", paquete)
                request.aseguradora = "ahorra-tracto"
                base = quotationService.requestQuotation(request, response, xml)
            }

            base
        }
        catch (e: IndexOutOfBoundsException){
            response.codigoError = "XML Creation error: No se encontro el cp: ${request.cp}"
            return response
        }
    }

    fun quote(request: QuotationRequest): ResponseAll{
        return allquotationService.quotation(::quote, request)
    }

    fun custom(request: RequestCustom): ResponseAll{

        val requestQuote = com.example.QualitasCore.models.QuotationRequest(
            cp = request.cp,
            clave = request.clave,
            descripcion = request.descripcion,
            paquete = "AMPLIA",
            aseguradora = "handling-tracto",
            descuento = request.descuento.toLong(),
            edad = request.edad,
            fechaNacimiento = request.fechaNacimiento,
            genero = request.genero,
            marca = request.marca,
            servicio = request.servicio,
            movimiento = "cotizacion",
            modelo = request.modelo,
            tipoValor = request.tipoValor
        )
        var paquete = ""

        loop@ for(i in 0 until request.coberturas.size){
            if(request.coberturas[i].clave == "1"){
                if(request.coberturas[i].selected) {
                    paquete = "AMPLIA"
                    break@loop
                }
            }
            else if(request.coberturas[i].clave == "2"){
                if(request.coberturas[i].selected){
                    paquete = "PLUS"
                    break@loop
                }
            }
            else if(request.coberturas[i].clave == "3"){
                if(request.coberturas[i].selected) {
                    paquete = "LIMITADA"
                    break@loop
                }
            }
            else
                paquete = "RC"
        }

        val threads = mutableListOf<Thread>()
        val collections = arrayOf("ANUAL", "SEMESTRAL", "TRIMESTRAL", "MENSUAL")
        val quotes: HashMap<String, GeneralResponse> = hashMapOf()
        val errors = mutableListOf<String>()

        collections.forEach {
            threads.add(
                Thread{
                    try {
                        var error = ""
                        var xml = xmlService.custom(request, paquete, it)
                        if (xml.contains("{{CoberturaRequerida}}")) {
                            error += "[Se agregaron coberturas requeridas para la cotizacion]"
                            xml = xml.replace("{{CoberturaRequerida}}", "")
                        }
                        if(xml.contains("{{CoberturaEliminada}}")){
                            error += "[Se eliminaron coberturas requeridas para la cotizacion]"
                            xml = xml.replace("{{CoberturaEliminada}}", "")
                        }
                        val base = quotationService.requestQuotation(requestQuote, GeneralResponse(), xml)
                        base.codigoError = "$error [${base.codigoError}]"
                        errors.add(base.codigoError)
                        quotes[it] = base
                    }
                    catch (e: Exception){
                        e.printStackTrace()
                    }
                }
            )
        }

        threads.forEach {
            it.start()
        }
        threads.forEach {
            it.join()
        }

        var errorFilter = ""

        errors.forEach {
            if(!errorFilter.contains(it) && it != " []")
                errorFilter += "$it"
        }
        val response = ResponseAll()

        response.aseguradora = "QUALITAS"
        response.descuento = request.descuento.toString()
        response.paquete = paquete

        response.cliente.direccion.codPostal = request.cp
        response.cliente.fechaNacimiento = request.fechaNacimiento
        response.cliente.genero = request.genero
        response.cliente.edad = request.edad

        response.vehiculo.clave = request.clave
        response.vehiculo.modelo = request.modelo
        response.vehiculo.marca = request.marca
        response.vehiculo.descripcion = request.descripcion
        response.vehiculo.servicio = request.servicio

        errorFilter = errorFilter.replace(" []", "").replace("[0184-- Excede el monto de la Suma x Categoria DM]", "[Monto de la Suma x Categoria DM no valido]").trim()
        if(errorFilter != "[Se agregaron coberturas requeridas para la cotizacion]" && errorFilter != "" && errorFilter != "[Se eliminaron coberturas requeridas para la cotizacion]" && errorFilter != "[Se agregaron coberturas requeridas para la cotizacion][Se eliminaron coberturas requeridas para la cotizacion]"){
            val exception = ExceptionModel(message = errorFilter, error = "Quotation Error")
            throw exception
        }

        response.codigoError = errorFilter
        if(quotes.containsKey("ANUAL"))
            response.coberturas = quotes["ANUAL"]!!.coberturas as ArrayList<Any>
        else if(quotes.containsKey("SEMESTRAL"))
            response.coberturas = quotes["SEMESTRAL"]!!.coberturas as ArrayList<Any>
        else if(quotes.containsKey("TRIMESTRAL"))
            response.coberturas = quotes["TRIMESTRAL"]!!.coberturas as ArrayList<Any>
        else if(quotes.containsKey(("MENSUAL")))
            response.coberturas = quotes["MENSUAL"]!!.coberturas as ArrayList<Any>

        response.cotizacion.add(
            PeriocidadPago(
                anual = quotes["ANUAL"]!!.cotizacion,
                semestral = quotes["SEMESTRAL"]!!.cotizacion,
                trimestral = quotes["TRIMESTRAL"]!!.cotizacion,
                mensual = quotes["MENSUAL"]!!.cotizacion
            )
        )
        return response
    }

    fun issue(request: GeneralResponse): GeneralResponse{
        try{
            request.aseguradora = "Tractos"
            val xml = xmlService.issueAutos(request)
            return issueService.responseIssue(xml, request, "")
        }
        catch (e: IndexOutOfBoundsException){
            request.codigoError = "XML Creation error: No se encontro la colonia relacionada al codigo postal ${request.cliente.direccion.codPostal}"
            return request
        }
    }
}