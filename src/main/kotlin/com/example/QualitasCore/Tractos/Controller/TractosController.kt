package com.example.QualitasCore.Tractos.Controller

import com.example.QualitasCore.Tractos.Service.TractosService
import com.example.QualitasCore.models.Custom.RequestCustom
import com.example.QualitasCore.models.GeneralResponse
import com.example.QualitasCore.models.QuotationRequest
import io.swagger.annotations.Api
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin("*")
@RequestMapping("/v1/qualitas-tractos/")
@Api(tags = arrayOf("TractosOperations"))
class TractosController {
    @Autowired
    lateinit var tractosService: TractosService

    @PostMapping("quote")
    fun quotationTractos(@RequestBody request: QuotationRequest): ResponseEntity<Any>{
        try{
            if(request.paquete.uppercase() == "ALL"){
                val base = tractosService.quote(request)
                return ResponseEntity(base, HttpStatus.OK)
            }
            else{
                val base = tractosService.quote(request, request.periodicidadPago.toString(), request.paquete)
                return ResponseEntity(base, HttpStatus.OK)
            }
        }
        catch (e: Exception){
            e.printStackTrace()
            return ResponseEntity("Error al cotizar", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    @PostMapping("custom")
    fun customTtractos(@RequestBody request: RequestCustom): ResponseEntity<Any>{
        val base = tractosService.custom(request)
        return ResponseEntity(base, HttpStatus.OK)
    }

    @PostMapping("issue-policy")
    fun issueTractos(@RequestBody request: GeneralResponse): ResponseEntity<Any>{
        try{
            val base = tractosService.issue(request)
            return ResponseEntity(base, HttpStatus.OK)
        }
        catch (e: Exception){
            e.printStackTrace()
            return ResponseEntity("Error al emitir", HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }
}