package com.example.QualitasCore.models

import com.fasterxml.jackson.annotation.JsonProperty

data class  ResponseAll(
    var aseguradora: String = "",

    var cliente: Cliente = Cliente("","","","","","","","",Direccion("","","","","","","",""),"","","",""),

    var vehiculo: Vehiculo =  Vehiculo("","","","","","","","","","","",service.PARTICULAR,""),

    var coberturas: ArrayList<Any> = arrayListOf(),

    var paquete: String = "",

    var descuento: String = "",

    var diasGracia: Int = 7,

    var periodicidadDePago: String = "",

    var cotizacion: ArrayList<Any> = arrayListOf(),

    var emision: Emision = Emision("","","","","","","","","", "","","",false),

    var pago: Pago = Pago("","","","","","","","","",0,""),

    var codigoError: Any = "",

    var urlRedireccion: String = ""
)

data class Amplia (
    @get:JsonProperty("amplia")@field:JsonProperty("amplia")
    var amplia: MutableList<Cobertura>? = null,
)

data class Limitada (
    @get:JsonProperty("limitada")@field:JsonProperty("limitada")
    var limitada: MutableList<Cobertura>? = null,
)

data class Rc(
    @get:JsonProperty("rc")@field:JsonProperty("rc")
    var rc: MutableList<Cobertura>? = null
)

data class CotizacionAmplia (
    @get:JsonProperty("amplia")@field:JsonProperty("amplia")
    var amplia: MutableList<PeriocidadPago>? = null,
)

data class CotizacionLimitada (
    @get:JsonProperty("limitada")@field:JsonProperty("limitada")
    var limitada: MutableList<PeriocidadPago>? = null,
)

data class CotizacionRc(
    @get:JsonProperty("rc")@field:JsonProperty("rc")
    var rc: MutableList<PeriocidadPago>? = null
)

data class PeriocidadPago (
    @get:JsonProperty("anual", required=true)@field:JsonProperty("anual", required=true)
    var anual: Cotizacion = Cotizacion("","","","","","","","","","","","",false),

    @get:JsonProperty("semestral", required=true)@field:JsonProperty("semestral", required=true)
    var semestral: Cotizacion = Cotizacion("","","","","","","","","","","","",false),

    @get:JsonProperty("trimestral", required=true)@field:JsonProperty("trimestral", required=true)
    var trimestral: Cotizacion = Cotizacion("","","","","","","","","","","","",false),

    @get:JsonProperty("mensual", required=true)@field:JsonProperty("mensual", required=true)
    var mensual: Cotizacion = Cotizacion("","","","","","","","","","","","",false),
)