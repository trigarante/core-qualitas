package com.example.QualitasCore.models

data class QuotationRequest(
    var aseguradora: String = "",
    val clave: String = "",
    val cp: String = "",
    val descripcion: String = "",
    val detalle: String = "",
    var descuento: Long = 0,
    val edad: String = "",
    val fechaNacimiento: String = "",
    val genero: String = "",
    val marca: String = "",
    val modelo: String = "",
    val movimiento: String = "",
    var paquete: String = "",
    var servicio: service = service.PARTICULAR,
    val tipoValor: TipoValor = TipoValor.CONVENIDO,
    val periodicidadPago: PaymentFrecuency = PaymentFrecuency.ANUAL
)

enum class TipoValor(var tipoValor: String){
    COMERCIAL("03"),
    VALORFACTURA("01"),
    CONVENIDO("0"),
    `COMERCIAL+10`("07")
}

enum class service(var service: String){
    PARTICULAR("PARTICULAR"),
    APP("APP"),
    APPPLUS("APPPLUS"),
    TAXI("TAXI"),
    MOTOPARTICULAR("MOTOPARTICULAR"),
    MOTOREPARTIDOR("MOTOREPARTIDOR"),
    PRIVADO("PARTICULAR"),
    CARGA("CARGA"),
    TRACTO("CARGA")
}

enum class PaymentFrecuency{
    ANUAL,
    SEMESTRAL,
    TRIMESTRAL,
    MENSUAL
}