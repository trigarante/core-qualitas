package com.example.QualitasCore.models.SpringFK


import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "coberturas")
class CoberturasModel: Serializable {
    @Id
    @Column(name = "id_cobertura")
    var idCobertura: Int = 0

    var nombre: String = ""

    @Column(unique = true)
    var clave: Int = 0

    var minimo: String = ""
    var maximo: String = ""

    @ManyToOne(optional = false, cascade = arrayOf(CascadeType.ALL), fetch = FetchType.EAGER, targetEntity = TipoJuegoModel::class)
    @JoinColumn(name = "tipo_juego_id", referencedColumnName = "tipo_juego_id")
    var tipoJuegoId: TipoJuegoModel = TipoJuegoModel()

    var incremento: String = ""

    var enabled: Boolean = true
}