package com.example.QualitasCore.models.SpringFK

import javax.persistence.*

@Entity
@Table(name = "concentrado_paquetes")
class ConcentradoPaquetesModel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Int = 0

    @ManyToOne(optional = false, cascade = arrayOf(CascadeType.ALL), fetch = FetchType.EAGER)
    @JoinColumn(name = "coberturaId", referencedColumnName = "clave")
    var cobertura: CoberturasModel = CoberturasModel()

    @ManyToOne(optional = false, cascade = arrayOf(CascadeType.ALL), fetch = FetchType.EAGER)
    @JoinColumn(name = "paqueteId", referencedColumnName = "id_paquete")
    var paquete: PaquetesModel = PaquetesModel()

    @ManyToOne(optional = false, cascade = arrayOf(CascadeType.ALL), fetch = FetchType.EAGER)
    @JoinColumn(name = "usoId", referencedColumnName = "id_uso")
    var uso: UsosModel = UsosModel()

    var default: String = ""

    var requerida: Boolean = true
}