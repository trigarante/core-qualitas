package com.example.QualitasCore.models.SpringFK

import javax.persistence.*

@Entity
@Table(name = "tipo_juegos")
class TipoJuegoModel {
    @Id
    @Column(name = "tipo_juego_id")
    var tipo_juego_id: Int = 0

    var tipo: String = ""
}