package com.example.QualitasCore.models.SpringFK

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "paquetes")
class PaquetesModel {
    @Id
    @Column(name= "id_paquete")
    var id: Int = 0

    @Column(name = "nombre")
    var text: String = ""

    @JsonIgnore
    var enabled: Boolean = true
}