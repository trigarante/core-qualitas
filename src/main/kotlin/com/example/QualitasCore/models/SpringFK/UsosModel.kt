package com.example.QualitasCore.models.SpringFK

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.hibernate.annotations.Generated
import org.hibernate.annotations.GeneratorType
import javax.persistence.*

@Entity
@Table(name = "usos")
class UsosModel {
    @Id
    @Column(name = "id_uso")
    var id: Int = 0
    @Column(name = "uso")
    var text: String = ""

    @get:JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    var enabled: Boolean = true
}