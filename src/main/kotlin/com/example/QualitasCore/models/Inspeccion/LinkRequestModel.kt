package com.example.QualitasCore.models.Inspeccion

data class LinkRequestModel(
    val serie: String,
    val claveAgente: String,
    val telAsegurado: String,
    val correo: String,
    val nombreAsegurado: String,
    val usuario: String,
    val correoAdicional: String = ""
)