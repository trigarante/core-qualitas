package com.example.QualitasCore.models


enum class Usos(var uso: String){
    PARTICULAR("PARTICULAR"),
    APP("APP"),
    APPPLUS("APPPLUS"),
    MOTOPARTICULAR("MOTOPARTICULAR"),
    MOTOREPARTIDOR("MOTOREPARTIDOR"),
    TRACTO("TRACTO"),
    car("CAR"),
    motos("15"),
    `private`("PRIVATE"),
    app("PRIVATE"),
    appplus("PRIVATE"),
    tracto("6")
    //taxi("8/90"),
    //todoTerreno("48"),
    //salvamento("SALVAMENTOS"),
    //legalizados("LEGALIZADOS"),
    //fronterizos("FRONTERIZOS")
}