package com.example.QualitasCore.models.catalog

import javax.persistence.*

@Entity
@Table(name ="CodPostales")
data class Direcciones_Model (
@Id
val id: String,
var CPostal : String,
var IDEdo_Qua : String,
var IDMun_Qua : String,
var IDColonia_Qua : String,
var Colonia : String,
var Municipio : String,
var Estado : String,
var Ciudad : String
        )