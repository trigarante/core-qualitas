package com.example.QualitasCore.models.catalog

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table
data class CatalogModel(
    @Id
    var id: String = "",
    var text: String = ""
)

@Entity
@Table
data class ExtraccionModel(
    @Id
    var id: String = "",
    var marca: String = "",
    var modelo: String = "",
    var submarca: String = "",
    var descripcion: String = ""
)

data class RequestInfoCarModel(
    val marca: String = "",
    val modelo: String = "",
    val subMarca: String = "",
    val descripcion: String = ""
)

@Entity
data class InfoCarModel(
    @Id
    val marca: String = "",
    val modelo: String = "",
    val subMarca: String = "",
    val descripcion: String = "",
    val clave: String = ""
)
