package com.example.QualitasCore.models.catalog
import javax.persistence.*

@Entity
@Table(name = "CatBancos")
data class Catalogos_Bancos_Model (
        @Id
        var id_banco : String,
        var Nombre : String,
        var Abreviacion : String
)