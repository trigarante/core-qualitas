package com.example.QualitasCore.models.catalog

import javax.persistence.*

@Table(name = "numeros_serie")
@Entity
data class NumeroSerieModel (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0,
    var vin: String = ""
)