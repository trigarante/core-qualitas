package com.example.QualitasCore.models.catalog

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
data class ValidationModel(
    @Id
    val id: String = "",
    val uso: String = "",
    val categoria: String = "",
    val Clave: String = ""
)