package com.example.QualitasCore.models.catalog

import javax.persistence.*

@Entity
@Table(name = "descuento")
data class DescuentosModel (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Int = 0,
    var particular: Int = 0,
    var uber: Int = 0,
    var motos: Int = 0,
    var negocio: String = ""
)