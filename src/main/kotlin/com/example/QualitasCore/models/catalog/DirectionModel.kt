package com.example.QualitasCore.models.catalog

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

//@Entity
//@Table(name = "CodPostales")
//data class DirectionModel(
//    @Id
//    var CPostal : String,
//    var IDEdo_Qua : String,
//    var IDMun_Qua : String,
//    var IDColonia_Qua : String,
//    var Colonia : String,
//    var Municipio : String,
//    var Estado : String,
//    var Ciudad : String
//)

@Entity
@Table(name = "sepomexCat")
data class DirectionModel(
    @Id
    val id: Int = 0,
    val d_codigo: Int = 0, //Codigo Postal
    val d_asenta: String = "", //Colonia
    val d_tipo_asenta: String = "",
    val D_mnpio: String = "",
    val d_estado: String = "",
    val d_ciudad: String? = "",
    val d_CP: Int = 0,
    val c_estado: String = "", //id Estado
    val c_oficina: Int = 0,
    val c_CP: String? = "",
    val c_tipo_asenta: Int = 0,
    val c_mnpio: String = "", //id Municipio
    val id_asenta_cpcons: String = "", //id Colonia
    val d_zona: String = "",
    val c_cve_ciudad: String? = "",
    val idCP: String = ""
)