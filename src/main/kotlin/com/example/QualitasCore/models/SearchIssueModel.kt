package com.example.QualitasCore.models

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "log_Emision")
data class SearchIssueModel(
    @Id
    val responseJson : String = ""
)
