package com.example.QualitasCore.models.Custom

class ResponseCoverages{
    var periodoDeGracia: PeriodoDeGracia = PeriodoDeGracia()
    var descuento: Descuento = Descuento()
    var coberturas: MutableList<Coverages> = mutableListOf()
}

class Coverages{
    var nombre: String = ""
    var abreviacion: String = ""
    var sumaAsegurada: String = ""
    var deducible: String = ""
    var clave: String = ""
    var obligatoria: Boolean = true
    var rangoDeducible: MutableList<value> = mutableListOf()
    var rangoSA: MutableList<value> = mutableListOf()
}

class PeriodoDeGracia {
    var default: String = "7"
    var disponible: Boolean = true
    var rango: MutableList<value> = mutableListOf()
}

class Descuento {
    var default: String = "23"
    var disponible: Boolean = true
    var rango: MutableList<value> = mutableListOf()
}


data class value(
    val value: String
)