package com.example.QualitasCore.models.Custom

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table
data class CoveragesModels (
    @Id
    var clave: String = "",
    var nombre: String = "",
    var minimo: Int = 0,
    var maximo: Int = 0,
    var default: String = "",
    var requerida: Boolean = false,
    var tipo_juego_id: String = "",
    var incremento: Int = 0
)