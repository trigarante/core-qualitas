package com.example.QualitasCore.models.Custom

import com.example.QualitasCore.models.TipoValor
import com.example.QualitasCore.models.service

data class RequestCustom(
    var aseguradora: String = "",
    val clave: String = "",
    val cp: String = "",
    val descripcion: String = "",
    var descuento: Long = 0,
    val edad: String = "",
    val fechaNacimiento: String = "",
    val genero: String = "",
    val marca: String = "",
    val modelo: String = "",
    val subMarca: String = "",
    val movimiento: String = "",
    var servicio: service = service.PARTICULAR,
    val tipoValor: TipoValor = TipoValor.CONVENIDO,
    val diasGracia: Int = 7,
    var coberturas: MutableList<RequestCoverage> = mutableListOf(),
    val valor: String = "",
    val idSolicitud: Int = 0
)

class RequestCoverage{
    val nombre: String = ""
    val abreviacion: String = ""
    var sumaAsegurada: String = ""
    var deducible: String = ""
    var clave: String = ""
    val descripcion: String = ""
    var selected: Boolean = true
}

