package com.example.QualitasCore.models

data class HandlingCoveragesModel(
    var insuranceCarrier: String = "",

    var key: String = "",

    var cp: String = "",

    var description: String = "",

    var discount: Long = 0,

    var age: String = "",

    var dateOfBirth: String = "",

    var gender: String = "",

    var brand: String = "",

    var model: String = "",

    var packet: String = "",

    var periodicity: String = "",

    var coverages: MutableList<Coverage> = mutableListOf()
)

data class Coverage(
    var name: String = "",
    var sumAssured: String = "",
    var deductible: String = ""
)