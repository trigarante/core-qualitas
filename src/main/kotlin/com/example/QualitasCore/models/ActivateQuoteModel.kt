package com.example.QualitasCore.models

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "enable_quote")
data class ActivateQuoteModel(
    @Id
    var id: Int = 0,
    var quote_type : String =  "",
    var is_active : Int = 0,
)