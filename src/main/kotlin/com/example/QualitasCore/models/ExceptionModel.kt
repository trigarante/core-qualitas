package com.example.QualitasCore.models

import com.fasterxml.jackson.annotation.JsonFormat
import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import kotlin.jvm.Throws


class ExceptionModel (
    override var message: String = "",
    var error: String = ""
) : Exception()

internal class ApiError private constructor() {
    var status: HttpStatus? = null

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    val timestamp: LocalDateTime
    var error: String? = null
    var message: String? = null

    init {
        timestamp = LocalDateTime.now()
    }

    constructor(status: HttpStatus?) : this() {
        this.status = status
    }

    constructor(status: HttpStatus?, ex: Throwable) : this() {
        this.status = status
        error = "Unexpected error"
        message = ex.localizedMessage
    }

    constructor(status: HttpStatus?, error: String?, ex: Throwable) : this() {
        this.status = status
        this.error = error
        message = ex.localizedMessage
    }
}