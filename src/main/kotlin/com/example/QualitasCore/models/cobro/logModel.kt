package com.example.QualitasCore.models.cobro

import javax.persistence.*

@Entity
@Table(name =  "log_cobros", schema = "Ws_Qualitas")
data class logModel (
    @Id
    var request : String,
    var request_aseguradora : String,
    var response_aseguradora : String,
    var response : String,
    var poliza : String,
    var estatus_cobro : String,
    var negocio : String
)