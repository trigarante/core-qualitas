package com.example.QualitasCore.Catalog

import com.example.QualitasCore.catalog.CatalogController
import com.example.QualitasCore.catalog.CatalogService
import org.json.JSONArray
import org.json.JSONObject
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.util.Assert
import kotlin.test.assertContains
import kotlin.test.assertNotNull
import kotlin.test.assertNotSame
import kotlin.test.assertTrue

@SpringBootTest
class CatalogApplicationTests {

    @Autowired
    lateinit var catalogController: CatalogController

    @Test
    fun brandsCarsLoadContent() {
        val base = catalogController.getBrands(CatalogService.usos.car)
        assertTrue(base.statusCodeValue == 200, "Good")
        assertTrue(base.body.toString().contains("NISSAN"), "Good")
        assertTrue(base.body.toString().contains("VOLKSWAGEN"), "Good")
    }

    @Test
    fun modelCarsLoadContent(){
        val base = catalogController.getmodel(CatalogService.usos.car, "NISSAN", "2015")
        assertTrue(base.statusCodeValue == 200, "Good")
        assertTrue(base.body.toString().uppercase().contains("VERSA"), "Good")
    }

    @Test
    fun variantCarsLoadContent(){
        val base = catalogController.getDescription(CatalogService.usos.car, "NISSAN", "2015", "VERSA")
        assertTrue(base.statusCodeValue == 200, "Good")
    }

}